 <div class="app-preview">
 	<nav class="ui-nav">
        <ul>
            <li class="js-list-index active"><a href="javascript:void(0);">客服列表</a></li>
        </ul>
    </nav>
    <div class="js-list-filter-region clearfix ui-box" style="position:relative;">
        <div>
			<a href="#create" class="ui-btn ui-btn-primary js-service_add">添加客服</a>
        </div>
        <br />
        <div class="alert">
            <p><span style="color: red;">温馨提示：</span>客户QQ需要在腾讯进行配置，在设置中将“安全级别设置”设置为”完全公开“<a href="http://shang.qq.com/v3/widget.html" target="_blank">点击配置</a></p>
        </div>
    </div>
    
    <div class="ui-box">
		<?php if($group_list['service_list']){ ?>
			<table class="ui-table ui-table-list" style="padding:0px;">
				<thead class="js-list-header-region tableFloatingHeaderOriginal">
					<tr>
						<th class="cell-15">添加时间</th>
						<th class="cell-25">QQ</th>
						<th class="cell-15">客服昵称</th>
						<th class="cell-15">联系方式</th>
						<th class="cell-15">真实名称</th>
						<th class="cell-25 text-right">操作</th>
					</tr>
				</thead>
				<tbody class="js-list-body-region">
					<?php foreach($group_list['service_list'] as $value){ ?>
						<tr service-id="<?php echo $value['service_id']?>">
							<td><?php echo date('Y-m-d',$value['add_time']);?></td>
							<td><?php echo $value['qq'];?></td>
							<td><?php echo $value['nickname'];?></td>
							<td><?php echo $value['tel'];?></td>
							<td><?php echo $value['truename'];?></td>
							<td class="text-right">
								<a href="#edit/<?php echo $value['service_id']?>">编辑资料</a>
								<span>-</span>
								<a href="javascript:void(0);" class="js-delete" sid="<?php echo $value['service_id']?>">删除</a>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		<?php }else{ ?>
			<div class="js-list-empty-region"></div>
		<?php } ?>
    </div>
    <div class="js-list-footer-region ui-box">
        <div>
            <div class="pagenavi js-page-list"><?php echo $group_list['page'];?></div>
        </div>
    </div>
</div>
<div class="app-preview">
	<nav class="ui-nav">
        <ul>
            <li id="js-nav-list-index" class="active">
                <a href="<?php dourl('store:service'); ?>">添加客服信息</a>
            </li>
        </ul>
    </nav>
	<div class="" style="background: #fff;border:1px solid #ccc;padding:10px 0;">
		<form class="form-horizontal" action="<?php dourl('create'); ?>" method="post">
			<fieldset>
				<div class="control-group">
					<label class="control-label">联系人 QQ：</label>
					<div class="controls">
						<input type="text" placeholder="请填写您常用的QQ号码" name="qq" class="input-xxlarge qq" value="" maxlength="15"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">头像预览：</label>
					<div class="controls">
						<img src="<?php echo option('config.site_url').'/upload/images/default_shop.png'; ?>" class="avatar_show" width="90px" height="90px">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">客服头像：</label>
					<div class="controls">
						<input type="text" placeholder="请上传客服头像" name="avatar" class="input-xxlarge avatar" value=""/>
						<a class="js-choose-bg control-action" href="javascript: void(0);">修改头像</a>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">客服昵称：</label>
					<div class="controls">
						<input type="text" placeholder="请填写客服昵称" name="nickname" class="input-xxlarge nickname" value="" maxlength="30"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">真实姓名：</label>
					<div class="controls">
						<input type="text" placeholder="请填写客服真实姓名" name="truename" class="input-xxlarge truename" maxlength="11" value=""/>
					</div>
				</div>
		
				<div class="control-group">
					<label class="control-label">联系电话：</label>
					<div class="controls">
						<input type="text" placeholder="请填写联系电话" name="tel" class="input-xxlarge tel" maxlength="11" value=""/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">邮箱：</label>
					<div class="controls">
						<input type="text" placeholder="请填写客服邮箱" name="email" class="input-xxlarge email" maxlength="50" value=""/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">客服简介：</label>
					<div class="controls">
						<textarea rows="" cols="" name="intro" class="input-xxlarge intro"></textarea>
					</div>
				</div>
				<div class="controls">
					<input type="hidden" name="service_id" class="service_id" value="0" />
					<button class="btn btn-large btn-primary submit-btn" type="button">添加客服</button>
				</div>
			</fieldset>
		</form>
</div>
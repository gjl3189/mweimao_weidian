<?php if(!defined('PIGCMS_PATH')) exit('deny access!');?>
<div class="modal-header">
	<a class="close js-news-modal-dismiss">×</a>
	<!-- 顶部tab -->
	<ul class="module-nav modal-tab">
		<li class="active"><a href="javascript:void(0);" class="js-modal-tab">粉丝终身制列表</a></li>
	</ul>
</div>
<div class="modal-body">
	<div class="tab-content">
		<div id="js-module-feature" class="tab-pane module-feature active">
			<table class="table">
				<colgroup>
					<col class="modal-col-title">
					<col class="modal-col-time" span="2">
					<col class="modal-col-action">
				</colgroup>
				<!-- 表格头部 -->
				<thead>
				<tr>
					<th class="title" style="background-color:#f5f5f5;">
						<div class="td-cont">
							<span>粉丝昵称</span>
						</div>
					</th>
					<th class="time" style="background-color:#f5f5f5;">
						<div class="td-cont">
							<span>关注时间</span>
						</div>
					</th>
					<th class="opts" style="background-color:#f5f5f5;">
						<div class="td-cont" style="padding:7px 0 3px 10px;">
							<form class="form-search" onsubmit="return false;">
								<div class="input-append">
									<input class="input-small js-modal-search-input" type="text" style="border-radius:4px 0px 0px 4px;"/><a href="javascript:void(0);" class="btn js-fetch-page js-modal-search" style="color:white;border-radius:0 4px 4px 0;margin-left:0px;">搜</a>
								</div>
							</form>
						</div>
					</th>
				</tr>
				</thead>
				<!-- 表格数据区 -->
				<tbody>
				<?php if (!empty($fans_list)) { ?>
					<?php foreach($fans_list as $fans){ ?>
						<tr>
							<td class="title" style="max-width:300px;">
								<div class="td-cont">
									<?php echo $fans['nickname']; ?>
								</div>
							</td>
							<td class="time">
								<div class="td-cont">
									<span><?php echo date('Y-m-d H:i:s', $fans['add_time']); ?></span>
								</div>
							</td>
							<td class="opts">
								<div class="td-cont">
									<button class="btn js-choose <?php echo empty($fans['status']) ? 'cancel' : ''; ?>" data-id="<?php echo $fans['pigcms_id'];?>">
										<?php if (empty($fans['status'])) { ?>
											恢复
										<?php } else { ?>
											取消
										<?php } ?>
									</button>
								</div>
							</td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td colspan="3" style="color: red;text-align: center;">还没有相关数据。</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div style="display:none;" class="js-confirm-choose left"></div>
	<div class="pagenavi js-page-list" style="margin-top:0;padding-top:2px;"><?php echo $page; ?></div>
</div>
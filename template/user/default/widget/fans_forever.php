<?php if(!defined('PIGCMS_PATH')) exit('deny access!');?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8"/> 
		<title>粉丝终身制列表</title>
		<link href="<?php echo TPL_URL;?>css/base.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.min.js"></script>
		<script type="text/javascript">
			var t = '';
			$(function(){
				$('.js-modal iframe',parent.document).height($('body').height());
                $('.modal-header .close').live('click',function(){
                    parent.login_box_close();
                });
				$('button.js-choose').live('click',function(){
					var id = $(this).data('id');
					var status = 0;
					var obj = this;
					if ($(this).hasClass('cancel')) {
						status = 1
					}
					$.get('<?php dourl('fans_forever', array('type'=>'status')); ?>', {'id': id, 'status': status}, function(data){
						if (status) {
							$(obj).removeClass('cancel').text('取消 ');
						} else {
							$(obj).addClass('cancel').text('恢复');
						}
						$('.notifications').html('<div class="alert in fade alert-success">' + data.err_msg + '</div>');
						t = setTimeout('msg_hide()', 3000);
					})
				});
				$('.js-page-list a').live('click',function(e){
					if(!$(this).hasClass('active')){
						var input_val = $('.js-modal-search-input').val();
						$('body').html('<div class="loading-more"><span></span></div>');
						$('body').load('<?php dourl('fans_forever',array('type'=>'more')); ?>',{p:$(this).data('page-num'),'keyword':input_val},function(){
							$('.js-modal-search-input').val(input_val);
							$('.js-modal iframe',parent.document).height($('body').height());
						});
					}
				});
				$('.js-modal-search').live('click',function(e){
					var input_val = $('.js-modal-search-input').val();
					$('body').html('<div class="loading-more"><span></span></div>');
					$('body').load('<?php dourl('fans_forever',array('type'=>'more')); ?>',{'keyword':input_val},function(e){
						$('.js-modal-search-input').val(input_val);
						$('.js-modal iframe',parent.document).height($('body').height());
					});
					return false;
				});

				//回车提交搜索
				$(window).keydown(function(event){
					if (event.keyCode == 13 && $('.js-modal-search-input').is(':focus')) {
						var input_val = $('.js-modal-search-input').val();
						$('body').html('<div class="loading-more"><span></span></div>');
						$('body').load('<?php dourl('fans_forever',array('type'=>'more')); ?>',{'keyword':input_val},function(){
							$('.js-modal iframe',parent.document).height($('body').height());
						});
						return false;
					}
				})
			});

			function msg_hide() {
				$('.notifications').html('');
				clearTimeout(t);
			}
		</script>
		<style type="text/css">
			.btn {
				padding: 5px 11px;
				background-color: #07d;
			}
			.page_input {
				height: 18px!important;
				margin-bottom: 4px!important;
			}
			.cancel {
				background-color: #00A5FF;
			}
		</style>
	</head>
	<body style="background-color:#ffffff;">
		<div class="modal-header">
			<a class="close js-news-modal-dismiss">×</a>
			<!-- 顶部tab -->
			<ul class="module-nav modal-tab">
				<li class="active"><a href="javascript:void(0);" class="js-modal-tab">粉丝终身制列表</a></li>
			</ul>
		</div>
		<div class="modal-body">
			<div class="tab-content">
				<div id="js-module-feature" class="tab-pane module-feature active">
					<table class="table">
						<colgroup>
							<col class="modal-col-title">
							<col class="modal-col-time" span="2">
							<col class="modal-col-action">
						</colgroup>
						<!-- 表格头部 -->
						<thead>
							<tr>
								<th class="title" style="background-color:#f5f5f5;">
									<div class="td-cont">
										<span>粉丝昵称</span>
									</div>
								</th>
								<th class="time" style="background-color:#f5f5f5;">
									<div class="td-cont">
										<span>添加时间</span>
									</div>
								</th>
								<th class="opts" style="background-color:#f5f5f5;">
									<div class="td-cont" style="padding:7px 0 3px 10px;">
										<form class="form-search" onsubmit="return false;">
											<div class="input-append">
												<input class="input-small js-modal-search-input" type="text" style="border-radius:4px 0px 0px 4px;"/><a href="javascript:void(0);" class="btn js-fetch-page js-modal-search" style="color:white;border-radius:0 4px 4px 0;margin-left:0px;">搜</a>
											</div>
										</form>
									</div>
								</th>
							</tr>
						</thead>
						<!-- 表格数据区 -->
						<tbody>
						<?php if (!empty($fans_list)) { ?>
							<?php foreach($fans_list as $fans){ ?>
								<tr>
									<td class="title" style="max-width:300px;">
										<div class="td-cont">
											<?php echo $fans['nickname']; ?>
										</div>
									</td>
									<td class="time">
										<div class="td-cont">
											<span><?php echo date('Y-m-d H:i:s', $fans['add_time']); ?></span>
										</div>
									</td>
									<td class="opts">
										<div class="td-cont">
											<button class="btn js-choose <?php echo empty($fans['status']) ? 'cancel' : ''; ?>" data-id="<?php echo $fans['pigcms_id'];?>">
												<?php if (empty($fans['status'])) { ?>
													恢复
												<?php } else { ?>
													取消
												<?php } ?>
											</button>
										</div>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td colspan="3" style="color: red;text-align: center;">还没有相关数据。</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div style="display:none;" class="js-confirm-choose left"></div>
			<div class="pagenavi js-page-list" style="margin-top:0;padding-top:2px;"><?php echo $page; ?></div>
		</div>
		<div class="js-notifications notifications"></div>
	</body>
</html>
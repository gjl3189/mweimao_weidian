<style>
.app-actions {
  position: fixed;
  bottom: 0;
  width: 850px;
  padding-top: 20px;
  clear: both;
  text-align: center;
  z-index: 2;
}
</style>

<div class="widget-list">
	<div class="ui-box">
		<div class="js-list-filter-region clearfix"></div>
			<div style="background:#fefbe4;border:1px solid #f3ecb9;color:#993300;padding:10px;margin-bottom:5px;font-size:12px;margin-top:5px;">温馨提示： 以下规则 选择保存后，将只用于本店铺及其名下分销店铺，如果您还有其他店铺，请前往 并对应操作！<br/>&#12288;&#12288;&#12288;&#12288;&#12288;您用户账户 还有短信：<span><?php echo $_SESSION['user']['smscount'];?></span> 条，<a style="text-decoration:underline;" href="user.php?c=account&a=sms_record" target="_blank">前往充值</a>
				<?php if (count($total_config) == 0) { ?>
				<br><span style="color:#f30">&#12288;&#12288;&#12288;&#12288;&#12288;平台尚未配置模板消息，本功能无法使用</span>
				<?php } ?>
			</div>
			<?php if (count($total_config) > 0) { ?>
			<table class="ui-table ui-table-list physical_list">
				<thead class="js-list-header-region tableFloatingHeaderOriginal">
					<tr class="widget-list-header">
						<th colspan="1" style="vertical-align:middle"><input type="checkbox" class="chekckAll">&#12288;全选&#12288;&#12288;
						<span style="display:inline-block"><font color="#f60">
						</span>	
						</font></th>
						<th></th>
						<th align="center" colspan='3'>功能开关</th>
					</tr>
				</thead>
				<tbody class="js-list-body-region">
					<?php foreach($notice_manage as $k=>$v) { ?>
						<tr class="widget-list-item">
								<td><input class="checkzu check_zu[<?php echo $v['id'];?>]" type="checkbox" value="1" <?php if($store_notice_manage['has_power_arr'][$v['id']]['qx_list']) { if($store_notice_manage['has_power_arr'][$v['id']]['qx_list'] == '1,2') {?>checked="checked"<?php }}?>>&#12288;<?php echo $v['name'];?></td>
								<td>&#12288; &#12288;</td>
								<td  align="center" colspan='3'>
									<input type="checkbox" style="display:none" class="checks0" checked="checked" name="<?php echo $v[id]?>" value="0">&nbsp;
										<input <?php if($store_notice_manage['has_power_arr'][$v['id']]) {if(in_array(1,$store_notice_manage['has_power_arr'][$v['id']])) {?> checked="checked" <?php }}?> type="checkbox" class="checks1"   name="<?php echo $v[id]?>" value="1">&nbsp;短信通知
										&#12288;
										<input <?php if($store_notice_manage['has_power_arr'][$v['id']]) {if(in_array(2,$store_notice_manage['has_power_arr'][$v['id']])) {?> checked="checked" <?php }}?>  type="checkbox" class="checks2" name="<?php echo $v[id]?>" value="2">&nbsp;微信通知
									
								</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php } ?>
			
			<div class="app-actions" style="bottom: 0px;">
				<div class="form-actions text-center">
					<input class="btn js-btn-quit-store" type="button" value="取 消">
					<input class="btn btn-primary js-btn-save-store" type="submit" value="保 存" data-loading-text="保 存...">
				</div>
			</div>			
			
		<!--  
			<div class="js-list-empty-region">
				<div>
					<div class="no-result widget-list-empty">还没有相关数据。</div>
				</div>
			</div>
		-->
	</div>
	<div class="js-list-footer-region ui-box"></div>
</div>

<script>
$(function(){
	$(".chekckAll").click(function(){
		if($(this).is(":checked") == true) {
			$(".js-list-body-region").find("input[type='checkbox']").attr("checked",true);
		} else {
			$(".js-list-body-region").find("input[type='checkbox']").attr("checked",false);
			$(".js-list-body-region").find(".checks0").attr("checked",true);
		}
	})

	$(".checkzu").click(function(){
		if($(this).is(":checked") == true) {
			$(this).closest("tr").find("input[type='checkbox']").attr("checked",true);	
		} else {
			$(this).closest("tr").find("input[type='checkbox']").attr("checked",false);
			$(this).closest("tr").find(".checks0").attr("checked",true);
		}	
	})


	$(".checks1").click(function(){
		var checks1_index =  $(".checks1").index($(this));
		
		if($(this).is(":checked") == true) {
			if($(".checks2").eq(checks1_index).is(":checked") == true) {
				$(".checkzu").eq(checks1_index).attr("checked",true);
			} else {
				$(".checkzu").eq(checks1_index).attr("checked",false);
			}
		}else{
			$(".checkzu").eq(checks1_index).attr("checked",false);
		}
	})

	$(".checks2").click(function(){
		var checks2_index =  $(".checks2").index($(this));
		
		if($(this).is(":checked") == true) {
			if($(".checks1").eq(checks2_index).is(":checked") == true) {
				$(".checkzu").eq(checks2_index).attr("checked",true);
			} else {
				$(".checkzu").eq(checks2_index).attr("checked",false);
			}
		} else{
			$(".checkzu").eq(checks2_index).attr("checked",false);
		}
	})	
	
})
</script>
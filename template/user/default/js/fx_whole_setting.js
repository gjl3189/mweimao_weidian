/**
 * Created by Administrator on 2015/10/29.
 */
var t = '';
$(function(){
    load_page('.app__content',load_url,{page:'whole_setting_content'},'');

    $('.margin-account').live('click',function(){
       // alert(1);return false;
        var no_check = 1;
        store_margin(no_check);

    });

    //启用审核批发商
    $('.audit > .ui-switch-off').live('click', function(e){
        var obj = this;
        $.post(is_required_to_audit, {'status':1}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-off').addClass('ui-switch-on');
            }
        });
    })

    //关闭审核批发商
    $('.audit > .ui-switch-on').live('click', function(e){
        var obj = this;
        $.post(is_required_to_audit, {'status':0}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-on').addClass('ui-switch-off');
            }
        });
    })

    //开启保证金模式
    $('.guidance > .ui-switch-off').live('click', function(e){
        var obj = this;
        $.post(is_required_margin, {'status':1}, function(data){
            if (data) {
                store_margin();
                $('.store-margin-account').css('display','block');
                $(obj).removeClass('ui-switch-off').addClass('ui-switch-on');
            }
        });
    });


    $('.btn-primary').live('click', function(){
        var bank_id = $("select[name='bank_id']").val();
        var opening_bank = $("input[name='opening_bank']").val();
        var bank_card_user = $("input[name='bank_card_user']").val();
        var bank_card = $("input[name='bank_card']").val();
        var no_check = $("input[name='no_check']").val();
        var pigcms_id = $("input[name='pigcms_id']").val();

        if (opening_bank == '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();

            $('.input-large1').closest('.control-group').addClass('error');
            $('.input-large1').after('<p class="help-block error-message">开户银行不能为空</p>');
            return false;
        }
        if (opening_bank != '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();
        }

        if (isNaN(bank_card)) {
            $('.control-group1').removeClass('error');
            $('.error-message').remove();

            $('.widget-account-pretty-text').closest('.control-group').addClass('error');
            $('.widget-account-pretty-text').after('<p class="help-block error-message">银行卡只能为数字</p>');
            return false;
        }

        if (bank_card == '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();

            $('.input-large1').closest('.control-group').addClass('error');
            $('.input-large1').after('<p class="help-block error-message">开户银行不能为空</p>');
            return false;
        }

        if (bank_card != '') {
            $('.control-group1').removeClass('error');
            $('.error-message').remove();
        }

        if (bank_card_user == '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();

            $('.input-large2').closest('.control-group').addClass('error');
            $('.input-large2').after('<p class="help-block error-message">开卡人姓名不能为空</p>');
            return false;
        }
        if (bank_card_user != '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();
        }

        $.post(setStoreMargin_url, {'type': 'add', 'pigcms_id':pigcms_id, 'no_check':no_check, 'bank_id': bank_id, 'opening_bank': opening_bank, 'bank_card_user': bank_card_user,'bank_card':bank_card}, function(data){
            if (data.error_code == 0) {
                $('.notifications').html('<div class="alert in fade alert-success">' + data.message + '</div>');
                $('.modal').animate({'margin-top': '-' + ($(window).scrollTop() + $(window).height()) + 'px'}, "slow",function(){
                    $('.modal-backdrop,.modal').remove();
                });
            } else {
                $('.notifications').html('<div class="alert in fade alert-error">' + data.message + '</div>');
            }
            t = setTimeout('msg_hide()', 3000);
        },'json')
    });

    $('.btn-cancel').live('click', function(){
        $('.modal').animate({'margin-top': '-' + ($(window).scrollTop() + $(window).height()) + 'px'}, "slow",function(){
            $('.modal-backdrop,.modal').remove();
        });
    });

    //卡号每4位分隔显示
    $('.js_bank_card').live('keyup', function(){
        var value = $(this).val();
        var tmp_value = '';
        if (value.length > 4 && value[4] != '') {
            for (i in value) {
                i++;
                tmp_value += value[i-1];
                if (i > 0 && i % 4 == 0) {
                    tmp_value += ' ';
                }
            }
            value = tmp_value;
        }
        $(this).next('.js-account-pretty').text(value);
        $(this).next('.js-account-pretty').show();
    });

    $('.js_bank_card').live('blur', function(){
        $(this).next('.js-account-pretty').hide();
    });

    $("input[name='bank_card']").live('blur', function(){
        var bank_card = $("input[name='bank_card']").val();
        if (isNaN(bank_card)) {
            $('.control-group1').removeClass('error');
            $('.error-message').remove();

            $('.widget-account-pretty-text').closest('.control-group').addClass('error');
            $('.widget-account-pretty-text').after('<p class="help-block error-message">银行卡只能为数字</p>');
            return false;
        }
        if (bank_card != '') {
            $('.control-group1').removeClass('error');
            $('.error-message').remove();
        }
    });
    $("input[name='opening_bank']").live('blur', function(){
        var opening_bank = $("input[name='opening_bank']").val();
        if (opening_bank == '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();

            $('.input-large1').closest('.control-group').addClass('error');
            $('.input-large1').after('<p class="help-block error-message">开户银行不能为空</p>');
            return false;
        }
        if (opening_bank != '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();
        }
    });

    $("input[name='bank_card_user']").live('blur', function(){
        var bank_card_user = $("input[name='bank_card_user']").val();
        if (bank_card_user == '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();

            $('.input-large2').closest('.control-group').addClass('error');
            $('.input-large2').after('<p class="help-block error-message">开卡人姓名不能为空</p>');
            return false;
        }
        if (bank_card_user != '') {
            $('.control-group').removeClass('error');
            $('.error-message').remove();
        }
    });

    //关闭保证金模式
    $('.guidance > .ui-switch-on').live('click', function(e){
        var obj = this;
        $.post(is_required_margin, {'status':0}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-on').addClass('ui-switch-off');
                $('.store-margin-account').css('display','none');
            }
        });
    })

    //设置店铺保证金额度
    $('.ui-btn-bond').live('click',function(e){
        var bond = parseFloat($("input[name='bond']").val());

        if(!/^\d+(\.\d+)?$/.test(bond))
        {
            alert('只能填写数字');
            return false;
        }

        $.post(update_store_bond, {'bond':bond}, function(data){
            if (data) {
                location.replace(location);
               // $(obj).removeClass('ui-switch-on').addClass('ui-switch-off');
            }
        });
    });


    //开启保证金最低额度提醒
    $('.margin_amount > .ui-switch-off').live('click', function(e){
        var obj = this;
        $.post(margin_amount, {'status':1}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-off').addClass('ui-switch-on');
            }
        });
    });

    //关闭保证金最低额度提醒
    $('.margin_amount > .ui-switch-on').live('click', function(e){
        var obj = this;
        $.post(margin_amount, {'status':0}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-on').addClass('ui-switch-off');
            }
        });
    });

    //设置保证金最低额度
    $('.ui-btn-margin').live('click',function(e){
        var margin_minimum = parseFloat($("input[name='margin_minimum']").val());
        if(!/^\d+(\.\d+)?$/.test(margin_minimum))
        {
            alert('只能填写数字');
            return false;
        }

        $.post(update_margin_minimum, {'margin_minimum':margin_minimum}, function(data){
            if (data) {
                location.replace(location);
                // $(obj).removeClass('ui-switch-on').addClass('ui-switch-off');
            }
        });
    });



    //开启排他分销
    $('.open-store > .ui-switch-off').live('click', function(e){
        var obj = this;
        $.post(open_store_whole, {'status':1}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-off').addClass('ui-switch-on');
            }
        });
    });

    //关闭排他分销
    $('.open-store > .ui-switch-on').live('click', function(e){
        var obj = this;
        $.post(open_store_whole, {'status':0}, function(data){
            if (data) {
                $(obj).removeClass('ui-switch-on').addClass('ui-switch-off');
            }
        });
    });
});

function store_margin(no_check){
    $.post(setStoreMargin_url, {'type': 'check','no_check':no_check}, function(data){
        //console.log(data);return false;
        if (data.error_code > 0) {
            var option = '';
            if(data.message.length > 0) {
                for(var i=0;i<data.message.length;i++) {
                    option += '<option value="'+data.message[i]['bank_id']+'">' + data.message[i]['name'] +'</option>'
                }
            }
            var html = '' +
                '<div class="modal-backdrop fade in"></div>' +
                '<div class="modal modal-contact hide fade in" data-reactid=".0" aria-hidden="false" style="margin-top: -1000px; display: block;"><div class="modal-header"><h6 class="modal-title">设置保证金收款账户</h6></div><div class="modal-body"><div class="ui-message-warning">请仔细填写账户信息，如果由于您填写错误导致资金流失，平台概不负责；</div>' +
                '<form class="form-horizontal"><div class="control-group">' +
                '<input type="hidden" value="'+no_check+'" name="no_check">' +
                '<input type="hidden" value="'+(!$.isEmptyObject(data.check) ? data.check['pigcms_id'] : '')+'" name="pigcms_id">' +
                '<label class="control-label">发卡银行：</label>' +
                '<div class="controls">' +
                '<select class="bank" name="bank_id">'+
                 option
                +'</select>' +
                '</div>' +
                '</div>' +
                '<div class="control-group"><label class="control-label"><em class="required">*</em>开户银行：</label>' +
                '<div class="controls controls1"><input class="input-large input-large1" type="text" value="' + (!$.isEmptyObject(data.check) ? data.check['opening_bank'] : '') + '" name="opening_bank" /></div></div>' +
                '<div class="control-group control-group1"><label class="control-label"><em class="required">*</em>银行卡卡号：</label>' +
                '<div class="controls">' +

                '<input class="js_bank_card input-large" type="text" value="' + (!$.isEmptyObject(data.check) ? data.check['bank_card'] : '') + '" name="bank_card" />' +
                '<div class="widget-account-pretty-text js-account-pretty"></div>' +
                '</div>' +
                '</div>' +
                '<div class="control-group"><label class="control-label"><em class="required">*</em>开卡人姓名：</label>' +
                '<div class="controls"><input class="input-large input-large2" type="text" value="'+(!$.isEmptyObject(data.check) ? data.check['bank_card_user'] : '')+'" name="bank_card_user" /></div>' +
                '</div><div class="form-actions"><input type="button" value="提交" class="btn btn-primary" />' +
                '<input type="button" value="取消" class="btn btn-cancel" /></div>' +
                '</form>' +
                '</div>'  +
                '</div>';
            $('body').append(html);
            $('.modal').animate({'margin-top': ($(window).scrollTop() + $(window).height() * 0.05) + 'px'}, "slow");
        }
    },'json')
}


function msg_hide() {
    $('.notifications').html('');
    clearTimeout(t);
}

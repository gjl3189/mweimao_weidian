            <?php
            $select_sidebar=isset($select_sidebar) ? $select_sidebar : ACTION_NAME;
            $uid = $user_session['uid'];

            if ($user_session['type'] == 1) {

                function checkIsShow($action_id, $uid, $controller_id = MODULE_NAME){

                    if ($action_id == 'create') return false;

                    $physicalRbac = M("Rbac_action")->getControlArr($uid, $controller_id);
                    if (in_array($action_id, $physicalRbac)) return true;

                    return false;
                }

            } else {
                function checkIsShow(){
                    return true;
                }
            }

            ?>
            <?php $select_sidebar=isset($select_sidebar)?$select_sidebar:ACTION_NAME;?>
            <aside class="ui-sidebar sidebar">
                <nav>
                    <ul>
                        <li>
                            <a href="<?php dourl('order:dashboard'); ?>">订单概况</a>
                        </li>
                        <li id="js-order-nav-list-all">
                            <a href="<?php dourl('order:all'); ?>">所有订单</a>
                        </li>
                        <?php if (!empty($_SESSION['drp_diy_store'])) { ?>
                            <li id="js-order-nav-list-selffetch">
                                <a href="<?php dourl('order:selffetch'); ?>"><?php echo $store_session['buyer_selffetch_name'] ? $store_session['buyer_selffetch_name'] : '到店自提' ?>订单</a>
                            </li>
                            <?php if(empty($version) && empty($_SESSION['sync_store'])){?>
                                <li <?php if($select_sidebar == 'codpay') echo 'class="active"';?>>
                                    <a href="<?php dourl('codpay'); ?>">货到付款订单</a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        <li <?php if($select_sidebar == 'order_return') echo 'class="active"';?>>
                            <a href="<?php dourl('order:order_return'); ?>">退货列表</a>
                        </li>
                        <li <?php if($select_sidebar == 'order_rights') echo 'class="active"';?>>
                            <a href="<?php dourl('order:order_rights'); ?>">维权列表</a>
                        </li>
                        <li id="js-order-nav-list-star">
                            <a href="<?php dourl('order:star'); ?>">加星订单</a>
                        </li>
                    </ul>
                    <ul>
                        <?php if (!empty($_SESSION['drp_diy_store'])) { ?>
                            <li <?php if($select_sidebar == 'delivery') echo 'class="active"';?>>
                                <a href="<?php dourl('delivery');?>">物流工具</a>
                            </li>
                        <?php } ?>
                    </ul>
                    <ul>
                        <?php 
                        if (!empty($_SESSION['drp_diy_store'])) {
                        ?>
                            <li <?php if(in_array($select_sidebar,array('add'))) echo 'class="active"';?>><a href="<?php dourl('order:add');?>">添加订单</a></li>
                        <?php 
                        }
                        ?>
                    </ul>
                    <h4>对账信息</h4>
                    <ul>
                        <li <?php if($select_sidebar == 'income') echo 'class="active"';?>><a href="<?php dourl('trade:income');?>">账务概况</a></li>
                        <?php if (empty($_SESSION['store']['drp_supplier_id'])) { ?>
                        <li <?php if($select_sidebar == 'check') echo 'class="active"';?>><a href="<?php dourl('order:check'); ?>">平台对账</a></li>
                        <li <?php if($select_sidebar == 'seller_check') echo 'class="active"';?>><a href="<?php dourl('order:seller_check');?>">分销商对账</a></li>
                        <li <?php if($select_sidebar == 'supplier_check') echo 'class="active"';?>><a href="<?php dourl('order:supplier_check');?>">供货商对账</a></li>
                        <li <?php if($select_sidebar == 'dealer_check') echo 'class="active"';?>><a href="<?php dourl('order:dealer_check'); ?>">经销商对账</a></li>
                        <?php } ?>
                        <?php if (!empty($_SESSION['store']['drp_supplier_id'])) { ?>
                        <li <?php if($select_sidebar == 'profit_check') echo 'class="active"';?>><a href="<?php dourl('order:profit_check'); ?>">分佣对账</a></li>
                        <?php } ?>
                        <?php /*if (checkIsShow('check', $uid)) { */?><!--
                        <li><a href="<?php /*dourl('order:check');*/?>">平台对账</a></li>
                        <?php /*} */?>

                        <?php /*if (checkIsShow('fx_bill_check', $uid)) { */?>
                        <li <?php /*if($select_sidebar == 'fx_bill_check') echo 'class="active"';*/?>><a href="<?php /*dourl('order:fx_bill_check');*/?>">分销对账</a></li>
                        <?php /*} */?>

                        <?php /*if (checkIsShow('ws_bill_check', $uid)) { */?>
                        <li <?php /*if($select_sidebar == 'ws_bill_check') echo 'class="active"';*/?>><a href="<?php /*dourl('order:ws_bill_check');*/?>">批发对账</a></li>
                        --><?php /*} */?>
                    </ul>
                </nav>
            </aside>
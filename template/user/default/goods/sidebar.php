<?php 
$select_sidebar=isset($select_sidebar) ? $select_sidebar : ACTION_NAME;
$uid = $user_session['physical_uid'];
?>
<aside class="ui-sidebar sidebar">
<nav>
    <?php if (!empty($_SESSION['drp_diy_store'])) { ?>
        
        <?php if(in_array(7, $rbac_result)){?>
            <?php if (physicalPermission::checkIsShow('create', $uid, 'goods', $user_session['type'])) { ?>
            <ul>
                <li>
                    <a class="ui-btn ui-btn-success" href="<?php dourl('create');?>">发布商品</a>
                </li>
            </ul>
            <?php } ?>
        
            <h4>商品管理</h4>
        <?php } ?>
        <ul>

        <?php if(in_array(7, $rbac_result)){?>

            <?php if (physicalPermission::checkIsShow('index', $uid, 'goods', $user_session['type'])) { ?>
            <li <?php if($select_sidebar == 'index') echo 'class="active"';?>>
                <a href="<?php dourl('index');?>">出售中的商品</a>
            </li>
            <?php } ?>

            <?php if (physicalPermission::checkIsShow('stockout', $uid, 'goods', $user_session['type'])) { ?>
            <li <?php if($select_sidebar == 'stockout') echo 'class="active"';?>>
                <a href="<?php dourl('stockout'); ?>">已售罄的商品</a>
            </li>
            <?php } ?>

            <?php if (physicalPermission::checkIsShow('soldout', $uid, 'goods', $user_session['type'])) { ?>
            <li <?php if($select_sidebar == 'soldout') echo 'class="active"';?>>
                <a href="<?php dourl('soldout'); ?>">仓库中的商品</a>
            </li>
            <?php } ?>

        <?php } ?>

        <?php if(in_array(PackageConfig::getRbacId(7,'goods','category'), $rbac_result)){?>
            <?php if (physicalPermission::checkIsShow('category', $uid, 'goods', $user_session['type'])) { ?>
            <li <?php if($select_sidebar == 'category') echo 'class="active"';?>>
                <a href="<?php dourl('category'); ?>">商品分组</a>
            </li>
            <?php } ?>
        <?php } ?>

        </ul>
        <?php if(in_array(PackageConfig::getRbacId(7,'goods','product_comment'), $rbac_result) || in_array(PackageConfig::getRbacId(7,'goods','store_comment'), $rbac_result)){?>
        <h4>评论管理</h4>
        <?php } ?>

        <ul>
        <?php if(in_array(PackageConfig::getRbacId(7,'goods','product_comment'), $rbac_result)){?>
            <?php if (physicalPermission::checkIsShow('product_comment', $uid, 'goods', $user_session['type'])) { ?>
            <li <?php if($select_sidebar == 'product_comment') echo 'class="active"';?>>
                <a href="<?php dourl('product_comment');?>">商品评价</a>
            </li>
            <?php } ?>
        <?php } ?>
        
        <?php if(empty($_SESSION['sync_store']) && empty($version)) {?>
            <?php if(in_array(PackageConfig::getRbacId(7,'goods','store_comment'), $rbac_result)){?>
                <?php if (physicalPermission::checkIsShow('store_comment', $uid, 'goods', $user_session['type'])) { ?>
                <li <?php if($select_sidebar == 'store_comment') echo 'class="active"';?>>
                    <a href="<?php dourl('store_comment'); ?>">店铺评价</a>
                </li>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        </ul>
        
        <?php if(in_array(PackageConfig::getRbacId(7,'goods','subject'), $rbac_result)){?>

        <h4>导购管理</h4>
        <?php if (physicalPermission::checkIsShow('subject', $uid, 'goods', $user_session['type'])) { ?>
        <li <?php if($select_sidebar == 'subject' || $select_sidebar == 'subtype' || $select_sidebar == 'subject_pinlun' || $select_sidebar == 'subject_diy') echo 'class="active"';?>>
             <a href="<?php dourl('subject'); ?>">导购专题</a>
        </li>
        <?php } ?>

        <?php } ?>
        
        <?php if($user_session['type']==1): ?>
        <!-- <h4>门店管理</h4>
        <ul>
            <li <?php if($select_sidebar == 'physical_stock') echo 'class="active"';?>>
                <a href="<?php dourl('physical_stock');?>">库存</a>
            </li>
        </ul> -->
        <?php endif; ?>
    <?php } ?>
</nav>
</aside>
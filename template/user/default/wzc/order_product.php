<!-- ▼ Main container -->
<nav class="ui-nav-table clearfix">
	<ul class="pull-left js-list-filter-region">
		<li id="js-list-nav-all" <?php echo $type == 'all' ? 'class="active"' : '' ?>>
			<a href="#all">所有</a>
		</li>
		<li id="js-list-nav-apply" <?php echo $type == 'apply' ? 'class="active"' : '' ?>>
			<a href="#apply">申请中</a>
		</li>
		<li id="js-list-nav-future" <?php echo $type == 'future' ? 'class="active"' : '' ?>>
			<a href="#future">预热中</a>
		</li>
		<li id="js-list-nav-on" <?php echo $type == 'on' ? 'class="active"' : '' ?>>
			<a href="#on">进行中</a>
		</li>
		<li id="js-list-nav-end" <?php echo $type == 'end' ? 'class="active"' : '' ?>>
			<a href="#end">已结束</a>
		</li>
	</ul>
</nav>

<div class="widget-list">
	<div class="js-list-filter-region clearfix ui-box" style="position:relative;">
		<div>
			<a href="<?php echo $config['site_url']; ?>/user.php?c=wzc&a=wzc_index" class="ui-btn ui-btn-primary">返回活动列表</a>
			<span style="color: red;"></span>
		</div>
	</div>
</div>

<div class="ui-box">
	<?php if ($mySupportList) { ?>
		<table class="ui-table ui-table-list" style="padding:0px;">
			<thead class="js-list-header-region tableFloatingHeaderOriginal">
				<tr>
					<th class="cell-15">ID</th>
					<th class="cell-15">活动名称</th>
					<th class="cell-15">支付金额</th>
					<th class="cell-15">订单编号</th>
					<th class="cell-15">交易状态</th>
					<th class="cell-25 text-right">查看</th>
				</tr>
			</thead>
			<tbody class="js-list-body-region">
				<?php $time = time(); ?>
				<?php foreach($mySupportList as $k=> $v) { ?>
					<tr class="js-present-detail">
						<td>
							<?php echo ($k+1); ?>
						</td>
						<td>
							<?php echo $v['productName']; ?>
						</td>
						<td>
							<?php echo $v['pay_money'] ?>
						</td>
						<td>
							<?php echo $v['zcpay_no'] ?>
						</td>
						<td><?php echo $v['order_status']==2 ? '交易成功' : '交易异常'; ?></td>
						<td class="text-right js-operate" data-unitary_id="<?php echo $v['id'] ?>">
							<a href="#order_info/<?php echo $v['id']; ?>" class="" >交易详情</a>
							<?php if($v['status']==7){ ?>
							<span>-</span>
							<?php if($v['is_quit']==1){  ?>
							<a href="javascript:;" style="color: red" class="quit_money">确认退款</a>
							<?php	}elseif($v['is_quit']==2){  ?>
							<a href="javascript:;">已退款</a>
							<?php	} }  ?>
						</td>
					</tr>
				<?php } ?>
				<?php if ($page) { ?>
					<thead class="js-list-header-region tableFloatingHeaderOriginal">
						<tr>
							<td colspan="8">
								<div class="pagenavi js-repay_page"><?php echo $page ?></div>
							</td>
						</tr>
					</thead>
				<?php } ?>
			</tbody>
		</table>
		<input type="hidden" name="product_id" value="<?php echo $productInfo['product_id']; ?>" id="product_id">
	<?php } else { ?>
		<div class="js-list-empty-region">
			<div>
				<div class="no-result widget-list-empty">还没有相关数据。</div>
			</div>
		</div>
	<?php } ?>
</div>
<div class="js-list-footer-region ui-box"></div>

<?php
class templateNews 
{
	public $appid;
	public $appsecret;
	public $error;
	public function __construct($appid = NULL, $appsecret = NULL) 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		$this->appid = $appid;
		$this->appsecret = $appsecret;
	}
	public function sendTempMsg($tempKey, $dataArr, $sendType = 0) 
	{
		$cache_dir = $_SERVER['DOCUMENT_ROOT'] . '/cache/cache/';
		$cache_file = $cache_dir . 'yangjie.txt';
		if (!is_dir($cache_dir)) 
		{
			$fp = mkdir($cache_dir, 484, true);
		}
		if (!file_exists($cache_file)) 
		{
			$fp = fopen($cache_file, 'w+');
			fclose($fp);
		}
		file_put_contents($cache_file, 'sendType:' . $sendType . "\r\n", FILE_APPEND);
		if ($sendType) 
		{
			$store_id = ((isset($_SESSION['tmp_store_id']) ? $_SESSION['tmp_store_id'] : $dataArr['store_id']));
			$supplier = D('Store_supplier')->field('supply_chain')->where(array('seller_id' => $store_id, 'type' => 1))->find();
			if (!empty($supplier)) 
			{
				$supplier = explode(',', $supplier['supply_chain']);
				if (!empty($supplier[1])) 
				{
					$store_id = $supplier[1];
				}
			}
			$bind = D('Weixin_bind')->where(array('store_id' => $store_id))->find();
			$dbinfo = D('Tempmsg')->where('tempkey=\'' . $tempKey . '\' AND token=\'' . $bind['authorizer_appid'] . '\' AND status=1')->find();
			$access_token_array = M('Weixin_bind')->get_access_token($store_id);
			if ($access_token_array['errcode']) 
			{
				$this->error = array('error_code' => $access_token_array['errcode'], 'error_msg' => $access_token_array['errmsg']);
			}
		}
		else 
		{
			$dbinfo = D('Tempmsg')->where('tempkey=\'' . $tempKey . '\' AND token=\'system\' AND status=1')->find();
			if (method_exists(D('Access_token_expires'), 'get_access_token')) 
			{
				$access_token_array = D('Access_token_expires')->get_access_token();
			}
			else 
			{
				$access_token_array = M('Access_token_expires')->get_access_token();
			}
			if ($access_token_array['errcode']) 
			{
				$this->error = array('error_code' => $access_token_array['errcode'], 'error_msg' => $access_token_array['errmsg']);
			}
		}
		file_put_contents($cache_file, 'error:' . json_encode($this->error) . "\r\n", FILE_APPEND);
		if (!empty($dbinfo) && !empty($access_token_array['access_token'])) 
		{
			$access_token = $access_token_array['access_token'];
			$requestUrl = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=' . $access_token;
			$data = $this->getData($tempKey, $dataArr, $dbinfo['textcolor'], $sendType);
			$sendData = '{"touser":"' . $dataArr['wecha_id'] . '","template_id":"' . $dbinfo['tempid'] . '","url":"' . $dataArr['href'] . '","topcolor":"' . $dbinfo['topcolor'] . '","data":' . $data . '}';
			file_put_contents($cache_file, 'sendData:' . $sendData . "\r\n", FILE_APPEND);
			$rs = $this->postCurl($requestUrl, $sendData);
			file_put_contents($cache_file, 'rs:' . json_encode($rs) . "\r\n\r\n", FILE_APPEND);
		}
	}
	public function getError($code, $msg) 
	{
		return $this->error;
	}
	public function getData($key, $dataArr, $color, $type) 
	{
		if ($type) 
		{
			$tempsArr = $this->templates();
		}
		else 
		{
			$tempsArr = $this->systemTemplates();
		}
		$data = $tempsArr[$key]['vars'];
		$data = array_flip($data);
		$jsonData = '';
		foreach ($dataArr as $k => $v ) 
		{
			if (in_array($k, array_flip($data))) 
			{
				$jsonData .= '"' . $k . '":{"value":"' . $v . '","color":"' . $color . '"},';
			}
		}
		$jsonData = rtrim($jsonData, ',');
		return '{' . $jsonData . '}';
	}
	public function templates() 
	{
		return array( 'OPENTM203170813' => array( 'name' => '商品库存不足提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '商品编码：{{keyword1.DATA}}' . "\n" . '商品名称：{{keyword2.DATA}}' . "\n" . '库存数量：{{keyword3.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM204146731' => array( 'name' => '退货申请提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => "\n" . '{{first.DATA}}' . "\n" . '订单编号：{{keyword1.DATA}}' . "\n" . '商品名称：{{keyword2.DATA}}' . "\n" . '订单金额：{{keyword3.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM203847595' => array( 'name' => '退货审核通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'keyword5', 'remark'), 'content' => "\n" . '{{first.DATA}}' . "\n" . '审核结果：{{keyword1.DATA}}' . "\n" . '商品信息：{{keyword2.DATA}}' . "\n" . '退货金额：{{keyword3.DATA}}' . "\n" . '审核说明：{{keyword4.DATA}}' . "\n" . '审核时间：{{keyword5.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM206164559' => array( 'name' => '审核成功通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '用户名：{{keyword1.DATA}}' . "\n" . '标题：{{keyword2.DATA}}' . "\n" . '时间：{{keyword3.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM400492077' => array( 'name' => '打款通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '用户ID：{{keyword1.DATA}}' . "\n" . '打款金额：{{keyword2.DATA}}' . "\n" . '打款时间：{{keyword3.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM207526849' => array( 'name' => '收款到账提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '活动名称：{{keyword1.DATA}}' . "\n" . '收款金额：{{keyword2.DATA}}' . "\n" . '收款账号：{{keyword3.DATA}}' . "\n" . '到账时间：{{keyword4.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM203170813' => array( 'name' => '商品库存不足提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '商品编码：{{keyword1.DATA}}' . "\n" . '商品名称：{{keyword2.DATA}}' . "\n" . '库存数量：{{keyword3.DATA}}' . "\n" . '{{remark.DATA}}' ), 'OPENTM401105405' => array( 'name' => '参团成功提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '团购商品：{{keyword1.DATA}}' . "\n" . '参团时间：{{keyword2.DATA}}' . "\n" . '{{remark.DATA}}' ), 'TM00353' => array( 'name' => '团购结果通知', 'vars' => array('first', 'Pingou_ProductName', 'Weixin_ID', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '商品名称：{{Pingou_ProductName.DATA}}' . "\n" . '团长：{{Weixin_ID.DATA}}' . "\n" . '{{Remark.DATA}}' ), 'OPENTM203917836' => array( 'name' => '(秒杀)活动变更通知', 'vars' => array('first', 'keyword1', 'keyword2', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '活动名称：{{keyword1.DATA}}' . "\n" . '变更内容：{{keyword2.DATA}}' . "\n" . '{{remark.DATA}}' ) );
	}
	public function systemTemplates() 
	{
		return array( 'OPENTM201752540' => array( 'name' => '订单支付成功通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    订单商品：{{keyword1.DATA}}' . "\n" . '                    订单编号：{{keyword2.DATA}}' . "\n" . '                    支付金额：{{keyword3.DATA}}' . "\n" . '                    支付时间：{{keyword4.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM205213550' => array( 'name' => '订单生成通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    时间：{{keyword1.DATA}}' . "\n" . '                    商品名称：{{keyword2.DATA}}' . "\n" . '                    订单号：{{keyword3.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM202521011' => array( 'name' => '订单完成通知', 'vars' => array('first', 'keyword1', 'keyword2', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    订单号：{{keyword1.DATA}}' . "\n" . '                    完成时间：{{keyword2.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM200565259' => array( 'name' => '订单发货提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    订单编号：{{keyword1.DATA}}' . "\n" . '                    物流公司：{{keyword2.DATA}}' . "\n" . '                    物流单号：{{keyword3.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM206547887' => array( 'name' => '分销订单通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'keyword5', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    订单编号：{{keyword1.DATA}}' . "\n" . '                    商品名称：{{keyword2.DATA}}' . "\n" . '                    下单时间：{{keyword3.DATA}}' . "\n" . '                    下单金额：{{keyword4.DATA}}' . "\n" . '                    分销商名称：{{keyword5.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM206328960' => array( 'name' => '分销订单支付成功通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    商品名称：{{keyword1.DATA}}' . "\n" . '                    商品佣金：{{keyword2.DATA}}' . "\n" . '                    订单状态：{{keyword3.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM206328970' => array( 'name' => '分销订单下单成功通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    商品名称：{{keyword1.DATA}}' . "\n" . '                    商品佣金：{{keyword2.DATA}}' . "\n" . '                    下单时间：{{keyword3.DATA}}' . "\n" . '                    订单状态：{{keyword4.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM220197216' => array( 'name' => '分销订单提成通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    订单号：{{keyword1.DATA}}' . "\n" . '                    订单金额：{{keyword2.DATA}}' . "\n" . '                    分成金额：{{keyword3.DATA}}' . "\n" . '                    时间：{{keyword4.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM207126233' => array( 'name' => '分销商申请成功提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    分销商名称：{{keyword1.DATA}}' . "\n" . '                    分销商电话：{{keyword2.DATA}}' . "\n" . '                    申请时间：{{keyword3.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'TM204601671' => array( 'name' => '访客消息通知', 'vars' => array('first', 'keynote1', 'keynote2', 'remark'), 'content' => "\n" . '                    {{first.DATA}}' . "\n" . '                    消息来自：{{keynote1.DATA}}' . "\n" . '                    发送时间：{{keynote2.DATA}}' . "\n" . '                    {{remark.DATA}}' ), 'OPENTM201072360' => array( 'name' => '余额不足提醒', 'vars' => array('first', 'keyword1', 'keyword2', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '                 账号：{{keyword1.DATA}}' . "\n" . '                 当前余额：{{keyword2.DATA}}' . "\n" . '                {{remark.DATA}}' ), 'OPENTM401560049' => array( 'name' => '店铺审核通知', 'vars' => array('first', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'keyword5', 'remark'), 'content' => '{{first.DATA}}' . "\n" . '                店铺编号：{{keyword1.DATA}}' . "\n" . '                店铺名称：{{keyword2.DATA}}' . "\n" . '                店铺店主：{{keyword3.DATA}}' . "\n" . '                手机号码：{{keyword4.DATA}}' . "\n" . '                注册时间：{{keyword5.DATA}}' . "\n" . '                {{remark.DATA}}' ) );
	}
	public function postCurl($url, $data) 
	{
		$ch = curl_init();
		$header = array('Accept-Charset: utf-8');
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$tmpInfo = curl_exec($ch);
		$errorno = curl_errno($ch);
		if ($errorno) 
		{
			return array('rt' => false, 'errorno' => $errorno);
		}
		$js = json_decode($tmpInfo, 1);
		if ($js['errcode'] == '0') 
		{
			return array('rt' => true, 'errorno' => 0);
		}
		return array('rt' => false, 'errorno' => $js['errcode'], 'errmsg' => $js['errmsg']);
	}
	public function curlGet($url) 
	{
		$ch = curl_init();
		$header = 'Accept-Charset: utf-8';
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$temp = curl_exec($ch);
		return $temp;
	}
}
?>
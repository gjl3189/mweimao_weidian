
<?php
class PhysicalPermission 
{
	static public $physicalRbac = array();
	static public function getRbacConfig() 
	{
		$rbacConfig = array( 'goods' => array('create' => '发布商品', 'index' => '出售中商品', 'stockout' => '已售罄商品', 'soldout' => '仓库中的商品', 'category' => '商品分组', 'product_comment' => '商品评价', 'store_comment' => '店铺评价', 'subject' => '导购专题', 'edit' => '修改商品', 'del_product' => '删除商品', 'checkoutProduct' => '商品导出', 'goods_category_add' => '分组添加', 'goods_category_edit' => '分组修改', 'goods_category_delete' => '分组删除', 'edit_group' => '商品修改分组', 'save_qrcode_activity' => '保存扫码活动', 'get_qrcode_activity' => '获取扫码活动', 'del_qrcode_activity' => '删除扫码活动', 'del_comment' => '删除评论', 'set_comment_status' => '修改评论审核状态', 'checkoutProduct' => '导出商品列表excel'), 'order' => array('dashboard' => '订单概况', 'all' => '所有订单', 'selffetch' => '到店自提订单', 'codpay' => '货到付款订单', 'order_return' => '退货列表', 'order_rights' => '维权列表', 'star' => '加星订单', 'activity' => '活动订单', 'add' => '添加订单', 'orderprint' => '订单打印机', 'check' => '平台对账', 'seller_check' => '分销商对账', 'supplier_check' => '供货商对账', 'dealer_check' => '经销商对账', 'order_checkout_csv' => '订单导出', 'save_bak' => '订单备注', 'add_star' => '订单加星', 'cancel_status' => '取消订单', 'package_product' => '发货弹窗', 'complate_status' => '交易完成', 'selffetch_status' => '到店自提完成', 'return_save' => '退单', 'return_over' => '退货完成'), 'trade' => array('delivery' => '物流工具', 'income' => '账务概况'), 'offline' => array('offline_list' => '商家做单管理', 'offline_index' => '商家线下做单') );
		return $rbacConfig;
	}
	static public function getRbacLink() 
	{
		$rbacLink = array( 'goods' => array('create', 'index', 'stockout', 'soldout', 'category', 'product_comment', 'store_comment', 'subject'), 'order' => array('dashboard', 'all', 'selffetch', 'codpay', 'order_return', 'order_rights', 'star', 'activity', 'add', 'check', 'seller_check', 'supplier_check', 'dealer_check', 'orderprint'), 'trade' => array('delivery', 'income'), 'offline' => array('offline_index', 'offline_list') );
		return $rbacLink;
	}
	static public function checkIsShow($action_id, $physical_uid, $controller_id, $user_type = 0) 
	{
		if (empty(self::$physicalRbac[$controller_id])) 
		{
			self::$physicalRbac[$controller_id] = M('Rbac_action')->getControlArr($physical_uid, $controller_id);
		}
		if ($user_type == 0) 
		{
			return true;
		}
		if (in_array($action_id, self::$physicalRbac[$controller_id])) 
		{
			return true;
		}
		return false;
	}
}
?>
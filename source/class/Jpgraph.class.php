
<?php
class Jpgraph 
{
	private $resource_id;
	private $save_dir = '';
	private $fonts = array('simhei' => 'fonts/simhei.ttf', 'lihei' => 'fonts/lihei.otf');
	public function __construct($width = 640, $height = 900, $save_dir = '') 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		$this->resource_id = imagecreatetruecolor($width, $height);
	}
	public function setFillColor($red, $green, $blue) 
	{
		$color = imagecolorallocate($this->resource_id, $red, $green, $blue);
		return $color;
	}
	public function addRectangle($x0 = 0, $y0 = 0, $width = 200, $height = 900, $fill_color) 
	{
		imagerectangle($this->resource_id, $x0, $y0, $height, $width, $fill_color);
		imagefilledrectangle($this->resource_id, $x0, $y0, $height, $width, $fill_color);
	}
	public function addCircle($center_x0 = 0, $center_y0 = 0, $width = 150, $height = 150, $fill_color) 
	{
		imageellipse($this->resource_id, $center_x0, $center_y0, $width, $height, $fill_color);
	}
	public function addLine($x0 = 0, $y0 = 0, $width = 640, $height = 2, $fill_color = '') 
	{
		imagesetthickness($this->resource_id, 3);
		imageline($this->resource_id, $x0, $y0, $width, $height, $fill_color);
	}
	public function addText($font_size = '12', $font_family, $x0 = 0, $y0 = 0, $text, $fill_color, $angle = 0) 
	{
		$font_family = dirname(__FILE__) . '/' . $this->fonts[$font_family];
		imagettftext($this->resource_id, $font_size, $angle, $x0, $y0, $fill_color, $font_family, $text);
	}
	public function addWatermark($image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $delete_image = false) 
	{
		if (!preg_match('/^(.*?)\\.(jpg|png|gif)$/i', $image)) 
		{
			$ext = '.jpg';
			$image_content = $this->download($image, $save_dir, $ext);
			$image = $image_content;
		}
		$image_info = getimagesize($image);
		if (empty($image_info)) 
		{
			return array('msg_code' => 1000, 0 => '图片不存在');
		}
		if (strtolower($image_info['mime']) == 'image/png') 
		{
			$water_image = imagecreatefrompng($image);
		}
		else if (strtolower($image_info['mime']) == 'image/jpeg') 
		{
			$water_image = imagecreatefromjpeg($image);
		}
		else if (strtolower($image_info['mime']) == 'image/gif') 
		{
			$water_image = imagecreatefromgif($image);
		}
		else 
		{
			return array('msg_code' => 1001, 0 => '无效的图片类型');
		}
		$src_width = $image_info[0];
		$src_height = $image_info[1];
		$result = imagecopyresampled($this->resource_id, $water_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_width, $src_height);
		if (!empty($delete_image) && file_exists($image)) 
		{
			@unlink($image);
		}
		if ($result) 
		{
			return array('msg_code' => 0, 0 => '水印生成成功');
		}
		return array('msg_code' => 1002, 0 => '水印生成失败');
	}
	public function resize($source_path, $save_dir = '', $target_width = 200, $target_height = 200, $fixed_orig = '') 
	{
		if (!preg_match('/^(.*?)\\.(jpg|png|gif)$/i', $source_path)) 
		{
			$ext = '.jpg';
			$image_content = $this->download($source_path, $save_dir, $ext);
			$source_path = $image_content;
			$source_image = imagecreatefromjpeg($source_path);
		}
		$source_info = getimagesize($source_path);
		$source_width = $source_info[0];
		$source_height = $source_info[1];
		$source_mime = $source_info['mime'];
		$ratio_orig = $source_width / $source_height;
		if ($fixed_orig == 'width') 
		{
			$target_height = $target_width / $ratio_orig;
		}
		else if ($fixed_orig == 'height') 
		{
			$target_width = $target_height * $ratio_orig;
		}
		else if ($ratio_orig < ($target_width / $target_height)) 
		{
			$target_width = $target_height * $ratio_orig;
		}
		else 
		{
			$target_height = $target_width / $ratio_orig;
		}
		if (preg_match('/^(.*?)\\.(jpg|png|gif)$/i', $source_path)) 
		{
			switch ($source_mime) 
			{
				case 'image/gif': $source_image = imagecreatefromgif($source_path);
				$ext = '.gif';
				break;
				case 'image/jpeg': $source_image = imagecreatefromjpeg($source_path);
				$ext = '.jpg';
				break;
				case 'image/png': $source_image = imagecreatefrompng($source_path);
				$ext = '.png';
				break;
			}
		}
		else 
		{
			$target_image = imagecreatetruecolor($target_width, $target_height);
			imagecopyresampled($target_image, $source_image, 0, 0, 0, 0, $target_width, $target_height, $source_width, $source_height);
			$filename = time() . $ext;
			imagejpeg($target_image, $save_dir . $filename, 100);
			@unlink($image_content);
			return array('http_path' => $filename, 'save_path' => $save_dir . $filename);
		}
	}
	public function setBackgroundColor($red = 227, $blue = 33, $green = 57) 
	{
		imagecolorallocate($this->resource_id, $red, $blue, $green);
	}
	public function output($image_type = 'png') 
	{
		if (strtolower($image_type) == 'png') 
		{
			header('Content-type:image/png');
		}
		else 
		{
			if ((strtolower($image_type) == 'jpg') || (strtolower($image_type) == 'jpeg')) 
			{
				header('Content-type:image/jpeg');
			}
			else if (strtolower($image_type) == 'gif') 
			{
				header('Content-type:image/gif');
			}
			else 
			{
				header('Content-type:image/png');
			}
		}
		imagepng($this->resource_id);
		imagedestroy($this->resource_id);
	}
	public function generate($save_dir, $filename = NULL, $image_type = 'png') 
	{
		if (is_null($filename)) 
		{
			$filename = date('YmdHis');
		}
		if (!empty($save_dir) && is_dir($save_dir)) 
		{
			$this->save_dir = $save_dir;
		}
		if (empty($this->save_dir) || !is_dir($this->save_dir)) 
		{
			return array('err_code' => 1003, 'err_msg' => '无效的文件目录');
		}
		if (!is_writable($this->save_dir)) 
		{
			return array('err_code' => 1004, 'err_msg' => '文件目录不可写');
		}
		if ((strtolower($image_type) == 'jpg') || (strtolower($image_type) == 'jpeg')) 
		{
			$filename .= '.jpg';
			$save_path = $this->save_dir . DIRECTORY_SEPARATOR . $filename;
			imagejpg($this->resource_id, $save_path, 5);
		}
		else if (strtolower($image_type) == 'gif') 
		{
			$filename .= '.gif';
			$save_path = $this->save_dir . DIRECTORY_SEPARATOR . $filename;
			imagegif($this->resource_id, $save_path, 5);
		}
		else 
		{
			$filename .= '.png';
			$save_path = $this->save_dir . DIRECTORY_SEPARATOR . $filename;
			imagepng($this->resource_id, $save_path, 5);
		}
		imagedestroy($this->resource_id);
		return array( 'err_code' => 0, 'err_msg' => array('name' => $filename, 'path' => $save_path) );
	}
	public function download($url, $save_dir = '', $ext = '') 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		ob_start();
		curl_exec($ch);
		$return_content = ob_get_contents();
		ob_end_clean();
		$return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if ($return_code != 200) 
		{
			$return_content = '';
		}
		$tmp_image = '';
		if (!empty($return_content)) 
		{
			$tmp_image = $save_dir . 'tmp_' . time() . $ext;
			$fp = @fopen($tmp_image, 'a');
			fwrite($fp, $return_content);
			fclose($fp);
		}
		return $tmp_image;
	}
}
?>
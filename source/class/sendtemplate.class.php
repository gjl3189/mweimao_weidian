
<?php
class sendtemplate 
{
	static public function smsTpl($type, $array = array()) 
	{
		switch ($type) 
		{
			case '1': $msg = '亲，您的宝贝已发货，' . $array['express_company'] . ':' . $array['express_no'] . '请注意查收，有问题请与本店联系！';
			break;
			case '2': $msg = '亲，包裹已创建发货，' . $array['express_company'] . ':' . $array['express_no'] . '请注意提醒买家收货哦！';
			break;
			case '3': $nickname = (($nickname ? $nickname : '匿名微信用户'));
			$msg = '亲，您的订单已生成，[ 买家: ' . $nickname . ' ] 订单号：' . $array['order_no'] . '稍候将通知本店客服！';
			break;
			case '4': $nickname = (($nickname ? $nickname : '匿名微信用户'));
			$msg = '亲，有顾客已生成，[ 买家: ' . $nickname . ' ] 订单号：' . $array['order_no'] . '请及时登录平台查看哦！';
			break;
			case '5': $msg = '尊敬的客户，您的订单已支付成功，[ 店铺名: ' . $array['storename'] . ' ] 订单号：' . $array['order_no'] . '，稍候将通知本店客服！';
			break;
			case '6': $nickname = (($nickname ? $nickname : '匿名微信用户'));
			$msg = '尊敬的客户，您店铺的订单已支付成功，[ 店铺名: ' . $array['storename'] . ' ]，购买人: ' . $nickname . '， 订单号：' . $array['order_no'] . '！';
			break;
			case '7': $msg = '尊敬的客户，您的订单已交易完成，[ 店铺名: ' . $array['storename'] . ' ] 订单号：' . $array['order_no'] . '，稍候将通知本店客服！';
			break;
			case '8': $nickname = (($nickname ? $nickname : '匿名微信用户'));
			$msg = '尊敬的客户，您店铺的订单已交易完成，[ 店铺名: ' . $array['storename'] . ' ]，购买人: ' . $nickname . '， 订单号：' . $array['order_no'] . '！';
			break;
			case '9': $storename = (($storename ? $storename : ''));
			$msg = '您好，恭喜您成为 【' . $storename . '】 的供货商。时间：' . $date . '。';
			break;
			case '10': $storename = (($storename ? $storename : ''));
			$msg = '您好，恭喜您成为 【' . $storename . '】 的分销商。时间：' . $date . '。';
			break;
			case '11': $msg = '{' . $array['supplier_info']['name'] . '}，您的分销订单，分销订单号：{' . $array['seller_order_info'][order_no] . '},已产生分润：{' . $array['profit'] . '}';
			break;
			case '12': $msg = '{' . $array['seller_info']['name'] . '}，您的订单，订单号：{' . $array['seller_order_info'][order_no] . '},已产生分润：{' . $array['profit'] . '}';
			break;
			case '13': $store_name = $array['store_name'];
			$msg = '亲，您的店铺：{' . $store_name . '}保证金已不足，赶快登入平台充值吧！';
			break;
			case '14': $store_name = $array['store_name'];
			$state = $array['state'];
			$msg = '您好，您所管理区域内的店铺：' . $store_name . '平台审核' . $state . '！赶紧登录平台查看吧！';
			break;
			case '15': $store_name = $array['store_name'];
			$state = $array['state'];
			$msg = '您好，您的店铺：' . $store_name . '平台审核' . $state . '！赶紧登录平台查看吧！';
			break;
			case '16': $store_name = $array['store_name'];
			$state = $array['state'];
			$msg = '您好，您的代理店铺：' . $store_name . '平台审核' . $state . '！赶紧登录平台查看吧！';
			break;
		}
	}
	static public function shopsmsTpl($type, $array = array()) 
	{
		switch ($type) 
		{
			case '1': $pifa_name = $array['storename'];
			$msg = '您好，您的批发店铺已经通过审核，批发商店铺名称：' . $pifa_name;
			break;
			case '2': $bzjje = $array['bond'];
			$supplier_storename = $array['name'];
			$supplier_bankname = $array['bankname'];
			$supplier_bankno = $array['bank_card'];
			$supplier_linkuser = $array['username'];
			$supplier_tel = $array['userphone'];
			$msg = $pifa_storename . '，您好，您的店铺已经通过审核，请提交保证金：{' . $bzjje . '}到{' . $supplier_storename . '}-{' . $supplier_bankname . '：' . $supplier_bankno . '}。供货商联系信息：' . $supplier_linkuser . '，' . $supplier_tel . '}';
			break;
			case '3': $supplier_storename = $array['supplier_store']['name'];
			$seller_storename = $array['seller_store']['name'];
			$sell_username = $array['seller_user_info']['nickname'];
			$sell_userphone = $array['seller_user_info']['phone'];
			$msg = $supplier_storename . '，您好，{' . $seller_storename . '}已成功提交保证金相应信息。请与{' . $seller_storename . '}联系，联系信息：{' . $sell_username . '，' . $sell_userphone . '}，请及时线下沟通';
			break;
			case '4': $supplier_storename = $array['supplier_store']['name'];
			$seller_storename = $array['seller_store']['name'];
			$supplier_username = $array['supplier_user_info']['nickname'];
			$supplier_userphone = $array['supplier_user_info']['phone'];
			$msg = '{' . $seller_storename . '}您好，您已成功提交保证金相应信息。请与{' . $supplier_storename . '}联系，联系信息：{' . $supplier_username . '，' . $supplier_userphone . '}，请及时线下沟通';
			break;
			case '5': $supplier_storename = $array['supplier_info']['name'];
			$seller_storename = $array['seller_info']['name'];
			$msg = '{' . $supplier_storename . '}您好，您已确认{' . $seller_storename . '}-{' . $array['bankname'] . '：' . $array['bond_information']['bank_card'] . '}向您汇入{￥' . $array['bond_information']['apply_recharge'] . '}';
			break;
			case '6': $supplier_storename = $array['supplier_info']['name'];
			$seller_storename = $array['seller_info']['name'];
			$msg = ' {' . $seller_storename . '}您好，{' . $supplier_storename . '}已确认成功提交{￥' . $array['bond_information']['apply_recharge'] . '}。';
			break;
			case '7': $order_no = $array['order_no'];
			$msg = '亲，订单：{' . $order_no . '}，的商品，客户已申请退货，请及时登录处理！';
			break;
			case '8': $order_no = $array['order_no'];
			$msg = '亲，您的订单：{' . $order_no . '}，商品退货申请提交成功，请等待商家处理！';
			break;
			case '9': $buyer_name = $array['buyer_name'];
			$tuan_name = $array['tuan_name'];
			$msg = '亲：{' . $buyer_name . '}，加入了您开的 {' . $tuan_name . '} 拼团活动！';
			break;
			case '10': $tuanzhang_name = $array['tuanzhang_name'];
			$tuan_name = $array['tuan_name'];
			$msg = '亲：您加入了，{' . $tuanzhang_name . '} 开启的{' . $tuan_name . '}拼团活动！';
			break;
			case '11': $tuan_name = $array['tuan_name'];
			$msg = '亲：{' . $tuan_name . '}活动已结束， 恭喜您拼团成功！';
			break;
			case '12': $tuan_name = $array['tuan_name'];
			$msg = '亲：{' . $tuan_name . '}活动已结束， 很遗憾，拼团失败！';
			break;
			case '13': $store_name = $array['store_name'];
			$msg = '亲，您的店铺：{' . $store_name . '}保证金已不足，赶快登入平台充值吧！';
			break;
			case '14': $usename = $array['helper_username'];
			$second = $array['second'];
			$msg = '亲，【' . $usename . '】成功帮助您提前了' . $second . '秒';
			break;
			case '15': $usename = $array['miaosha_username'];
			$second = $array['second'];
			$msg = '您成功帮助【' . $usename . '】提前了' . $second . '秒';
			break;
			case '16': $order_no = $array['order_no'];
			$msg = '亲，你已经审核订单：{' . $order_no . '}，的商品的退货申请！';
			break;
			case '17': $order_no = $array['order_no'];
			$msg = '亲，您的订单：{' . $order_no . '}，商品退货申请商家已经进行审核，请及时查看！';
			break;
		}
	}
}
?>
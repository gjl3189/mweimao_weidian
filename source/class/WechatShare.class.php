
<?php
class WechatShare 
{
	public $appid = '';
	public $appsecret = '';
	public $error = array();
	public function __construct() 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		$this->appid = option('config.wechat_appid');
		$this->appsecret = option('config.wechat_appsecret');
	}
	public function getSgin($share_conf, $return_sign_data = false, $default_url = '') 
	{
		if (0 < $_SESSION['wap_user']['app_id']) 
		{
			if ($_SESSION['wap_user']['app_id'] == 1) 
			{
				$domain = D('Store')->where(array('store_id' => $_SESSION['wap_user']['store_id']))->field('source_site_url')->find();
				$url = $domain['source_site_url'] . '/index.php?g=Home&m=Auth&a=share';
			}
			else if ($_SESSION['wap_user']['app_id'] == 2) 
			{
				$domain = D('Store')->where(array('store_id' => $_SESSION['wap_user']['store_id']))->field('source_site_url')->find();
				$url = $domain['source_site_url'] . '/wap.php?c=Weidian&a=share';
			}
			$json = $this->https_request($url);
			$ticket = $json['ticket'];
			$this->appid = $json['appid'];
		}
		else 
		{
			import('WechatApi');
			$tokenObj = new WechatApi(array('appid' => $this->appid, 'appsecret' => $this->appsecret));
			$access_token = $tokenObj->get_access_token();
			$ticket = $this->getTicket($access_token['access_token']);
		}
		$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		if ($return_sign_data && !empty($default_url)) 
		{
			$url = $default_url;
		}
		$sign_data = $this->addSign($ticket, $url);
		if ($return_sign_data) 
		{
			return $sign_data;
		}
		$share_html = $this->createHtml($sign_data, $share_conf);
		return $share_html;
	}
	public function getSynShare() 
	{
		import('WechatApi');
		$tokenObj = new WechatApi(array('appid' => $this->appid, 'appsecret' => $this->appsecret));
		$access_token = $tokenObj->get_access_token();
		$ticket = $this->getTicket($access_token['access_token']);
		return array('appid' => $this->appid, 'ticket' => $ticket);
	}
	public function getError() 
	{
		dump($this->error);
	}
	public function addSign($ticket, $url) 
	{
		$timestamp = time();
		$nonceStr = 'str' . rand(100000, 999999);
		$array = array('noncestr' => $nonceStr, 'jsapi_ticket' => $ticket, 'timestamp' => $timestamp, 'url' => $url);
		ksort($array);
		$signPars = '';
		foreach ($array as $k => $v ) 
		{
			if (('' != $v) && ('sign' != $k)) 
			{
				if ($signPars == '') 
				{
					$signPars .= $k . '=' . $v;
				}
				else 
				{
					$signPars .= '&' . $k . '=' . $v;
				}
			}
		}
		$result = array('appId' => $this->appid, 'timestamp' => $timestamp, 'nonceStr' => $nonceStr, 'url' => $url, 'signature' => SHA1($signPars));
		return $result;
	}
	public function getUrl() 
	{
		$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		if (isset($_GET['code']) && isset($_GET['state']) && ($_GET['state'] == 'oauth')) 
		{
			$url = $this->clearUrl($url);
			if (isset($_GET['wecha_id'])) 
			{
				$url .= '&wecha_id=' . $this->wecha_id;
			}
			return $url;
		}
		return $url;
	}
	public function clearUrl($url) 
	{
		$param = explode('&', $url);
		$i = 0;
		$count = count($param);
		while ($i < $count) 
		{
			if (preg_match('/^(code=|state=|wecha_id=).*/', $param[$i])) 
			{
				unset($param[$i]);
			}
			++$i;
		}
		return join('&', $param);
	}
	public function getToken() 
	{
	}
	public function getTicket($token) 
	{
		$now = time();
		$ticketData = S($this->appid . '_shareTicket');
		if (($ticketData['ticket'] == '') || ($ticketData['expires_in'] < $now)) 
		{
			$url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=' . $token . '&type=jsapi';
			$res = $this->https_request($url);
			if (0 < $res['errcode']) 
			{
				return array('errcode' => $res['errcode'], 'errmsg' => $res['errMsg']);
			}
			S($this->appid . '_shareTicket', array('ticket' => $res['ticket'], 'expires_in' => $res['expires_in'] + $now));
			return $res['ticket'];
		}
		return $ticketData['ticket'];
	}
	public function createHtml($sign_data, $share_conf) 
	{
		$share_conf['title'] = $this->formartHtml($share_conf['title']);
		$share_conf['link'] = $this->formartHtml($share_conf['link']);
		$share_conf['desc'] = str_replace(array("\r\n", "\r", "\n"), '', $share_conf['desc']);
		$html = "\t" . '<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>' . "\r\n\t" . '<script type="text/javascript">' . "\r\n\t\t" . 'var scan_qrcode_callback = \'\';' . "\r\n\t\t" . 'var app_share_title = \'' . $share_conf['title'] . '\';' . "\r\n\t\t" . 'var app_share_desc = \'' . $share_conf['desc'] . '\';' . "\r\n\t\t" . 'var app_share_link = \'' . $share_conf['link'] . '\';' . "\r\n\t\t" . 'var app_share_imgUrl = \'' . $share_conf['imgUrl'] . '\';' . "\r\n\t\t\r\n\t\t" . 'wx.config({' . "\r\n\t\t" . '  debug: false,' . "\r\n\t\t" . '  appId: ' . "\t" . '\'' . $sign_data['appId'] . '\',' . "\r\n\t\t" . '  timestamp: ' . $sign_data['timestamp'] . ',' . "\r\n\t\t" . '  nonceStr: \'' . $sign_data['nonceStr'] . '\',' . "\r\n\t\t" . '  signature: \'' . $sign_data['signature'] . '\',' . "\r\n\t\t" . '  jsApiList: [' . "\r\n\t" . '    ' . "\t" . '\'checkJsApi\',' . "\r\n\t\t" . '    \'onMenuShareTimeline\',' . "\r\n\t\t" . '    \'onMenuShareAppMessage\',' . "\r\n\t\t" . '    \'onMenuShareQQ\',' . "\r\n\t\t" . '    \'onMenuShareWeibo\',' . "\r\n\t\t\t" . '\'openLocation\',' . "\r\n\t\t\t" . '\'getLocation\',' . "\r\n\t\t\t" . '\'addCard\',' . "\r\n\t\t\t" . '\'chooseCard\',' . "\r\n\t\t\t" . '\'openCard\',' . "\r\n\t\t\t" . '\'hideMenuItems\',' . "\r\n\t\t\t" . '\'getLocation\',' . "\r\n\t\t\t" . '\'scanQRCode\'' . "\r\n\t\t" . '  ]' . "\r\n\t\t" . '});' . "\r\n\t" . '</script>' . "\r\n\t" . '<script type="text/javascript">' . "\r\n\t" . 'wx.ready(function () {' . "\r\n\t" . '  // 1 判断当前版本是否支持指定 JS 接口，支持批量判断' . "\r\n\t" . '  /*document.querySelector(\'#checkJsApi\').onclick = function () {' . "\r\n\t" . '    wx.checkJsApi({' . "\r\n\t" . '      jsApiList: [' . "\r\n\t" . '        \'getNetworkType\',' . "\r\n\t" . '        \'previewImage\'' . "\r\n\t" . '      ],' . "\r\n\t" . '      success: function (res) {' . "\r\n\t" . '        //alert(JSON.stringify(res));' . "\r\n\t" . '      }' . "\r\n\t" . '    });' . "\r\n\t" . '  };*/' . "\r\n\t" . '  // 2. 分享接口' . "\r\n\t" . '  // 2.1 监听“分享给朋友”，按钮点击、自定义分享内容及分享结果接口' . "\r\n\t" . '    wx.onMenuShareAppMessage({' . "\r\n\t\t\t" . 'title: \'' . $share_conf['title'] . '\',' . "\r\n\t\t\t" . 'desc: \'' . $share_conf['desc'] . '\',' . "\r\n\t\t\t" . 'link: \'' . $share_conf['link'] . '\',' . "\r\n\t\t\t" . 'imgUrl: \'' . $share_conf['imgUrl'] . '\',' . "\r\n\t\t\t" . 'type: \'\', // 分享类型,music、video或link，不填默认为link' . "\r\n\t\t\t" . 'dataUrl: \'\', // 如果type是music或video，则要提供数据链接，默认为空' . "\r\n\t\t\t" . 'uid: \'' . $share_conf['uid'] . '\',' . "\r\n\t\t\t" . 'store_id: \'' . $share_conf['store_id'] . '\',' . "\r\n\t\t\t" . 'data_id: \'' . $share_conf['data_id'] . '\',' . "\r\n\t\t\t" . 'types: \'' . $share_conf['types'] . '\',' . "\r\n\t\t\t" . 'success: function () { ' . "\r\n\t\t\t\t" . '$("#js-share-guide").addClass("hide");' . "\r\n\t\t\t\t" . 'try{' . "\r\n\t\t\t\t\t" . 'shareHandle(\'frined\',\'' . $share_conf['types'] . '\',\'' . $share_conf['uid'] . '\',\'' . $share_conf['store_id'] . '\',\'' . $share_conf['data_id'] . '\');' . "\r\n\t\t\t\t" . '} catch(e) {' . "\r\n\t\t\t\t\r\n\t\t\t\t" . '}' . "\r\n\t\t\t\t" . '//shareHandle(to,types,uid,store_id,data_id)' . "\r\n\t\t" . '    },' . "\r\n\t\t" . '    cancel: function () { ' . "\r\n\t\t" . '        //alert(\'分享朋友失败\');' . "\r\n\t\t" . '    },' . "\r\n\t\t" . '    trigger: function () {' . "\r\n\t\t\t\t" . 'try {' . "\r\n\t\t\t\t" . '    if(this.uid != \'\' && this.store_id != \'\'){' . "\r\n\t\t\t\t\t\t" . 'var link   = this.link;' . "\r\n\t\t\t\t\t\t" . 'var title  = this.title;' . "\r\n\t\t\t\t\t\t" . 'var imgUrl = this.imgUrl;' . "\r\n\t\t\t\t\t\t" . 'var desc   = this.desc;' . "\r\n\t\t\t\t\t\t" . '$.ajaxSetup({async:false});' . "\r\n\t\t\t\t\t\t" . 'try {' . "\r\n\t\t\t\t\t\t\t" . '$.post(create_fans_supplier_store_url,{\'uid\':this.uid,\'store_id\':this.store_id},function(data){' . "\r\n\t\t\t\t\t\t\t" . '    var data = $.parseJSON(data);' . "\r\n\t\t\t\t\t\t\t\t" . 'link   = data.link;' . "\r\n\t\t\t\t\t\t\t\t" . 'title  = data.title;' . "\r\n\t\t\t\t\t\t\t\t" . 'imgUrl = data.imgUrl;' . "\r\n\t\t\t\t\t\t\t\t" . 'desc   = data.desc;' . "\r\n\t\t\t\t\t\t\t" . '});' . "\r\n\t\t\t\t\t\t" . '} catch (e) {' . "\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t" . '}' . "\r\n\t\t\t\t\t\t" . 'this.link   = link;' . "\r\n\t\t\t\t\t\t" . 'this.title  = title;' . "\r\n\t\t\t\t\t\t" . 'this.imgUrl = imgUrl;' . "\r\n\t\t\t\t\t\t" . 'this.desc   = desc;' . "\r\n\t\t\t\t\t" . '}' . "\r\n\t\r\n\t\t\t\t\t" . 'if ($(\'input[name="share"]\').length > 0) {' . "\r\n\t\t\t\t\t" . '    var desc = $(\'input[name="share"]\').val().trim();' . "\r\n\t" . '                    this.desc = desc;' . "\r\n\t\t\t\t\t" . '}' . "\r\n\t\r\n\t\t\t\t\t" . 'if ($(\'.sec_msg\').text().length > 0) {' . "\r\n\t\t\t\t\t" . '    var desc = $(\'.sec_msg\').text().trim();' . "\r\n\t" . '                    this.desc = desc;' . "\r\n\t\t\t\t\t" . '}' . "\r\n\t\r\n\t\t\t\t\t" . 'if ($(\'.con-h1\').text().length > 0){' . "\r\n\t" . '                    var title = $(\'.con-h1\').text().trim();' . "\r\n\t" . '                    this.title = title;' . "\r\n\t\t\t\t\t" . '}' . "\r\n\t\r\n\t\t\t\t\t" . 'if ($(\'.con-h2\').text().length > 0){' . "\r\n\t" . '                    var desc = $(\'.con-h2\').text().trim();' . "\r\n\t" . '                    this.desc = desc;' . "\r\n\t\t\t\t\t" . '}' . "\r\n\t\t\t\t" . '} catch (e) {' . "\r\n\t\t\t\t" . '}' . "\r\n\t\t\t" . '}' . "\r\n\t\t" . '});' . "\r\n\r\n\r\n\t" . '  // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口' . "\r\n\t\t" . 'wx.onMenuShareTimeline({' . "\r\n\t\t\t" . 'title: \'' . $share_conf['title'] . '\',' . "\r\n\t\t\t" . 'link: \'' . $share_conf['link'] . '\',' . "\r\n\t\t\t" . 'imgUrl: \'' . $share_conf['imgUrl'] . '\',' . "\r\n\t\t\t" . 'uid: \'' . $share_conf['uid'] . '\',' . "\r\n\t\t" . '    store_id: \'' . $share_conf['store_id'] . '\',' . "\r\n\t\t" . '    types: \'' . $share_conf['types'] . '\',' . "\r\n\t\t" . '    data_id: \'' . $share_conf['data_id'] . '\',' . "\r\n\t\t" . '    success: function () {' . "\r\n\t\t" . '    ' . "\t" . '$("#js-share-guide").addClass("hide");' . "\r\n\t\t\t\t" . 'shareHandle(\'frined\',\'' . $share_conf['types'] . '\',\'' . $share_conf['uid'] . '\',\'' . $share_conf['store_id'] . '\',\'' . $share_conf['data_id'] . '\');' . "\r\n\t\t" . '    },' . "\r\n\t\t" . '    cancel: function () { ' . "\r\n\t\t" . '        //alert(\'分享朋友圈失败\');' . "\r\n\t\t" . '    },' . "\r\n\t\t" . '    trigger: function () {' . "\r\n\t\t\t\t" . '//try {' . "\r\n\t\t\t\t\t" . '//alert(11)' . "\r\n\t\t\t\t\t" . 'if(this.uid != \'\' && this.store_id != \'\'){' . "\r\n\t\t\t\t\t" . '//' . "\t" . 'alert(22)' . "\r\n\t\t\t\t\t\t" . 'var link   = this.link;' . "\r\n\t\t\t\t\t\t" . 'var title  = this.title;' . "\r\n\t\t\t\t\t\t" . 'var imgUrl = this.imgUrl;' . "\r\n\t\t\t\t\t\t" . 'var desc   = this.desc;' . "\r\n\t\t\t\t\t\t" . '//alert(33)' . "\r\n\t\r\n\t\t\t\t\t\t" . 'try {' . "\r\n\t\t\t\t\t\t\t" . '$.post(create_fans_supplier_store_url,{\'uid\':this.uid,\'store_id\':this.store_id},function(data){' . "\r\n\t\t\t\t\t\t\t" . '    var data = $.parseJSON(data);' . "\r\n\t\t\t\t\t\t\t\t" . 'link   = data.link;' . "\r\n\t\t\t\t\t\t\t\t" . 'title  = data.title;' . "\r\n\t\t\t\t\t\t\t\t" . 'imgUrl = data.imgUrl;' . "\r\n\t\t\t\t\t\t\t\t" . 'desc   = data.desc;' . "\r\n\t\t\t\t\t\t\t" . '});' . "\r\n\t\t\t\t\t\t" . '} catch (e) {' . "\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t" . '}' . "\r\n\t\t\t\t\t\t" . 'this.link   = link;' . "\r\n\t\t\t\t\t\t" . 'this.title  = title;' . "\r\n\t\t\t\t\t\t" . 'this.imgUrl = imgUrl;' . "\r\n\t\t\t\t\t\t" . 'this.desc   = desc;' . "\r\n\t\t\t\t\t" . '}' . "\r\n\t\t\t\t" . '//} catch(e){' . "\r\n\t\t\t\t\t\r\n\t\t\t\t" . '//}' . "\r\n\t\t\t" . '}' . "\r\n\t\t" . '    ' . "\r\n\t\t" . '});' . "\t\r\n\r\n\t" . '  // 2.4 监听“分享到微博”按钮点击、自定义分享内容及分享结果接口' . "\r\n\t\t" . 'wx.onMenuShareWeibo({' . "\r\n\t\t\t" . 'title: \'' . $share_conf['title'] . '\',' . "\r\n\t\t\t" . 'desc: \'' . $share_conf['desc'] . '\',' . "\r\n\t\t\t" . 'link: \'' . $share_conf['link'] . '\',' . "\r\n\t\t\t" . 'imgUrl: \'' . $share_conf['imgUrl'] . '\',' . "\r\n\t\t\t" . 'uid: \'' . $share_conf['uid'] . '\',' . "\r\n\t\t" . '    store_id: \'' . $share_conf['store_id'] . '\',' . "\r\n\t\t" . '    types: \'' . $share_conf['types'] . '\',' . "\r\n\t\t" . '    data_id: \'' . $share_conf['data_id'] . '\',' . "\r\n\t\t" . '    success: function () {' . "\r\n\t\t" . '    ' . "\t" . '$("#js-share-guide").addClass("hide"); ' . "\r\n\t\t\t\t" . 'shareHandle(\'frined\',\'' . $share_conf['types'] . '\',\'' . $share_conf['uid'] . '\',\'' . $share_conf['store_id'] . '\',\'' . $share_conf['data_id'] . '\');' . "\r\n\t\t" . '       ' . "\t" . '//alert(\'分享微博成功\');' . "\r\n\t\t" . '    },' . "\r\n\t\t" . '    cancel: function () { ' . "\r\n\t\t" . '        //alert(\'分享微博失败\');' . "\r\n\t\t" . '    },' . "\r\n\t\t" . '    trigger: function () {' . "\r\n\t\t\t" . '    if(this.uid != \'\' && this.store_id != \'\'){' . "\r\n\t\t\t\t\t" . 'var link   = this.link;' . "\r\n\t\t\t\t\t" . 'var title  = this.title;' . "\r\n\t\t\t\t\t" . 'var imgUrl = this.imgUrl;' . "\r\n\t\t\t\t\t" . 'var desc   = this.desc;' . "\r\n\t\t\t\t\t" . '$.ajaxSetup({async:false});' . "\r\n\t\t\t\t\t" . '$.post(create_fans_supplier_store_url,{\'uid\':this.uid,\'store_id\':this.store_id},function(data){' . "\r\n\t\t\t\t\t" . '    var data = $.parseJSON(data);' . "\r\n\t\t\t\t\t\t" . 'link   = data.link;' . "\r\n\t\t\t\t\t\t" . 'title  = data.title;' . "\r\n\t\t\t\t\t\t" . 'imgUrl = data.imgUrl;' . "\r\n\t\t\t\t\t\t" . 'desc   = data.desc;' . "\r\n\t\t\t\t\t" . '});' . "\r\n\t\t\t\t\t" . 'this.link   = link;' . "\r\n\t\t\t\t\t" . 'this.title  = title;' . "\r\n\t\t\t\t\t" . 'this.imgUrl = imgUrl;' . "\r\n\t\t\t\t\t" . 'this.desc   = desc;' . "\r\n\t\t\t\t" . '}' . "\r\n\t\t\t" . '}' . "\r\n\t\t" . '});' . "\r\n\t\t" . 'wx.error(function (res) {' . "\r\n\t\t\t" . '/*if(res.errMsg == \'config:invalid signature\'){' . "\r\n\t\t\t\t" . 'wx.hideOptionMenu();' . "\r\n\t\t\t" . '}else if(res.errMsg == \'config:invalid url domain\'){' . "\r\n\t\t\t\t" . 'wx.hideOptionMenu();' . "\r\n\t\t\t" . '}else{' . "\r\n\t\t\t\t" . 'wx.hideOptionMenu();' . "\r\n\t\t\t\t" . '//alert(res.errMsg);' . "\r\n\t\t\t" . '}' . "\r\n\t\t\t" . 'if(res.errMsg){' . "\r\n\t\t\t\t" . 'wx.hideOptionMenu();' . "\r\n\t\t\t" . '}' . "\r\n\t\t\t" . '*/' . "\r\n\t\t" . '});' . "\r\n\r\n\t\t" . '//调用扫一扫' . "\r\n\t\t" . 'scan_qrcode_func = function() {' . "\r\n\t\t\t" . 'var data = \'\';' . "\r\n\t\t\t" . 'wx.scanQRCode({' . "\r\n\t\t\t\t" . 'needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，' . "\r\n\t\t\t\t" . 'scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有' . "\r\n\t\t\t\t" . 'success: function (res) {' . "\r\n\t\t\t\t\t" . 'try {' . "\r\n\t\t\t\t\t" . ' scan_qrcode_callback(res.resultStr); // 当needResult 为 1 时，扫码返回的结果' . "\r\n\t\t\t\t\t" . ' } catch (e) {' . "\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t" . '}' . "\r\n\t\t\t\t" . '}' . "\r\n\t\t\t" . '});' . "\r\n\t\t" . '}' . "\r\n\t\t\r\n\t\t" . 'wx.showOptionMenu();' . "\r\n\t\t\r\n\t" . '});' . "\r\n\t\r\n\t" . '/*' . "\r\n\t" . '*types ：类别' . "\r\n\t" . '*/' . "\r\n\t" . 'function shareHandle(to,types,uid,store_id,data_id) {' . "\r\n\r\n\r\n\t\t" . 'var submitData = {' . "\r\n\t\t\t" . '/*module: window.shareData.moduleName,' . "\r\n\t\t\t" . 'moduleid: window.shareData.moduleID,' . "\r\n\t\t\t" . ' token:\'' . $this->wxuser['token'] . '\',' . "\r\n\t\t\t" . 'wecha_id:\'' . $this->wecha_id . '\', */' . "\r\n\t\t\t" . 'uid:uid,' . "\r\n\t\t\t" . 'store_id:store_id,' . "\r\n\t\t\t" . 'types:types,' . "\r\n\t\t\t" . 'data_id:data_id,' . "\r\n\t\t\t" . '/*url: window.shareData.sendFriendLink,*/' . "\r\n\t\t\t" . 'to:to' . "\r\n\t\t" . '};' . "\r\n\r\n\t\t\t\t\r\n\t\t" . '$.post(\'/wap/share.php\',submitData,function (data) {},\'json\')' . "\r\n\r\n\t\t" . '}' . "\r\n" . '</script>';
		return $html;
	}
	public function formartHtml($string) 
	{
		return addslashes($string);
	}
	protected function https_request($url, $data = NULL) 
	{
		$curl = curl_init();
		$header = array('Accept-Charset: utf-8');
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		if (!empty($data)) 
		{
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		$errorno = curl_errno($curl);
		if ($errorno) 
		{
			return array('curl' => false, 'errorno' => $errorno);
		}
		$res = json_decode($output, 1);
		if ($res['errcode']) 
		{
			return array('errcode' => $res['errcode'], 'errmsg' => $res['errmsg']);
		}
		return $res;
	}
	public function getShareData($params = array()) 
	{
		$params['moduleName'] = (empty($params['moduleName']) ? MODULE_NAME : $params['moduleName']);
		$params['moduleID'] = (empty($params['moduleID']) ? 0 : $params['moduleID']);
		$params['imgUrl'] = (empty($params['imgUrl']) ? '' : $params['imgUrl']);
		if (empty($params['sendFriendLink'])) 
		{
			$params['sendFriendLink'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$parseUrl = parse_url($params['sendFriendLink']);
			parse_str(htmlspecialchars_decode($parseUrl['query']), $query);
			$wecha_id = ((isset($query['wecha_id']) ? $query['wecha_id'] : ''));
			if (1 == count($query)) 
			{
				$wecha_id = '\\?wecha_id=' . $wecha_id;
			}
			else 
			{
				$wecha_id = '&wecha_id=' . $wecha_id . '|' . 'wecha_id=' . $wecha_id . '&';
			}
			$params['sendFriendLink'] = preg_replace('/' . $wecha_id . '/i', '', $params['sendFriendLink']);
		}
		else 
		{
			$params['sendFriendLink'] = stripslashes($params['sendFriendLink']);
		}
		$params['tTitle'] = (empty($params['tTitle']) ? '' : shareFilter($params['tTitle']));
		$params['tContent'] = (empty($params['tContent']) ? $params['tTitle'] : shareFilter($params['tContent']));
		$shareData = str_replace('\\/', '/', json_encode($params));
		$html = "\t\t" . '<script type="text/javascript">' . "\r\n\t\t\t\t" . 'window.shareData = ' . $shareData . ';' . "\r\n\t\t" . '</script>';
		return $html;
	}
}
?>
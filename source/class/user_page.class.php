
<?php
class Page 
{
	public $firstRow;
	public $nowPage;
	public $totalPage;
	public $totalRows;
	public $listRows;
	public function __construct($totalRows, $listRows, $nowPage = '') 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		$this->totalRows = $totalRows;
		$this->nowPage = (!empty($_POST['p']) ? intval($_POST['p']) : 1);
		if (!empty($nowPage)) 
		{
			$this->nowPage = $nowPage;
		}
		$this->listRows = $listRows;
		$this->totalPage = ceil($totalRows / $listRows);
		if (($this->totalPage < $this->nowPage) && (0 < $this->totalPage)) 
		{
			$this->nowPage = $this->totalPage;
		}
		$this->firstRow = $listRows * ($this->nowPage - 1);
	}
	public function show() 
	{
		if ($this->totalRows == 0) 
		{
			return false;
		}
		$now = $this->nowPage;
		$total = $this->totalPage;
		$str = '<span class="total">共 ' . $this->totalRows . ' 条，每页 ' . $this->listRows . ' 条</span> ';
		if ($total == 1) 
		{
			return $str;
		}
		if (1 < $now) 
		{
			$str .= '<a class="prev fetch_page" data-page-num="' . ($now - 1) . '" href="javascript:void(0);">上一页</a>';
		}
		if (($now != 1) && (4 < $now) && (6 < $total)) 
		{
			$str .= ' ... ';
		}
		$i = 1;
		while ($i <= 5) 
		{
			if ($now <= 1) 
			{
				$page = $i;
			}
			else if (($total - 1) < $now) 
			{
				$page = ($total - 5) + $i;
			}
			else 
			{
				$page = ($now - 3) + $i;
			}
			if (($page != $now) && (0 < $page)) 
			{
				if ($page <= $total) 
				{
					$str .= '<a class="fetch_page num" data-page-num="' . $page . '" href="javascript:void(0);">' . $page . '</a>';
				}
				else 
				{
					break;
				}
			}
			else if ($page == $now) 
			{
				$str .= '<a class="num active" data-page-num="' . $page . '" href="javascript:void(0);">' . $page . '</a>';
			}
			++$i;
		}
		if ($now != $total) 
		{
			$str .= '<a class="fetch_page next" data-page-num="' . ($now + 1) . '" href="javascript:void(0);">下一页</a>';
		}
		$str .= ' <input type="text" class="page_input js-page_input" style="width:30px;" onkeyup="this.value=this.value.replace(/\\D/g,\'\')" onafterpaste="this.value=this.value.replace(/\\D/g,\'\')" /> <a href="javascript:void(0)" data-page-num="" class="page_btn js-page_btn">跳转</a>';
		$str .= '<script>$(function () {$(".js-page_input").blur(function () {var page = $(this).val(); $(".js-page_btn").attr("data-page-num", page);})})</script>';
		return $str;
	}
}
?>
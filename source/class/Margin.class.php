
<?php
import('source.class.SendPoints');
class Margin 
{
	static private $_SUPPORT_MARGIN = true;
	static private $_PRE_RECHARGE = false;
	static public $_STORE_ID = 0;
	static private $_CASH_MIN_AMOUNT = 0;
	static private $_CREDIT_DEPOSIT_RATIO = 0;
	static private $_CREDIT_FLOW_CHARGES = 0;
	static private $_STORECREDIT_TO_MONEY_CHARGES = 0;
	static private $_ONLINE_TRADE_MONEY = 0;
	static private $_PLATFORM_CREDIT_RULE = 1;
	static private $_PLATFORM_CREDIT_USE_VALUE = 1;
	static private $_ONLINE_TRADE_CREDIT_TYPE = 0;
	static private $_OFFLINE_TRADE_CREDIT_TYPE = 0;
	static private $_OFFLINE_TRADE_STORE_TYPE = 0;
	static private $_CASH_PROVISIONS = 0;
	static private $_SALES_RATIO = 0;
	static private $_PLATFORM_CREDIT_NAME = '积分';
	static private $_RECHARGE_NOTICE_OPEN = 1;
	static private $_RECHARGE_NOTICE_MAXCOUNT = 1;
	static private $_OPEN_USER_GIVE_POINT = 1;
	static private $_GIVE_POINT_SERVICE_FEE = 0;
	static private $_CREDIT_WEIGHT = 500;
	static private $_MIN_MARGIN_BALANCE = 0;
	static private $_OPEN_PROMOTION_REWARD = 1;
	static private $_PROMOTION_REWARD_RATE = 0;
	static private $_OPEN_MARGIN_WITHDRAWAL = 1;
	static private $_MARGIN_WITHDRAWAL_AMOUNT_MIN = 0;
	static public function init($store_id = 0) 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		if (0 < $store_id) 
		{
			$store_id = M('Store_supplier')->getSupplierId($store_id);
			self::$_STORE_ID = $store_id;
		}
		self::$_CASH_MIN_AMOUNT = option('credit.cash_min_amount');
		self::$_CREDIT_DEPOSIT_RATIO = option('credit.credit_deposit_ratio') / 100;
		self::$_CREDIT_FLOW_CHARGES = option('credit.credit_flow_charges') / 100;
		self::$_STORECREDIT_TO_MONEY_CHARGES = option('credit.storecredit_to_money_charges') / 100;
		self::$_ONLINE_TRADE_MONEY = option('credit.online_trade_money');
		self::$_PLATFORM_CREDIT_RULE = ((0 < option('credit.platform_credit_rule') ? option('credit.platform_credit_rule') : self::$_PLATFORM_CREDIT_RULE));
		self::$_PLATFORM_CREDIT_USE_VALUE = ((0 < option('credit.platform_credit_use_value') ? option('credit.platform_credit_use_value') : self::$_PLATFORM_CREDIT_USE_VALUE));
		self::$_ONLINE_TRADE_CREDIT_TYPE = ((0 < option('credit.online_trade_credit_type') ? option('credit.online_trade_credit_type') : self::$_ONLINE_TRADE_CREDIT_TYPE));
		self::$_OFFLINE_TRADE_CREDIT_TYPE = ((0 < option('credit.offline_trade_credit_type') ? option('credit.offline_trade_credit_type') : self::$_OFFLINE_TRADE_CREDIT_TYPE));
		self::$_OFFLINE_TRADE_STORE_TYPE = ((0 < option('credit.offline_trade_store_type') ? option('credit.offline_trade_store_type') : self::$_OFFLINE_TRADE_STORE_TYPE));
		self::$_CASH_PROVISIONS = option('credit.cash_provisions') / 100;
		self::$_SALES_RATIO = option('config.sales_ratio') / 100;
		self::$_PLATFORM_CREDIT_NAME = option('credit.platform_credit_name');
		self::$_RECHARGE_NOTICE_OPEN = option('credit.recharge_notice_open');
		self::$_RECHARGE_NOTICE_MAXCOUNT = option('credit.recharge_notice_maxcount');
		self::$_OPEN_USER_GIVE_POINT = option('credit.open_user_give_point');
		self::$_GIVE_POINT_SERVICE_FEE = option('credit.give_point_service_fee');
		self::$_CREDIT_WEIGHT = ((0 < option('credit.credit_weight') ? option('credit.credit_weight') : 0));
		self::$_MIN_MARGIN_BALANCE = ((0 < option('credit.min_margin_balance') ? option('credit.min_margin_balance') : 0));
		self::$_OPEN_PROMOTION_REWARD = ((0 < option('credit.open_promotion_reward') ? option('credit.open_promotion_reward') : 0));
		self::$_PROMOTION_REWARD_RATE = option('credit.promotion_reward_rate') / 100;
		self::$_OPEN_MARGIN_WITHDRAWAL = option('credit.open_margin_withdrawal');
		self::$_MARGIN_WITHDRAWAL_AMOUNT_MIN = option('credit.margin_withdrawal_amount_min');
	}
	static public function check() 
	{
		return self::$_SUPPORT_MARGIN = option('credit.platform_credit_open');
	}
	static public function pre_recharge() 
	{
		self::check();
		self::$_PRE_RECHARGE = option('credit.force_use_platform_credit');
		return self::$_SUPPORT_MARGIN && self::$_PRE_RECHARGE;
	}
	static public function setting($key, $value = '') 
	{
		if (empty($key)) 
		{
			return false;
		}
		$key = '_' . strtoupper($key);
		if (empty($value)) 
		{
			if (empty(self::$key)) 
			{
				return false;
				self::$$key = $value;
			}
		}
		else 
		{
			self::$$key = $value;
		}
		return self::$$key;
	}
	static public function check_give_point() 
	{
		self::check();
		self::$_OPEN_USER_GIVE_POINT = option('credit.open_user_give_point');
		return self::$_SUPPORT_MARGIN && self::$_OPEN_USER_GIVE_POINT;
	}
	static public function point_alias() 
	{
		return self::$_PLATFORM_CREDIT_NAME = ((option('credit.platform_credit_name') != '' ? option('credit.platform_credit_name') : self::$_PLATFORM_CREDIT_NAME));
	}
	static public function recharge($order_no, $trade_no, $amount, $payment_method, $status = 1, $bak = '') 
	{
		if (0 < $amount) 
		{
			if (self::log($amount, 1, time(), $status, '', $bak, true, true, $order_no, $trade_no, time(), $payment_method)) 
			{
				M('Common_data')->setMargin($amount);
			}
		}
	}
	static public function orderRate($order_id) 
	{
		$order_products = D('Order_product')->where(array('order_id' => $order_id))->select();
		$sub_total = D('Order_product')->where(array('order_id' => $order_id))->sum('pro_price * pro_num');
		$order_rate = array();
		if (!empty($order_products)) 
		{
			foreach ($order_products as $order_product ) 
			{
				if (empty($order_product['original_product_id']) || ($order_product['product_id'] == $order_product['original_product_id'])) 
				{
					$key = $order_product['product_id'] . '_' . md5($order_product['sku_data']);
				}
				else if (!empty($order_product['original_product_id'])) 
				{
					$key = $order_product['original_product_id'] . '_' . md5($order_product['sku_data']);
				}
				$order_rate[$key] = $order_product['pro_price'] / $sub_total;
				$order_rate[$key] = number_format($order_rate[$key], 2, '.', '');
			}
		}
		return $order_rate;
	}
	static public function orderPoint($order_id, $products = array()) 
	{
		if (empty($order_id) && empty($products)) 
		{
			return 0;
		}
		$return_point = 0;
		if (0 < $order_id) 
		{
			$order = D('Order')->field('order_id,user_order_id')->where(array('order_id' => $order_id))->find();
			if (empty($order)) 
			{
				return 0;
			}
			$order_id = ((!empty($order['user_order_id']) ? $order['user_order_id'] : $order_id));
			$return_point = D('Order_product')->where(array('order_id' => $order_id))->sum('return_point * pro_num');
		}
		else if (!empty($products)) 
		{
			foreach ($products as $product ) 
			{
				$product_info = D('Product')->field('price,return_point')->where(array('product_id' => $product['product_id']))->find();
				if (!empty($product['sku_id']) && ($product_info['return_point'] == 0)) 
				{
					$product_sku = D('Product_sku')->field('price')->where(array('product_id' => $product['product_id'], 'sku_id' => $product['sku_id']))->find();
					if (!empty($product_sku)) 
					{
						$return_point += Margin::convert($product_sku['price'], 'money') * $product['pro_num'];
					}
				}
				else if (!empty($product_info)) 
				{
					if (0 < $product_info['return_point']) 
					{
						$return_point += $product_info['return_point'] * $product['pro_num'];
					}
					else 
					{
						$return_point += Margin::convert($product_info['price'], 'money') * $product['pro_num'];
					}
				}
			}
		}
		return (!empty($return_point) ? $return_point : 0);
	}
	public function productPoint($product_id, $sku_id = 0, $convert_rate = 0) 
	{
		if (empty($product_id)) 
		{
			return 0;
		}
		$return_product_point = 0;
		$product = D('Product')->field('price,open_return_point,return_point')->where(array('product_id' => $product_id))->find();
		if (!empty($product['open_return_point'])) 
		{
			$return_product_point = $product['return_point'];
		}
		else if (!empty($sku_id)) 
		{
			$product_sku = D('Product_sku')->field('price')->where(array('product_id' => $product_id, 'sku_id' => $sku_id))->find();
			$return_product_point = self::convert($product_sku['price'], 'point', $convert_rate);
		}
		else 
		{
			$return_product_point = self::convert($product['price'], 'point', $convert_rate);
		}
		return $return_product_point;
	}
	static public function service_fee($point, $service_fee_rate = 0) 
	{
		if ($point <= 0) 
		{
			return 0;
		}
		if ($service_fee_rate <= 0) 
		{
			$service_fee_rate = self::$_CREDIT_DEPOSIT_RATIO;
		}
		$service_fee = $point * $service_fee_rate;
		return number_format($service_fee, 2, '.', '');
	}
	static public function consume($amount, $type, $bak = '', $status = 2, $order_id = '') 
	{
		if (0 < $amount) 
		{
			if ($type == 1) 
			{
				if ($amount <= self::balance()) 
				{
					self::log(-$amount, $type, time(), $status, $order_id, $bak, false, true);
					return true;
				}
				return false;
			}
			if ($type == 2) 
			{
				$service_fee = 0;
				$order = D('Order')->field('sub_total,total,postage,cash_point,point2money_rate,is_offline,offline_type, return_point')->where(array('order_id' => $order_id))->find();
				$is_offline = ((!empty($order['is_offline']) ? true : false));
				$offline_type = ((!empty($order['offline_type']) ? $order['offline_type'] : 0));
				$order_return_point = 0;
				if (!$is_offline) 
				{
					if (self::$_ONLINE_TRADE_CREDIT_TYPE == 0) 
					{
						$pay_money = $order['total'];
					}
					else 
					{
						$pay_money = $order['total'] - ($order['cash_point'] / $order['point2money_rate']);
					}
				}
				else if (empty($offline_type)) 
				{
					if (self::$_OFFLINE_TRADE_CREDIT_TYPE == 0) 
					{
						$pay_money = $order['total'];
					}
					else 
					{
						$pay_money = $order['total'] - ($order['cash_point'] / $order['point2money_rate']);
					}
				}
				else if (self::$_OFFLINE_TRADE_STORE_TYPE == 0) 
				{
					$pay_money = $order['total'];
				}
				else 
				{
					$pay_money = $order['total'] - ($order['cash_point'] / $order['point2money_rate']);
				}
				$pay_money -= $order['postage'];
				$pay_money = ((0 < $pay_money ? $pay_money : 0));
				$pay_money2point = self::convert($pay_money, 'point');
				$order_products = M('Order_product')->getProducts($order_id);
				foreach ($order_products as $order_product ) 
				{
					if (!empty($order_product['is_present'])) 
					{
						continue;
					}
					$pro_price2point = self::convert($order_product['pro_price'], 'point');
					$return_point = $order_product['return_point'];
					$order_return_point += (($order_product['pro_price'] * $order_product['pro_num']) / $order['sub_total']) * $pay_money2point * (($return_point * $order_product['pro_num']) / ($pro_price2point * $order_product['pro_num']));
				}
				if (($order['is_offline'] == 1) && empty($order_products)) 
				{
					$order_return_point = $order['return_point'];
				}
				$service_fee = self::service_fee($order_return_point);
			}
			if (self::check_balance($service_fee)) 
			{
				self::log(-$service_fee, $type, time(), $status, $order_id, $bak, false, true);
			}
		}
	}
	static public function log($amount, $type, $add_time, $status = 1, $order_id = '', $bak = '', $sync_total = false, $sync_balance = false, $order_no = '', $trade_no = '', $paid_time = '', $payment_method = '') 
	{
		$log = D('Platform_margin_log')->where(array('order_no' => $order_no, 'store_id' => self::$_STORE_ID))->find();
		if (!empty($order_no) && !empty($log)) 
		{
			$data = array();
			$data['status'] = $status;
			if (!empty($trade_no)) 
			{
				$data['trade_no'] = $trade_no;
			}
			if (!empty($paid_time)) 
			{
				$data['paid_time'] = $paid_time;
			}
			if (!empty($payment_method)) 
			{
				$data['payment_method'] = $payment_method;
			}
			if (($log['status'] == 2) && !empty($trade_no)) 
			{
				return $order_no;
			}
			if (D('Platform_margin_log')->where(array('order_no' => $order_no, 'store_id' => self::$_STORE_ID, 'status' => 0))->data($data)->save()) 
			{
				if (!empty($sync_total)) 
				{
					D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('margin_total', $log['amount']);
				}
				if (!empty($sync_balance)) 
				{
					D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('margin_balance', $log['amount']);
				}
				return $order_no;
			}
			return false;
		}
		$store = D('Store')->field('margin_total,margin_balance')->where(array('store_id' => self::$_STORE_ID))->find();
		$order_no = ((!empty($order_no) ? $order_no : date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999)));
		$data = array();
		$data['order_id'] = $order_id;
		$data['store_id'] = self::$_STORE_ID;
		$data['order_no'] = $order_no;
		$data['trade_no'] = $trade_no;
		$data['amount'] = number_format($amount, 2, '.', '');
		$data['payment_method'] = $payment_method;
		$data['type'] = $type;
		$data['add_time'] = $add_time;
		$data['bak'] = $bak;
		$data['status'] = $status;
		$data['paid_time'] = $paid_time;
		$data['margin_total'] = (!empty($store['margin_total']) ? $store['margin_total'] : 0);
		$data['margin_balance'] = (!empty($store['margin_balance']) ? $store['margin_balance'] : 0);
		if (D('Platform_margin_log')->data($data)->add()) 
		{
			if (!empty($sync_total)) 
			{
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('margin_total', $amount);
			}
			if (!empty($sync_balance)) 
			{
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('margin_balance', $amount);
			}
			return $order_no;
		}
		return false;
	}
	static public function user_point_log($uid, $order_id, $store_id, $point, $status, $type, $bak, $channel = 0, $order_no = '', $sync_total = false, $sync_balance = false, $sync_unbalance = false, $sync_order_return_point = false, $sync_given = false, $sync_received = false) 
	{
		if (!empty($order_no)) 
		{
			$data = array();
			$data['status'] = $status;
			if (D('User_point_log')->where(array('order_no' => $order_no, 'uid' => $uid, 'store_id' => $store_id))->data($data)->save()) 
			{
				return $order_no;
			}
			return false;
		}
		$order_no = ((!empty($order_no) ? $order_no : date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999)));
		$user = D('User')->field('point_total,point_balance,point_unbalance')->where(array('uid' => $uid))->find();
		if (empty(self::$_STORE_ID) && !empty($store_id)) 
		{
			self::init($store_id);
		}
		$data = array();
		$data['order_no'] = $order_no;
		$data['uid'] = $uid;
		$data['order_id'] = $order_id;
		$data['store_id'] = $store_id;
		$data['supplier_id'] = self::$_STORE_ID;
		$data['point'] = $point;
		$data['status'] = $status;
		$data['type'] = $type;
		$data['channel'] = $channel;
		$data['bak'] = $bak;
		$data['add_time'] = time();
		$data['add_date'] = date('Ymd');
		$data['point_total'] = $user['point_total'];
		$data['point_balance'] = $user['point_balance'];
		$data['point_unbalance'] = $user['point_unbalance'];
		$data['point_send_base'] = 0;
		if (in_array($type, array(0, 5, 6, 7))) 
		{
			self::$_CREDIT_WEIGHT = ((0 < option('credit.credit_weight') ? option('credit.credit_weight') : 0));
			$tmp_log = D('User_point_log')->field('point_send_base')->where(array( 'uid' => $uid, 'type' => array( 'in', array(0, 5, 6, 7) ) ))->order('pigcms_id DESC')->find();
			$point_unbalance = ((0 < ($user['point_unbalance'] + $point) ? $user['point_unbalance'] + $point : 0));
			$point_send_base = SendPoints::getCardinalNumber($tmp_log['point_send_base'], $point_unbalance, self::$_CREDIT_WEIGHT);
			$data['point_send_base'] = $point_send_base;
		}
		if (D('User_point_log')->data($data)->add()) 
		{
			if (!empty($sync_total)) 
			{
				D('User')->where(array('uid' => $uid))->setInc('point_total', $point);
			}
			if (!empty($sync_balance)) 
			{
				D('User')->where(array('uid' => $uid))->setInc('point_balance', $point);
			}
			if (!empty($sync_unbalance)) 
			{
				D('User')->where(array('uid' => $uid))->setInc('point_unbalance', $point);
			}
			if (!empty($sync_given)) 
			{
				D('User')->where(array('uid' => $uid))->setInc('point_given', abs($point));
			}
			if (!empty($sync_received)) 
			{
				D('User')->where(array('uid' => $uid))->setInc('point_received', abs($point));
			}
			if ($type == 1) 
			{
				$store = D('Store')->field('uid')->where(array('store_id' => self::$_STORE_ID))->find();
				if ($store['uid'] != $uid) 
				{
					self::store_point_log(abs($point), 0, 0, '消费抵现积分', $channel, $order_id);
				}
			}
			else if ($type == 0) 
			{
				$now = date('Ymd');
				$day_point = D('Day_platform_point')->where(array('add_date' => $now))->find();
				if (!empty($day_point)) 
				{
					D('Day_platform_point')->where(array('pigcms_id' => $day_point['pigcms_id']))->setInc('point', abs($point));
				}
				else 
				{
					D('Day_platform_point')->data(array('point' => abs($point), 'add_date' => $now))->add();
				}
			}
			if ($sync_order_return_point && ($type == 0)) 
			{
				D('Order')->where(array('order_id' => $order_id))->data(array('return_point' => $point))->save();
			}
			return $order_no;
		}
		return false;
	}
	static public function store_point_log($point, $status, $type, $bak, $channel = 0, $order_id = '', $order_no = '', $service_fee_rate = 0, $sync_point_total = false, $sync_point_balance = false, $sync_withdrawal = true, $store_id = 0) 
	{
		if (!empty($store_id)) 
		{
			self::$_STORE_ID = $store_id;
		}
		if (($point == 0) || empty(self::$_STORE_ID)) 
		{
			return false;
		}
		$order_no = ((!empty($order_no) ? $order_no : date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999)));
		$store = D('Store')->field('uid,point_total,point_balance,bank_id,opening_bank,bank_card,bank_card_user,withdrawal_type,sales_ratio')->where(array('store_id' => self::$_STORE_ID))->find();
		if ($type == 2) 
		{
			$service_fee_rate = ((!empty($service_fee_rate) ? $service_fee_rate : self::$_STORECREDIT_TO_MONEY_CHARGES));
			$store_point = $point;
			$point = abs($point);
			$exchange_point = $point;
			$service_fee = $point * $service_fee_rate;
			$point -= $service_fee;
			$point2money = $point / self::$_PLATFORM_CREDIT_USE_VALUE;
			$point2money = number_format($point2money, 2, '.', '');
			$service_fee_point = number_format($service_fee, 2, '.', '');
			$service_fee_money = number_format($service_fee / self::$_PLATFORM_CREDIT_USE_VALUE, 2, '.', '');
			$point = $store_point;
		}
		else if ($type == 5) 
		{
			$user_point = abs($point);
		}
		$data = array();
		$data['order_id'] = $order_id;
		$data['store_id'] = self::$_STORE_ID;
		$data['order_no'] = $order_no;
		$data['point'] = $point;
		if (!empty($point2money)) 
		{
			$data['amount'] = $point2money;
		}
		$data['status'] = $status;
		$data['type'] = $type;
		$data['service_fee_rate'] = $service_fee_rate * 100;
		$data['channel'] = $channel;
		$data['bak'] = $bak;
		$data['add_time'] = time();
		$data['point_total'] = $store['point_total'];
		$data['point_balance'] = $store['point_balance'];
		if (D('Store_point_log')->data($data)->add()) 
		{
			if (!empty($sync_point_total)) 
			{
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point_total', $point);
			}
			if (!empty($sync_point_balance)) 
			{
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point_balance', $point);
			}
			if ($type == 2) 
			{
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point2money', $exchange_point);
				if (0 < $service_fee_point) 
				{
					self::platform_point_log($service_fee_point, 1, 1, '积分变现服务费', 0, self::$_STORE_ID, '', true);
				}
				if (!empty($sync_withdrawal) && (0 < $point2money)) 
				{
					$data = array();
					$data['supplier_id'] = 0;
					$data['trade_no'] = date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999);
					$data['uid'] = $store['uid'];
					$data['store_id'] = self::$_STORE_ID;
					$data['bank_id'] = $store['bank_id'];
					$data['opening_bank'] = $store['opening_bank'];
					$data['bank_card'] = $store['bank_card'];
					$data['bank_card_user'] = $store['bank_card_user'];
					$data['withdrawal_type'] = $store['withdrawal_type'];
					$data['status'] = 1;
					$data['add_time'] = time();
					$data['amount'] = $point2money;
					$data['type'] = 3;
					$data['channel'] = 1;
					$tmp_sales_ratio = ((0 < $store['sales_ratio'] ? $store['sales_ratio'] : self::$_SALES_RATIO));
					$data['sales_ratio'] = $tmp_sales_ratio * 100;
					D('Store_withdrawal')->data($data)->add();
				}
				else if (0 < $point2money) 
				{
					D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point2money_balance', $point2money);
				}
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point2money_total', $point2money);
				if (0 < $service_fee_money) 
				{
					D('Store')->where(array('store_id' => self::$_STORE_ID))->setDec('income', $service_fee_money);
					D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point2money_service_fee', $service_fee_money);
				}
			}
			else if ($type == 5) 
			{
				D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('point2user', $user_point);
				self::user_point_log($store['uid'], 0, self::$_STORE_ID, $user_point, 1, $type, '兑换用户积分', 0, '', true, true);
			}
			return $order_no;
		}
		return false;
	}
	static public function platform_point_log($point, $status, $type, $bak, $channel = 0, $store_id = '', $order_no = '', $sync_point_income = false) 
	{
		$order_no = ((!empty($order_no) ? $order_no : date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999)));
		$point_income = M('Common_data')->getData('point_income');
		$data = array();
		$data['store_id'] = (!empty($store_id) ? $store_id : self::$_STORE_ID);
		$data['order_no'] = $order_no;
		$data['point'] = $point;
		$data['status'] = $status;
		$data['type'] = $type;
		$data['channel'] = $channel;
		$data['bak'] = $bak;
		$data['add_time'] = time();
		$data['point_income'] = $point_income;
		if (D('Platform_point_log')->data($data)->add()) 
		{
			if (!empty($sync_point_income)) 
			{
				M('Common_data')->setData('point_income', $point);
			}
		}
	}
	static public function convert($value, $target = 'point', $convert_rate = 0) 
	{
		$target = strtolower($target);
		if ($target == 'point') 
		{
			if (empty($convert_rate)) 
			{
				$convert_rate = self::$_PLATFORM_CREDIT_RULE;
			}
			$value *= $convert_rate;
		}
		else if ($target == 'money') 
		{
			if (empty($convert_rate)) 
			{
				$convert_rate = self::$_PLATFORM_CREDIT_USE_VALUE;
			}
			$value /= $convert_rate;
		}
		return number_format($value, 2, '.', '');
	}
	static public function balance($store_id = '') 
	{
		if (!empty($store_id)) 
		{
			self::init($store_id);
		}
		if (empty(self::$_STORE_ID)) 
		{
			return 0;
		}
		$store = D('Store')->field('margin_balance')->where(array('store_id' => self::$_STORE_ID))->find();
		if (!empty($store)) 
		{
			return $store['margin_balance'];
		}
		return 0;
	}
	static public function check_balance($amount, $notice = true, $store_id = '', $channel = 0) 
	{
		if ($amount <= 0) 
		{
			return true;
		}
		if (!empty($store_id)) 
		{
			self::init($store_id);
		}
		if (empty(self::$_STORE_ID)) 
		{
			return true;
		}
		$margin_balance = self::balance();
		if (empty($channel)) 
		{
			if (($margin_balance < self::$_MIN_MARGIN_BALANCE) && self::pre_recharge()) 
			{
				if ($notice && self::$_RECHARGE_NOTICE_OPEN) 
				{
					self::recharge_notice(self::$_STORE_ID, $margin_balance);
				}
				return false;
			}
		}
		if (($margin_balance < $amount) && self::pre_recharge()) 
		{
			if ($notice && self::$_RECHARGE_NOTICE_OPEN) 
			{
				self::recharge_notice(self::$_STORE_ID, $margin_balance);
			}
			return false;
		}
		return true;
	}
	static public function recharge_notice($store_id, $margin_balance, $recharge_notice_maxcount = 0) 
	{
		if (empty(self::$_RECHARGE_NOTICE_MAXCOUNT)) 
		{
			self::$_RECHARGE_NOTICE_MAXCOUNT = 1;
		}
		if (empty($recharge_notice_maxcount)) 
		{
			$recharge_notice_maxcount = self::$_RECHARGE_NOTICE_MAXCOUNT;
		}
		$now = time();
		$store = D('Store')->field('last_recharge_notice_date,recharge_notice_count')->where(array('store_id' => $store_id))->find();
		if (($store['last_recharge_notice_date'] <= $now - 1800) && ($store['recharge_notice_count'] < $recharge_notice_maxcount)) 
		{
			import('source.class.Notice');
			Notice::bbjNotice($store_id, $margin_balance);
			$data = array();
			if (date('Ymd', $store['last_recharge_notice_date']) < date('Ymd', $now)) 
			{
				$data['recharge_notice_count'] = 1;
			}
			else 
			{
				$data['recharge_notice_count'] = $store['recharge_notice_count'] + 1;
			}
			$data['last_recharge_notice_date'] = $now;
			D('Store')->where(array('store_id' => $store_id))->data($data)->save();
		}
	}
	static public function promotion_reward_log($admin_id, $amount, $status, $type, $reward_rate, $service_fee, $bak, $order_id = 0, $store_id = NULL, $sync_total = false, $sync_balance = false, $sync_platform_total = false) 
	{
		$admin = D('Admin')->field('reward_total,reward_balance')->where(array('id' => $admin_id))->find();
		if (empty($admin)) 
		{
			return false;
		}
		$data = array();
		$data['admin_id'] = $admin_id;
		if (is_array($order_id)) 
		{
			$data['order_offline_id'] = $order_id['order_id'];
		}
		else 
		{
			$data['order_id'] = $order_id;
		}
		$data['amount'] = $amount;
		$data['status'] = $status;
		$data['type'] = $type;
		$data['reward_rate'] = $reward_rate;
		$data['service_fee'] = $service_fee;
		$data['bak'] = $bak;
		$data['store_id'] = (!empty($store_id) ? $store_id : self::$_STORE_ID);
		$data['add_time'] = time();
		$data['reward_total'] = $admin['reward_total'];
		$data['reward_balance'] = $admin['reward_balance'];
		if (D('Promotion_reward_log')->data($data)->add()) 
		{
			if (!empty($sync_total)) 
			{
				D('Admin')->where(array('id' => $admin_id))->setInc('reward_total', $amount);
			}
			if (!empty($sync_balance)) 
			{
				D('Admin')->where(array('id' => $admin_id))->setInc('reward_balance', $amount);
			}
			if (!empty($sync_platform_total)) 
			{
				if (0 < $amount) 
				{
					M('Common_data')->setData('promotion_reward', $amount);
				}
				else if ($amount < 0) 
				{
					M('Common_data')->setData('promotion_reward_send', $amount);
				}
			}
		}
	}
	static public function set_store_point($point, $set_balance = true, $set_total = true, $store_id = '') 
	{
		if (empty($point)) 
		{
			return false;
		}
		$store_id = ((!empty($store_id) ? $store_id : self::$_STORE_ID));
		if ($set_balance) 
		{
			$result = D('Store')->where(array('store_id' => $store_id))->setInc('point_balance', $point);
		}
		if ($set_total) 
		{
			$result = D('Store')->where(array('store_id' => $store_id))->setInc('point_total', $point);
		}
		return $result;
	}
	static public function cash_provision_log($amount, $type, $point, $bak, $order_no = '') 
	{
		if ($amount <= 0) 
		{
			return false;
		}
		if ($type == 2) 
		{
			$amount = $amount * -1;
		}
		$cash_provision_balance = M('Common_data')->getData('cash_provision_balance');
		$order_no = ((!empty($order_no) ? $order_no : date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999)));
		$data = array();
		$data['store_id'] = self::$_STORE_ID;
		$data['order_no'] = $order_no;
		$data['amount'] = $amount;
		$data['point'] = $point;
		$data['type'] = $type;
		$data['bak'] = $bak;
		$data['add_time'] = time();
		$data['add_date'] = date('Ymd');
		$data['cash_provision_balance'] = $cash_provision_balance;
		if (D('Cash_provision_log')->data($data)->add()) 
		{
			M('Common_data')->setData('cash_provision_balance', $amount);
		}
	}
	static public function promotion_reward($service_fee, $order_id, $store_id = 0, $channel = 0, $is_self = 0) 
	{
		$store_id = ((!empty($store_id) ? intval($store_id) : self::$_STORE_ID));
		$reward_total = 0;
		if ((0 < $service_fee) && (0 < $store_id) && (0 < $order_id)) 
		{
			$order = D('Order')->field('store_id')->where(array('order_id' => $order_id))->find();
			$reward_settings = M('Admin_bonus_config')->getConfig();
			$promotion_admins = M('Admin_bonus_config')->getProfitAdmins($order['store_id']);
			if (!empty($reward_settings) && !empty($promotion_admins)) 
			{
				foreach ($promotion_admins as $promotion_admin ) 
				{
					$reward_rate = 0;
					if ($promotion_admin['type'] == 3) 
					{
						$reward_setting = $reward_settings['agent'];
					}
					else if ($promotion_admin['type'] == 2) 
					{
						$reward_setting = $reward_settings['area' . $promotion_admin['area_level']];
					}
					if (!empty($is_self)) 
					{
						if (empty($channel)) 
						{
							$reward_rate = ((!empty($reward_setting['self_online']) ? $reward_setting['self_online'] : 0));
						}
						else 
						{
							$reward_rate = ((!empty($reward_setting['self_offline']) ? $reward_setting['self_offline'] : 0));
						}
					}
					else if (empty($channel)) 
					{
						$reward_rate = ((!empty($reward_setting['platform_online']) ? $reward_setting['platform_online'] : 0));
					}
					else 
					{
						$reward_rate = ((!empty($reward_setting['platform_offline']) ? $reward_setting['platform_offline'] : 0));
					}
					if (empty($reward_rate) || ($reward_rate <= 0)) 
					{
						$reward_rate = self::$_PROMOTION_REWARD_RATE;
					}
					$reward_rate /= 100;
					if ((0 < $service_fee) && (0 < $reward_rate)) 
					{
						$reward_amount = $service_fee * $reward_rate;
						$reward_amount = intval($reward_amount * 100) / 100;
						self::promotion_reward_log($promotion_admin['id'], $reward_amount, 1, 0, $reward_rate * 100, $service_fee, '推广奖励', $order_id, $store_id, true, true, true);
						$reward_total += $reward_amount;
					}
				}
			}
		}
		if (0 < $reward_total) 
		{
			$data = array();
			$data['income'] = number_format(-$reward_total, 2, '.', '');
			$data['add_time'] = time();
			$data['type'] = 4;
			$data['store_id'] = $store_id;
			$data['bak'] = '推广奖励';
			if (D('Platform_income')->data($data)->add()) 
			{
				D('Order')->where(array('order_id' => $order_id))->data(array('promotion_reward' => $reward_total))->save();
			}
		}
		return number_format($reward_total, 2, '.', '');
	}
	static public function promotion_reward_offline($service_fee, $order_id, $store_id = 0, $channel = 0, $is_self = 0) 
	{
		$store_id = ((!empty($store_id) ? intval($store_id) : self::$_STORE_ID));
		$reward_total = 0;
		if ((0 < $service_fee) && (0 < $store_id)) 
		{
			$reward_settings = M('Admin_bonus_config')->getConfig();
			$promotion_admins = M('Admin_bonus_config')->getProfitAdmins($store_id);
			if (!empty($reward_settings) && !empty($promotion_admins)) 
			{
				foreach ($promotion_admins as $promotion_admin ) 
				{
					$reward_rate = 0;
					if ($promotion_admin['type'] == 3) 
					{
						$reward_setting = $reward_settings['agent'];
					}
					else if ($promotion_admin['type'] == 2) 
					{
						$reward_setting = $reward_settings['area' . $promotion_admin['area_level']];
					}
					if (!empty($is_self)) 
					{
						if (empty($channel)) 
						{
							$reward_rate = ((!empty($reward_setting['self_online']) ? $reward_setting['self_online'] : 0));
						}
						else 
						{
							$reward_rate = ((!empty($reward_setting['self_offline']) ? $reward_setting['self_offline'] : 0));
						}
					}
					else if (empty($channel)) 
					{
						$reward_rate = ((!empty($reward_setting['platform_online']) ? $reward_setting['platform_online'] : 0));
					}
					else 
					{
						$reward_rate = ((!empty($reward_setting['platform_offline']) ? $reward_setting['platform_offline'] : 0));
					}
					$reward_rate /= 100;
					if ((0 < $service_fee) && (0 < $reward_rate)) 
					{
						$reward_amount = $service_fee * $reward_rate;
						$reward_amount = intval($reward_amount * 100) / 100;
						self::promotion_reward_log($promotion_admin['id'], $reward_amount, 1, 0, $reward_rate * 100, $service_fee, '店铺手工做单，推广奖励', array('order_id' => $order_id), $store_id, true, true, true);
						$reward_total += $reward_amount;
					}
				}
			}
		}
		if (0 < $reward_total) 
		{
			$data = array();
			$data['income'] = number_format(-$reward_total, 2, '.', '');
			$data['add_time'] = time();
			$data['type'] = 4;
			$data['store_id'] = $store_id;
			$data['bak'] = '推广奖励';
			if (D('Platform_income')->data($data)->add()) 
			{
				D('Order_offline')->where(array('order_id' => $order_id))->data(array('promotion_reward' => $reward_total))->save();
			}
		}
		return number_format($reward_total, 2, '.', '');
	}
	static public function sendPoint($order_id) 
	{
		$order = D('Order')->field('status,is_fx,user_order_id,order_id,store_id,fx_order_id,user_order_id,uid,sub_total,total,cash_point,point2money_rate,is_offline,offline_type,type,postage,pay_money, return_point')->where(array('order_id' => $order_id))->find();
		if (empty($order)) 
		{
			return false;
		}
		if ($order['type'] == 5) 
		{
			$fx_order = D('Fx_order')->field('store_id')->where(array('fx_order_id' => $order['fx_order_id'], 'supplier_id' => self::$_STORE_ID))->find();
			self::$_STORE_ID = $fx_order['store_id'];
		}
		$store = D('Store')->field('wxpay,uid')->where(array('store_id' => self::$_STORE_ID))->find();
		$user_order_id = ((!empty($order['user_order_id']) ? $order['user_order_id'] : $order['order_id']));
		if (!empty($order['user_order_id'])) 
		{
			$user_order = D('Order')->field('status,is_fx,user_order_id,order_id,store_id,fx_order_id,user_order_id,uid,sub_total,total,cash_point,point2money_rate,is_offline,offline_type,type,postage,pay_money')->where(array('order_id' => $user_order_id))->find();
			$tmp_store = D('Store')->field('root_supplier_id')->where(array('store_id' => $user_order['store_id']))->find();
			$supplier_id = $tmp_store['root_supplier_id'];
		}
		else 
		{
			$user_order = $order;
			if (!empty($user_order['fx_order_id'])) 
			{
				$fx_order = D('Fx_order')->field('store_id')->where(array('fx_order_id' => $user_order['fx_order_id']))->find();
				$supplier_id = $fx_order['store_id'];
			}
			else 
			{
				$supplier_id = $user_order['store_id'];
			}
		}
		if (in_array($user_order['type'], array(6, 7))) 
		{
			return false;
		}
		$supplier = D('Store')->field('name,income,balance')->where(array('store_id' => $supplier_id))->find();
		$wholesale_orders = D('Order')->where(array('user_order_id' => $user_order_id, 'type' => 5))->select();
		if (!empty($wholesale_orders)) 
		{
			foreach ($wholesale_orders as $wholesale_order ) 
			{
				$tmp_return_point = D('Return')->where(array('order_id' => $wholesale_order['order_id']))->sum('platform_point');
				if (0 < $tmp_return_point) 
				{
					$tmp_return_amount = $tmp_return_point / $user_order['point2money_rate'];
					D('Financial_record')->where(array('order_id' => $wholesale_order['order_id'], 'type' => 3))->setInc('income', -$tmp_return_amount);
					D('Store')->where(array('store_id' => $wholesale_order['store_id']))->setDec('income', $tmp_return_point);
					D('Supp_dis_relation')->where(array('distributor_id' => self::$_STORE_ID, 'supplier_id' => $wholesale_order['store_id']))->setDec('paid', $tmp_return_amount);
					D('Supp_dis_relation')->where(array('distributor_id' => self::$_STORE_ID, 'supplier_id' => $wholesale_order['store_id']))->setDec('sales', $tmp_return_amount);
					D('Supp_dis_relation')->where(array('distributor_id' => self::$_STORE_ID, 'supplier_id' => $wholesale_order['store_id']))->setInc('return_owe', $tmp_return_amount);
				}
			}
		}
		$uid = $user_order['uid'];
		$is_offline = ((!empty($user_order['is_offline']) ? 1 : 0));
		$offline_type = ((!empty($user_order['offline_type']) ? 1 : 0));
		$cash_point = $user_order['cash_point'];
		$return_total = 0;
		$return_point_total = 0;
		if (empty($is_offline)) 
		{
			$return_price_total = D('Return')->where(array('order_id' => $user_order_id))->sum('product_money');
			$return_postage_total = D('Return')->where(array('order_id' => $user_order_id))->sum('postage_money');
			$return_total = $return_price_total + $return_postage_total;
			$return_total = ((!empty($return_total) ? $return_total : 0));
			$return_point_total = D('Return')->where(array('order_id' => $user_order_id))->sum('platform_point');
			$return_point_total = ((!empty($return_point_total) ? $return_point_total : 0));
		}
		$send_point = true;
		if (D('User_point_log')->where(array('order_id' => $user_order['order_id'], 'uid' => $user_order['uid'], 'status' => 1, 'type' => 0))->count('pigcms_id')) 
		{
			$send_point = false;
			$order_used_point = $cash_point;
		}
		else 
		{
			$order_used_point = $cash_point - $return_point_total;
		}
		if (0 < $order_used_point) 
		{
			$balance = $supplier['balance'] - ($order_used_point / $user_order['point2money_rate']);
			$balance = ((0 < $balance ? $balance : 0));
			$income = $supplier['income'] - ($return_point_total / $user_order['point2money_rate']);
			$income = ((0 < $income ? $income : 0));
			if (0 <= $balance) 
			{
				$data = array();
				$data['balance'] = number_format($balance, 2, '.', '');
				if ($order['type'] != 5) 
				{
					$data['income'] = number_format($income, 2, '.', '');
				}
				D('Store')->where(array('store_id' => $supplier_id))->data($data)->save();
			}
		}
		if (!$send_point) 
		{
			return true;
		}
		$order_total = $user_order['total'] - $return_total;
		$order_products = M('Order_product')->getProducts($user_order_id);
		$shopping_point = 0;
		$return_product_point = 0;
		$return_service_fee = 0;
		if (empty($is_offline)) 
		{
			if (self::$_ONLINE_TRADE_CREDIT_TYPE == 0) 
			{
				$pay_money = $user_order['total'];
			}
			else 
			{
				$pay_money = $user_order['total'] - ($user_order['cash_point'] / $user_order['point2money_rate']);
			}
		}
		else if (empty($offline_type)) 
		{
			if (self::$_OFFLINE_TRADE_CREDIT_TYPE == 0) 
			{
				$pay_money = $user_order['total'];
			}
			else 
			{
				$pay_money = $user_order['total'] - ($user_order['cash_point'] / $user_order['point2money_rate']);
			}
		}
		else if (self::$_OFFLINE_TRADE_STORE_TYPE == 0) 
		{
			$pay_money = $user_order['total'];
		}
		else 
		{
			$pay_money = $user_order['total'] - ($user_order['cash_point'] / $user_order['point2money_rate']);
		}
		$pay_money -= $user_order['postage'];
		$pay_money = ((0 < $pay_money ? $pay_money : 0));
		$pay_money2point = self::convert($pay_money, 'point', $user_order['point2money_rate']);
		foreach ($order_products as $order_product ) 
		{
			if (!empty($order_product['is_present'])) 
			{
				continue;
			}
			$pro_price2point = $order_product['pro_price'] * $user_order['point2money_rate'];
			$return_point = $order_product['return_point'];
			$return_qty = 0;
			if (empty($is_offline)) 
			{
				$return_qty = D('Return_product')->where(array('order_product_id' => $order_product['pigcms_id'], 'order_id' => $user_order_id))->sum('pro_num');
				$return_qty = ((!empty($return_qty) ? $return_qty : 0));
			}
			$pro_num = $order_product['pro_num'] - $return_qty;
			if (0 < $return_qty) 
			{
				$return_product_point += ($order_product['pro_price'] / $user_order['sub_total']) * $pay_money2point * ($return_point / $pro_price2point) * $return_qty;
			}
			if (0 < $pro_num) 
			{
				$shopping_point += ($order_product['pro_price'] / $user_order['sub_total']) * $pay_money2point * ($return_point / $pro_price2point) * $pro_num;
			}
		}
		if (0 < $return_product_point) 
		{
			$return_service_fee = number_format($return_product_point * self::$_CREDIT_DEPOSIT_RATIO, 2, '.', '');
			self::log($return_service_fee, 3, time(), 2, $user_order_id, '商品退货返还服务费', false, true);
		}
		if (0 < $user_order['cash_point']) 
		{
			D('Store_point_log')->where(array('order_id' => $user_order_id, 'store_id' => self::$_STORE_ID, 'type' => 0))->data(array('status' => 1))->save();
			if (0 < $return_point_total) 
			{
				self::store_point_log(-$return_point_total, 1, 3, '退货返还抵现积分', 0, $user_order_id);
			}
			if (empty($offline_type)) 
			{
				$point_trade_fee = number_format($order_used_point * self::$_CREDIT_FLOW_CHARGES, 2, '.', '');
				if (0 < $point_trade_fee) 
				{
					self::store_point_log(-$point_trade_fee, 1, 4, '积分流转服务费', $is_offline, $user_order_id, '', self::$_CREDIT_FLOW_CHARGES);
					self::platform_point_log($point_trade_fee, 1, 0, '积分流转服务费', $is_offline, self::$_STORE_ID, '', true);
				}
				$store_get_point = $order_used_point - $point_trade_fee;
				if (0 < $store_get_point) 
				{
					self::set_store_point($store_get_point);
				}
				D('User_point_log')->where(array('order_id' => $user_order_id, 'uid' => $uid, 'type' => 1))->data(array('status' => 1))->save();
				D('User')->where(array('uid' => $uid))->setInc('point_used', $order_used_point);
			}
			else if ($offline_type == 1) 
			{
				if (0 < $order_used_point) 
				{
					D('Store')->where(array('store_id' => self::$_STORE_ID))->setInc('cash_point', $order_used_point);
				}
			}
			else if ($offline_type == 2) 
			{
				if (0 < $order_used_point) 
				{
					D('User_point_log')->where(array('order_id' => $user_order_id, 'uid' => $store['uid'], 'type' => 1))->data(array('status' => 1))->save();
					D('User')->where(array('uid' => $store['uid']))->setInc('point_used', $order_used_point);
				}
			}
			if ($order_used_point < $user_order['cash_point']) 
			{
				D('Order')->where(array('order_id' => $user_order_id))->data(array('cash_point' => $order_used_point))->save();
			}
		}
		$service_fee = D('Platform_margin_log')->where(array( 'order_id' => $user_order_id, 'store_id' => self::$_STORE_ID, 'type' => array( 'in', array(2, 3) ) ))->sum('amount');
		$service_fee = ((!empty($service_fee) ? abs($service_fee) : 0));
		$service_fee_total = $service_fee;
		if ($service_fee_total) 
		{
			M('Common_data')->setData('margin_used', $service_fee_total);
		}
		if ($order['is_offline'] && empty($order_products)) 
		{
			$shopping_point = $order['return_point'];
		}
		if (0 < $shopping_point) 
		{
			self::user_point_log($uid, $user_order_id, $order['store_id'], $shopping_point, 1, 0, '购物送积分', $is_offline, '', true, false, true, true);
		}
		if ((0 < $cash_point) && (0 < $return_point_total)) 
		{
			$return_point_total = (($cash_point < $return_point_total ? $cash_point : $return_point_total));
			self::user_point_log($uid, $user_order_id, $order['store_id'], $return_point_total, 1, 2, '退货返还积分', $is_offline, '', true, true);
		}
		if (0 < $service_fee_total) 
		{
			$data = array();
			$data['income'] = number_format($service_fee, 2, '.', '');
			$data['add_time'] = time();
			$data['type'] = 3;
			$data['store_id'] = self::$_STORE_ID;
			$data['bak'] = '保证金服务费(含 ' . (self::$_CASH_PROVISIONS * 100) . '% 提现备付金)';
			if (D('Platform_income')->data($data)->add()) 
			{
				$now = date('Ymd');
				$day_service_fee = D('Day_platform_service_fee')->where(array('add_date' => $now))->find();
				if (!empty($day_service_fee)) 
				{
					D('Day_platform_service_fee')->where(array('pigcms_id' => $day_service_fee['pigcms_id']))->setInc('amount', $service_fee);
				}
				else 
				{
					D('Day_platform_service_fee')->data(array('add_date' => $now, 'amount' => $service_fee))->add();
				}
			}
			$cash_provision = $service_fee * self::$_CASH_PROVISIONS;
			$service_fee -= $cash_provision;
			if (0 < $cash_provision) 
			{
				self::cash_provision_log($cash_provision, 0, $shopping_point, '交易完成平台添加提现备付金', $user_order['order_no']);
			}
		}
		$reward_total = 0;
		if ((0 < $service_fee) && !empty(self::$_OPEN_PROMOTION_REWARD)) 
		{
			$reward_total = self::promotion_reward($service_fee_total, $user_order_id, self::$_STORE_ID, $is_offline, $store['wxpay']);
		}
		$net_income = $service_fee - $reward_total;
		if (0 < $net_income) 
		{
			M('Common_data')->setData('trade_net_income', $net_income);
		}
		$service_fee = $service_fee_total - $reward_total;
		if (0 < $service_fee) 
		{
			M('Common_data')->setData('income', $service_fee);
		}
		D('Platform_margin_log')->where(array( 'order_id' => $user_order_id, 'store_id' => self::$_STORE_ID, 'type' => array( 'in', array(2, 3) ) ))->data(array('status' => 2, 'add_time' => time()))->save();
	}
}
?>
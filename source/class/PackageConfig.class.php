
<?php
class PackageConfig 
{
	static public function setDefaultModule() 
	{
		$default_data = array( 1 => array('name' => '店铺概况', 'modules' => 'store'), 2 => array('name' => '页面管理', 'modules' => 'store'), 3 => array('name' => '通用模块', 'modules' => 'store,market'), 4 => array('name' => '店铺服务', 'modules' => 'store'), 5 => array('name' => '店铺设置', 'modules' => 'store,cashier'), 6 => array('name' => '物流配置', 'modules' => 'store,gift,o2o'), 7 => array('name' => '商品管理', 'modules' => 'store,market'), 8 => array('name' => '基础订单管理', 'modules' => 'newChange,marketingActivity,platformScore'), 9 => array('name' => '财务概览', 'modules' => 'platformScore,distribution,agent'), 10 => array('name' => '会员管理', 'modules' => 'store'), 11 => array('name' => '营销活动', 'modules' => 'store,newChange,gift'), 12 => array('name' => '游戏活动', 'modules' => 'marketingActivity'), 13 => array('name' => '店铺绑定微信', 'modules' => 'store'), 14 => array('name' => '分销推广功能', 'modules' => 'distribution,agent,advanceDistribution'), 15 => array('name' => '经销批发功能', 'modules' => 'agent'), 16 => array('name' => '门店管理功能', 'modules' => 'o2o'), 17 => array('name' => '门店订单功能', 'modules' => 'o2o') );
		$default_module = array();
		foreach ($default_data as $k => $v ) 
		{
			if (self::chk_modules_menu($v['modules'])) 
			{
				$default_module[$v['name']] = $k;
			}
		}
		return $default_module;
	}
	static public function all_access() 
	{
		return array( 'store' => array('index', 'wei_page', 'wei_page_category', 'ucenter', 'storenav', 'service', 'business_hours', 'certification'), 'goods' => array('index', 'stockout', 'soldout', 'create', 'category', 'product_comment', 'store_comment', 'subject', 'subtype', 'subject_pinlun', 'subject_diy'), 'case' => array('ad', 'banner', 'page', 'attachment'), 'setting' => array('store', 'config'), 'cashier' => array('index'), 'order' => array('activity', 'order_rights', 'order_return', 'codpay', 'selffetch', 'all', 'dashboard', 'star', 'activity', 'add', 'orderprint', 'check', 'seller_check', 'supplier_check', 'dealer_check'), 'fans' => array('statistic', 'tag', 'store_points', 'points_apply'), 'fx' => array('distribution_index', 'seller_order', 'wholesale_order', 'seller', 'agency', 'my_team', 'distribution', 'supplier_market', 'whole_setting', 'setting', 'degree', 'shop_promotion', 'dividends_setting', 'contact_information', 'wholesale_market', 'my_wholesale', 'my_supplier', 'my_order', 'aptitude_tpl', 'wholesale_degree'), 'wxapp' => array('api'), 'weixin' => array('auth'), 'unitary' => array('unitary_index'), 'bargain' => array('index'), 'seckill' => array('seckill_index'), 'cutprice' => array('cutprice_index'), 'wzc' => array('wzc_index'), 'lottery' => array('lottery_index'), 'lottery_words' => array('words_index'), 'lottery_moneytree' => array('moneytree_index'), 'shakelottery' => array('shakelottery_index'), 'yousetdiscount' => array('yousetdiscount_index'), 'article' => array('index'), 'tuan' => array('tuan_index'), 'appmarket' => array('presale', 'present'), 'peerpay' => array('peerpay_index'), 'preferential' => array('coupon'), 'reward' => array('reward_index'), 'substore' => array('store_list', 'set_admin', 'set_stock', 'warn_stock', 'statistic', 'order', 'courier', 'logistic_config', 'delivery', 'wxbind'), 'trade' => array('income', 'delivery'), 'offline' => array('offline_index', 'offline_list') );
	}
	static public function setDefaultModuleRbacConfig() 
	{
		$default_module_rbac_config = array( 1 => array( 'c' => 'store', 'a' => array('index') ), 5 => array( 'c' => 'setting', 'a' => array('store') ), 6 => array( 'c' => 'setting', 'a' => array('config') ), 7 => array( 'c' => 'goods', 'a' => array('index', 'create', 'stockout', 'soldout') ), 8 => array( 'c' => 'mix', 'a' => array('c=trade&a=delivery', 'c=order&a=order_rights', 'c=order&a=order_return', 'c=order&a=codpay', 'c=order&a=selffetch', 'c=order&a=all', 'c=order&a=dashboard') ), 13 => array( 'c' => 'weixin', 'a' => array('auth') ), 704 => array( 'c' => 'goods', 'a' => array('subject', 'subtype', 'subject_pinlun', 'subject_diy') ), 1108 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=bargain') ), 1109 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=seckill') ), 1110 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=crowdfunding') ), 1117 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=unitary') ), 1118 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=cutprice') ), 1119 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=red_packet') ), 1201 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=lottery') ), 1202 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=guajiang') ), 1203 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=jiugong') ), 1204 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=luckyFruit') ), 1205 => array( 'c' => 'mix', 'a' => array('c=wxapp&a=api&act=goldenEgg') ) );
		return $default_module_rbac_config;
	}
	static public function getRbacId($m, $c, $a) 
	{
		$_config = self::setRbacConfig($m);
		foreach ($_config as $key => $value ) 
		{
			if (($value['c'] == $c) && ($value['a'] == $a)) 
			{
				return $key;
			}
		}
	}
	static public function getRbacIdByArr($rbac_result, $c, $a) 
	{
		$default_module_rbac_config = self::setDefaultModuleRbacConfig();
		foreach ($default_module_rbac_config as $key => $value ) 
		{
			if ($value['c'] == 'mix') 
			{
				$search_str = 'c=' . $c . '&a=' . $a;
				if (!empty($_GET['act'])) 
				{
					$search_str .= '&act=' . trim($_GET['act']);
				}
				if (in_array($search_str, $default_module_rbac_config[$key]['a'])) 
				{
					return $key;
					if (($value['c'] == $c) && in_array($a, $default_module_rbac_config[$key]['a'])) 
					{
						return $key;
					}
				}
			}
			else 
			{
				return $key;
			}
		}
		$rbac_config = self::setRbacConfig();
		$search_keys = self::array_keys_redefine($rbac_config);
		$rbac_result = array_intersect($search_keys, $rbac_result);
		foreach ($rbac_result as $key => $value ) 
		{
			$rbac_id = self::getRbacId($value, $c, $a);
			if (empty($rbac_id)) 
			{
				continue;
			}
			return $rbac_id;
		}
	}
	static public function setDefaultLink($module_id = array(), $rbac_val) 
	{
		if (count($module_id) != 2) 
		{
			return '';
		}
		$rbacconfig = self::setRbacConfig($module_id[0]);
		$rbacconfig_1 = self::setRbacConfig($module_id[1]);
		$link_arr = array_merge(self::array_keys_redefine($rbacconfig), self::array_keys_redefine($rbacconfig_1));
		$link_arr = array_intersect($rbac_val, $link_arr);
		sort($link_arr);
		if (!empty($link_arr)) 
		{
			if ($rbacconfig[$link_arr[0]]['c']) 
			{
				$c = $rbacconfig[$link_arr[0]]['c'];
				$a = $rbacconfig[$link_arr[0]]['a'];
			}
			else 
			{
				$c = $rbacconfig_1[$link_arr[0]]['c'];
				$a = $rbacconfig_1[$link_arr[0]]['a'];
			}
			return dourl($c . ':' . $a);
		}
		return '';
	}
	static public function setRbacConfig($m = '') 
	{
		$rbac_config = array( 1 => array( 101 => array('name' => '店铺概况', 'c' => 'store', 'a' => 'index', 'modules' => 'store', 'is_show' => 1) ), 2 => array( 201 => array('name' => '微页面', 'c' => 'store', 'a' => 'wei_page', 'modules' => 'store', 'is_show' => 1), 202 => array('name' => '微页面分类', 'c' => 'store', 'a' => 'wei_page_category', 'modules' => 'store', 'is_show' => 1), 203 => array('name' => '会员主页', 'c' => 'store', 'a' => 'ucenter', 'modules' => 'store', 'is_show' => 1), 204 => array('name' => '底部菜单导航', 'c' => 'store', 'a' => 'storenav', 'modules' => 'store', 'is_show' => 1) ), 3 => array( 301 => array('name' => '公告广告设置', 'c' => 'case', 'a' => 'ad', 'modules' => 'store', 'is_show' => 1), 302 => array('name' => '店铺推广管理', 'c' => 'case', 'a' => 'banner', 'modules' => 'market', 'is_show' => 1), 303 => array('name' => '自定义页面模块', 'c' => 'case', 'a' => 'page', 'modules' => 'store', 'is_show' => 1), 304 => array('name' => '我的文件', 'c' => 'case', 'a' => 'attachment', 'modules' => 'store', 'is_show' => 1), 305 => array('name' => '店铺动态', 'c' => 'article', 'a' => 'index', 'modules' => 'store,market', 'is_show' => 1) ), 4 => array( 401 => array('name' => '客服列表', 'c' => 'store', 'a' => 'service', 'modules' => 'store', 'is_show' => 1), 403 => array('name' => '店铺认证', 'c' => 'store', 'a' => 'certification', 'modules' => 'store', 'is_show' => 1) ), 5 => array( 501 => array('name' => '店铺信息', 'c' => 'setting', 'a' => 'store#info', 'modules' => 'store', 'is_show' => 1), 502 => array('name' => '联系我们', 'c' => 'setting', 'a' => 'store#contact', 'modules' => 'store', 'is_show' => 1), 503 => array('name' => '消息/通知管理', 'c' => 'setting', 'a' => 'store#notice_list', 'modules' => 'store', 'is_show' => 1), 504 => array('name' => '独立收银台', 'c' => 'cashier', 'a' => 'index', 'modules' => 'cashier', 'is_show' => 1), 505 => array('name' => '微信绑定', 'c' => 'store', 'a' => 'wxbind', 'modules' => 'store', 'is_show' => 1) ), 6 => array( 601 => array('name' => '上门自提', 'c' => 'setting', 'a' => 'config#selffetch', 'modules' => 'store', 'is_show' => 1), 602 => array('name' => '送朋友', 'c' => 'setting', 'a' => 'config#friend', 'modules' => 'gift', 'is_show' => 1), 603 => array('name' => '货到付款', 'c' => 'setting', 'a' => 'config#offline_payment', 'modules' => 'store', 'is_show' => 1), 604 => array('name' => '开启本地物流', 'c' => 'setting', 'a' => 'config#local_logistic', 'modules' => 'o2o', 'is_show' => 1) ), 7 => array( 701 => array('name' => '商品分组', 'c' => 'goods', 'a' => 'category', 'modules' => 'store', 'is_show' => 1), 702 => array('name' => '商品评价', 'c' => 'goods', 'a' => 'product_comment', 'modules' => 'store', 'is_show' => 1), 703 => array('name' => '店铺评价', 'c' => 'goods', 'a' => 'store_comment', 'modules' => 'market', 'is_show' => 1), 704 => array('name' => '特色导购功能', 'c' => 'goods', 'a' => 'subject', 'modules' => 'store', 'is_show' => 1), 705 => array('name' => '商品导出功能', 'c' => 'goods', 'a' => 'dump', 'modules' => 'store', 'is_show' => 1), 706 => array('name' => '商品推广功能', 'c' => 'goods', 'a' => 'promotion', 'modules' => 'store', 'is_show' => 1) ), 8 => array( 801 => array('name' => '加星订单', 'c' => 'order', 'a' => 'star', 'modules' => 'store', 'is_show' => 1), 802 => array('name' => '活动订单', 'c' => 'order', 'a' => 'activity', 'modules' => 'newChange,marketingActivity', 'is_show' => 1), 803 => array('name' => '添加订单', 'c' => 'order', 'a' => 'add', 'modules' => 'store', 'is_show' => 1), 804 => array('name' => '订单打印机', 'c' => 'order', 'a' => 'orderprint', 'modules' => 'store', 'is_show' => 1), 805 => array('name' => '商家做单管理', 'c' => 'offline', 'a' => 'offline_list', 'modules' => 'platformScore', 'is_show' => 1), 806 => array('name' => '商家线下做单', 'c' => 'offline', 'a' => 'offline_index', 'modules' => 'platformScore', 'is_show' => 1) ), 9 => array( 901 => array('name' => '平台充值', 'c' => 'trade', 'a' => 'income', 'modules' => 'platformScore', 'is_show' => 1), 902 => array('name' => '平台对账', 'c' => 'order', 'a' => 'check', 'modules' => 'distribution', 'is_show' => 1), 903 => array('name' => '分销商对账', 'c' => 'order', 'a' => 'seller_check', 'modules' => 'distribution', 'is_show' => 1), 904 => array('name' => '供货商对账', 'c' => 'order', 'a' => 'supplier_check', 'modules' => 'agent', 'is_show' => 1), 905 => array('name' => '经销商对账', 'c' => 'order', 'a' => 'dealer_check', 'modules' => 'agent', 'is_show' => 1) ), 10 => array( 1001 => array('name' => '会员数据', 'c' => 'fans', 'a' => 'statistic', 'modules' => 'store', 'is_show' => 1), 1002 => array('name' => '会员等级', 'c' => 'fans', 'a' => 'tag', 'modules' => 'store', 'is_show' => 1), 1003 => array('name' => '积分规则', 'c' => 'fans', 'a' => 'store_points', 'modules' => 'store', 'is_show' => 1), 1004 => array('name' => '积分使用流水', 'c' => 'fans', 'a' => 'points_apply', 'modules' => 'store', 'is_show' => 1) ), 11 => array( 1101 => array('name' => '赠品', 'c' => 'appmarket', 'a' => 'present', 'modules' => 'store', 'is_show' => 1), 1102 => array('name' => '满减/送', 'c' => 'reward', 'a' => 'reward_index', 'modules' => 'store', 'is_show' => 1), 1103 => array('name' => '优惠券', 'c' => 'preferential', 'a' => 'coupon', 'modules' => 'store', 'is_show' => 1), 1104 => array('name' => '找人代付', 'c' => 'peerpay', 'a' => 'peerpay_index', 'modules' => 'gift', 'is_show' => 1), 1105 => array('name' => '预售', 'c' => 'appmarket', 'a' => 'presale', 'modules' => 'newChange', 'is_show' => 1), 1106 => array('name' => '拼团活动', 'c' => 'tuan', 'a' => 'tuan_index', 'modules' => 'newChange', 'is_show' => 1), 1107 => array('name' => '一元夺宝(新)', 'c' => 'unitary', 'a' => 'unitary_index', 'modules' => 'newChange', 'is_show' => 1), 1111 => array('name' => '砍价(新)', 'c' => 'bargain', 'a' => 'index', 'modules' => 'newChange', 'is_show' => 1), 1112 => array('name' => '秒杀(新)', 'c' => 'seckill', 'a' => 'seckill_index', 'modules' => 'newChange', 'is_show' => 1), 1113 => array('name' => '降价拍(新)', 'c' => 'cutprice', 'a' => 'cutprice_index', 'modules' => 'newChange', 'is_show' => 1), 1114 => array('name' => '微众筹(新)', 'c' => 'wzc', 'a' => 'wzc_index', 'modules' => 'newChange', 'is_show' => 1), 1116 => array('name' => '优惠接力', 'c' => 'yousetdiscount', 'a' => 'yousetdiscount_index', 'modules' => 'newChange', 'is_show' => 1), 1117 => array('name' => '微聚力', 'c' => 'helping', 'a' => 'helping_index', 'modules' => 'newChange', 'is_show' => 1) ), 12 => array( 1201 => array('name' => '抽奖活动合集', 'c' => 'lottery', 'a' => 'lottery_index', 'modules' => 'marketingActivity', 'is_show' => 1), 1202 => array('name' => '集字游戏', 'c' => 'lottery_words', 'a' => 'words_index', 'modules' => 'marketingActivity', 'is_show' => 1), 1203 => array('name' => '摇钱树', 'c' => 'lottery_moneytree', 'a' => 'moneytree_index', 'modules' => 'marketingActivity', 'is_show' => 1), 1204 => array('name' => '摇一摇抽奖', 'c' => 'shakelottery', 'a' => 'shakelottery_index', 'modules' => 'marketingActivity', 'is_show' => 1) ), 13 => array( 1301 => array('name' => '店铺绑定微信', 'c' => 'weixin', 'a' => 'auth', 'modules' => 'store', 'is_show' => 1) ), 14 => array( 1401 => array('name' => '分销配置', 'c' => 'fx', 'a' => 'setting', 'modules' => 'distribution', 'is_show' => 1), 1402 => array('name' => '分销统计', 'c' => 'fx', 'a' => 'distribution_index', 'modules' => 'distribution', 'is_show' => 1), 1403 => array('name' => '分销商订单', 'c' => 'fx', 'a' => 'seller_order', 'modules' => 'distribution', 'is_show' => 1), 1404 => array('name' => '经销商订单', 'c' => 'fx', 'a' => 'wholesale_order', 'modules' => 'agent', 'is_show' => 1), 1405 => array('name' => '我的分销商', 'c' => 'fx', 'a' => 'seller', 'modules' => 'distribution', 'is_show' => 1), 1406 => array('name' => '我的经销商', 'c' => 'fx', 'a' => 'agency', 'modules' => 'agent', 'is_show' => 1), 1407 => array('name' => '我的团队', 'c' => 'fx', 'a' => 'my_team', 'modules' => 'advanceDistribution', 'is_show' => 1), 1408 => array('name' => '分销排名', 'c' => 'fx', 'a' => 'distribution', 'modules' => 'distribution', 'is_show' => 1), 1409 => array('name' => '本店商品(设置批发分销)', 'c' => 'fx', 'a' => 'supplier_market', 'modules' => 'distribution,agent', 'is_show' => 1), 1410 => array('name' => '分销等级', 'c' => 'fx', 'a' => 'degree', 'modules' => 'distribution', 'is_show' => 1), 1411 => array('name' => '推广海报设置', 'c' => 'fx', 'a' => 'shop_promotion', 'modules' => 'distribution', 'is_show' => 1), 1412 => array('name' => '推广奖金设置', 'c' => 'fx', 'a' => 'dividends_setting', 'modules' => 'advanceDistribution', 'is_show' => 1), 1413 => array('name' => '联系我们', 'c' => 'fx', 'a' => 'contact_information', 'modules' => 'distribution', 'is_show' => 1), 1414 => array('name' => '资质模板', 'c' => 'fx', 'a' => 'aptitude_tpl', 'modules' => 'distribution,agent', 'is_show' => 1) ), 15 => array( 1501 => array('name' => '批发设置', 'c' => 'fx', 'a' => 'whole_setting', 'modules' => 'agent', 'is_show' => 1), 1502 => array('name' => '批发市场', 'c' => 'fx', 'a' => 'wholesale_market', 'modules' => 'agent', 'is_show' => 1), 1503 => array('name' => '我卖的商品', 'c' => 'fx', 'a' => 'my_wholesale', 'modules' => 'agent', 'is_show' => 1), 1504 => array('name' => '我的供货商', 'c' => 'fx', 'a' => 'my_supplier', 'modules' => 'agent', 'is_show' => 1), 1505 => array('name' => '我的订单', 'c' => 'fx', 'a' => 'my_order', 'modules' => 'agent', 'is_show' => 1) ), 16 => array( 1601 => array('name' => '管理门店(新建)', 'c' => 'substore', 'a' => 'store_list', 'modules' => 'o2o', 'is_show' => 1), 1602 => array('name' => '管理门店管理员', 'c' => 'substore', 'a' => 'set_admin', 'modules' => 'o2o', 'is_show' => 1), 1603 => array('name' => '发布商品', 'c' => 'goods', 'a' => 'create', 'modules' => 'o2o', 'is_show' => 1), 1604 => array('name' => '商品库存', 'c' => 'substore', 'a' => 'set_stock', 'modules' => 'o2o', 'is_show' => 1), 1605 => array('name' => '库存报警', 'c' => 'substore', 'a' => 'warn_stock', 'modules' => 'o2o', 'is_show' => 1), 1606 => array('name' => '销售统计', 'c' => 'substore', 'a' => 'statistic', 'modules' => 'o2o', 'is_show' => 1) ), 17 => array( 1701 => array('name' => '门店订单管理', 'c' => 'substore', 'a' => 'order', 'modules' => 'o2o', 'is_show' => 1), 1702 => array('name' => '配送员管理', 'c' => 'substore', 'a' => 'courier', 'modules' => 'o2o', 'is_show' => 1), 1703 => array('name' => '物流配置', 'c' => 'substore', 'a' => 'logistic_config', 'modules' => 'o2o', 'is_show' => 1), 1704 => array('name' => '物流工具', 'c' => 'substore', 'a' => 'delivery', 'modules' => 'o2o', 'is_show' => 1) ) );
		$rbac_config_new = array();
		foreach ($rbac_config as $key => $value ) 
		{
			foreach ($rbac_config[$key] as $k => $v ) 
			{
				if (self::chk_modules_menu($v['modules'])) 
				{
					$rbac_config_new[$key][$k] = $v;
				}
			}
		}
		return (empty($m) ? $rbac_config_new : $rbac_config_new[$m]);
	}
	public function chk_offline_access($c, $a) 
	{
		return self::getRbacId(8, $c, $a);
	}
	public function chk_fx_setting($c, $a, $menu) 
	{
		if (in_array(self::getRbacId(14, $c, $a), $menu)) 
		{
			return true;
		}
		return false;
	}
	private function chk_modules_menu($str) 
	{
		if (strpos($str, ',') !== false) 
		{
			$tmp_str_arr = explode(',', $str);
			foreach ($tmp_str_arr as $key => $value ) 
			{
				if (!(strpos(MODUELS, $value) !== false)) 
				{
					continue;
				}
				return true;
			}
			return false;
		}
		if (strpos(MODUELS, $str . ',') !== false) 
		{
			return true;
		}
		return false;
	}
	private function array_keys_redefine($value) 
	{
		return (empty($value) ? array() : array_keys($value));
	}
}
?>
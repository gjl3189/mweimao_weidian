
<?php
class orderPrint 
{
	public $serverUrl;
	public $key;
	public $topdomain;
	public $token;
	public function __construct() 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		if (function_exists('option')) 
		{
			$this->key = option('config.service_key');
		}
		else 
		{
			$this->key = C('config.service_key');
		}
		$this->serverUrl = 'http://up.pigcms.cn/';
		$this->topdomain = $this->getTopDomain();
		$this->token = $token;
	}
	public function printit($store_id, $companyid = 0, $ordertype = '', $content = '', $paid = 0, $qr = '', $number = 0) 
	{
		$companyid = intval($companyid);
		$printers = D('Store_orderprint_machine')->where(array('store_id' => $store_id, 'is_open' => 1))->select();
		$usePrinters = array();
		$usePrinters = $printers;
		if ($paid == '2') 
		{
			$paid = 0;
		}
		if ($usePrinters) 
		{
			foreach ($usePrinters as $p ) 
			{
				if ($p['type'] == '1') 
				{
					if ($paid == '0') 
					{
						continue;
					}
				}
				if (($p['mobile'] == '') || ($p['mobile'] != '')) 
				{
					$data = array('content' => $content, 'machine_code' => $p['terminal_number'], 'machine_key' => $p['keys']);
					$url = $this->serverUrl . 'server.php?m=server&c=orderPrint&a=printit&count=' . $p['counts'] . '&key=' . $this->key . '&domain=' . $this->topdomain;
					$rt[$p['terminal_number']] = $this->api_notice_increment($url, $data);
				}
				else if ($p['username'] != '') 
				{
					$content = str_replace('*******************************', '************************', $content);
					$content = str_replace('※※※※※※※※※※※※※※※※', '※※※※※※※※※※※※', $content);
					$data = array('content' => $content);
					$url = $this->serverUrl . 'server.php?m=server&c=orderPrint&a=fcprintit&count=' . $p['counts'] . '&mkey=' . $p['keys'] . '&mcode=' . $p['terminal_number'] . '&name=' . $p['username'] . '&domain=' . $this->topdomain;
					if (!empty($qr)) 
					{
						$url = $url . '&qr=' . urlencode($qr);
					}
					$rt[$p['terminal_number']] = $this->api_notice_increment($url, $data);
				}
			}
			return $rt;
		}
	}
	public function api_notice_increment($url, $data) 
	{
		$ch = curl_init();
		$header[] = 'Accept-Charset: utf-8';
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$tmpInfo = curl_exec($ch);
		$errorno = curl_errno($ch);
		if ($errorno) 
		{
			return $errorno;
		}
		return $tmpInfo;
	}
	public function getTopDomain() 
	{
		$host = $_SERVER['HTTP_HOST'];
		$host = strtolower($host);
		if (strpos($host, '/') !== false) 
		{
			$parse = @parse_url($host);
			$host = $parse['host'];
		}
		$topleveldomaindb = array('com', 'edu', 'gov', 'int', 'mil', 'net', 'org', 'biz', 'info', 'pro', 'name', 'museum', 'coop', 'aero', 'xxx', 'idv', 'mobi', 'cc', 'me');
		$str = '';
		foreach ($topleveldomaindb as $v ) 
		{
			$str .= (($str ? '|' : '')) . $v;
		}
		$matchstr = '[^\\.]+\\.(?:(' . $str . ')|\\w{2}|((' . $str . ')\\.\\w{2}))$';
		if (preg_match('/' . $matchstr . '/ies', $host, $matchs)) 
		{
			$domain = $matchs[0];
		}
		else 
		{
			$domain = $host;
		}
		return $domain;
	}
	protected function https_request($url, $data = NULL) 
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		if (!empty($data)) 
		{
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		curl_close($curl);
		return $output;
	}
}
?>
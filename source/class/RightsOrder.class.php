
<?php
class RightsOrder 
{
	static public function apply($order, $apply_data) 
	{
		if (empty($order) || empty($apply_data)) 
		{
			return false;
		}
		$order_product = D('')->table('Order_product as op')->join('Order AS o ON o.order_id = op.order_id', 'left')->where('op.pigcms_id = \'' . $apply_data['pigcms_id'] . '\'')->field('*, op.pro_num as pro_number')->find();
		if (empty($order_product)) 
		{
			return false;
		}
		$product = D('Product')->where('product_id = \'' . $order_product['product_id'] . '\'')->find();
		if (empty($product)) 
		{
			return false;
		}
		$data = array();
		$data['dateline'] = time();
		$data['order_id'] = $order['order_id'];
		$data['order_no'] = $order['order_no'];
		$data['type'] = $apply_data['type'];
		$data['phone'] = $apply_data['phone'];
		$data['content'] = $apply_data['content'];
		if (!empty($apply_data['images'])) 
		{
			foreach ($apply_data['images'] as &$image ) 
			{
				$image = getAttachment($image);
			}
			$data['images'] = serialize($apply_data['images']);
		}
		$data['status'] = 1;
		$data['store_id'] = $order['store_id'];
		$data['uid'] = $order['uid'];
		$data['is_fx'] = $order_product['is_fx'];
		if ($product['wholesale_product_id'] == 0) 
		{
			$data['is_fx'] = 0;
		}
		else if (($product['wholesale_product_id'] != 0) && ($product['store_id'] == $order_product['store_id'])) 
		{
			$data['is_fx'] = 2;
		}
		else 
		{
			$data['is_fx'] = 1;
		}
		$data['user_rights_id'] = 0;
		$result = D('Rights')->data($data)->add();
		if (!$result) 
		{
			return false;
		}
		$number = M('Rights_product')->rightsNumber($order['order_id'], $apply_data['pigcms_id']);
		$return_product_data = array();
		$return_product_data['order_id'] = $order['order_id'];
		$return_product_data['order_no'] = $order['order_no'];
		$return_product_data['rights_id'] = $result;
		$return_product_data['order_product_id'] = $apply_data['pigcms_id'];
		$return_product_data['product_id'] = $order_product['product_id'];
		$return_product_data['sku_id'] = $order_product['sku_id'];
		$return_product_data['sku_data'] = $order_product['sku_data'];
		$return_product_data['pro_num'] = $apply_data['number'];
		$return_product_data['pro_price'] = $order_product['pro_price'];
		$return_product_data['supplier_id'] = $order_product['supplier_id'];
		$return_product_data['original_product_id'] = $order_product['original_product_id'];
		$return_product_data['user_rights_id'] = $result;
		D('Rights_product')->data($return_product_data)->add();
		if ($order_product['pro_number'] <= $number + $apply_data['number']) 
		{
			D('Order_product')->where(array('pigcms_id' => $apply_data['pigcms_id']))->data(array('rights_status' => 2))->save();
		}
		else 
		{
			D('Order_product')->where(array('pigcms_id' => $apply_data['pigcms_id']))->data(array('rights_status' => 1))->save();
		}
		$order_list = D('')->table('Order AS o')->join('Order_product AS op ON o.order_id = op.order_id', 'LEFT')->where('o.user_order_id = \'' . $order_product['order_id'] . '\' AND (op.product_id = \'' . $product['product_id'] . '\' OR op.product_id = \'' . $product['wholesale_product_id'] . '\') AND op.sku_data = \'' . $order_product['sku_data'] . '\'')->select();
		if (is_array($order_list)) 
		{
			foreach ($order_list as $tmp_order ) 
			{
				if ($order_product['pro_number'] <= $number + $apply_data['number']) 
				{
					D('Order_product')->where(array('pigcms_id' => $tmp_order['pigcms_id']))->data(array('rights_status' => 2))->save();
				}
				else 
				{
					D('Order_product')->where(array('pigcms_id' => $tmp_order['pigcms_id']))->data(array('rights_status' => 1))->save();
				}
				$data['order_id'] = $tmp_order['order_id'];
				$data['order_no'] = $tmp_order['order_no'];
				$data['store_id'] = $tmp_order['store_id'];
				$product = D('Product')->where(array('product_id' => $tmp_order['product_id']))->find();
				if (empty($product)) 
				{
					return false;
				}
				if ($product['wholesale_product_id'] == 0) 
				{
					$data['is_fx'] = 0;
				}
				else if (($product['wholesale_product_id'] != 0) && ($product['store_id'] == $tmp_order['store_id'])) 
				{
					$data['is_fx'] = 2;
				}
				else 
				{
					$data['is_fx'] = 1;
				}
				$data['user_rights_id'] = $result;
				$drp_rights_id = D('Rights')->data($data)->add();
				if (!$drp_rights_id) 
				{
					return false;
				}
				$return_product_data['order_id'] = $tmp_order['order_id'];
				$return_product_data['rights_id'] = $drp_rights_id;
				$return_product_data['order_product_id'] = $tmp_order['pigcms_id'];
				$return_product_data['product_id'] = $tmp_order['product_id'];
				$return_product_data['pro_price'] = $tmp_order['pro_price'];
				D('Rights_product')->data($return_product_data)->add();
			}
		}
		return $result;
		while ($i <= 3) 
		{
			$drp_store = D('Store')->where(array('store_id' => $drp_supplier_id))->find();
			$order_tmp = D('')->table('Order AS o')->join('Order_product AS op ON o.order_id = op.order_id', 'left')->where('(o.order_id = \'' . $order['order_id'] . '\' or o.user_order_id = \'' . $order['order_id'] . '\') and o.store_id = \'' . $drp_supplier_id . '\' AND (op.original_product_id = \'' . $order_product['original_product_id'] . '\' OR op.original_product_id = 0) AND op.sku_data = \'' . $order_product['sku_data'] . '\'')->find();
			if ($order_product['pro_num'] <= $number + $apply_data['number']) 
			{
				D('Order_product')->where(array('pigcms_id' => $order_tmp['pigcms_id']))->data(array('rights_status' => 2))->save();
			}
			else 
			{
				D('Order_product')->where(array('pigcms_id' => $order_tmp['pigcms_id']))->data(array('rights_status' => 1))->save();
			}
			$data['order_id'] = $order_tmp['order_id'];
			$data['order_no'] = $order_tmp['order_no'];
			$data['store_id'] = $drp_store['store_id'];
			if (empty($drp_store['drp_supplier_id']) || ($drp_store['drp_supplier_id'] == $drp_store['store_id'])) 
			{
				$data['is_fx'] = 0;
			}
			else 
			{
				$data['is_fx'] = 1;
			}
			$data['user_rights_id'] = $result;
			$drp_return_id = D('Rights')->data($data)->add();
			if (!$drp_return_id) 
			{
				return false;
			}
			$return_product_data['order_id'] = $order_tmp['order_id'];
			$return_product_data['order_no'] = $order_tmp['order_no'];
			$return_product_data['rights_id'] = $drp_return_id;
			$return_product_data['order_product_id'] = $order_tmp['pigcms_id'];
			$return_product_data['product_id'] = $order_tmp['product_id'];
			$return_product_data['pro_price'] = $order_tmp['pro_price'];
			D('Rights_product')->data($return_product_data)->add();
			if (empty($drp_store['drp_supplier_id']) || ($drp_store['drp_supplier_id'] == $drp_store['store_id'])) 
			{
				break;
			}
			$drp_supplier_id = $drp_store['drp_supplier_id'];
			if ($i == 2) 
			{
				$product = D('Product')->where(array('product_id' => $order_product['original_product_id']))->find();
				if (empty($product)) 
				{
					break;
				}
				$drp_supplier_id = $product['store_id'];
			}
			++$i;
		}
		return $result;
	}
}
?>
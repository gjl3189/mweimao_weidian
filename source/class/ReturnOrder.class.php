
<?php
class ReturnOrder 
{
	static public function apply($order, $apply_data) 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		if (empty($order) || empty($apply_data)) 
		{
			return false;
		}
		$order_product = D('')->table('Order_product as op')->join('Order AS o ON o.order_id = op.order_id', 'left')->where('op.pigcms_id = \'' . $apply_data['pigcms_id'] . '\'')->field('*, op.pro_num as pro_number')->find();
		if (empty($order_product)) 
		{
			return false;
		}
		$product = D('Product')->where('product_id = \'' . $order_product['product_id'] . '\'')->find();
		if (!empty($product['supplier_id']) && !empty($product['wholesale_product_id'])) 
		{
			$supplier_id = $product['supplier_id'];
		}
		else 
		{
			$supplier_id = $product['store_id'];
		}
		if (empty($product)) 
		{
			return false;
		}
		$order_status = $apply_data['order_status'];
		if (empty($order_status)) 
		{
			$order_status = $order['status'];
		}
		$data = array();
		$data['return_no'] = '1000' . date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999);
		$data['dateline'] = time();
		$data['order_id'] = $order['order_id'];
		$data['order_no'] = $order['order_no'];
		$data['type'] = $apply_data['type'];
		$data['phone'] = $apply_data['phone'];
		$data['content'] = $apply_data['content'];
		$data['order_status'] = $order_status;
		if (!empty($apply_data['images'])) 
		{
			foreach ($apply_data['images'] as &$image ) 
			{
				$image = getAttachment($image);
			}
			$data['images'] = serialize($apply_data['images']);
		}
		$data['status'] = 1;
		$data['store_id'] = $order['store_id'];
		$data['uid'] = $order['uid'];
		if (!empty($order_product['is_packaged']) && !empty($order_product['in_package_status'])) 
		{
			$data['is_delivered'] = 1;
		}
		else 
		{
			$data['is_delivered'] = 0;
		}
		if (($product['wholesale_product_id'] == 0) && ($product['store_id'] == $order_product['store_id'])) 
		{
			$data['is_fx'] = 0;
		}
		else if (($product['wholesale_product_id'] != 0) && ($product['store_id'] == $order_product['store_id'])) 
		{
			$data['is_fx'] = 2;
		}
		else 
		{
			$data['is_fx'] = 1;
		}
		$discount = 10;
		$order_discount = array();
		if ($product['wholesale_product_id']) 
		{
			$order_discount = D('Order_discount')->where('order_id = \'' . $order['order_id'] . '\' AND store_id = \'' . $product['supplier_id'] . '\'')->find();
		}
		else 
		{
			$order_discount = D('Order_discount')->where('order_id = \'' . $order['order_id'] . '\' AND store_id = \'' . $product['store_id'] . '\'')->find();
		}
		if (!empty($order_discount)) 
		{
			$discount = $order_discount['discount'];
		}
		$data['user_return_id'] = 0;
		$result = D('Return')->data($data)->add();
		if (!$result) 
		{
			return false;
		}
		$number = M('Return_product')->returnNumber($order['order_id'], $apply_data['pigcms_id']);
		$return_product_data = array();
		$return_product_data['order_id'] = $order['order_id'];
		$return_product_data['return_id'] = $result;
		$return_product_data['order_product_id'] = $apply_data['pigcms_id'];
		$return_product_data['product_id'] = $order_product['product_id'];
		$return_product_data['sku_id'] = $order_product['sku_id'];
		$return_product_data['sku_data'] = $order_product['sku_data'];
		$return_product_data['pro_num'] = $apply_data['number'];
		$return_product_data['pro_price'] = $order_product['pro_price'];
		$return_product_data['supplier_id'] = $supplier_id;
		$return_product_data['original_product_id'] = $order_product['original_product_id'];
		$return_product_data['discount'] = $discount;
		$return_product_data['user_return_id'] = $result;
		D('Return_product')->data($return_product_data)->add();
		if ($order_product['pro_number'] <= $number + $apply_data['number']) 
		{
			D('Order_product')->where(array('pigcms_id' => $apply_data['pigcms_id']))->data(array('return_status' => 2))->save();
		}
		else 
		{
			D('Order_product')->where(array('pigcms_id' => $apply_data['pigcms_id']))->data(array('return_status' => 1))->save();
		}
		$order_list = D('')->table('Order AS o')->join('Order_product AS op ON o.order_id = op.order_id', 'LEFT')->where('o.user_order_id = \'' . $order_product['order_id'] . '\' AND (op.product_id = \'' . $product['product_id'] . '\' OR op.product_id = \'' . $product['wholesale_product_id'] . '\') AND op.sku_data = \'' . $order_product['sku_data'] . '\'')->order('o.order_id ASC')->select();
		if (is_array($order_list)) 
		{
			foreach ($order_list as $tmp_order ) 
			{
				if ($order_product['pro_number'] <= $number + $apply_data['number']) 
				{
					D('Order_product')->where(array('pigcms_id' => $tmp_order['pigcms_id']))->data(array('return_status' => 2))->save();
				}
				else 
				{
					D('Order_product')->where(array('pigcms_id' => $tmp_order['pigcms_id']))->data(array('return_status' => 1))->save();
				}
				$data['order_id'] = $tmp_order['order_id'];
				$data['order_no'] = $tmp_order['order_no'];
				$data['store_id'] = $tmp_order['store_id'];
				$product = D('Product')->where(array('product_id' => $tmp_order['product_id']))->find();
				if (empty($product)) 
				{
					return false;
				}
				if (($product['wholesale_product_id'] == 0) && ($product['store_id'] == $tmp_order['store_id'])) 
				{
					$data['is_fx'] = 0;
				}
				else if (($product['wholesale_product_id'] != 0) && ($product['store_id'] == $tmp_order['store_id'])) 
				{
					$data['is_fx'] = 2;
				}
				else 
				{
					$data['is_fx'] = 1;
				}
				$data['user_return_id'] = $result;
				$drp_return_id = D('Return')->data($data)->add();
				if (!$drp_return_id) 
				{
					return false;
				}
				$return_product_data['order_id'] = $tmp_order['order_id'];
				$return_product_data['return_id'] = $drp_return_id;
				$return_product_data['order_product_id'] = $tmp_order['pigcms_id'];
				$return_product_data['product_id'] = $tmp_order['product_id'];
				$return_product_data['pro_price'] = $tmp_order['pro_price'];
				D('Return_product')->data($return_product_data)->add();
			}
		}
		return $result;
		while ($i <= 3) 
		{
			$drp_store = D('Store')->where(array('store_id' => $drp_supplier_id))->find();
			$order_tmp = D('')->table('Order AS o')->join('Order_product AS op ON o.order_id = op.order_id', 'left')->where('(o.order_id = \'' . $order['order_id'] . '\' or o.user_order_id = \'' . $order['order_id'] . '\') and o.store_id = \'' . $drp_supplier_id . '\' AND (op.original_product_id = \'' . $order_product['original_product_id'] . '\' OR op.original_product_id = 0) AND op.sku_data = \'' . $order_product['sku_data'] . '\'')->find();
			if ($order_product['pro_num'] <= $number + $apply_data['number']) 
			{
				D('Order_product')->where(array('pigcms_id' => $order_tmp['pigcms_id']))->data(array('return_status' => 2))->save();
			}
			else 
			{
				D('Order_product')->where(array('pigcms_id' => $order_tmp['pigcms_id']))->data(array('return_status' => 1))->save();
			}
			$data['order_id'] = $order_tmp['order_id'];
			$data['order_no'] = $order_tmp['order_no'];
			$data['store_id'] = $drp_store['store_id'];
			if (empty($drp_store['drp_supplier_id']) || ($drp_store['drp_supplier_id'] == $drp_store['store_id'])) 
			{
				$data['is_fx'] = 0;
			}
			else 
			{
				$data['is_fx'] = 1;
			}
			$data['user_return_id'] = $result;
			$drp_return_id = D('Return')->data($data)->add();
			if (!$drp_return_id) 
			{
				return false;
			}
			$return_product_data['order_id'] = $order_tmp['order_id'];
			$return_product_data['return_id'] = $drp_return_id;
			$return_product_data['order_product_id'] = $order_tmp['pigcms_id'];
			$return_product_data['product_id'] = $order_tmp['product_id'];
			$return_product_data['pro_price'] = $order_tmp['pro_price'];
			D('Return_product')->data($return_product_data)->add();
			if (empty($drp_store['drp_supplier_id']) || ($drp_store['drp_supplier_id'] == $drp_store['store_id'])) 
			{
				break;
			}
			$drp_supplier_id = $drp_store['drp_supplier_id'];
			if ($i == 2) 
			{
				$product = D('Product')->where(array('product_id' => $order_product['original_product_id']))->find();
				if (empty($product)) 
				{
					break;
				}
				$drp_supplier_id = $product['store_id'];
			}
			++$i;
		}
		return $result;
	}
	static public function checkReturnStatus($return) 
	{
		$count = D('')->table('Return as r')->join('Return_product as rp on r.id = rp.return_id')->where('rp.order_product_id = \'' . $return['order_product_id'] . '\' AND r.status != 2 AND r.status != 6')->field('sum(rp.pro_num) as count')->find();
		$count = $count['count'] + 0;
		$order_product = D('Order_product')->where('pigcms_id = \'' . $return['order_product_id'] . '\'')->find();
		$return_status = 0;
		if ((0 < $count) && ($order_product['pro_num'] <= $count)) 
		{
			$return_status = 2;
		}
		else if ((0 < $count) && ($count < $order_product['pro_num'])) 
		{
			$return_status = 1;
		}
		D('Order_product')->where('user_order_id = \'' . $order_product['user_order_id'] . '\' AND sku_data = \'' . $order_product['sku_data'] . '\' AND (product_id = \'' . $order_product['product_id'] . '\' OR product_id = \'' . $order_product['original_product_id'] . '\')')->data('return_status = \'' . $return_status . '\'')->save();
	}
	static public function checkOrderStatus($order) 
	{
		$count = D('Order_product')->where('order_id = \'' . $order['order_id'] . '\' AND return_status != \'2\'')->field('count(*) as count')->find();
		$time = time();
		if ($count['count'] == 0) 
		{
			if (($order['status'] == 2) && ($order['payment_method'] == 'codpay')) 
			{
				M('Order')->cancelOrder($order, 2);
				if ($order['user_order_id']) 
				{
					$order_list = D('Order')->where('(order_id = \'' . $order['user_order_id'] . '\' OR user_order_id = \'' . $order['user_order_id'] . '\') AND order_id != \'' . $order['order_id'] . '\'')->select();
					foreach ($order_list as $order_tmp ) 
					{
						$suppliers = explode(',', $order_tmp['suppliers']);
						if (($order_tmp['order_id'] != $order['order_id']) && !empty($suppliers) && !in_array($order['store_id'], $suppliers)) 
						{
							continue;
						}
						$count = D('Order_product')->where('order_id = \'' . $order_tmp['order_id'] . '\' AND return_status != \'2\'')->field('count(*) as count')->find();
						if ($count['count'] == 0) 
						{
							if (($order_tmp['status'] == 2) && ($order_tmp['payment_method'] == 'codpay')) 
							{
								M('Order')->cancelOrder($order_tmp, 2);
							}
							else 
							{
								D('Order')->where('order_id = \'' . $order_tmp['order_id'] . '\'')->data(array('status' => 6, 'refund_time' => $time))->save();
							}
						}
					}
				}
			}
			else 
			{
				D('Order')->where('order_id = \'' . $order['order_id'] . '\'')->data(array('status' => 6, 'refund_time' => $time))->save();
				if ($order['user_order_id']) 
				{
					$order_list = D('Order')->where('(order_id = \'' . $order['user_order_id'] . '\' OR user_order_id = \'' . $order['user_order_id'] . '\') AND order_id != \'' . $order['order_id'] . '\'')->select();
					foreach ($order_list as $order_tmp ) 
					{
						$suppliers = explode(',', $order_tmp['suppliers']);
						if (($order_tmp['order_id'] != $order['order_id']) && !empty($suppliers) && !in_array($order['store_id'], $suppliers)) 
						{
							continue;
						}
						$count = D('Order_product')->where('order_id = \'' . $order_tmp['order_id'] . '\' AND return_status != \'2\'')->field('count(*) as count')->find();
						if ($count['count'] == 0) 
						{
							if ($order_tmp['payment_method'] == 'codpay') 
							{
								M('Order')->cancelOrder($order_tmp, 2);
							}
							else 
							{
								D('Order')->where('order_id = \'' . $order_tmp['order_id'] . '\'')->data(array('status' => 6, 'refund_time' => $time))->save();
							}
						}
					}
				}
			}
		}
	}
	static public function batchReturn($order_list = array(), $message = '') 
	{
		if (empty($order_list) || !is_array($order_list)) 
		{
			return NULL;
		}
		if (empty($message)) 
		{
			$message = '团购失败，自动退货';
		}
		foreach ($order_list as $order ) 
		{
			$order_product = D('Order_product')->where(array('order_id' => $order['order_id']))->find();
			if (empty($order_product)) 
			{
				continue;
			}
			$data = array();
			$data['pigcms_id'] = $order_product['pigcms_id'];
			$data['type'] = 5;
			$data['phone'] = $order['address_tel'];
			$data['content'] = $message;
			$data['number'] = $order['pro_num'];
			$result = self::apply($order, $data);
			if ($result) 
			{
				D('Order')->where(array('order_id' => $order['order_id']))->data('`data_money` = `total`')->save();
			}
		}
	}
}
?>
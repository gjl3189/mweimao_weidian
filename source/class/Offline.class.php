
<?php
class Offline 
{
	static public function add($store_id, $uid, $total, $cash, $platform_point, $cat_id, $product_name, $number = 1, $bak = '') 
	{
		$user = D('User')->where(array('uid' => $uid))->find();
		if (empty($user)) 
		{
			json_return(1000, '未找到相应的用户');
		}
		if ((0 < option('config.user_point_total')) && (option('config.user_point_total') <= $user['point_balance'] + $user['point_unbalance'])) 
		{
			json_return(1000, '用户的积分过多，暂停手工做单');
		}
		$credit_setting = option('credit');
		$store = D('Store')->where(array('store_id' => $store_id))->find();
		if (empty($store)) 
		{
			json_return(1000, '未找到相应的店铺');
		}
		$store_user = D('User')->where(array('uid' => $store['uid']))->find();
		if (empty($store_user)) 
		{
			json_return(1000, '未找到相应的店铺用户');
		}
		if ($store['margin_balance'] < $cash) 
		{
			json_return(1000, '店铺的充值现金不足');
		}
		if (($store_user['point_balance'] + $store['point_balance']) < $platform_point) 
		{
			json_return(1000, $credit_setting['platform_credit_name'] . '不足');
		}
		Margin::init($store_id);
		$service_fee = Margin::service_fee($total * $credit_setting['platform_credit_rule']);
		$platform_point_money = 0;
		if (0 < $credit_setting['platform_credit_use_value']) 
		{
			$platform_point_money = round($platform_point / $credit_setting['platform_credit_use_value'], 2);
		}
		if (0 < $credit_setting['offline_trade_money']) 
		{
			if ((($cash / $service_fee) * 100) < $credit_setting['offline_trade_money']) 
			{
				json_return(1000, '现金比不能少于' . $credit_setting['offline_trade_money'] . '%');
			}
		}
		if (($cash + $platform_point_money) != $service_fee) 
		{
			json_return(1000, '数据错误，请刷新重试');
		}
		$store_point = 0;
		$store_user_point = 0;
		if (0 < $platform_point_money) 
		{
			if ($store_user['point_balance'] < $platform_point) 
			{
				$store_user_point = $store_user['point_balance'];
				$store_point = $platform_point - $store_user_point;
			}
			else if ($platform_point <= $store_user['point_balance']) 
			{
				$store_user_point = $platform_point;
			}
		}
		$return_point = $total * $credit_setting['platform_credit_rule'];
		$time = time();
		$data = array();
		$data['dateline'] = $time;
		$data['order_no'] = date('YmdHis', $_SERVER['REQUEST_TIME']) . '-' . mt_rand(10000, 99999);
		$data['uid'] = $uid;
		$data['store_id'] = $store_id;
		$data['total'] = $total;
		$data['service_fee'] = $service_fee;
		$data['cash'] = $cash;
		$data['point'] = $platform_point;
		$data['store_point'] = $store_point;
		$data['store_user_point'] = $store_user_point;
		$data['cat_id'] = $cat_id;
		$data['product_name'] = $product_name;
		$data['number'] = $number;
		$data['return_point'] = $return_point;
		$data['point2money_rate'] = $credit_setting['platform_credit_use_value'];
		$data['status'] = 0;
		$data['check_status'] = 0;
		$data['bak'] = $bak;
		$is_admin_check = true;
		if ($total < option('config.offline_money')) 
		{
			$data['check_status'] = 1;
			$data['check_dateline'] = $data['dateline'];
		}
		$order_id = D('Order_offline')->data($data)->add();
		$data['id'] = $order_id;
		$order_offline = $data;
		if ($order_id) 
		{
			if (0 < $cash) 
			{
				$order_no = date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999);
				$data = array();
				$data['order_offline_id'] = $order_id;
				$data['store_id'] = $store_id;
				$data['order_no'] = $order_no;
				$data['trade_no'] = $order_no;
				$data['amount'] = -1 * $cash;
				$data['payment_method'] = '';
				$data['type'] = 2;
				$data['status'] = ($is_admin_check ? 1 : 2);
				$data['add_time'] = $time;
				$data['bak'] = '店铺线下做单扣除服务费';
				$data['margin_total'] = (!empty($store['margin_total']) ? $store['margin_total'] : 0);
				$data['margin_balance'] = (!empty($store['margin_balance']) ? $store['margin_balance'] : 0);
				if (D('Platform_margin_log')->data($data)->add()) 
				{
					D('Store')->where(array('store_id' => $store_id))->data('`margin_balance` = `margin_balance` - ' . $cash)->save();
				}
			}
			if (0 < $store_point) 
			{
				$data = array();
				$data['store_id'] = $store_id;
				$data['order_offline_id'] = $order_id;
				$data['order_no'] = date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999);
				$data['point'] = -1 * $store_point;
				$data['status'] = ($is_admin_check ? 1 : 2);
				$data['type'] = 0;
				$data['add_time'] = time();
				$data['point_total'] = $store['point_total'];
				$data['point_balance'] = $store['point_balance'];
				$data['channel'] = 1;
				$data['bak'] = '店铺线下做单使用平台币';
				if (D('Store_point_log')->data($data)->add()) 
				{
					D('Store')->where(array('store_id' => $store_id))->data('`point_balance` = `point_balance` - ' . $store_point)->save();
				}
			}
			if (0 < $store_user_point) 
			{
				$data = array();
				$data['order_offline_id'] = $order_id;
				$data['uid'] = $store_user['uid'];
				$data['order_no'] = date('YmdHis', $_SERVER['REQUEST_TIME']) . mt_rand(100000, 999999);
				$data['point'] = -1 * $store_user_point;
				$data['status'] = ($is_admin_check ? 0 : 1);
				$data['type'] = 1;
				$data['store_id'] = $store_id;
				$data['supplier_id'] = $store_id;
				$data['add_time'] = time();
				$data['add_date'] = date('Ymd');
				$data['point_total'] = $user['point_total'];
				$data['point_balance'] = $user['point_balance'];
				$data['point_unbalance'] = $user['point_unbalance'];
				$data['point_send_base'] = 0;
				$data['channel'] = 1;
				$data['bak'] = '店铺线下做单使用平台币';
				if (D('User_point_log')->data($data)->add()) 
				{
					D('User')->where(array('uid' => $store_user['uid']))->setDec('point_balance', $store_user_point);
				}
			}
			Notice::sendOffline($order_offline);
			return array('err_code' => 0, 'err_msg' => $order_id);
		}
		json_return(1000, '订单创建失败');
	}
	static public function complete($order_offline) 
	{
		$order_id = $order_offline['id'];
		$store_id = $order_offline['store_id'];
		$store = D('Store')->where(array('store_id' => $order_offline['store_id']))->find();
		$store_user = D('User')->where(array('uid' => $store['uid']))->find();
		$user = D('User')->where(array('uid' => $order_offline['uid']))->find();
		if (0 < $order_offline['cash']) 
		{
			D('Platform_margin_log')->where(array('order_offline_id' => $order_id))->data(array('status' => 2))->save();
			M('Common_data')->setData('margin_used', $order_offline['cash']);
		}
		if (0 < $order_offline['store_user_point']) 
		{
			D('User_point_log')->where(array('order_offline_id' => $order_id))->data(array('status' => 1))->save();
			D('User')->where(array('uid' => $store_user['uid']))->setInc('point_used', $order_offline['store_user_point']);
		}
		if ($order_offline['store_point']) 
		{
			D('Store_point_log')->where(array('order_offline_id' => $order_id))->data(array('status' => 2))->save();
			D('Store')->where(array('store_id' => $store_id))->setInc('cash_point', $order_offline['store_point']);
		}
		$order_no = $order_offline['order_no'];
		if (0 < $order_offline['return_point']) 
		{
			$data = array();
			$data['order_no'] = $order_no;
			$data['uid'] = $user['uid'];
			$data['order_offline_id'] = $order_id;
			$data['store_id'] = $store_id;
			$data['supplier_id'] = $store_id;
			$data['point'] = $order_offline['return_point'];
			$data['status'] = 1;
			$data['type'] = 0;
			$data['channel'] = 1;
			$data['bak'] = '手工做单发放积分';
			$data['add_time'] = time();
			$data['add_date'] = date('Ymd');
			$data['point_total'] = $user['point_total'];
			$data['point_balance'] = $user['point_balance'];
			$data['point_unbalance'] = $user['point_unbalance'];
			$data['point_send_base'] = 0;
			$credit_weight = ((0 < option('credit.credit_weight') ? option('credit.credit_weight') : 0));
			$tmp_log = D('User_point_log')->field('point_send_base')->where(array( 'uid' => $user['uid'], 'type' => array( 'in', array(0, 5, 6, 7) ) ))->order('pigcms_id DESC')->find();
			$point_unbalance = max(0, $user['point_unbalance'] + $order_offline['return_point']);
			$point_send_base = SendPoints::getCardinalNumber($tmp_log['point_send_base'], $point_unbalance, $credit_weight);
			$data['point_send_base'] = $point_send_base + 0;
			if (D('User_point_log')->data($data)->add()) 
			{
				D('User')->where(array('uid' => $user['uid']))->data('`point_total` = `point_total` + ' . $order_offline['return_point'] . ', `point_unbalance` = `point_unbalance` + ' . $order_offline['return_point'])->save();
			}
		}
		D('Order_offline')->where(array('id' => $order_offline['id']))->data(array('status' => 1))->save();
		Margin::init($store_id);
		$service_fee = $order_offline['cash'];
		if (0 < $order_offline['cash']) 
		{
			$data = array();
			$data['income'] = number_format($service_fee, 2, '.', '');
			$data['add_time'] = time();
			$data['type'] = 3;
			$data['store_id'] = $store_id;
			$data['bak'] = '保证金服务费(含 ' . option('credit.cash_provisions') . '% 提现备付金)';
			if (D('Platform_income')->data($data)->add()) 
			{
				$now = date('Ymd');
				$day_service_fee = D('Day_platform_service_fee')->where(array('add_date' => $now))->find();
				if (!empty($day_service_fee)) 
				{
					D('Day_platform_service_fee')->where(array('pigcms_id' => $day_service_fee['pigcms_id']))->setInc('amount', $service_fee);
				}
				else 
				{
					D('Day_platform_service_fee')->data(array('add_date' => $now, 'amount' => $service_fee))->add();
				}
			}
			$cash_provision = ($service_fee * option('credit.cash_provisions')) / 100;
			$service_fee -= $cash_provision;
			if (0 < $cash_provision) 
			{
				Margin::cash_provision_log($cash_provision, 0, $order_offline['return_point'], '手工做单交易完成平台添加提现备付金', $order_offline['order_no']);
			}
		}
		$reward_total = 0;
		if (0 < $service_fee) 
		{
			$reward_total = Margin::promotion_reward_offline($order_offline['cash'], $order_id, $store_id, 1, $store['wxpay']);
		}
		$net_income = $service_fee - $reward_total;
		if (0 < $net_income) 
		{
			M('Common_data')->setData('trade_net_income', $net_income);
		}
		$service_fee = $order_offline['cash'] - $reward_total;
		if (0 < $service_fee) 
		{
			M('Common_data')->setData('income', $service_fee);
		}
		if (0 < $order_offline['point']) 
		{
			Margin::platform_point_log($order_offline['point'], 1, 3, '商家做单服务费抵现', 1, '', '', true);
		}
		Notice::orderOfflineComplete($order_offline);
		return true;
	}
}
?>

<?php
class ShopNotice 
{
	static public function AuditDealerSuccess($supplier_info, $seller_info) 
	{
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM206164559';
		$admin_url = option('config.site_url') . '/account.php';
		$supplier_user_info = M('User')->getUserById($supplier_info['uid']);
		$supplier_store = M('Store')->getStoreBySub($supplier_info['store_id']);
		$supplier_linkname = (($supplier_user_info['name'] ? $supplier_user_info['name'] : $supplier_info['name']));
		$supplier_userphone = $supplier_info['tel'];
		$seller_user_info = M('User')->getUserById($seller_info['uid']);
		$return_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $supplier_info['store_id'] . '\' and uid=\'' . $seller_info['uid'] . '\'')->find();
		if (isset($return_wxuser_info['openid'])) 
		{
			$seller_openid = $return_wxuser_info['openid'];
		}
		else if (isset($seller_info['openid'])) 
		{
			$seller_openid = $seller_info['openid'];
		}
		else 
		{
			$seller_openid = '';
		}
		if ($supplier_info['bank_id']) 
		{
			$bankname = D('Bank')->where('bank_id = \'' . $supplier_info['bank_id'] . '\'')->find();
		}
		$bankname = (($bankname ? $bankname : ''));
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_info['store_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_user_info['smscount']) 
			{
				if ($supplier_userphone) 
				{
					$msg = sendtemplate::shopsmsTpl('1', array('storename' => $seller_info['name']));
					$params['sms'] = array('mobile' => $supplier_userphone, 'store_id' => $supplier_info['store_id'], 'token' => 'test', 'content' => $msg, 'sendType' => '');
					MessageFactory::method($params, array('smsMessage'));
				}
				if ($seller_user_info['phone']) 
				{
					$msg = sendtemplate::shopsmsTpl('2', array('bond' => $supplier_info['bond'], 'name' => $supplier_info['name'], 'bankname' => $bankname, 'bank_card' => $supplier_info['bank_card'], 'username' => $supplier_linkname, 'userphone' => $supplier_userphone));
					$params['sms'] = array('mobile' => $seller_user_info['phone'], 'store_id' => $supplier_info['store_id'], 'token' => 'test', 'content' => $msg, 'sendType' => '');
					MessageFactory::method($params, array('smsMessage'));
				}
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			if ($supplier_store['openid']) 
			{
				$params = array();
				$template_data = array('wecha_id' => $supplier_store['openid'], 'first' => '经销商通过审核成功通知', 'keyword1' => $supplier_info['name'], 'keyword2' => '经销商通过审核成功', 'keyword3' => date('Y-m-d H:i', time()), 'remark' => '目前您的批发商【' . $seller_info['name'] . ' 】提交的审核资料已通过审核');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				MessageFactory::method($params, array('TemplateMessage'));
			}
			if ($seller_openid) 
			{
				$params = array();
				$template_data = array('wecha_id' => $seller_openid, 'first' => '经销商通过审核成功通知', 'keyword1' => $seller_info['name'], 'keyword2' => '经销商通过审核成功', 'keyword3' => date('Y-m-d H:i', time()), 'remark' => '目前您在供货商【' . $supplier_info['name'] . ' 】提交的审核资料已通过审核,为避免失去批发资格,请您登陆' . $admin_url . '查看详情并完成后续操作');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				MessageFactory::method($params, array('TemplateMessage'));
			}
		}
	}
	static public function UpdateRecharge($recharge_log) 
	{
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM400492077';
		$supplier_info = D('Store')->where(array('store_id' => $recharge_log['supplier_id']))->find();
		$seller_info = D('Store')->where(array('store_id' => $recharge_log['distributor_id']))->find();
		$supplier_user_info = M('User')->getUserById($supplier_info['uid']);
		$supplier_info = M('Store')->getStoreBySub($supplier_info['store_id']);
		$supplier_linkname = (($supplier_user_info['name'] ? $supplier_user_info['name'] : $supplier_info['name']));
		$supplier_userphone = (($supplier_info['tel'] ? $supplier_info['tel'] : $supplier_user_info['phone']));
		$seller_user_info = M('User')->getUserById($seller_info['uid']);
		$seller_info_by_store = M('Store')->getStoreBySub($supplier_info['store_id'], $seller_info['uid']);
		if ($recharge_log[bank_id]) 
		{
			$bankname = D('Bank')->where('bank_id = \'' . $supplier_info['bank_id'] . '\'')->find();
		}
		$bankname = (($bankname ? $bankname : ''));
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($recharge_log['supplier_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_user_info['smscount']) 
			{
				if ($supplier_info['tel']) 
				{
					$msg = sendtemplate::shopsmsTpl('3', array('supplier_user_info' => $supplier_user_info, 'seller_user_info' => $seller_user_info, 'supplier_store' => $supplier_info, 'seller_store' => $seller_info, 'recharge_log' => $recharge_log));
					$params['sms'] = array('mobile' => $supplier_info['tel'], 'store_id' => $supplier_info['store_id'], 'token' => 'test', 'content' => $msg);
					MessageFactory::method($params, array('smsMessage'));
				}
				if ($seller_user_info['phone']) 
				{
					$msg = sendtemplate::shopsmsTpl('4', array('supplier_user_info' => $supplier_user_info, 'seller_user_info' => $seller_user_info, 'supplier_store' => $supplier_info, 'seller_store' => $seller_info, 'recharge_log' => $recharge_log, 'bankname' => $bankname));
					$params['sms'] = array('mobile' => $seller_user_info['phone'], 'store_id' => $supplier_info['store_id'], 'token' => 'test', 'content' => $msg);
					MessageFactory::method($params, array('smsMessage'));
				}
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			if ($supplier_info['sub_openid']) 
			{
				$params = array();
				$template_data = array('wecha_id' => $supplier_info['sub_openid'], 'first' => '经销商给您店铺打款通知', 'keyword1' => $seller_user_info['uid'], 'keyword2' => $recharge_log['apply_recharge'], 'keyword3' => date('Y-m-d H:i', $recharge_log['add_time']), 'remark' => '经销商店铺【' . $seller_info[name] . '】 打款账号【' . $recharge_log['bank_card'] . ' 】，打款人手机号：' . $recharge_log['phone'] . ' ');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				MessageFactory::method($params, array('TemplateMessage'));
			}
			if ($seller_info_by_store['sub_openid']) 
			{
				$params = array();
				$template_data = array('wecha_id' => $seller_info_by_store['sub_openid'], 'first' => '您向供货商打款通知', 'keyword1' => $seller_info[name], 'keyword2' => $recharge_log['apply_recharge'], 'keyword3' => date('Y-m-d H:i', $recharge_log['add_time']), 'remark' => '目前您在供货商【' . $supplier_info['name'] . ' 】打款账号【' . $recharge_log['bank_card'] . ' 】，打款人手机号：' . $recharge_log['phone'] . ' ');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				MessageFactory::method($params, array('TemplateMessage'));
			}
		}
	}
	static public function UpdateBond($bond_information) 
	{
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM207526849';
		$supplier_store_id = $bond_information['supplier_id'];
		$supplier_info = M('Store')->getStoreBySub($supplier_store_id);
		$supplier_user_info = M('User')->getUserById($supplier_info['uid']);
		$seller_id = $bond_information['distributor_id'];
		$seller_storeinfo = D('Store')->where(array('store_id' => $seller_id))->find();
		$seller_info = M('Store')->getStoreBySub($supplier_store_id, $seller_storeinfo['uid']);
		$seller_user_info = M('User')->getUserById($seller_storeinfo['uid']);
		if ($bond_information[bank_id]) 
		{
			$bankname = D('Bank')->where('bank_id = \'' . $bond_information['bank_id'] . '\'')->find();
		}
		$bankname = (($bankname ? $bankname : '暂未公示的账户'));
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($bond_information['supplier_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_user_info['smscount']) 
			{
				if ($supplier_info['tel']) 
				{
					$msg = sendtemplate::shopsmsTpl('5', array('supplier_info' => $supplier_info, 'seller_info' => $seller_info, 'bond_information' => $bond_information, 'bankname' => $bankname));
					$params['sms'] = array('mobile' => $supplier_info['tel'], 'store_id' => $supplier_store_id, 'token' => 'test', 'content' => $msg);
					MessageFactory::method($params, array('smsMessage'));
				}
				if ($seller_info['tel']) 
				{
					$msg = sendtemplate::shopsmsTpl('6', array('supplier_info' => $supplier_info, 'seller_info' => $seller_info, 'bond_information' => $bond_information, 'bankname' => $bankname));
					$params['sms'] = array('mobile' => $seller_info['tel'], 'store_id' => $supplier_store_id, 'token' => 'test', 'content' => $msg);
					MessageFactory::method($params, array('smsMessage'));
				}
			}
		}
		$supplier_info_bank_card = (($supplier_info['bank_card'] ? anonymous($supplier_info['bank_card'], 3, 4) : '暂未公示卡号'));
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			if ($supplier_info['sub_openid']) 
			{
				$params = array();
				$template_data = array('wecha_id' => $supplier_info['sub_openid'], 'first' => '【' . $seller_info[name] . '】打款到帐的通知', 'keyword1' => '您已确认您的批发商打款到账', 'keyword2' => '￥' . $bond_information['apply_recharge'], 'keyword3' => $supplier_info_bank_card, 'keyword4' => date('Y-m-d H:i', time()) . '（手工确认时间）', 'remark' => '经销商店铺【' . $seller_info[name] . '】 打款账号【' . $bond_information['bank_card'] . ' 】，打款人手机号：' . $bond_information['phone'] . ' ');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				MessageFactory::method($params, array('TemplateMessage'));
			}
			if ($seller_info['sub_openid']) 
			{
				$params = array();
				$template_data = array('wecha_id' => $seller_info['sub_openid'], 'first' => '【' . $seller_info[name] . '】打款到帐的通知', 'keyword1' => '您的供货商已确认您的款项到账', 'keyword2' => '￥' . $bond_information['apply_recharge'], 'keyword3' => $supplier_info_bank_card, 'keyword4' => date('Y-m-d H:i', time()) . '（手工确认时间）', 'remark' => '目前您在供货商【' . $supplier_info['name'] . ' 】打款账号【' . $bond_information['bank_card'] . ' 】，打款人手机号：' . $bond_information['phone'] . ' ');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				MessageFactory::method($params, array('TemplateMessage'));
			}
		}
	}
	static public function Updatekucun($order_info, $product_list, $store) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM203170813';
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($store['store_id'], $wx_tpl_no);
		foreach ($product_list as $k => $product ) 
		{
			$msgInfo = array('word1' => '订单' . $order_info['order_no'] . '中的商品【' . $product['name'] . '】数量不足，请补充门店库存并手动分单', 'goods_no' => $product['product_id'], 'goods_name' => $product['name'], 'quantity' => $product['pro_num']);
			$order_no = option('config.orderid_prefix') . $order_info['order_no'];
			$msg = '尊敬的店主，您收到订单:' . $order_no . '中【' . $product['name'] . '】在所有门店缺货，请补充并手动分单。';
			$openid = $store['sub_openid'];
			if ($this_power['allow_weixin_msg']) 
			{
				if ($openid) 
				{
					import('source.class.Factory');
					import('source.class.MessageFactory');
					$template_data = array('wecha_id' => $openid, 'first' => $msg, 'keyword1' => $product['product_id'], 'keyword2' => $product['name'], 'keyword3' => $product['pro_num'], 'remark' => '请尽快补充库存');
					$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
					$param_array[] = 'TemplateMessage';
				}
			}
			$user_info = M('User')->getUserById($store['uid']);
			$mobile = (($store['tel'] ? $store['tel'] : $user_info['phone']));
			if ($this_power['allow_mobile_msg']) 
			{
				if ($mobile) 
				{
					$date = date('Y-m-d H:i:s', time());
					$params['sms'] = array('mobile' => $mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$param_array[] = 'sms';
				}
			}
			if (count($param_array)) 
			{
				MessageFactory::method($params, $param_array);
			}
		}
	}
	static public function ReturnApply($buyer_uid, $order_product, $order, $order_product_id, $result) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM204146731';
		$order_product_info = D('Order_product')->where('pigcms_id = \'' . $order_product_id . '\'')->find();
		$supplier_store = M('Store')->getSuppliers($order['store_id']);
		$supplier_user_info = M('User')->getUserById($supplier_store['uid']);
		$now_Store = D('Store')->where(array('store_id' => $order['store_id']))->find();
		$return_user_info = M('User')->getUserById($buyer_uid);
		$return_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $supplier_store['store_id'] . '\' and uid=\'' . $buyer_uid . '\'')->find();
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_store['store_id'], $wx_tpl_no);
		$return_product_info = D('Return_product')->where(array('order_id' => $order['order_id'], 'return_id' => $result))->find();
		$params_array = array();
		if ($this_power['allow_mobile_msg']) 
		{
			if (preg_match('/^1[0-9]{10}$/', $supplier_store['tel'])) 
			{
				$msg = sendtemplate::shopsmsTpl('7', array('order_no' => $order['order_no']));
				$params['sms'] = array('store_id' => $supplier_store[store_id], 'mobile' => $supplier_store['tel'], 'token' => 'test', 'content' => $msg);
				$params_array['sms'] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if (preg_match('/^1[0-9]{10}$/', $return_user_info['phone'])) 
			{
				$msg = sendtemplate::shopsmsTpl('8', array('order_no' => $order['order_no']));
				$params['sms'] = array('store_id' => $supplier_store[store_id], 'mobile' => $return_user_info['phone'], 'token' => 'test', 'content' => $msg);
				$params_array['sms'] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			$params = array();
			$params_array = array();
			if ($supplier_store['openid']) 
			{
				$template_data = array('wecha_id' => $supplier_store['openid'], 'first' => ' 您好，您有新的退货订单。请及时查看和处理!', 'keyword1' => $order['order_no'], 'keyword2' => $order_product['name'], 'keyword3' => $return_product_info['pro_price'], 'remark' => '状态：' . '退货申请提醒');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($return_wxuser_info['openid']) 
			{
				$template_data = array('wecha_id' => $return_wxuser_info['openid'], 'first' => ' 您好，您有新的退货订单。请及时查看和处理!', 'keyword1' => $order['order_no'], 'keyword2' => $order_product['name'], 'keyword3' => $return_product_info['pro_price'], 'remark' => '状态：' . '退货申请提醒');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function ReturnApplyAuth($status, $return) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM203847595';
		if (($return['store_id'] != $_SESSION['store']['store_id']) || ($return['is_fx'] != '0')) 
		{
			json_return(1001, '您无权操作此退货申请');
		}
		$store_id = $return['store_id'];
		$supplier_store = M('Store')->getSuppliers($store_id);
		$where['store_id'] = $store_id;
		$where['order_no'] = $return['order_no'];
		$order = D('Order')->where($where)->field('uid,order_no')->find();
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_store['store_id'], $wx_tpl_no);
		$this_power['allow_weixin_msg'] = 1;
		$return_product_info = D('Return_product')->where(array('order_id' => $return['order_id'], 'return_id' => $return['id']))->find();
		$product = M('Product')->get(array('product_id' => $return_product_info['product_id']), '*');
		$return_product_info['name'] = $product['name'];
		$buyer_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $supplier_store['store_id'] . '\' and uid=\'' . $order['uid'] . '\'')->find();
		$buyer_subscribe_openid = ((empty($buyer_wxuser_info) ? '' : $buyer_wxuser_info['openid']));
		if ($this_power['allow_mobile_msg']) 
		{
			if (preg_match('/^1[0-9]{10}$/', $supplier_store['tel'])) 
			{
				$msg = sendtemplate::shopsmsTpl('16', array('order_no' => $order['order_no']));
				$params['sms'] = array('store_id' => $supplier_store['store_id'], 'mobile' => $supplier_store['tel'], 'token' => 'test', 'content' => $msg);
				$params_array['sms'] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if (preg_match('/^1[0-9]{10}$/', $return['phone'])) 
			{
				$msg = sendtemplate::shopsmsTpl('17', array('order_no' => $order['order_no']));
				$params['sms'] = array('store_id' => $supplier_store['store_id'], 'mobile' => $return['phone'], 'token' => 'test', 'content' => $msg);
				$params_array['sms'] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			$params = array();
			$params_array = array();
			$total = $return['product_money'] + $return['postage_money'];
			if ($supplier_store['openid']) 
			{
				if ($status == 3) 
				{
					$template_data = array('wecha_id' => $supplier_store['openid'], 'first' => ' 您好，您已经处理' . $return['order_no'] . '的退货申请!', 'keyword1' => '同意退货', 'keyword2' => $return_product_info['name'], 'keyword3' => total, 'keyword4' => $return['store_content'], 'keyword5' => date('Y-m-d H:i:s'), 'remark' => '状态：' . '同意退货处理');
				}
				else 
				{
					$template_data = array('wecha_id' => $supplier_store['openid'], 'first' => ' 您好，您的退货申请已经受理。请及时查看!', 'keyword1' => '不同意退货', 'keyword2' => $return_product_info['name'], 'keyword3' => total, 'keyword4' => $return['store_content'], 'keyword5' => date('Y-m-d H:i:s'), 'remark' => '状态：' . '不同意退货处理');
				}
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($buyer_subscribe_openid) 
			{
				if ($status == 3) 
				{
					$template_data = array('wecha_id' => $buyer_subscribe_openid, 'first' => ' 您好，您的退货申请已经受理。请及时查看!', 'keyword1' => '同意退货', 'keyword2' => $return_product_info['name'], 'keyword3' => total, 'keyword4' => $return['store_content'], 'keyword5' => date('Y-m-d H:i:s'), 'remark' => '状态：' . '商家同意退货提醒');
				}
				else 
				{
					$template_data = array('wecha_id' => $buyer_subscribe_openid, 'first' => ' 您好，您的退货申请已经受理。请及时查看!', 'keyword1' => '不同意退货', 'keyword2' => $return_product_info['name'], 'keyword3' => total, 'keyword4' => $return['store_content'], 'keyword5' => date('Y-m-d H:i:s'), 'remark' => '状态：' . '商家不同意退货提醒');
				}
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function TuanSuccessNotice($tuanzhang_uid, $buyer_uid, $store_id, $tuan_id, $url) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM401105405';
		$supplier_store = M('Store')->getSuppliers($store_id);
		$supplier_store_info = M('Store')->getStoreBySub($store_id);
		$supplier_user_info = M('User')->getUserById($supplier_store['uid']);
		$supplier_info_tel = (($supplier_store['tel'] ? $supplier_store['tel'] : $supplier_user_info['phone']));
		$tuanzhang_user_info = M('User')->getUserById($tuanzhang_uid);
		$tuanzhang_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $supplier_store['store_id'] . '\' and uid=\'' . $tuanzhang_uid . '\'')->find();
		$tuan_info = D('')->table('Tuan t')->join('Product p On t.product_id=p.product_id', 'LEFT')->field('t.*,p.name product_name')->where('t.id=\'' . $tuan_id . '\'')->find();
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_store['store_id'], $wx_tpl_no);
		$buyer_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $supplier_store['store_id'] . '\' and uid=\'' . $buyer_uid . '\'')->find();
		$buyer_user_info = M('User')->getUserById($buyer_uid);
		$tuanzhang_name = (($tuanzhang_user['nickname'] ? $tuanzhang_user['nickname'] : '匿名微信用户'));
		$buyer_name = (($buyer_user_info['nickname'] ? $buyer_user_info['nickname'] : '匿名微信用户'));
		$tuan_name = $tuan_info['name'];
		$product_name = $tuan_info['product_name'];
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_user_info['smscount']) 
			{
				if ($tuanzhang_user_info['phone']) 
				{
					$msg = sendtemplate::shopsmsTpl('9', array('tuan_name' => $tuan_name, 'buyer_name' => $buyer_name));
					$params['sms'] = array('mobile' => $tuanzhang_user_info['phone'], 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
					MessageFactory::method($params, array('smsMessage'));
				}
				if ($buyer_user_info['phone']) 
				{
					$msg = sendtemplate::shopsmsTpl('10', array('tuan_name' => $tuan_name, 'tuanzhang_name' => $buyer_name));
					$params['sms'] = array('mobile' => $buyer_user_info['phone'], 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
					MessageFactory::method($params, array('smsMessage'));
				}
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			if ($tuanzhang_wxuser_info['openid']) 
			{
				unset($params);
				$template_data = array('store_id' => $store_id, 'href' => $url, 'wecha_id' => $tuanzhang_wxuser_info['openid'], 'first' => $buyer_name . '加入了您开的【' . $tuan_name . '】拼团活动', 'keyword1' => $product_name, 'keyword2' => date('Y-m-d H:i:s', time()), 'remark' => '新用户参团成功');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($buyer_wxuser_info['openid']) 
			{
				unset($params);
				$template_data = array('store_id' => $store_id, 'href' => $url, 'wecha_id' => $buyer_wxuser_info['openid'], 'first' => '您加入了拼团活动【' . $tuan_name . '】', 'keyword1' => $product_name, 'keyword2' => date('Y-m-d H:i:s', time()), 'remark' => '参团成功');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function TuanResultNotice($success_uid_arr = array(), $fail_uid_arr = array(), $tuan_id, $tuanzhang_uid, $url) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'TM00353';
		$tuan_info = D('')->table('Tuan t')->join('Product p On t.product_id=p.product_id', 'LEFT')->field('t.*,p.name product_name')->where('t.id=\'' . $tuan_id . '\'')->find();
		$store_id = $tuan_info['store_id'];
		$tuan_name = $tuan_info['name'];
		$product_name = $tuan_info['product_name'];
		$supplier_store = M('Store')->getSuppliers($store_id);
		$supplier_store_info = M('Store')->getStoreBySub($store_id);
		$supplier_user_info = M('User')->getUserById($supplier_store['uid']);
		$supplier_info_tel = (($supplier_store['tel'] ? $supplier_store['tel'] : $supplier_user_info['phone']));
		$tuanzhang_user_info = M('User')->getUserById($tuanzhang_uid);
		$tuanzhang_nickname = (($tuanzhang_user_info['nickname'] ? $tuanzhang_user_info['nickname'] : '匿名用户'));
		$success_uid_list = implode(',', $success_uid_arr);
		$fail_uid_list = implode(',', $fail_uid_arr);
		if (count($success_uid_list)) 
		{
			$success_user_arr_infos = D('')->table('Subscribe_store ss')->join('User u On ss.uid=u.uid', 'LEFT')->where('ss.store_id=\'' . $store_id . '\' and ss.uid in (' . $success_uid_list . ')')->field('ss.*,u.phone')->select();
		}
		if (count($fail_uid_list)) 
		{
			$fail_user_arr_infos = D('')->table('Subscribe_store ss')->join('User u On ss.uid=u.uid', 'LEFT')->where('ss.store_id=\'' . $store_id . '\' and ss.uid in (' . $fail_uid_list . ')')->field('ss.*,u.phone')->select();
		}
		$allow_success_send = false;
		$allow_fail_send = false;
		if ($success_user_arr_infos) 
		{
			foreach ($success_user_arr_infos as $k => $v ) 
			{
				$success_user_arr_info[$v['uid']] = $v;
				$allow_success_send = true;
			}
		}
		if ($fail_user_arr_infos) 
		{
			foreach ($fail_user_arr_infos as $k => $v ) 
			{
				$fail_user_arr_info[$v['uid']] = $v;
				$allow_fail_send = true;
			}
		}
		$buyer_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $supplier_store['store_id'] . '\' and uid=\'' . $buyer_uid . '\'')->find();
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($store_id, $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_user_info['smscount']) 
			{
				if ($allow_success_send) 
				{
					unset($params);
					foreach ($success_user_arr_info as $k => $v ) 
					{
						if ($v['phone']) 
						{
							$msg = sendtemplate::shopsmsTpl('11', array('tuan_name' => $tuan_name));
							$params['sms'] = array('mobile' => $v['phone'], 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
							MessageFactory::method($params, array('smsMessage'));
						}
					}
				}
				if ($allow_fail_send) 
				{
					foreach ($fail_user_arr_infos as $k => $v ) 
					{
						if ($v['phone']) 
						{
							$msg = sendtemplate::shopsmsTpl('12', array('tuan_name' => $tuan_name));
							$params['sms'] = array('mobile' => $v['phone'], 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
							MessageFactory::method($params, array('smsMessage'));
						}
					}
				}
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			if ($allow_success_send) 
			{
				unset($params);
				foreach ($success_user_arr_info as $k => $v ) 
				{
					if ($v['openid']) 
					{
						$template_data = array('wecha_id' => $v['openid'], 'first' => '恭喜您，您参加的团购【' . $tuan_name . '】已成功，我们会尽快为您发货', 'Pingou_ProductName' => $product_name, 'Weixin_ID' => $tuanzhang_nickname, 'remark' => '拼团成功');
						if ($url) 
						{
							$template_data['href'] = $url;
						}
						$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
						$params_array['template'] = 'TemplateMessage';
						MessageFactory::method($params, $params_array);
					}
				}
			}
			if ($allow_fail_send) 
			{
				unset($params);
				foreach ($fail_user_arr_infos as $k => $v ) 
				{
					if ($v['openid']) 
					{
						$template_data = array('wecha_id' => $v['openid'], 'first' => '很遗憾，您参加的团购【' . $tuan_name . '】已失败！', 'Pingou_ProductName' => $product_name, 'Weixin_ID' => $tuanzhang_nickname, 'remark' => '拼团失败');
						if ($url) 
						{
							$template_data['href'] = $url;
						}
						$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
						$params_array['template'] = 'TemplateMessage';
						MessageFactory::method($params, $params_array);
					}
				}
			}
		}
	}
	static public function MsHelpNotice($store_id, $miaosha_uid, $helper_uid, $second) 
	{
		if (!$second || !$store_id || !$miaosha_uid || !$helper_uid) 
		{
			return false;
		}
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM203917836';
		$supplier_store = M('Store')->getSuppliers($store_id);
		$supplier_store_info = M('Store')->getStoreBySub($store_id);
		$supplier_user_info = M('User')->getUserById($supplier_store['uid']);
		$supplier_info_tel = (($supplier_store['tel'] ? $supplier_store['tel'] : $supplier_user_info['phone']));
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_store['store_id'], $wx_tpl_no);
		$miaosha_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $store_id . '\' and uid=\'' . $miaosha_uid . '\'')->find();
		$miaosha_user_info = M('User')->getUserById($miaosha_uid);
		$miaosha_username = (($miaosha_user_info['nickname'] ? $miaosha_user_info['nickname'] : '匿名微信用户'));
		$helper_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $store_id . '\' and uid=\'' . $helper_uid . '\'')->find();
		$helper_user_info = M('User')->getUserById($buyer_uid);
		$helper_username = (($helper_user_info['nickname'] ? $helper_user_info['nickname'] : '匿名微信用户'));
		if (($this_power['allow_mobile_msg'] == 1) && $supplier_user_info['smscount']) 
		{
			if ($miaosha_user_info['phone']) 
			{
				$msg = sendtemplate::shopsmsTpl('14', array('second' => $second, 'helper_username' => $helper_username));
				$params['sms'] = array('mobile' => $miaosha_user_info['phone'], 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
				MessageFactory::method($params, array('smsMessage'));
			}
			if ($helper_user_info['phone']) 
			{
				$msg = sendtemplate::shopsmsTpl('15', array('second' => $second, 'miaosha_username' => $miaosha_username));
				$params['sms'] = array('mobile' => $helper_user_info['phone'], 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
				MessageFactory::method($params, array('smsMessage'));
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			unset($params);
			if ($miaosha_wxuser_info['openid']) 
			{
				$template_data = array('wecha_id' => $miaosha_wxuser_info['openid'], 'first' => '秒杀帮助提醒', 'keyword1' => '秒杀活动', 'keyword2' => '秒杀帮助提前秒数', 'remark' => '【' . $helper_username . '】成功帮助您提前了' . $second . '秒');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($helper_wxuser_info['openid']) 
			{
				unset($params);
				$template_data = array('wecha_id' => $helper_wxuser_info['openid'], 'first' => '秒杀帮助提醒', 'keyword1' => '秒杀活动', 'keyword2' => '秒杀帮助提前秒数', 'remark' => '您成功帮助【' . $miaosha_username . '】提前了' . $second . '秒');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function bbjNotice($store_id, $balance) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM201072360';
		$supplier_store = M('Store')->getSuppliers($store_id);
		$supplier_store_info = M('Store')->getStoreBySub($store_id);
		$supplier_user_info = M('User')->getUserById($supplier_store['uid']);
		$supplier_info_tel = (($supplier_store['tel'] ? $supplier_store['tel'] : $supplier_user_info['phone']));
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_store['store_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_user_info['smscount']) 
			{
				$msg = sendtemplate::shopsmsTpl('13', array('store_name' => $supplier_store['name']));
				$params['sms'] = array('mobile' => $supplier_info_tel, 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
				MessageFactory::method($params, array('smsMessage'));
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			unset($params);
			if ($supplier_store_info['sub_openid']) 
			{
				$template_data = array('wecha_id' => $supplier_store_info['sub_openid'], 'first' => '充值余额不足通知', 'keyword1' => '店铺名（' . $supplier_store[name] . '）', 'keyword2' => $balance, 'remark' => '您的店铺【' . $supplier_store[name] . '】充值余额不足,请及时充值');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function yydbResultNotice($success_uid, $fail_uid_arr = array(), $yydb_id) 
	{
		if (!$success_uid || !count($fail_uid_arr) || !$yydb_id) 
		{
			return false;
		}
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM201072360';
		$unitary_info = D('Unitary')->where(array('id' => $yydb_id))->find();
		$store_id = $unitary_info['store_id'];
		$supplier_store = M('Store')->getSuppliers($store_id);
		$supplier_store_info = M('Store')->getStoreBySub($store_id);
		$supplier_user_info = M('User')->getUserById($supplier_store['uid']);
		$supplier_info_tel = (($supplier_store['tel'] ? $supplier_store['tel'] : $supplier_user_info['phone']));
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($supplier_store['store_id'], $wx_tpl_no);
		$success_wxuser_info = D('Subscribe_store')->where('store_id = \'' . $store_id . '\' and uid=\'' . $success_uid . '\'')->find();
		$success_user_info = M('User')->getUserById($success_uid);
		$success_username = (($success_user_info['nickname'] ? $success_user_info['nickname'] : '匿名微信用户'));
		$fail_uid_list = implode(',', $fail_uid_arr);
		if (count($fail_uid_list)) 
		{
			$fail_user_arr_infos = D('')->table('Subscribe_store ss')->join('User u On ss.uid=u.uid', 'LEFT')->where('ss.store_id=\'' . $store_id . '\' and ss.uid in (' . $fail_uid_list . ')')->field('ss.*,u.phone')->select();
		}
		if ($this_power['allow_mobile_msg'] == 1) 
		{
		}
		if ($this_power['allow_weixin_msg']) 
		{
			if ($helper_wxuser_info['openid']) 
			{
				unset($params);
				$template_data = array('wecha_id' => $helper_wxuser_info['openid'], 'first' => '秒杀帮助提醒', 'keyword1' => '秒杀活动', 'keyword2' => '秒杀帮助提前秒数', 'remark' => '您成功帮助【' . $miaosha_username . '】提前了' . $second . '秒');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '1');
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if (count($fail_user_arr_infos)) 
			{
			}
		}
	}
	static public function fxsLimitResultNotice($store_id, $openid, $storename) 
	{
		$wx_tpl_no = 'TM204601671';
		$this_power = M('Store_notice_manage')->getStorePowerByTpl($store_id, $wx_tpl_no);
		if ($this_power['allow_weixin_msg']) 
		{
			import('source.class.Factory');
			import('source.class.MessageFactory');
			$template_data = array('wecha_id' => $openid, 'first' => '您好，您的分销商数量已达上限。', 'keynote1' => '店铺名:' . $storename, 'keynote2' => date('Y-m-d H:i:s', time()), 'remark' => '您的店铺' . $storename . '分销商数量已达上限,请及时扩展');
			$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data, 'sendType' => '0');
			MessageFactory::method($params, array('TemplateMessage'));
		}
	}
}
?>
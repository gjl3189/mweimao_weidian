
<?php
class Notice 
{
	static public function sendOut($uid, $nowOrder, $first_product_name) 
	{
		$now_store_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $nowOrder[store_id] . '\'')->field('s.uid,s.store_id,s.name,u.openid,u.phone')->find();
		$order_no = $nowOrder['order_no'];
		$buyer_info = M('User')->getUserById($uid);
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM205213550';
		$params_array = array();
		if (!$nowOrder['suppliers']) 
		{
			Notice::send_print_by_machine($nowOrder, 0);
			$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($nowOrder[store_id], $wx_tpl_no);
			if ($this_power['allow_mobile_msg'] == 1) 
			{
				if ($buyer_info['phone']) 
				{
					$params = array();
					$params_mobile = $buyer_info['phone'];
					$date = date('Y-m-d H:i:s', time());
					$msg = sendtemplate::smsTpl('3', array('nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $nowOrder['order_no']));
					$params['sms'] = array('store_id' => $now_store_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
				}
				if ($now_store_info['phone']) 
				{
					$params = array();
					$params_mobile = $now_store_info['phone'];
					$date = date('Y-m-d H:i:s', time());
					$msg = sendtemplate::smsTpl('4', array('nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $nowOrder['order_no']));
					$params['sms'] = array('store_id' => $now_store_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
				}
			}
			if ($this_power['allow_weixin_msg'] == 1) 
			{
				$params = array();
				if ($buyer_info['openid']) 
				{
					$openid = $buyer_info['openid'];
					$template_data = array('wecha_id' => $openid, 'first' => '亲，您的订单已生成，' . '稍候将通知本店客服！', 'keyword1' => date('Y年m月d日  H:i', time()), 'keyword2' => $first_product_name, 'keyword3' => $order_no, 'remark' => '状态：' . '已生成订单');
					$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
					$params_array[] = 'TemplateMessage';
					MessageFactory::method($params, $params_array);
				}
				if ($now_store_info['openid']) 
				{
					$template_data = array('wecha_id' => $now_store_info['openid'], 'first' => '亲，有顾客已下单，' . '请及时登录平台查看哦！', 'keyword1' => date('Y年m月d日  H:i', time()), 'keyword2' => $first_product_name, 'keyword3' => $order_no, 'remark' => '状态：' . '已生成订单');
					$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
					$params_array[] = 'TemplateMessage';
					MessageFactory::method($params, $params_array);
				}
			}
		}
		else 
		{
			$trueOrder = M('Order')->getUserTrueOrder($nowOrder['order_id']);
			Notice::send_print_by_machine($trueOrder, 0);
			$Store_list = D('')->table('Store s')->join('User u on u.uid=s.uid')->where('s.store_id in (' . $nowOrder[suppliers] . ')')->field('s.uid,s.store_id,s.name,u.openid,u.phone,u.smscount')->order('u.smscount desc')->select();
			$status_mobile = 0;
			$status_weixin = 0;
			foreach ($Store_list as $order ) 
			{
				$gh_uid_id_arr[] = $order['uid'];
				$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($order[store_id], $wx_tpl_no);
				if ($this_power['allow_mobile_msg'] == 1) 
				{
					if ($buyer_info['phone']) 
					{
						$params = array();
						$params_mobile = $buyer_info['phone'];
						$date = date('Y-m-d H:i:s', time());
						$msg = sendtemplate::smsTpl('3', array('nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $order_no));
						$params['sms'] = array('store_id' => $order[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
						$params_array[] = 'smsMessage';
						MessageFactory::method($params, $params_array);
					}
					if ($order['phone']) 
					{
						$params = array();
						$params_mobile = $order['phone'];
						$date = date('Y-m-d H:i:s', time());
						$msg = sendtemplate::smsTpl('4', array('nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $order_no));
						$params['sms'] = array('store_id' => $order[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
						$params_array[] = 'smsMessage';
						MessageFactory::method($params, $params_array);
					}
				}
				if ($this_power['allow_weixin_msg'] == 1) 
				{
					$params = array();
					if ($buyer_info['openid']) 
					{
						$openid = $buyer_info['openid'];
						$template_data = array('wecha_id' => $openid, 'first' => '亲，您的订单已生成，' . '稍候将通知本店客服！', 'keyword1' => date('Y年m月d日  H:i', time()), 'keyword2' => $first_product_name, 'keyword3' => $order_no, 'remark' => '状态：' . '已生成订单');
						$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
						$params_array[] = 'TemplateMessage';
						MessageFactory::method($params, $params_array);
					}
					if ($order['openid']) 
					{
						$template_data = array('wecha_id' => $order['openid'], 'first' => '亲，有顾客已下单，' . '请及时登录平台查看哦！', 'keyword1' => date('Y年m月d日  H:i', time()), 'keyword2' => $first_product_name, 'keyword3' => $order_no, 'remark' => '状态：' . '已生成订单');
						$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
						$params_array[] = 'TemplateMessage';
						MessageFactory::method($params, $params_array);
					}
				}
			}
		}
	}
	static public function OrderPaymentSuccess($nowOrder, $first_product_name) 
	{
		$uid = (($uid ? $uid : $nowOrder['uid']));
		$ctotal = (($nowOrder['total'] ? $nowOrder['total'] . '元' : ''));
		$now_store_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $nowOrder[store_id] . '\'')->field('s.uid,s.store_id,s.name,u.openid,u.phone')->find();
		$buyer_info = M('User')->getUserById($uid);
		import('source.class.Factory');
		import('source.class.MessageFactory');
		$wx_tpl_no = 'OPENTM201752540';
		$wx_tpl_no2 = 'OPENTM206328960';
		$params_array = array();
		$gh_uid_id_arr = array();
		$used_smscount_uid = array();
		import('source.class.sendtemplate');
		if (($nowOrder['suppliers'] == $nowOrder['store_id']) || empty($nowOrder['suppliers'])) 
		{
			Notice::send_print_by_machine($nowOrder, 1);
			$top_store_info = $nowOrder;
			$is_fx_store = 0;
			$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($nowOrder[store_id], $wx_tpl_no);
			if ($this_power['allow_mobile_msg'] == 1) 
			{
				if ($buyer_info['phone']) 
				{
					$params = array();
					$params_mobile = $buyer_info['phone'];
					$date = date('Y-m-d H:i:s', time());
					$msg = sendtemplate::smsTpl('5', array('storename' => $now_store_info[name], 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $nowOrder['order_no']));
					$params['sms'] = array('store_id' => $gh_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
				}
				if ($now_store_info['phone']) 
				{
					$params = array();
					$params_mobile = $now_store_info['phone'];
					$date = date('Y-m-d H:i:s', time());
					$msg = sendtemplate::smsTpl('6', array('storename' => $now_store_info[name], 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $nowOrder['order_no']));
					$params['sms'] = array('store_id' => $gh_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
				}
			}
			if ($this_power['allow_weixin_msg'] == 1) 
			{
				if ($buyer_info['openid']) 
				{
					$openid = $buyer_info['openid'];
					$template_data = array('wecha_id' => $openid, 'first' => '亲，您的订单已支付完成，' . '稍候将通知本店客服！', 'keyword1' => $first_product_name, 'keyword2' => $nowOrder['order_no'], 'keyword3' => $ctotal, 'keyword4' => date('Y年m月d日  H:i', time()), 'remark' => '状态：' . '已支付完成');
					$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
					$params_array[] = 'TemplateMessage';
					MessageFactory::method($params, $params_array);
				}
				if ($now_store_info['openid']) 
				{
					$template_data = array('wecha_id' => $now_store_info['openid'], 'first' => '亲，有顾客已下单，' . '请及时登录平台查看哦！', 'keyword1' => $first_product_name, 'keyword2' => $nowOrder['order_no'], 'keyword3' => $ctotal, 'keyword4' => date('Y年m月d日  H:i', time()), 'remark' => '状态：' . '已支付完成');
					$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
					$params_array[] = 'TemplateMessage';
					MessageFactory::method($params, $params_array);
				}
			}
		}
		else 
		{
			$order_list = D('')->table('Order o')->join('Store s on o.store_id=s.store_id')->join('User u on u.uid=s.uid')->where(' o.store_id in (o.suppliers) and o.user_order_id = \'' . $nowOrder[order_id] . '\' and o.order_id !=\'' . $nowOrder[order_id] . '\' ')->field('o.*, s.uid,s.store_id,s.name,u.openid,u.phone,u.smscount')->order('u.smscount desc')->select();
			$status_mobile = 0;
			$status_weixin = 0;
			foreach ($order_list as $order ) 
			{
				Notice::send_print_by_machine($order, 1);
				$gh_uid_id_arr[] = $order['uid'];
				$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($order[store_id], $wx_tpl_no);
				if ($this_power['allow_mobile_msg'] == 1) 
				{
					$params = array();
					$params_mobile = $order['phone'];
					$date = date('Y-m-d H:i:s', time());
					$msg = sendtemplate::smsTpl('6', array('storename' => '【分销店铺:' . $now_store_info[name] . '】', 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $order['order_no']));
					$params['sms'] = array('store_id' => $order[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
					if ($status_mobile == 0) 
					{
						$status_mobile = 1;
						$used_smscount_uid[] = $order['uid'];
						if ($buyer_info['phone']) 
						{
							$params = array();
							$params_mobile = $buyer_info['phone'];
							$date = date('Y-m-d H:i:s', time());
							$msg = sendtemplate::smsTpl('5', array('storename' => $now_store_info[name], 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $nowOrder['order_no']));
							$params['sms'] = array('store_id' => $order[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
							$params_array[] = 'smsMessage';
							MessageFactory::method($params, $params_array);
						}
						if ($now_store_info['phone']) 
						{
							$params = array();
							$params_mobile = $now_store_info['phone'];
							$date = date('Y-m-d H:i:s', time());
							$msg = sendtemplate::smsTpl('6', array('storename' => $now_store_info[name], 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $nowOrder['order_no']));
							$params['sms'] = array('store_id' => $gh_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
							$params_array[] = 'smsMessage';
							MessageFactory::method($params, $params_array);
						}
					}
				}
				if ($this_power['allow_weixin_msg'] == 1) 
				{
					$template_data = array('wecha_id' => $order['openid'], 'first' => '亲，有顾客已下单，' . '请及时登录平台查看哦！', 'keyword1' => $first_product_name, 'keyword2' => $order['order_no'], 'keyword3' => $order['total'], 'keyword4' => date('Y年m月d日  H:i', time()), 'remark' => '状态：' . '已支付完成');
					$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
					$params_array[] = 'TemplateMessage';
					MessageFactory::method($params, $params_array);
					if ($status_weixin == 0) 
					{
						$status_weixin = 1;
						$params = array();
						if ($buyer_info['openid']) 
						{
							$openid = $buyer_info['openid'];
							$template_data = array('wecha_id' => $openid, 'first' => '亲，您的订单已支付完成，' . '稍候将通知本店客服！', 'keyword1' => $first_product_name, 'keyword2' => $nowOrder['order_no'], 'keyword3' => $ctotal, 'keyword4' => date('Y年m月d日  H:i', time()), 'remark' => '状态：' . '已支付完成');
							$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
							$params_array[] = 'TemplateMessage';
							MessageFactory::method($params, $params_array);
						}
						$profit = D('Financial_record')->where(array('order_id' => $nowOrder['order_id']))->sum('income');
						if ($now_store_info['openid']) 
						{
							$template_data = array('wecha_id' => $now_store_info['openid'], 'first' => '亲，您分销的商品有买家已支付成功哦！', 'keyword1' => $first_product_name, 'keyword2' => $profit, 'keyword3' => '已支付完成', 'remark' => '您可以进入【' . $now_store_info[name] . '】微店，了解更多详情！');
							$params['template'] = array('template_id' => $wx_tpl_no2, 'template_data' => $template_data);
							$params_array[] = 'TemplateMessage';
							MessageFactory::method($params, $params_array);
						}
					}
				}
			}
			$result = array_diff($gh_uid_id_arr, $used_smscount_uid);
			if (count($result)) 
			{
				M('User')->deduct_sms($result, 3);
			}
		}
	}
	static public function orderComplete($order_info) 
	{
		$now = time();
		$buyer_order_info = M('Order')->getUserTrueOrder($order_info['order_id']);
		$buyer_info = M('User')->getUserById($buyer_order_info['uid']);
		$buyer_by_store_info = M('Store')->getStore($buyer_order_info['store_id']);
		$ctotal = (($order_info['total'] ? $order_info['total'] . '元' : ''));
		$top_store_info = M('Store')->getSuppliers($order_info['store_id']);
		import('source.class.Factory');
		import('source.class.MessageFactory');
		$wx_tpl_no = 'OPENTM202521011';
		$params_array = array();
		$store_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $top_store_info[store_id] . '\'')->field('s.uid,s.store_id,s.name,u.openid,u.phone')->find();
		$storename = (($buyer_by_store_info['name'] ? $buyer_by_store_info['name'] : $store_name['name']));
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($top_store_info[store_id], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			import('source.class.sendtemplate');
			if ($buyer_info['phone']) 
			{
				$params = array();
				$params_mobile = $buyer_info['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('7', array('storename' => $storename, 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $order_info['order_no']));
				$params['sms'] = array('store_id' => $top_store_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($store_info['phone']) 
			{
				$params = array();
				$params_mobile = $store_info['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('8', array('storename' => $storename, 'nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $order_info['order_no']));
				$params['sms'] = array('store_id' => $top_store_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			$params = array();
			if ($buyer_info['openid']) 
			{
				$openid = $buyer_info['openid'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您在【店铺:' . $storename . '】的订单已交易完成 ！', 'keyword1' => $buyer_order_info['order_no'], 'keyword2' => date('Y年m月d日  H:i', $now), 'remark' => '状态：' . '已交易完成');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($store_info['openid']) 
			{
				$template_data = array('wecha_id' => $store_info['openid'], 'first' => '亲，顾客的下单，已交易完成，' . '请及时登录平台核对！', 'keyword1' => $order_info['order_no'], 'keyword2' => date('Y年m月d日  H:i', $now), 'remark' => '状态：' . '已交易完成');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($order_info['fx_order_id'] != '0') 
		{
			Notice::FxOrderProfit($order_info);
		}
	}
	static public function OrderShipped($order_info, $store_id, $express_company, $express_no) 
	{
		$express_company = (($express_company ? $express_company : '无物流'));
		$express_no = (($express_no ? $express_no : '无运单号'));
		$buyer_order_info = M('Order')->getUserTrueOrder($order_info['order_id']);
		$buy_user = M('User')->getUserById($buyer_order_info['uid']);
		import('source.class.Factory');
		import('source.class.MessageFactory');
		$wx_tpl_no = 'OPENTM200565259';
		$params_array = array();
		$store_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $order_info[store_id] . '\'')->field('s.uid,s.store_id,u.openid,u.phone')->find();
		$gonghuostore_id = $store_id;
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($gonghuostore_id, $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			import('source.class.sendtemplate');
			if ($buy_user['phone']) 
			{
				$params = array();
				$params_mobile = $buy_user['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('1', array('express_company' => $express_company, 'express_no' => $express_no));
				$params['sms'] = array('store_id' => $gonghuostore_id, 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($store_info['phone']) 
			{
				$params = array();
				$params_mobile = $store_info['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('2', array('express_company' => $express_company, 'express_no' => $express_no));
				$params['sms'] = array('store_id' => $gonghuostore_id, 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			$params = array();
			if ($buy_user['openid']) 
			{
				$openid = $buy_user['openid'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您的宝贝已发货，' . '请注意查收，有问题请与本店联系！', 'keyword1' => $buyer_order_info['order_no'], 'keyword2' => $express_company, 'keyword3' => $express_no, 'remark' => '状态：' . '已发货');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($store_info['openid']) 
			{
				$template_data = array('wecha_id' => $store_info['openid'], 'first' => '亲，包裹已创建发货，' . '请注意提醒买家收货哦！', 'keyword1' => $order_info['order_no'], 'keyword2' => $express_company, 'keyword3' => $express_no, 'remark' => '状态：' . '已发货');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function FxOrderPaymentSuccess() 
	{
	}
	static public function FxOrderProfit($order_info) 
	{
		if (($order_info['user_order_id'] == '') || ($order_info['user_order_id'] == '0') || ($order_info['user_order_id'] == $order_info['order_id'])) 
		{
			return false;
		}
		$user_order_id = $order_info['user_order_id'];
		$profit = D('Financial_record')->where(array('order_id' => $user_order_id))->sum('income');
		$seller_order_info = M('Order')->get(array('order_id' => $user_order_id));
		$wx_tpl_no = 'OPENTM220197216';
		$supplier_store_id = $order_info['store_id'];
		$seller_store_id = $seller_order_info['store_id'];
		$supplier_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $order_info[store_id] . '\'')->field('s.uid,s.store_id,u.openid,u.phone')->find();
		$seller_info = D('')->table('User u')->join('Store s On u.uid = s.uid')->where('s.store_id = \'' . $seller_store_id . '\'')->field('s.uid,s.store_id,u.openid,u.phone')->find();
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($supplier_info['store_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_info['phone']) 
			{
				$params = array();
				$params_mobile = $supplier_info['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('11', array('supplier_info' => $supplier_info, 'supplier_order_info' => $order_info, 'seller_info' => $seller_info, 'seller_order_info' => $seller_order_info, 'profit' => $profit));
				$params['sms'] = array('store_id' => $order_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($seller_info['phone']) 
			{
				$params = array();
				$params_mobile = $seller_info['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('12', array('supplier_info' => $supplier_info, 'supplier_order_info' => $order_info, 'seller_info' => $seller_info, 'seller_order_info' => $seller_order_info, 'profit' => $profit));
				$params['sms'] = array('store_id' => $order_info[store_id], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			$params = array();
			if (!empty($supplier_info['openid'])) 
			{
				$params = array();
				$template_data = array('wecha_id' => $supplier_info['openid'], 'first' => '您好，您名下分销商的订单分配利润成功。', 'keyword1' => $order_info['order_no'], 'keyword2' => $order_info['total'], 'keyword3' => $profit, 'keyword4' => date('Y-m-d H:i'), 'remark' => '状态：分润成功');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				MessageFactory::method($params, array('TemplateMessage'));
			}
			if (!empty($seller_info['openid'])) 
			{
				$params = array();
				$template_data = array('wecha_id' => $seller_info['openid'], 'first' => '您好，您分销商品的订单分配利润完毕', 'keyword1' => $seller_order_info['order_no'], 'keyword2' => $seller_order_info['total'], 'keyword3' => $profit, 'keyword4' => date('Y-m-d H:i'), 'remark' => '状态：分润成功');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				MessageFactory::method($params, array('TemplateMessage'));
			}
		}
	}
	static public function FxStoreApplication($supplier_store, $top_store_info, $tmp_user, $remark, $tel, $name, $store_id) 
	{
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM207126233';
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($top_store_info[store_id], $wx_tpl_no);
		$supplier_wecha_id = ((!empty($tmp_user['openid']) ? $tmp_user['openid'] : ''));
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			$supplier_mobile = (($tmp_user['phone'] ? $tmp_user['phone'] : $top_store_info['tel']));
			if (preg_match('/^1[0-9]{10}$/', $supplier_mobile)) 
			{
				$params = array();
				$msg = sendtemplate::smsTpl('9', array('storename' => $now_store_info[name]));
				$params['sms'] = array('mobile' => $supplier_mobile, 'store_id' => $top_store_info['store_id'], 'token' => 'test', 'content' => $msg);
				MessageFactory::method($params, array('smsMessage'));
			}
			$user_mobile = $tel;
			if (preg_match('/^1[0-9]{10}$/', $supplier_mobile)) 
			{
				$params = array();
				$msg = sendtemplate::smsTpl('10', array('storename' => $supplier_store[name]));
				$params['sms'] = array('mobile' => $user_mobile, 'store_id' => $top_store_info['store_id'], 'token' => 'test', 'content' => $msg);
				MessageFactory::method($params, array('smsMessage'));
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			$params = array();
			if (!empty($supplier_wecha_id)) 
			{
				$params = array();
				$template_data = array('wecha_id' => $supplier_wecha_id, 'first' => '您好，恭喜您成为 【' . $name . '】 的供货商。', 'keyword1' => $name, 'keyword2' => $tel, 'keyword3' => date('Y-m-d H:i:s'), 'remark' => '状态：' . $remark);
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			$seller_wecha_id = ((!empty($_SESSION['wap_user']['openid']) ? $_SESSION['wap_user']['openid'] : $_SESSION['openid']));
			if (!empty($seller_wecha_id)) 
			{
				$params = array();
				$template_data = array('href' => option('config.wap_site_url') . '/home.php?id=' . $store_id, 'wecha_id' => $seller_wecha_id, 'first' => '您好，恭喜您成为 【' . $supplier_store['name'] . '】 的分销商。', 'keyword1' => $name, 'keyword2' => $tel, 'keyword3' => date('Y-m-d H:i:s'), 'remark' => '状态：' . $remark);
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function bbjNotice($store_id, $balance) 
	{
		$param_array = array();
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM201072360';
		$supplier_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $store_id . '\'')->field('s.uid,s.name,s.store_id,u.openid,u.phone,u.smscount')->find();
		$supplier_info_tel = $supplier_info['phone'];
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($store_id, $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($supplier_info['smscount']) 
			{
				$msg = sendtemplate::smsTpl('13', array('store_name' => $supplier_info['name']));
				$params['sms'] = array('mobile' => $supplier_info_tel, 'store_id' => $store_id, 'token' => 'test', 'content' => $msg);
				MessageFactory::method($params, array('smsMessage'));
			}
		}
		if ($this_power['allow_weixin_msg']) 
		{
			unset($params);
			if ($supplier_info['openid']) 
			{
				$template_data = array('wecha_id' => $supplier_info['openid'], 'first' => '充值余额不足通知', 'keyword1' => '店铺名（' . $supplier_info['name'] . '）', 'keyword1' => '店铺名', 'keyword2' => $balance, 'remark' => '您的店铺【' . $supplier_info['name'] . '】充值余额不足,请及时充值');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array['template'] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function send_print_by_machine($nowOrder, $type_status = '0') 
	{
		$now_store_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $nowOrder[store_id] . '\'')->field('s.uid,s.store_id,s.name,u.openid,u.phone')->find();
		import('source.class.ArrayToStr');
		import('source.class.Factory');
		import('source.class.MessageFactory');
		$tmparr = '';
		$company['name'] = $now_store_info['name'];
		$order_no = $nowOrder['order_no'];
		$order_id = $nowOrder['order_id'];
		$order_infos = D('Order')->where('order_id = \'' . $order_id . '\'')->find();
		$uid = $nowOrder['uid'];
		$product_list = D('')->table('Order_product as op')->join('Product p ON p.product_id = op.product_id', 'left')->where('op.order_id=' . $order_id)->field('op.pro_price,op.pro_num,p.name,p.product_id ')->select();
		foreach ($product_list as $k => &$v ) 
		{
			$v['price'] = $v['pro_price'];
			$v['num'] = $v['pro_num'];
		}
		$order = $nowOrder;
		$shipping_method = '';
		if ($order['shipping_method'] == 'express') 
		{
			$shipping_method = '快递发送';
		}
		else if ($order['shipping_method'] == 'selffetch') 
		{
			$shipping_method = '上门自提';
		}
		$status = '';
		if ($order_infos['status'] == '1') 
		{
			$status = '未支付';
		}
		else if ($order_infos['status'] == 2) 
		{
			$status = '未发货';
		}
		else if ($order_infos['status'] == 3) 
		{
			$status = '已发货';
		}
		else if ($order_infos['status'] == 4) 
		{
			$status = '已完成';
		}
		else if ($order_infos['status'] == 5) 
		{
			$status = '已取消';
		}
		else if ($order_infos['status'] == 6) 
		{
			$status = '退款中';
		}
		else if ($order_infos['status'] == 7) 
		{
			$status = '已收货';
		}
		$address = ((!empty($order['address']) ? unserialize($order['address']) : array()));
		if (count($address_detail)) 
		{
			$address_detail = $address['province'] . ' ' . $address['city'] . ' ' . $address['area'] . ' ' . $address['address'];
		}
		else 
		{
			$address_detail = '';
		}
		$nickname = (($order['address_user'] ? $order['address_user'] : ''));
		$phone = (($order['address_tel'] ? $order['address_tel'] : ''));
		$msg = array('des' => trim($_POST['omark']), 'status' => $status, 'truename' => trim($nickname), 'tel' => trim($phone), 'address' => trim($address_detail), 'buytime' => $order['add_time'], 'orderid' => $order['order_no'], 'price' => $order['sub_total'], 'total' => $order['total'], 'shipping_method' => $shipping_method, 'list' => $product_list, 'type_status' => $type_status);
		$msg = ArrayToStr::array_to_str($msg, 0);
		$store_id = $now_store_info['store_id'];
		$companyid = 222;
		$paid = '1';
		$qr = '';
		$type = '222222';
		if (($type_status == '0') || ($type_status == '')) 
		{
			$type_status = 2;
		}
		$arrays = array('store_id' => $store_id, 'companyid' => $companyid, 'content' => $msg, 'paid' => $type_status, 'qr' => $qr, 'type' => $type);
		$params['printer'] = $arrays;
		$return = MessageFactory::method($params, array('printerMessage'));
	}
	static public function sendOffline($order_offline) 
	{
		$now_store_info = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $order_offline['store_id'] . '\'')->field('s.uid, s.store_id, s.name, u.openid, u.phone')->find();
		if (empty($now_store_info)) 
		{
			return NULL;
		}
		$order_no = $order_offline['order_no'];
		$buyer_info = M('User')->getUserById($order_offline['uid']);
		import('source.class.Factory');
		import('source.class.MessageFactory');
		import('source.class.sendtemplate');
		$wx_tpl_no = 'OPENTM205213550';
		$params_array = array();
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($order_offline['store_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			if ($buyer_info['phone']) 
			{
				$params = array();
				$params_mobile = $buyer_info['phone'];
				$date = date('Y-m-d H:i:s', time());
				$msg = sendtemplate::smsTpl('3', array('nickname' => $buyer_info['nickname'], 'openid' => $buyer_info['openid'], 'order_no' => $order_offline['order_no']));
				$params['sms'] = array('store_id' => $now_store_info['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			$params = array();
			if ($buyer_info['openid']) 
			{
				$openid = $buyer_info['openid'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您的线下手工做单已生成', 'keyword1' => date('Y年m月d日  H:i', time()), 'keyword2' => '来自店铺【' . $now_store_info['name'] . '】的线下手工做单', 'keyword3' => '订单号：' . $order_no . ',订单金额：' . $order_offline['total'], 'remark' => '状态：已生成订单');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($now_store_info['openid']) 
			{
				$template_data = array('wecha_id' => $now_store_info['openid'], 'first' => '亲，您的线下手工做单已生成', 'keyword1' => date('Y年m月d日  H:i', time()), 'keyword2' => $order_offline['product_name'], 'keyword3' => $order_no, 'remark' => '状态：已生成订单');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function orderOfflineComplete($order_offline) 
	{
		$now = time();
		$user = M('User')->getUserById($order_offline['uid']);
		import('source.class.Factory');
		import('source.class.MessageFactory');
		$wx_tpl_no = 'OPENTM202521011';
		$params_array = array();
		$store = D('')->table('User u')->join('Store s ON u.uid = s.uid')->where('s.store_id = \'' . $order_offline['store_id'] . '\'')->field('s.uid, s.store_id, s.name, u.openid, u.phone')->find();
		$this_power = M('Store_system_notice_manage')->getStorePowerByTpl($order_offline['store_id'], $wx_tpl_no);
		if ($this_power['allow_mobile_msg'] == 1) 
		{
			import('source.class.sendtemplate');
			if ($user['phone']) 
			{
				$params = array();
				$params_mobile = $user['phone'];
				$date = date('Y-m-d H:i:s', $now);
				$msg = sendtemplate::smsTpl('7', array('storename' => $store['name'], 'nickname' => $user['nickname'], 'openid' => $user['openid'], 'order_no' => $order_offline['order_no']));
				$params['sms'] = array('store_id' => $order_offline['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($store['phone']) 
			{
				$params = array();
				$params_mobile = $store['phone'];
				$date = date('Y-m-d H:i:s', $now);
				$msg = sendtemplate::smsTpl('8', array('storename' => $store['name'], 'nickname' => $user['nickname'], 'openid' => $user['openid'], 'order_no' => $order_offline['order_no']));
				$params['sms'] = array('store_id' => $order_offline['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($this_power['allow_weixin_msg'] == 1) 
		{
			$params = array();
			if ($user['openid']) 
			{
				$openid = $user['openid'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您在【店铺:' . $store['name'] . '】的店铺手工做单已交易完成 ！获得消费积分为：' . $order_offline['return_point'], 'keyword1' => $order_offline['order_no'], 'keyword2' => date('Y年m月d日  H:i', $now), 'remark' => '状态：已交易完成');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			if ($store['openid']) 
			{
				$template_data = array('wecha_id' => $store['openid'], 'first' => '亲，您的手工做单已交易完成', 'keyword1' => $order_offline['order_no'], 'keyword2' => date('Y年m月d日  H:i', $now), 'remark' => '状态：已交易完成');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
	static public function platform_to_agent($store_id, $agent_id, $area_manager_id, $data) 
	{
		$wx_tpl_no = 'OPENTM401560049';
		$storeInfo = D('Store')->where(array('store_id' => $store_id))->find();
		$userInfo = D('User')->where(array('uid' => $storeInfo))->find();
		$tel = (($storeInfo['tel'] ? $storeInfo['tel'] : $userInfo['phone']));
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Factory.class.php';
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'MessageFactory.class.php';
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'sendtemplate.class.php';
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Sms.class.php';
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templateNews.class.php';
		if ($storeInfo) 
		{
			if (($data == 1) && $tel) 
			{
				$params = array();
				$params_mobile = $tel;
				$msg = sendtemplate::smsTpl('15', array('store_name' => $storeInfo['name'], 'state' => '成功'));
				$params['sms'] = array('store_id' => $storeInfo['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			else if (($data == 3) && $tel) 
			{
				$params = array();
				$params_mobile = $tel;
				$msg = sendtemplate::smsTpl('15', array('store_name' => $storeInfo['name'], 'state' => '失败'));
				$params['sms'] = array('store_id' => $storeInfo['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			if (($data == 1) && $userInfo['openid']) 
			{
				$params = array();
				$openid = $userInfo['openid'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您的【店铺:' . $storeInfo['name'] . '】平台审核通过', 'keyword1' => $storeInfo['store_id'], 'keyword2' => $storeInfo['name'], 'keyword3' => $storeInfo['linkman'], 'keyword4' => $tel, 'keyword5' => date('Y-m-d H:i', $storeInfo['date_added']), 'remark' => '请登录系统查看详情');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			else if (($data == 3) && $userInfo['openid']) 
			{
				$params = array();
				$openid = $userInfo['openid'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您的【店铺:' . $storeInfo['name'] . '】平台审核未通过', 'keyword1' => $storeInfo['store_id'], 'keyword2' => $storeInfo['name'], 'keyword3' => $storeInfo['linkman'], 'keyword4' => $tel, 'keyword5' => date('Y-m-d H:i', $storeInfo['date_added']), 'remark' => '请登录系统查看详情');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($agent_id) 
		{
			$agentInfo = D('Admin')->where(array('id' => $agent_id))->find();
			if ($agentInfo['phone'] && ($data == 1)) 
			{
				$params = array();
				$params_mobile = $agentInfo['phone'];
				$msg = sendtemplate::smsTpl('16', array('store_name' => $storeInfo['name'], 'state' => '成功'));
				$params['sms'] = array('store_id' => $storeInfo['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			else 
			{
				if ($agentInfo['phone'] && ($data == 3)) 
				{
					$params = array();
					$params_mobile = $agentInfo['phone'];
					$msg = sendtemplate::smsTpl('16', array('store_name' => $storeInfo['name'], 'state' => '失败'));
					$params['sms'] = array('store_id' => $storeInfo['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
				}
			}
			if (($data == 1) && $agentInfo['open_id']) 
			{
				$params = array();
				$openid = $agentInfo['open_id'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您所代理的【店铺:' . $storeInfo['name'] . '】平台审核通过', 'keyword1' => $storeInfo['store_id'], 'keyword2' => $storeInfo['name'], 'keyword3' => $storeInfo['linkman'], 'keyword4' => $tel, 'keyword5' => date('Y-m-d H:i', $storeInfo['date_added']), 'remark' => '请登录系统查看详情');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			else if (($data == 3) && $agentInfo['open_id']) 
			{
				$params = array();
				$openid = $agentInfo['open_id'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您所代理的【店铺:' . $storeInfo['name'] . '】平台审核未通过', 'keyword1' => $storeInfo['store_id'], 'keyword2' => $storeInfo['name'], 'keyword3' => $storeInfo['linkman'], 'keyword4' => $tel, 'keyword5' => date('Y-m-d H:i', $storeInfo['date_added']), 'remark' => '请登录系统查看详情');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
		if ($area_manager_id) 
		{
			$managerInfo = D('Admin')->where(array('id' => $area_manager_id))->find();
			if ($managerInfo['phone'] && ($data == 1)) 
			{
				$params = array();
				$params_mobile = $managerInfo['phone'];
				$msg = sendtemplate::smsTpl('14', array('store_name' => $storeInfo['name'], 'state' => '成功'));
				$params['sms'] = array('store_id' => $storeInfo['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
				$params_array[] = 'smsMessage';
				MessageFactory::method($params, $params_array);
			}
			else 
			{
				if ($managerInfo['phone'] && ($data == 3)) 
				{
					$params = array();
					$params_mobile = $managerInfo['phone'];
					$msg = sendtemplate::smsTpl('14', array('store_name' => $storeInfo['name'], 'state' => '失败'));
					$params['sms'] = array('store_id' => $storeInfo['store_id'], 'mobile' => $params_mobile, 'token' => 'test', 'content' => $msg, 'sendType' => '');
					$params_array[] = 'smsMessage';
					MessageFactory::method($params, $params_array);
				}
			}
			if (($data == 1) && $managerInfo['open_id']) 
			{
				$params = array();
				$openid = $managerInfo['open_id'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您所管理区域内的【店铺:' . $storeInfo['name'] . '】平台审核通过', 'keyword1' => $storeInfo['store_id'], 'keyword2' => $storeInfo['name'], 'keyword3' => $storeInfo['linkman'], 'keyword4' => $tel, 'keyword5' => date('Y-m-d H:i', $storeInfo['date_added']), 'remark' => '请登录系统查看详情');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
			else if (($data == 3) && $managerInfo['open_id']) 
			{
				$params = array();
				$openid = $managerInfo['open_id'];
				$template_data = array('wecha_id' => $openid, 'first' => '亲，您所管理区域内的【店铺:' . $storeInfo['name'] . '】平台审核未通过', 'keyword1' => $storeInfo['store_id'], 'keyword2' => $storeInfo['name'], 'keyword3' => $storeInfo['linkman'], 'keyword4' => $tel, 'keyword5' => date('Y-m-d H:i', $storeInfo['date_added']), 'remark' => '请登录系统查看详情');
				$params['template'] = array('template_id' => $wx_tpl_no, 'template_data' => $template_data);
				$params_array[] = 'TemplateMessage';
				MessageFactory::method($params, $params_array);
			}
		}
	}
}
?>
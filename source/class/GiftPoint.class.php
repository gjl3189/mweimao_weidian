
<?php
class GiftPoint 
{
	public function __construct($product_list = array(), $config = array()) 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
	}
	static public function order($point, $order_id, $store_id = '', $uid = '') 
	{
		if (empty($point) || empty($order_id) || empty($store_id)) 
		{
			return false;
		}
		$data = array('point' => $point, 'order_id' => $order_id, 'store_id' => $store_id, 'uid' => $uid, 'point_type' => 3, 'add_time' => time());
		D('Platform_user_points_log')->data($data)->add();
		D('User')->where(array('uid' => $uid))->setDec('point_gift', $point);
		D('User')->where(array('uid' => $uid))->setInc('spend_point_gift', $point);
	}
	static public function pointToUser($user_id) 
	{
		if (empty($user_id)) 
		{
			return false;
		}
		$platform_config = D('Credit_setting')->field('follow_platform_credit')->find();
		if (!empty($platform_config['follow_platform_credit'])) 
		{
			$result = D('User')->where(array('uid' => $user_id))->setInc('point_gift', $platform_config['follow_platform_credit']);
			if ($result) 
			{
				$data['user_id'] = $user_id;
				$data['point'] = $platform_config['follow_platform_credit'];
				$data['point_type'] = 1;
				$data['add_time'] = time();
				D('Platform_user_points_log')->data($data)->add();
			}
			$data = array('point' => $point, 'order_id' => 0, 'store_id' => 0, 'uid' => $user_id, 'point_type' => 2, 'add_time' => time());
			D('Platform_user_points_log')->data($data)->add();
		}
	}
}
?>
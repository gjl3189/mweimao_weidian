
<?php
class Page 
{
	public $firstRow;
	public $nowPage;
	public $totalPage;
	public $totalRows;
	public $listRows;
	public function __construct($totalRows, $listRows, $nowPage = '') 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		$this->totalRows = $totalRows;
		$this->nowPage = (!empty($_POST['p']) ? intval($_POST['p']) : 1);
		if (!empty($nowPage)) 
		{
			$this->nowPage = $nowPage;
		}
		$this->listRows = $listRows;
		$this->totalPage = ceil($totalRows / $listRows);
		if (($this->totalPage < $this->nowPage) && (0 < $this->totalPage)) 
		{
			$this->nowPage = $this->totalPage;
		}
		$this->firstRow = $listRows * ($this->nowPage - 1);
	}
	public function show() 
	{
		if ($this->totalRows == 0) 
		{
			return false;
		}
		$now = $this->nowPage;
		$total = $this->totalPage;
		$str = '<a href="javascript:void(0)" class="w-button w-button-aside w-button-disabled"><span style="color:#333">共 ' . $this->totalRows . ' 条，每页 ' . $this->listRows . ' 条</span></a>';
		if ($total == 1) 
		{
			return $str;
		}
		if (1 < $now) 
		{
			$str .= '<a class="prev fetch_page w-button w-button-aside" data-page-num="' . ($now - 1) . '" href="javascript:void(0);"><span>上一页</span></a>';
		}
		if (($now != 1) && (4 < $now) && (6 < $total)) 
		{
			$str .= ' ... ';
		}
		$i = 1;
		while ($i <= 5) 
		{
			if ($now <= 1) 
			{
				$page = $i;
			}
			else if (($total - 1) < $now) 
			{
				$page = ($total - 5) + $i;
			}
			else 
			{
				$page = ($now - 3) + $i;
			}
			if (($page != $now) && (0 < $page)) 
			{
				if ($page <= $total) 
				{
					$str .= '<a class="fetch_page num w-button w-button-aside" data-page-num="' . $page . '" href="javascript:void(0);"><span>' . $page . '</span></a>';
				}
				else 
				{
					break;
				}
			}
			else if ($page == $now) 
			{
				$str .= '<a class="num active w-button w-button-main" data-page-num="' . $page . '" href="javascript:void(0);"><span>' . $page . '</span></a>';
			}
			++$i;
		}
		if ($now != $total) 
		{
			$str .= '<a class="fetch_page next w-button w-button-aside" data-page-num="' . ($now + 1) . '" href="javascript:void(0);">下一页</a>';
		}
		return $str;
	}
}
?>
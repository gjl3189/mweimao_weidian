
<?php
class Points 
{
	static public function upDegree($uid, $store_id, $degree_id) 
	{
		if (empty($uid) || empty($store_id) || empty($degree_id)) 
		{
			return array('err_code' => 1000, 'err_msg' => '缺少参数');
		}
		$user_degree = D('User_degree')->where(array('store_id' => $store_id, 'id' => $degree_id))->find();
		if (empty($user_degree)) 
		{
			return array('err_code' => 1000, 'err_msg' => '未找到相应的等级');
		}
		$store_user_data = M('Store_user_data')->getUserData($store_id, $uid);
		if ($store_user_data['point'] < $user_degree['points_limit']) 
		{
			return array('err_code' => 1000, 'err_msg' => '您的积分不够，不能升级');
		}
		$month = 12;
		if ($user_degree['degree_month']) 
		{
			$month = $user_degree['degree_month'];
		}
		$date = date('Ymd', strtotime('+' . $month . 'month'));
		$data = array();
		$data['degree_date'] = $date;
		$data['degree_id'] = $user_degree['id'];
		$data['point'] = $store_user_data['point'] - $user_degree['points_limit'];
		if (D('Store_user_data')->where(array('store_id' => $store_id, 'uid' => $uid))->data($data)->save()) 
		{
			$data = array();
			$data['uid'] = $uid;
			$data['store_id'] = $store_id;
			$data['points'] = -1 * $user_degree['points_limit'];
			$data['type'] = 11;
			$data['is_available'] = 1;
			$data['timestamp'] = time();
			D('User_points')->data($data)->add();
			return array('err_code' => 0, 'err_msg' => '升级成功');
		}
		return array('err_code' => 1000, 'err_msg' => '升级失败');
	}
	static public function sign($uid = 0, $store_id = 0, $seller_id = 0) 
	{
		if (empty($uid) || empty($store_id)) 
		{
			return false;
		}
		$store_points_config = D('Store_points_config')->where(array('store_id' => $store_id))->find();
		if (empty($store_points_config)) 
		{
			return false;
		}
		$time = time();
		$timestamp = strtotime(date('Y-m-d 00:00:00', $time));
		$where = array();
		$where['store_id'] = $store_id;
		$where['uid'] = $uid;
		$where['type'] = 5;
		$where['timestamp'] = array('>=', $timestamp);
		$count = D('User_points')->where($where)->count('id');
		if (0 < $count) 
		{
			return false;
		}
		$data = array();
		$data['uid'] = $uid;
		$data['store_id'] = $store_id;
		$data['type'] = 5;
		$data['is_available'] = 1;
		$data['timestamp'] = $time;
		if ($store_points_config['sign_type'] == 0) 
		{
			$data['points'] = $store_points_config['sign_fixed_point'];
		}
		else if ($store_points_config['sign_type'] == 1) 
		{
			$days = M('Store_user_data')->getUserSignDay($store_id, $uid);
			if ($store_points_config['sign_plus_day'] <= $days) 
			{
				$days = $store_points_config['sign_plus_day'] - 1;
			}
			$add_points = $days * $store_points_config['sign_plus_addition'];
			$data['points'] = $store_points_config['sign_plus_start'] + $add_points;
		}
		$result = D('User_points')->data($data)->add();
		$is_fx = false;
		if ($seller_id) 
		{
			$data['drp_store_id'] = $seller_id;
			$data['type'] = 4;
			D('Store_points')->data($data)->add();
			$is_fx = true;
		}
		if ($result) 
		{
			M('Store_user_data')->changePoint($store_id, $uid, $data['points'], $is_fx);
			M('Store_user_data')->setUserSignDay($store_id, $uid);
		}
	}
	static public function subscribe($uid = 0, $store_id = 0, $seller_id = 0, $store_point_add = false) 
	{
		if (empty($uid) || empty($store_id)) 
		{
			return false;
		}
		$store_points_config = D('Store_points_config')->where(array('store_id' => $store_id))->find();
		if (empty($store_points_config)) 
		{
			return false;
		}
		$user_points = D('User_points')->where(array('uid' => $uid, 'store_id' => $store_id, 'type' => 1))->find();
		if (!empty($user_points) && ($_SERVER['REMOTE_ADDR'] != '101.226.102.237')) 
		{
			return true;
		}
		$time = time();
		if ($store_points_config['is_subscribe']) 
		{
			if ((0 < $store_points_config['drp1_subscribe_point']) && ($_SERVER['REMOTE_ADDR'] != '101.226.102.237')) 
			{
				$data = array();
				$data['uid'] = $uid;
				$data['store_id'] = $store_id;
				$data['points'] = $store_points_config['drp1_subscribe_point'];
				$data['type'] = 1;
				$data['is_available'] = 1;
				$data['timestamp'] = $time;
				$result = D('User_points')->data($data)->add();
				if ($result) 
				{
					M('Store_user_data')->changePoint($store_id, $uid, $data['points']);
				}
			}
			if ($seller_id) 
			{
				if (empty($store_points_config['drp2_subscribe_point']) && empty($store_points_config['drp3_subscribe_point'])) 
				{
					return NULL;
				}
				$store_supplier = D('Store_supplier')->field('supplier_id')->where(array('seller_id' => $seller_id, 'type' => 1))->find();
				if (!empty($store_supplier)) 
				{
					$drp2_store_id = ((isset($seller_id) ? $seller_id : 0));
					$drp3_store_id = ((isset($store_supplier['supplier_id']) ? $store_supplier['supplier_id'] : 0));
					if ($drp2_store_id) 
					{
						$store = D('Store')->where(array('store_id' => $drp2_store_id))->find();
						if (!empty($store)) 
						{
							$data = array();
							$data['uid'] = $store['uid'];
							$data['store_id'] = $store_id;
							$data['points'] = $store_points_config['drp2_subscribe_point'];
							$data['type'] = 8;
							$data['is_available'] = 1;
							$data['timestamp'] = $time;
							$result = D('User_points')->data($data)->add();
							if ($result) 
							{
								M('Store_user_data')->changePoint($store_id, $store['uid'], $data['points']);
							}
						}
					}
					if ($drp3_store_id) 
					{
						$store = D('Store')->where(array('store_id' => $drp3_store_id))->find();
						if (!empty($store)) 
						{
							$data = array();
							$data['uid'] = $store['uid'];
							$data['store_id'] = $store_id;
							$data['points'] = $store_points_config['drp3_subscribe_point'];
							$data['type'] = 8;
							$data['is_available'] = 1;
							$data['timestamp'] = $time;
							$result = D('User_points')->data($data)->add();
							if ($result) 
							{
								M('Store_user_data')->changePoint($store_id, $store['uid'], $data['points']);
							}
						}
					}
				}
			}
		}
		if ($store_point_add) 
		{
			Points::drpStore($uid, $store_id, $seller_id);
		}
	}
	static public function shareLink($uid = 0, $store_id = 0, $seller_id = 0) 
	{
		if (empty($uid) || empty($store_id)) 
		{
			return false;
		}
		$store_points_config = D('Store_points_config')->where(array('store_id' => $store_id))->find();
		if (empty($store_points_config)) 
		{
			return false;
		}
		if ($store_points_config['is_share']) 
		{
			$open_id = $_SESSION['openid'];
			$share_link = D('Share_link')->where(array('store_id' => $store_id, 'uid' => $uid, 'open_id' => $open_id))->find();
			if (!empty($share_link)) 
			{
				return false;
			}
			$data = array();
			$data['dateline'] = time();
			$data['store_id'] = $store_id;
			$data['uid'] = $uid;
			$data['seller_id'] = $seller_id;
			$data['open_id'] = $open_id;
			D('Share_link')->data($data)->add();
			$count = D('Share_link')->where(array('store_id' => $store_id, 'uid' => $uid, 'user_type' => 0))->count('id');
			if (($store_points_config['share_click_num'] <= $count) && $store_points_config['share_click_num']) 
			{
				$num = floor($count / $store_points_config['share_click_num']);
				$points = $num * $store_points_config['share_click_point'];
				$data = array();
				$data['uid'] = $uid;
				$data['store_id'] = $store_id;
				$data['type'] = 4;
				$data['is_available'] = 1;
				$data['timestamp'] = time();
				$data['points'] = $points;
				D('User_points')->data($data)->add();
				M('Store_user_data')->changePoint($store_id, $uid, $points);
				D('Share_link')->where(array('store_id' => $store_id, 'uid' => $uid, 'user_type' => 0))->data(array('user_type' => 1))->order('id asc')->limit($num * $store_points_config['share_click_num'])->save();
			}
			if ($seller_id) 
			{
				$count = D('Share_link')->where(array('store_id' => $store_id, 'uid' => $uid, 'seller_id' => $seller_id, 'store_type' => 0))->count('id');
				if (($store_points_config['share_click_num'] <= $count) && $store_points_config['share_click_num']) 
				{
					$num = floor($count / $store_points_config['share_click_num']);
					$points = $num * $store_points_config['share_click_point'];
					$data = array();
					$data['uid'] = $uid;
					$data['store_id'] = $store_id;
					$data['type'] = 3;
					$data['is_available'] = 1;
					$data['timestamp'] = time();
					$data['points'] = $points;
					D('Store_points')->data($data)->add();
					M('Store_user_data')->changeStorePoint($store_id, $uid, $points);
					D('Share_link')->where(array('store_id' => $store_id, 'uid' => $uid, 'store_type' => 0))->data(array('store_type' => 1))->order('id asc')->limit($num * $store_points_config['share_click_num'])->save();
				}
			}
		}
	}
	static public function change($uid = 0, $store_id, $point = 0, $bak = '') 
	{
		if (empty($uid) || empty($store_id) || empty($point)) 
		{
			return NULL;
		}
		$data = array();
		$data['uid'] = $uid;
		$data['store_id'] = $store_id;
		$data['points'] = $point;
		$data['type'] = 7;
		$data['is_available'] = 1;
		$data['timestamp'] = time();
		$data['bak'] = $bak;
		if (D('User_points')->data($data)->add()) 
		{
			M('Store_user_data')->changePoint($store_id, $uid, $point);
		}
	}
	static public function drpStore($uid = 0, $store_id = 0, $seller_id = 0) 
	{
		if (empty($uid) || empty($store_id) || empty($seller_id)) 
		{
			return false;
		}
		$store_points_config = D('Store_points_config')->where(array('store_id' => $store_id))->find();
		if (empty($store_points_config)) 
		{
			return false;
		}
		$time = time();
		if ($store_points_config['drp1_spoint']) 
		{
			$data = array();
			$data['uid'] = $uid;
			$data['store_id'] = $store_id;
			$data['drp_store_id'] = $seller_id;
			$data['points'] = $store_points_config['drp1_spoint'];
			$data['type'] = 1;
			$data['is_available'] = 1;
			$data['timestamp'] = $time;
			if (D('Store_points')->data($data)->add()) 
			{
				M('Store_user_data')->changeStorePoint($store_id, $uid, $data['points']);
			}
		}
		if (empty($store_points_config['drp2_spoint']) && empty($store_points_config['drp3_spoint'])) 
		{
			return NULL;
		}
		$store_supplier = D('Store_supplier')->field('supply_chain')->where(array('seller_id' => $seller_id, 'type' => 1))->find();
		if ($store_supplier) 
		{
			$supply_chain_arr = explode(',', $store_supplier['supply_chain']);
			$drp2_store_id = $supply_chain_arr[count($supply_chain_arr) - 1];
			$drp3_store_id = ((isset($supply_chain_arr[count($supply_chain_arr) - 2]) ? $supply_chain_arr[count($supply_chain_arr) - 2] : 0));
			if ($drp2_store_id) 
			{
				$store = D('Store')->where(array('store_id' => $drp2_store_id))->find();
				if (!empty($store)) 
				{
					$data = array();
					$data['uid'] = $store['uid'];
					$data['store_id'] = $store_id;
					$data['drp_store_id'] = $drp2_store_id;
					$data['points'] = $store_points_config['drp2_spoint'];
					$data['type'] = 5;
					$data['is_available'] = 1;
					$data['timestamp'] = $time;
					if (D('Store_points')->data($data)->add()) 
					{
						M('Store_user_data')->changeStorePoint($store_id, $store['uid'], $data['points']);
					}
				}
			}
			if ($drp3_store_id) 
			{
				$store = D('Store')->where(array('store_id' => $drp3_store_id))->find();
				if (!empty($store)) 
				{
					$data = array();
					$data['uid'] = $store['uid'];
					$data['store_id'] = $store_id;
					$data['drp_store_id'] = $drp3_store_id;
					$data['points'] = $store_points_config['drp3_spoint'];
					$data['type'] = 5;
					$data['is_available'] = 1;
					$data['timestamp'] = $time;
					if (D('Store_points')->data($data)->add()) 
					{
						M('Store_user_data')->changeStorePoint($store_id, $store['uid'], $data['points']);
					}
				}
			}
		}
	}
	static public function getPointConfig($uid = 0, $store_id = 0) 
	{
		if (empty($uid) || empty($store_id)) 
		{
			return array();
		}
		$store_user_data = M('Store_user_data')->getUserData($store_id, $uid);
		if ($store_user_data['point'] <= 0) 
		{
			return array();
		}
		$store_points_config = D('Store_points_config')->where(array('store_id' => $store_id))->find();
		if (empty($store_points_config)) 
		{
			return array();
		}
		if (empty($store_points_config['is_offset']) || ($store_points_config['price'] <= 0)) 
		{
			return array();
		}
		if ($store_points_config['price'] <= 0) 
		{
			return array();
		}
		$return = array();
		$return['point'] = $store_user_data['point'];
		$return['price'] = $store_points_config['price'];
		$return['is_percent'] = $store_points_config['is_percent'];
		$return['percent'] = $store_points_config['offset_cash'];
		$return['is_limit'] = $store_points_config['is_limit'];
		$return['offset_limit'] = $store_points_config['offset_limit'];
		$user_degree = D('User_degree')->where(array('id' => $store_user_data['degree_id'], 'store_id' => $store_id))->find();
		if ($user_degree['is_points_discount_toplimit']) 
		{
			$return['is_limit'] = 1;
			$return['offset_limit'] = $user_degree['points_discount_toplimit'];
		}
		if ($user_degree['is_points_discount_ratio']) 
		{
			$return['is_percent'] = 1;
			$return['percent'] = $user_degree['points_discount_ratio'];
		}
		if (($return['is_percent'] && ($return['percent'] == 0)) || ($return['is_limit'] && ($return['offset_limit'] == 0))) 
		{
			return array();
		}
		return $return;
	}
	static public function getPointMoney($uid = 0, $store_id = 0, $product_money = 0) 
	{
		if (empty($uid) || empty($store_id) || empty($product_money)) 
		{
			return array();
		}
		$store_points_config = Points::getPointConfig($uid, $store_id);
		if (empty($store_points_config)) 
		{
			return array();
		}
		if (empty($store_points_config['point']) || ($store_points_config['price'] <= 0)) 
		{
			return array();
		}
		$max_money = $store_points_config['point'] / $store_points_config['price'];
		$point = $store_points_config['point'];
		if ($product_money < $max_money) 
		{
			$max_money = $product_money;
			$point = ceil($max_money * $store_points_config['price']);
		}
		if ($store_points_config['is_percent'] && ((($product_money * $store_points_config['percent']) / 100) < $max_money)) 
		{
			$max_money = ($product_money * $store_points_config['percent']) / 100;
			$point = ceil($max_money * $store_points_config['price']);
		}
		if ($store_points_config['is_limit'] && ($store_points_config['offset_limit'] < $max_money)) 
		{
			$max_money = $store_points_config['offset_limit'];
			$point = ceil($max_money * $store_points_config['price']);
		}
		$max_money = floor($max_money * 100) / 100;
		$point = ceil($max_money * $store_points_config['price']);
		return array('money' => $max_money, 'point' => $point);
	}
	static public function returnPoint($order = array()) 
	{
		if (empty($order)) 
		{
			return NULL;
		}
		$order_point = D('Order_point')->where(array('order_id' => $order['order_id']))->find();
		if (empty($order_point) || empty($order_point['point'])) 
		{
			return NULL;
		}
		$data = array();
		$data['uid'] = $order_point['uid'];
		$data['store_id'] = $order_point['store_id'];
		$data['order_id'] = $order_point['order_id'];
		$data['points'] = $order_point['point'];
		$data['type'] = 10;
		$data['is_available'] = 1;
		$data['timestamp'] = time();
		if (D('User_points')->data($data)->add()) 
		{
			D('Store_user_data')->where(array('store_id' => $order_point['store_id'], 'uid' => $order_point['uid']))->setInc('point', $order_point['point']);
		}
	}
	static public function orderPoint($order) 
	{
		if (empty($order)) 
		{
			return false;
		}
		if ($order['user_order_id']) 
		{
			$order = D('Order')->where(array('order_id' => $order['user_order_id']))->find();
			if (empty($order)) 
			{
				return false;
			}
		}
		$order_product_list = M('Order_product')->orderProduct($order['order_id']);
		if (empty($order_product_list)) 
		{
			return NULL;
		}
		$order_product_money_by_store = array();
		$pro_num_by_store = array();
		$supplier_store_id = 0;
		$product_point_arr = array();
		foreach ($order_product_list as $order_product ) 
		{
			$store_id = $order_product['store_id'];
			if ($order_product['wholesale_supplier_id']) 
			{
				$store_id = $order_product['wholesale_supplier_id'];
			}
			else 
			{
				$supplier_store_id = $store_id;
			}
			$pro_num = $order_product['pro_num'];
			if ($order_product['return_status'] == 2) 
			{
				$pro_num = 0;
			}
			else if ($order_product['return_status'] == 1) 
			{
				$return_product_num = M('Return_product')->returnNumber($order['order_id'], $order_product['pigcms_id'], true);
				$pro_num = max(0, $order_product['pro_num'] - $return_product_num);
			}
			$product_point_arr[$store_id] += $order_product['point'] * $pro_num;
			$pro_num_by_store[$store_id] += $pro_num;
			$order_product_money_by_store[$store_id] += $order_product['pro_num'] * $order_product['pro_price'];
		}
		if (empty($pro_num_by_store[$supplier_store_id])) 
		{
			Points::returnPoint($order);
		}
		if (array_sum($pro_num_by_store) == 0) 
		{
			return NULL;
		}
		$order_data = Order::orderDiscount($order, $order_product_list);
		$money_total = 0;
		$supplier_store_points_config = array();
		foreach ($order_product_money_by_store as $store_id => $order_product_money ) 
		{
			$store_points_config = D('Store_points_config')->where(array('store_id' => $store_id))->find();
			if ($store_id == $supplier_store_id) 
			{
				$supplier_store_points_config = $store_points_config;
			}
			$reward_money = $order_data['order_reward_list'][$store_id][0]['content']['cash'];
			$coupon_money = $order_data['order_coupon_list'][$store_id]['money'];
			$discount_money = $order_data['discount_money_list'][$store_id];
			$point_money = (($order_data['order_point']['store_id'] == $store_id ? $order_data['order_point']['money'] : 0));
			$float_money = (($store_id == $order['store_id'] ? $order['float_amount'] : 0));
			$return_money = M('Return')->getOrderReturnAmount($order['order_id']);
			$postage_arr = unserialize($order['fx_postage']);
			$money = max(0, ((($order_product_money - $reward_money - $coupon_money - $discount_money - $point_money) + $float_money) - $return_money) + $postage_arr[$store_id]);
			$money_total += $money;
			$point = 0;
			if (!empty($store_points_config)) 
			{
				if (($store_points_config['order_consume'] == 0) && (0 < $store_points_config['consume_money'])) 
				{
					$point = floor($money / $store_points_config['consume_money']) * $store_points_config['consume_point'];
				}
				else if (($store_points_config['order_consume'] == 1) && (0 < $store_points_config['proport_money'])) 
				{
					$point = floor($money / $store_points_config['proport_money']);
				}
			}
			$point += $product_point_arr[$store_id];
			if (0 < $point) 
			{
				$data = array();
				$data['uid'] = $order['uid'];
				$data['store_id'] = $store_id;
				$data['order_id'] = $order['order_id'];
				$data['points'] = $point;
				$data['type'] = 2;
				$data['is_available'] = 1;
				$data['timestamp'] = time();
				if (D('User_points')->data($data)->add()) 
				{
					M('Store_user_data')->changePoint($store_id, $order['uid'], $point);
				}
			}
		}
		if (($order['is_fx'] == 0) || empty($supplier_store_points_config)) 
		{
			return NULL;
		}
		if (0 < $supplier_store_points_config['drp1_spoint_money']) 
		{
			$point = floor($money_total / $supplier_store_points_config['drp1_spoint_money']);
			if (0 < $point) 
			{
				$store = M('Store')->getStore($order['store_id']);
				if (!empty($store)) 
				{
					$data = array();
					$data['uid'] = $store['uid'];
					$data['store_id'] = $supplier_store_id;
					$data['drp_store_id'] = $order['store_id'];
					$data['order_id'] = $order['order_id'];
					$data['points'] = $point;
					$data['type'] = 2;
					$data['is_available'] = 1;
					$data['timestamp'] = time();
					if (D('Store_points')->data($data)->add()) 
					{
						M('Store_user_data')->changeStorePoint($supplier_store_id, $store['uid'], $point);
					}
				}
			}
		}
		$order_list = D('Order')->where(array( 'user_order_id' => $order['order_id'], 'order_id' => array('!=', $order['order_id']) ))->order('order_id ASC')->limit(2)->select();
		foreach ($order_list as $key => $order_tmp ) 
		{
			$i = $key + 2;
			if (0 < $supplier_store_points_config['drp' . $i . '_spoint_money']) 
			{
				$store = M('Store')->getStore($order_tmp['store_id']);
				if (empty($store['drp_level'])) 
				{
					break;
				}
				$income = max(0, M('Financial_record')->getTotal(array('store_id' => $order_tmp['store_id'], 'order_id' => $order_tmp['order_id'])));
				$point = floor($income / $supplier_store_points_config['drp' . $i . '_spoint_money']);
				if (0 < $point) 
				{
					$data = array();
					$data['uid'] = $store['uid'];
					$data['store_id'] = $supplier_store_id;
					$data['drp_store_id'] = $order_tmp['store_id'];
					$data['order_id'] = $order_tmp['order_id'];
					$data['points'] = $point;
					$data['type'] = 2;
					$data['is_available'] = 1;
					$data['timestamp'] = time();
					if (D('Store_points')->data($data)->add()) 
					{
						M('Store_user_data')->changeStorePoint($supplier_store_id, $store['uid'], $point);
					}
				}
			}
		}
	}
	static public function drpDegree($store_id, $deduct_point = false, $uid = 0) 
	{
		$allow_drp = option('config.open_store_drp');
		if (empty($allow_drp)) 
		{
			return false;
		}
		if (empty($store_id)) 
		{
			return false;
		}
		$store = D('Store')->field('store_id,uid,drp_degree_id,last_time_statistics,drp_supplier_id,root_supplier_id')->where(array('store_id' => $store_id, 'drp_approve' => 1))->find();
		if (empty($store['drp_supplier_id']) && !empty($uid)) 
		{
			$store = D('Store')->field('store_id,uid,drp_degree_id,last_time_statistics,drp_supplier_id,root_supplier_id')->where(array('uid' => $uid, 'root_supplier_id' => $store_id, 'drp_approve' => 1))->find();
			if (!empty($store)) 
			{
				$store_id = $store['store_id'];
			}
		}
		if (empty($store)) 
		{
			return false;
		}
		$supplier_id = M('Store_supplier')->getSupplierId($store_id);
		$supplier = D('Store')->field('open_drp_degree,drp_deduct_point_month,drp_deduct_point_sales,drp_deduct_point')->where(array('store_id' => $supplier_id))->find();
		if (empty($supplier['open_drp_degree'])) 
		{
			return false;
		}
		$drp_degree = D('Drp_degree')->field('pigcms_id,condition_point')->where(array('pigcms_id' => $store['drp_degree_id'], 'store_id' => $supplier_id))->find();
		$drp_degree_id = ((!empty($drp_degree['pigcms_id']) ? $drp_degree['pigcms_id'] : 0));
		$drp_degree_point = ((!empty($drp_degree['condition_point']) ? $drp_degree['condition_point'] : 0));
		$store_data = D('Store_user_data')->field('store_point')->where(array('store_id' => $supplier_id, 'uid' => $store['uid']))->find();
		$store_point = ((!empty($store_data['store_point']) ? $store_data['store_point'] : 0));
		if ($deduct_point && (0 < $supplier['drp_deduct_point_month']) && (0 < $supplier['drp_deduct_point_sales']) && (0 < $supplier['drp_deduct_point'])) 
		{
			$now = time();
			$date1_stamp = ((!empty($store['last_time_statistics']) ? $store['last_time_statistics'] : 0));
			$date2_stamp = $now;
			$date_1['m'] = explode('-', date('Y-m', $date1_stamp))[1];
			$date_1['y'] = explode('-', date('Y-m', $date1_stamp))[0];
			$date_2['m'] = explode('-', date('Y-m', $date2_stamp))[1];
			$date_2['y'] = explode('-', date('Y-m', $date2_stamp))[0];
			$month = abs(((($date_2['y'] - $date_1['y']) * 12) + $date_2['m']) - $date_1['m']);
			if ($supplier['drp_deduct_point_month'] <= $month) 
			{
				$start_time = date('Y-m', strtotime('-' . $supplier['drp_deduct_point_month'] . ' month')) . '-01 00:00:00';
				$end_time = date('Y-m-t', strtotime('-1 month')) . ' 23:59:59';
				$where = array();
				$where['store_id'] = $store_id;
				$where['is_fx'] = 1;
				$where['status'] = 4;
				$where['_string'] = 'add_time >= ' . strtotime($start_time) . ' AND add_time <= ' . strtotime($end_time);
				$sales = D('Order')->where($where)->sum('total + (cash_point / point2money_rate)');
				$sales = ((!empty($sales) ? $sales : 0));
				if ($sales < $supplier['drp_deduct_point_sales']) 
				{
					if ($supplier['drp_deduct_point'] <= $store_point) 
					{
						$store_point -= $supplier['drp_deduct_point'];
						$data = array();
						$data['uid'] = $store['uid'];
						$data['store_id'] = $supplier_id;
						$data['drp_store_id'] = $store_id;
						$data['points'] = -$supplier['drp_deduct_point'];
						$data['type'] = 1;
						$data['is_available'] = 1;
						$data['timestamp'] = $now;
						if (D('Store_points')->data($data)->add()) 
						{
							M('Store_user_data')->changeStorePoint($supplier_id, $store['uid'], -$supplier['drp_deduct_point'], false);
						}
					}
					D('Store')->where(array('store_id' => $store_id))->data(array('last_time_statistics' => $now))->save();
				}
			}
		}
		$change_drp_degree_id = $drp_degree_id;
		if ($drp_degree_point < $store_point) 
		{
			$drp_degree = D('Drp_degree')->field('pigcms_id')->where(array( 'store_id' => $supplier_id, 'condition_point' => array('<=', $store_point) ))->order('condition_point DESC')->find();
			$change_drp_degree_id = $drp_degree_id;
			if (!empty($drp_degree['pigcms_id'])) 
			{
				$change_drp_degree_id = $drp_degree['pigcms_id'];
			}
		}
		else if ($store_point < $drp_degree_point) 
		{
			$change_drp_degree_id = 0;
			$drp_degree = D('Drp_degree')->field('pigcms_id')->where(array( 'store_id' => $supplier_id, 'condition_point' => array('<=', $store_point) ))->order('condition_point DESC')->find();
			if (empty($drp_degree['pigcms_id'])) 
			{
				$change_drp_degree_id = $drp_degree['pigcms_id'];
			}
		}
		if ($change_drp_degree_id != $drp_degree_id) 
		{
			if (D('Store')->where(array('store_id' => $store_id))->data(array('drp_degree_id' => $change_drp_degree_id))->save()) 
			{
				$drp_degree_id = $change_drp_degree_id;
			}
		}
		return $drp_degree_id;
	}
}
?>

<?php
class Order 
{
	public $product_list;
	public $store_id_list;
	public $uid;
	public $reward_list;
	public $user_coupon_list;
	public $discount_list;
	public $postage_free_list;
	public $points_list;
	public function __construct($product_list = array(), $config = array()) 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		if (!empty($product_list)) 
		{
			$this->productByStore($product_list);
		}
		if (is_array($config)) 
		{
			foreach ($config as $key => $val ) 
			{
				$this->$key = $val;
			}
		}
		else 
		{
			$this->config = $config;
		}
		if (!isset($config['uid'])) 
		{
			if (isset($_SESSION['wap_user']['uid'])) 
			{
				$this->uid = $_SESSION['wap_user']['uid'];
			}
			else if (isset($_SESSION['user']['uid'])) 
			{
				$this->uid = $_SESSION['user']['uid'];
			}
			else 
			{
				$this->uid = 0;
			}
		}
	}
	public function all() 
	{
		if (empty($this->reward_list)) 
		{
			$this->reward();
		}
		if (empty($this->user_coupon_list)) 
		{
			$this->coupon();
		}
		if (empty($this->discount_list)) 
		{
			$this->discount();
		}
		$return = array();
		$return['reward_list'] = $this->reward_list;
		$return['user_coupon_list'] = $this->user_coupon_list;
		$return['discount_list'] = $this->discount_list;
		$return['postage_free_list'] = $this->postage_free_list;
		return $return;
	}
	public function productByStore($product_list = array()) 
	{
		if (empty($product_list)) 
		{
			return array();
		}
		if (!is_array($product_list)) 
		{
			return array();
		}
		$data = array();
		foreach ($product_list as $product ) 
		{
			$store_id = $product['store_id'];
			if ($product['wholesale_product_id']) 
			{
				$store_id = $product['wholesale_supplier_id'];
			}
			$this->store_id_list[$store_id] = $store_id;
			$data[$store_id][] = $product;
		}
		$this->product_list = $data;
		return $data;
	}
	public function score($uid = 0) 
	{
		if (!empty($uid)) 
		{
			$this->uid = $uid;
		}
		if (empty($this->uid)) 
		{
			return false;
		}
		if (empty($this->product_list)) 
		{
			return array();
		}
		if (!empty($this->point_list)) 
		{
			return $this->point_list;
		}
		$points_list = array();
		foreach ($this->product_list as $store_id => $product_list ) 
		{
			$money = 0;
			foreach ($product_list as $product ) 
			{
				$money += $product['pro_price'] * $product['pro_num'];
			}
			$points = D('Points')->where('store_id = \'' . $store_id . '\' AND type = \'3\' AND trade_or_amount <= ' . $money)->field('points')->find();
			$points_list[$store_id]['points'] = $points['points'] + 0;
			$points_list[$store_id]['money'] = $money + 0;
		}
		$this->points_list = $points_list;
		return $points_list;
	}
	public function reward() 
	{
		if (empty($this->product_list) || empty($this->uid)) 
		{
			return array();
		}
		import('source.class.Appmarket');
		$aeward_list = array();
		foreach ($this->product_list as $store_id => $product_list ) 
		{
			$is_self_product = true;
			$uid = $product_list[0]['uid'];
			if ($store_id != $product_list[0]['store_id']) 
			{
				$is_self_product = false;
				$store = D('Store')->where('store_id = \'' . $store_id . '\'')->find();
				$uid = $store['uid'];
			}
			$product_id_arr = array();
			$product_price_arr = array();
			$total_price = 0;
			foreach ($product_list as $product ) 
			{
				if ($is_self_product) 
				{
					$product_id_arr[] = $product['product_id'];
					$product_price_arr[$product['product_id']]['price'] = $product['pro_price'];
					$product_price_arr[$product['product_id']]['pro_num'] = $product['pro_num'];
					$total_price += $product['pro_price'] * $product['pro_num'];
				}
				else 
				{
					$product_id_arr[] = $product['wholesale_product_id'];
					$product_price_arr[$product['wholesale_product_id']]['price'] = $product['pro_price'];
					$product_price_arr[$product['wholesale_product_id']]['pro_num'] = $product['pro_num'];
					$total_price += $product['pro_price'] * $product['pro_num'];
				}
			}
			$reward_arr = array();
			$reward_arr['product_id_arr'] = $product_id_arr;
			$reward_arr['store_id'] = $store_id;
			$reward_arr['uid'] = $uid;
			$product_arr = array();
			$product_arr['product_price_arr'] = $product_price_arr;
			$product_arr['total_price'] = $total_price;
			$reward_list[$store_id] = Appmarket::getAeward($reward_arr, $product_arr);
		}
		$this->reward_list = $reward_list;
		unset($reward_list);
		return $this->reward_list;
	}
	public function coupon() 
	{
		if (empty($this->uid)) 
		{
			return array();
		}
		if (empty($this->reward_list)) 
		{
			$this->reward();
		}
		if (empty($this->reward_list)) 
		{
			return array();
		}
		$user_coupon_list = array();
		foreach ($this->reward_list as $store_id => $reward_list ) 
		{
			$user_coupon_detail_list = M('User_coupon')->getListByProductId($reward_list['product_price_list'], $store_id, $this->uid);
			$tmp = Appmarket::getCoupon($reward_list, $user_coupon_detail_list);
			if (!empty($tmp)) 
			{
				$user_coupon_list[$store_id] = $tmp;
			}
		}
		$this->user_coupon_list = $user_coupon_list;
		unset($user_coupon_list);
		return $this->user_coupon_list;
	}
	public function discount($uid = 0, $store_id_list = array()) 
	{
		if (!empty($uid)) 
		{
			$this->uid = $uid;
		}
		if (empty($this->uid)) 
		{
			return array();
		}
		if (empty($this->store_id_list)) 
		{
			$this->store_id_list = $store_id_list;
		}
		if (empty($this->store_id_list)) 
		{
			return array();
		}
		$discount_list = array();
		$postage_free_list = array();
		foreach ($this->store_id_list as $store_id ) 
		{
			$store_user_data = M('Store_user_data')->getUserData($store_id, $this->uid);
			$user_degree = D('User_degree')->where(array('id' => $store_user_data['degree_id']))->find();
			$discount_list[$store_id] = 10;
			$postage_free_list[$store_id] = 0;
			if ($user_degree['discount']) 
			{
				$discount_list[$store_id] = $user_degree['discount'];
			}
			if ($user_degree['is_postage_free']) 
			{
				$postage_free_list[$store_id] = $user_degree['is_postage_free'];
			}
		}
		$this->postage_free_list = $postage_free_list;
		$this->discount_list = $discount_list;
		return $discount_list;
	}
	static public function orderDiscount($order, $order_product_list = array(), $is_return_money = false) 
	{
		if (empty($order)) 
		{
			return array();
		}
		if (empty($order_product_list)) 
		{
			$order_product_list = M('Order_product')->getProducts($order['order_id']);
		}
		if (empty($order_product_list)) 
		{
			return array();
		}
		$order_id = $order['order_id'];
		if ($order['user_order_id']) 
		{
			$order_id = $order['user_order_id'];
		}
		$order_ward_list = M('Order_reward')->getByOrderId($order_id);
		$order_coupon_list = M('Order_coupon')->getList($order_id);
		$tmp_order_coupon_list = array();
		foreach ($order_coupon_list as $tmp ) 
		{
			$tmp_order_coupon_list[$tmp['store_id']] = $tmp;
		}
		$order_coupon_list = $tmp_order_coupon_list;
		unset($tmp_order_coupon_list);
		$order_discount_list = M('Order_discount')->getByOrderId($order_id);
		foreach ($order_discount_list as $order_discount ) 
		{
			$order_discount_list[$order_discount['store_id']] = $order_discount['discount'];
		}
		$order_point = D('Order_point')->where(array('order_id' => $order_id))->find();
		$discount_money = 0;
		$discount_money_list = array();
		$supplier_store_id = 0;
		if ($order_id != $order['order_id']) 
		{
			$tmp_order_ward_list = array();
			$tmp_order_coupon_list = array();
			foreach ($order_product_list as $order_product ) 
			{
				$supplier_store_id = $order_product['store_id'];
				if ($order_product['wholesale_product_id']) 
				{
					if (isset($order_ward_list[$order_product['supplier_id']]) && !empty($order_ward_list[$order_product['supplier_id']])) 
					{
						$tmp_order_ward_list[$order_product['supplier_id']] = $order_ward_list[$order_product['supplier_id']];
					}
					if (isset($order_coupon_list[$order_product['supplier_id']]) && !empty($order_coupon_list[$order_product['supplier_id']])) 
					{
						$tmp_order_coupon_list[$order_product['supplier_id']] = $order_coupon_list[$order_product['supplier_id']];
					}
				}
				else 
				{
					if (isset($order_ward_list[$order_product['store_id']]) && !empty($order_ward_list[$order_product['store_id']])) 
					{
						$tmp_order_ward_list[$order_product['store_id']] = $order_ward_list[$order_product['store_id']];
					}
					if (isset($order_coupon_list[$order_product['store_id']]) && !empty($order_coupon_list[$order_product['store_id']])) 
					{
						$tmp_order_coupon_list[$order_product['store_id']] = $order_coupon_list[$order_product['store_id']];
					}
				}
				$master_order_product = D('')->field('p.*, op.pro_num, op.pro_price, op.discount')->table('Order_product AS op')->join('Product AS p ON p.product_id = op.product_id', 'LEFT')->where('`op`.`order_id` = \'' . $order_id . '\' AND (`op`.`product_id` = \'' . $order_product['product_id'] . '\' OR `op`.`original_product_id` = \'' . $order_product['product_id'] . '\') AND `op`.`sku_data` = \'' . addslashes($order_product['sku_data']) . '\'')->find();
				$discount = 10;
				$store_id = $master_order_product['store_id'];
				if ($master_order_product['wholesale_product_id'] && isset($order_discount_list[$master_order_product['supplier_id']])) 
				{
					$discount = $order_discount_list[$master_order_product['supplier_id']];
					$store_id = $master_order_product['supplier_id'];
				}
				else if (empty($master_order_product['wholesale_product_id']) && isset($order_discount_list[$master_order_product['store_id']])) 
				{
					$discount = $order_discount_list[$master_order_product['store_id']];
					$store_id = $master_order_product['store_id'];
				}
				if ((0 < $master_order_product['discount']) && ($master_order_product['discount'] <= 10)) 
				{
					$discount = $master_order_product['discount'];
				}
				$discount_money += ($order_product['pro_num'] * $master_order_product['pro_price'] * (10 - $discount)) / 10;
				$discount_money_list[$store_id] += ($order_product['pro_num'] * $master_order_product['pro_price'] * (10 - $discount)) / 10;
			}
			$order_ward_list = $tmp_order_ward_list;
			$order_coupon_list = $tmp_order_coupon_list;
			unset($tmp_order_ward_list);
			unset($tmp_order_coupon_list);
		}
		else 
		{
			foreach ($order_product_list as $order_product ) 
			{
				$supplier_store_id = $order_product['store_id'];
				$discount = 10;
				$store_id = $order_product['store_id'];
				if ($order_product['wholesale_product_id'] && isset($order_discount_list[$order_product['supplier_id']])) 
				{
					$discount = $order_discount_list[$order_product['supplier_id']];
					$store_id = $order_product['supplier_id'];
				}
				else if (empty($order_product['wholesale_product_id']) && isset($order_discount_list[$order_product['store_id']])) 
				{
					$discount = $order_discount_list[$order_product['store_id']];
					$store_id = $order_product['store_id'];
				}
				if ((0 < $order_product['discount']) && ($order_product['discount'] <= 10)) 
				{
					$discount = $order_product['discount'];
				}
				$discount_money += ($order_product['pro_num'] * $order_product['pro_price'] * (10 - $discount)) / 10;
				$discount_money_list[$store_id] += ($order_product['pro_num'] * $order_product['pro_price'] * (10 - $discount)) / 10;
			}
		}
		if ($order_point['store_id'] != $supplier_store_id) 
		{
			$order_point = array();
		}
		if ($is_return_money) 
		{
			$return = 0;
			if ($order_ward_list) 
			{
				foreach ($order_ward_list as $order_ward ) 
				{
					if (empty($order_ward)) 
					{
						continue;
					}
					foreach ($order_ward as $tmp ) 
					{
						$return += $tmp['content']['cash'];
					}
				}
			}
			if ($order_coupon_list) 
			{
				foreach ($order_coupon_list as $order_coupon ) 
				{
					$return += $order_coupon['money'];
				}
			}
			$return += $discount_money + $order_point['money'];
		}
		else 
		{
			$return = array();
			$return['order_ward_list'] = $order_ward_list;
			$return['order_coupon_list'] = $order_coupon_list;
			$return['order_discount_list'] = $order_discount_list;
			$return['discount_money'] = number_format($discount_money, 2, '.', '');
			$return['discount_money_list'] = $discount_money_list;
			$return['order_point'] = $order_point;
		}
		return $return;
	}
	static public function complate($order_id, $status = array(7)) 
	{
		import('source.class.Points');
		import('source.class.Margin');
		$order = D('Order')->where(array( 'order_id' => $order_id, 'status' => array('in', $status) ))->find();
		if (empty($order)) 
		{
			return array('err_code' => 1001, 'err_msg' => '没有待交易完成的订单');
		}
		$store = D('Store')->where(array('store_id' => $order['store_id']))->find();
		$now = time();
		$where = array();
		$where['order_id'] = $order_id;
		$where['store_id'] = $store['store_id'];
		$data = array();
		$data['status'] = 4;
		$data['complate_time'] = $now;
		if (empty($order['is_fx']) && empty($order['user_order_id'])) 
		{
			$return = D('Return')->where(array( 'order_id' => $order_id, 'status' => array( 'in', array(1, 4) ) ))->count('id');
			if (0 < $return) 
			{
				return array('err_code' => 1002, 'err_msg' => '有退货正在处理中，无法交易完成');
			}
			if (M('Order')->editStatus($where, $data)) 
			{
				$return_total = D('Return')->where(array('order_id' => $order_id, 'type' => 5))->sum('product_money + postage_money');
				$income = M('Financial_record')->getOrderProfit($order_id, array(1, 3));
				if (0 < $return_total) 
				{
					D('Store')->where(array('store_id' => $store['store_id']))->setDec('income', $return_total);
				}
				if (empty($order['useStorePay']) && (0 < $income)) 
				{
					D('Store')->where(array('store_id' => $store['store_id']))->setDec('unbalance', $income);
					D('Store')->where(array('store_id' => $store['store_id']))->setInc('balance', $income);
				}
				if (0 < $return_total) 
				{
					D('Financial_record')->where(array('store_id' => $store['store_id'], 'order_id' => $order_id, 'type' => 3))->data(array('income' => -$return_total))->save();
				}
				if (0 < $return_total) 
				{
					$sales = $store['sales'] - $return_total;
					$sales = ((0 < $sales ? $sales : 0));
					D('Store')->where(array('store_id' => $store['store_id']))->data(array('sales' => $sales))->save();
				}
				$where = array();
				$where['order_id'] = $order_id;
				$where['store_id'] = $store['store_id'];
				$where['status'] = 1;
				$data = array();
				$data['status'] = 3;
				M('Financial_record')->editStatus($where, $data);
				M('Store_user_data')->upUserData($store['store_id'], $order['uid'], 'complete', 1);
				Points::orderPoint($order);
				if (Margin::check()) 
				{
					Margin::init($store['store_id']);
					Margin::sendPoint($order_id, 0);
				}
				import('source.class.Notice');
				Notice::orderComplete($order);
				return array('err_code' => 0, 'err_msg' => '订单交易完成');
			}
			return array('err_code' => 1003, 'err_msg' => '订单状态修改失败');
		}
		if (M('Order')->editStatus($where, $data)) 
		{
			if (Margin::check()) 
			{
				Margin::init($store['store_id']);
				Margin::sendPoint($order_id);
			}
			import('source.class.Notice');
			Notice::orderComplete($order);
			return array('err_code' => 0, 'err_msg' => '订单交易完成');
		}
		return array('err_code' => 1003, 'err_msg' => '订单状态修改失败');
	}
	static public function drpDegreeReward($supplier_id, $seller_id, $product_id, $drp_level) 
	{
		if (empty($supplier_id) || ($seller_id == $supplier_id)) 
		{
			return 0;
		}
		$open_drp_degree = option('config.open_drp_degree');
		if (empty($open_drp_degree)) 
		{
			return 0;
		}
		$seller = D('Store')->field('drp_degree_id,drp_supplier_id')->where(array('store_id' => $seller_id))->find();
		if (empty($seller['drp_degree_id']) || empty($seller['drp_supplier_id'])) 
		{
			return 0;
		}
		$supplier = D('Store')->field('open_drp_degree')->where(array('store_id' => $supplier_id))->find();
		if (empty($supplier['open_drp_degree'])) 
		{
			return 0;
		}
		$where = array();
		$where['pigcms_id'] = $seller['drp_degree_id'];
		$where['store_id'] = $supplier_id;
		$where['status'] = 1;
		$drp_degree = M('Drp_degree')->getDrpDegree($where);
		if (empty($drp_degree) || ($drp_degree['seller_reward_' . $drp_level] <= 0)) 
		{
			return 0;
		}
		$product_drp_degree = M('Product_drp_degree');
		$where = array();
		$where['product_id'] = $product_id;
		$where['degree_id'] = $seller['drp_degree_id'];
		$product_drp_degree_info = $product_drp_degree->getDrpDegree($where);
		if (empty($product_drp_degree_info)) 
		{
			$drp_degree_reward = $drp_degree['seller_reward_' . $drp_level];
		}
		else 
		{
			$drp_degree_reward = $product_drp_degree_info['seller_reward_' . $drp_level];
		}
		$drp_degree_reward = ((0 < $drp_degree_reward ? $drp_degree_reward / 100 : 0));
		return $drp_degree_reward;
	}
}
?>

<?php
class SendPoints 
{
	static private $_AUTO_SEND = 0;
	static private $_SEND_TIME = '00';
	static private $_CREDIT_WEIGHT = 500;
	static private $_TODAY_CREDIT_WEIGHT = '0.00';
	static private $_PERSEND = 500;
	static public function getCardinalNumber($cardinalnumber = 0, $user_unbalance, $credit_weight = 500) 
	{
		$user_unbalance = max(1, $user_unbalance);
		$cardinalnumber = intval($cardinalnumber);
		$fafang_jishu_start = max(0, $cardinalnumber - $credit_weight);
		$fafang_jishu_end = $cardinalnumber;
		if (($fafang_jishu_start < $user_unbalance) && ($user_unbalance < $fafang_jishu_end) && ($credit_weight <= $cardinalnumber)) 
		{
			$today_send = $fafang_jishu_end / $credit_weight;
		}
		else if ($user_unbalance <= $fafang_jishu_start) 
		{
			$today_send = ceil($user_unbalance / $credit_weight);
		}
		else 
		{
			$today_send = floor($user_unbalance / $credit_weight);
		}
		$new_cardinalnumber = $today_send * $credit_weight;
		return $new_cardinalnumber;
	}
	static public function init() 
	{
		self::$_AUTO_SEND = option('credit.auto_send');
		self::$_SEND_TIME = option('credit.day_send_credit_time');
		self::$_CREDIT_WEIGHT = option('credit.credit_weight');
		self::$_TODAY_CREDIT_WEIGHT = option('credit.today_credit_weight');
		return (self::check(self::$_AUTO_SEND, self::$_SEND_TIME) == false ? false : self::auto_send(1));
	}
	static private function check($auto_send, $send_time) 
	{
		$flag = false;
		$now = date('H');
		if (($auto_send == 1) && ($send_time == $now)) 
		{
			$flag = true;
		}
		return $flag;
	}
	static private function getRealCreditWeight($credit_weight) 
	{
		$cash_provision_balance = D('Cash_provision_log')->where(array('add_date' => date('Ymd', strtotime('-1 day')), 'type' => '0'))->sum('amount');
		if (!$cash_provision_balance) 
		{
			return 0;
		}
		$condition['_string'] = 'add_date < ' . date('Ymd') . ' AND type in(0, 5, 6, 7) ';
		$temp_ids_arr = D('User_point_log')->field('max(pigcms_id) as p')->where($condition)->group('uid')->select();
		$temp_ids = array();
		foreach ($temp_ids_arr as $key => $value ) 
		{
			array_push($temp_ids, $value['p']);
		}
		$yestoday_user_pool = D('User_point_log')->where('pigcms_id in (' . join(',', array_values($temp_ids)) . ') and point_send_base >=' . $credit_weight)->sum('point_send_base');
		$day_send_all_points = floor($yestoday_user_pool / $credit_weight);
		$point_weight = $cash_provision_balance / $day_send_all_points;
		$point_weight = number_format($point_weight, 2, '.', '');
		return $point_weight;
	}
	static private function prepareSend() 
	{
		$now_date = date('Ymd');
		$release_record = D('Release_point')->where(array('add_date' => $now_date))->find();
		if ($release_record && ($release_record['status'] == 1)) 
		{
			return false;
		}
		$credit_weight = self::$_CREDIT_WEIGHT;
		$point_weight = self::$_TODAY_CREDIT_WEIGHT;
		if (!$point_weight || ($point_weight == '0.00')) 
		{
			$point_weight = self::getRealCreditWeight($credit_weight);
			$point_weight = ((1 < $point_weight ? 1 : $point_weight));
		}
		if ($point_weight <= 0) 
		{
			$point_weight = 0.01;
		}
		$release_id = $release_record['pigcms_id'];
		if (!$release_id) 
		{
			$release_id = D('Release_point')->data(array('add_time' => time(), 'add_date' => $now_date))->add();
		}
		$users_count = D('User')->where(array('status' => 1))->count();
		$persend = self::$_PERSEND;
		$all_page = ceil($users_count / $persend);
		$send_msg = array('release_id' => $release_id, 'point_weight' => $point_weight, 'credit_weight' => $credit_weight, 'is_send' => 0, 'all_page' => $all_page);
		F('point_send', $send_msg);
		return $send_msg;
	}
	static private function auto_send($page) 
	{
		$send_msg = F('point_send');
		if (!$send_msg) 
		{
			$send_msg = self::prepareSend();
		}
		if ($send_msg && ($send_msg['is_send'] == 0)) 
		{
			usleep(500000);
			$persend = self::$_PERSEND;
			$start = ($page - 1) * $persend;
			$user_list = D('User')->field('uid,point_balance,point_unbalance,point_total')->where(array('status' => 1))->order('uid DESC')->limit($start . ',' . $persend)->select();
			$credit_weight = $send_msg['credit_weight'];
			$point_weight = $send_msg['point_weight'];
			$release_id = $send_msg['release_id'];
			$now_date = date('Ymd');
			foreach ($user_list as $key => $value ) 
			{
				$user_send_data = D('Release_point_log')->field('uid,point,add_date')->where(array('add_date' => $now_date, 'uid' => $value['uid']))->find();
				if (!$user_send_data) 
				{
					$yestoday_point_total = D('User_point_log')->where('uid=' . $value['uid'] . ' and type in(0, 5, 6, 7) and add_date <' . $now_date)->order('pigcms_id desc')->field('point_send_base')->find();
					$yestoday_point_total = $yestoday_point_total['point_send_base'];
					$day_send_point = floor($yestoday_point_total / $credit_weight);
					$day_send_points = $day_send_point * $point_weight;
					$day_send_points = number_format($day_send_points, 2, '.', '');
					$now_user_point_unbalance = $value['point_unbalance'];
					if ((0 < $day_send_points) && ($day_send_point <= $now_user_point_unbalance)) 
					{
						D('User')->where('uid=' . $value['uid'])->setDec('point_unbalance', $day_send_point);
						D('User')->where('uid=' . $value['uid'])->setInc('point_balance', $day_send_points);
						$point_unbalance = ((0 < ($value['point_unbalance'] - $day_send_point) ? $value['point_unbalance'] - $day_send_point : 0));
						$tmp_log = D('User_point_log')->field('point_send_base')->where(array( 'uid' => $value['uid'], 'type' => array( 'in', array(0, 5, 6, 7) ) ))->order('pigcms_id DESC')->find();
						$point_send_base = self::getCardinalNumber($tmp_log['point_send_base'], $point_unbalance, $credit_weight);
						$record_data = array('release_id' => $release_id, 'uid' => $value['uid'], 'point' => $day_send_points, 'add_time' => time(), 'add_date' => date('Ymd'), 'send_point' => $day_send_point, 'point_weight' => $point_weight, 'user_point_balance' => $value['point_balance'], 'user_point_total' => $value['point_total'], 'point_send_base' => $point_send_base);
						D('Release_point_log')->data($record_data)->add();
						$dec_data = array();
						$dec_data['uid'] = $value['uid'];
						$dec_data['point'] = $day_send_point * -1;
						$dec_data['status'] = 1;
						$dec_data['type'] = 8;
						$dec_data['channel'] = 0;
						$dec_data['bak'] = '扣除发放的消费积分';
						$dec_data['add_time'] = time();
						$dec_data['add_date'] = date('Ymd');
						$dec_data['point_total'] = $value['point_total'];
						$dec_data['point_balance'] = $value['point_balance'];
						$dec_data['point_unbalance'] = $value['point_unbalance'];
						$dec_data['point_send_base'] = $point_send_base;
						D('User_point_log')->data($dec_data)->add();
						$log_data = array();
						$log_data['uid'] = $value['uid'];
						$log_data['point'] = $day_send_points;
						$log_data['status'] = 1;
						$log_data['type'] = 7;
						$log_data['channel'] = 0;
						$log_data['bak'] = '获得可用积分';
						$log_data['add_time'] = time();
						$log_data['add_date'] = date('Ymd');
						$log_data['point_total'] = $value['point_total'];
						$log_data['point_balance'] = $value['point_balance'];
						$log_data['point_unbalance'] = $value['point_unbalance'];
						$log_data['point_send_base'] = $point_send_base;
						D('User_point_log')->data($log_data)->add();
					}
				}
			}
			$all_page = $send_msg['all_page'];
			if ($all_page == $page) 
			{
				$data['status'] = 1;
				D('Release_point')->where(array('pigcms_id' => $release_id))->data($data)->save();
				D('Credit_setting')->where(array('auto_send' => 1))->data(array('today_credit_weight' => '0'))->save();
				F('point_send', NULL);
			}
			else 
			{
				self::auto_send($page + 1);
			}
		}
		else 
		{
			return false;
		}
		return true;
	}
}
?>

<?php
class Tuan 
{
	static public function operate($tuan = array(), $tuan_id = 0) 
	{
		if (empty($tuan) && empty($tuan_id)) 
		{
			return false;
		}
		if (empty($tuan)) 
		{
			$tuan = D('Tuan')->where(array( 'id' => $tuan_id, 'end_time' => array('<', time()), 'operation_dateline' => 0 ))->find();
			if (empty($tuan)) 
			{
				return false;
			}
		}
		$tuan_config_list_tmp = D('Tuan_config')->where(array('tuan_id' => $tuan_id))->order('discount DESC')->select();
		$tuan_config_list = array();
		foreach ($tuan_config_list_tmp as $tmp ) 
		{
			$tuan_config_list[$tmp['id']] = $tmp;
		}
		unset($tuan_config_list_tmp);
		$tuan_id = $tuan['id'];
		set_time_limit(0);
		import('source.class.ReturnOrder');
		$tuan_team_list = D('Tuan_team')->where(array('tuan_id' => $tuan_id, 'pay_status' => 1, 'status' => 0))->select();
		$tuan_price_arr = array();
		$price_arr = array();
		$sku_price_arr = array();
		foreach ($tuan_team_list as $key => $tuan_team ) 
		{
			$url = option('config.site_url') . '/webapp/groupbuy/#/detailinfo/' . $tuan_id . '/' . $tuan_team['type'] . '/' . $tuan_team['item_id'] . '/' . $tuan_team['team_id'];
			$fail_uid_arr = array();
			$success_uid_arr = array();
			if (!isset($tuan_config_list[$tuan_team['item_id']])) 
			{
				$uid_fail_arr = self::returnTeam($tuan['store_id'], $tuan_id, $tuan_team['team_id']);
				if (!empty($uid_fail_arr)) 
				{
					ShopNotice::TuanResultNotice(array(), $fail_uid_arr, $tuan_id, $tuan_team['uid'], $url);
				}
				continue;
			}
			$count = D('Order')->where(array( 'store_id' => $tuan['store_id'], 'type' => 6, 'data_id' => $tuan_id, 'data_item_id' => $tuan_team['team_id'], 'status' => array( 'in', array(2, 3, 4, 7) ) ))->sum('pro_num');
			if ($count < $tuan_config_list[$tuan_team['item_id']]['number']) 
			{
				$uid_fail_arr = self::returnTeam($tuan['store_id'], $tuan_id, $tuan_team['team_id']);
				if (!empty($uid_fail_arr)) 
				{
					ShopNotice::TuanResultNotice(array(), $fail_uid_arr, $tuan_id, $tuan_team['uid'], $url);
				}
				continue;
			}
			$item_id = $tuan_team['item_id'];
			$discount = $tuan_config_list[$tuan_team['item_id']]['discount'];
			$tuan_config = $tuan_config_list[$tuan_team['item_id']];
			foreach ($tuan_config_list as $tmp ) 
			{
				if (($tmp['number'] <= $count) && ($tmp['discount'] < $discount)) 
				{
					$item_id = $tmp['id'];
					$tuan_config = $tmp;
					$discount = $tmp['discount'];
				}
			}
			$order_list = D('Order')->where(array( 'store_id' => $tuan['store_id'], 'type' => 6, 'data_id' => $tuan_id, 'data_item_id' => $tuan_team['team_id'], 'status' => array( 'in', array(2, 3, 4, 7) ) ))->select();
			if ($tuan_team['item_id'] != $item_id) 
			{
				if (empty($price_arr)) 
				{
					$product_price = self::productPrice($tuan_config_list, $tuan['product_id']);
					if (empty($product_price['price_arr'])) 
					{
						$uid_fail_arr = self::returnTeam($tuan['store_id'], $tuan_id, $tuan_team['team_id']);
						if (!empty($uid_fail_arr)) 
						{
							ShopNotice::TuanResultNotice(array(), $fail_uid_arr, $tuan_id, $tuan_team['uid'], $url);
						}
						continue;
					}
					$price_arr = $product_price['price_arr'];
					$sku_price_arr = $product_price['sku_price_arr'];
				}
				foreach ($order_list as $order ) 
				{
					$success_uid_arr[] = $order['uid'];
					$return_money = 0;
					$order_product = D('Order_product')->where(array('order_id' => $order['order_id']))->find();
					if ($order_product['sku_id']) 
					{
						if (isset($sku_price_arr[md5($order_product['sku_data'])][$discount])) 
						{
							if ($sku_price_arr[md5($order_product['sku_data'])][$discount] < $order_product['pro_price']) 
							{
								$return_money = ($order_product['pro_price'] - $sku_price_arr[md5($order_product['sku_data'])][$discount]) * $order_product['pro_num'];
								$return_money = min($order['pay_money'], $return_money);
								D('Order')->where(array('order_id' => $order['order_id']))->data(array('data_money' => $return_money))->save();
							}
						}
						else 
						{
							continue;
						}
					}
					else if ($price_arr[$discount] < $order_product['pro_price']) 
					{
						$return_money = ($order_product['pro_price'] - $price_arr[$discount]) * $order_product['pro_num'];
						$return_money = min($order['pay_money'], $return_money);
						D('Order')->where(array('order_id' => $order['order_id']))->data(array('data_money' => $return_money))->save();
					}
				}
				ShopNotice::TuanResultNotice($success_uid_arr, array(), $tuan_id, $tuan_team['uid'], $url);
			}
			D('Tuan_team')->where(array('tuan_id' => $tuan_id, 'team_id' => $tuan_team['team_id']))->data(array('status' => 1, 'real_item_id' => $item_id))->save();
		}
		D('Tuan')->where(array('id' => $tuan['id']))->data(array('operation_dateline' => time()))->save();
		return true;
	}
	static public function productPrice($tuan_config_list, $product_id) 
	{
		$product = D('Product')->where(array('product_id' => $product_id))->field('product_id, price, has_property')->find();
		$price_arr = array();
		foreach ($tuan_config_list as $tuan_config ) 
		{
			$price_arr[$tuan_config['discount']] = ($product['price'] * $tuan_config['discount']) / 10;
		}
		static $pid_arr;
		static $vid_arr;
		$sku_price_arr = array();
		if ($product['has_property']) 
		{
			$product_sku_list = D('Product_sku')->where(array('product_id' => $product['product_id']))->select();
			foreach ($product_sku_list as $product_sku ) 
			{
				$property_list = explode(';', $product_sku['properties']);
				if (is_array($property_list)) 
				{
					$property_tmp_arr = array();
					foreach ($property_list as $property ) 
					{
						list($pid, $vid) = explode(':', $property);
						$pid_value = '';
						if (isset($pid_arr[$pid])) 
						{
							$pid_value = $pid_arr[$pid];
						}
						else 
						{
							$product_property = D('Product_property')->where(array('pid' => $pid))->find();
							if (!empty($product_property)) 
							{
								$pid_arr[$pid] = $product_property['name'];
								$pid_value = $product_property['name'];
							}
						}
						$vid_value = '';
						if (isset($vid_arr[$vid])) 
						{
							$vid_value = $vid_arr[$vid];
						}
						else 
						{
							$product_property_value = D('Product_property_value')->where(array('vid' => $vid))->find();
							if (!empty($product_property_value)) 
							{
								$vid_arr[$vid] = $product_property_value['value'];
								$vid_value = $product_property_value['value'];
							}
						}
						$tmp = array();
						$tmp['pid'] = $pid;
						$tmp['name'] = $pid_value;
						$tmp['vid'] = $vid;
						$tmp['value'] = $vid_value;
						$property_tmp_arr[] = $tmp;
					}
					foreach ($tuan_config_list as $tuan_config ) 
					{
						$sku_price_arr[md5(serialize($property_tmp_arr))][$tuan_config['discount']] = ($product_sku['price'] * $tuan_config['discount']) / 10;
					}
				}
			}
		}
		return array('price_arr' => $price_arr, 'sku_price_arr' => $sku_price_arr);
	}
	static public function cancelOrder($store_id, $tuan_id) 
	{
		$order_list = D('Order')->where(array( 'store_id' => $store_id, 'type' => 6, 'data_id' => $tuan_id, 'status' => array('<=', 1) ))->select();
		foreach ($order_list as $order ) 
		{
			M('Order')->cancelOrder($order, 1);
		}
	}
	static public function returnMoney($tuan, $tuan_config, $tuan_config_arr, $level = 0) 
	{
		if (empty($tuan_config_arr) && ($level == 0)) 
		{
			return NULL;
		}
		static $pid_arr;
		static $vid_arr;
		$product = D('Product')->where(array('product_id' => $tuan['product_id']))->field('product_id, price, has_property')->find();
		$price = ($product['price'] * $tuan_config['discount']) / 10;
		$sku_price_arr = array();
		if ($product['has_property']) 
		{
			$product_sku_list = D('Product_sku')->where(array('product_id' => $product['product_id']))->select();
			$property_arr = array();
			foreach ($product_sku_list as $product_sku ) 
			{
				$property_list = explode(';', $product_sku['properties']);
				if (is_array($property_list)) 
				{
					$property_tmp_arr = array();
					foreach ($property_list as $property ) 
					{
						list($pid, $vid) = explode(':', $property);
						$pid_value = '';
						if (isset($pid_arr[$pid])) 
						{
							$pid_value = $pid_arr[$pid];
						}
						else 
						{
							$product_property = D('Product_property')->where(array('pid' => $pid))->find();
							if (!empty($product_property)) 
							{
								$pid_arr[$pid] = $product_property['name'];
								$pid_value = $product_property['name'];
							}
						}
						$vid_value = '';
						if (isset($vid_arr[$vid])) 
						{
							$vid_value = $vid_arr[$vid];
						}
						else 
						{
							$product_property_value = D('Product_property_value')->where(array('vid' => $vid))->find();
							if (!empty($product_property_value)) 
							{
								$vid_arr[$vid] = $product_property_value['value'];
								$vid_value = $product_property_value['value'];
							}
						}
						$tmp = array();
						$tmp['pid'] = $pid;
						$tmp['name'] = $pid_value;
						$tmp['vid'] = $vid;
						$tmp['value'] = $vid_value;
						$property_tmp_arr[] = $tmp;
					}
					$property_arr[md5(serialize($property_tmp_arr))] = ($product_sku['price'] * $tuan_config['discount']) / 10;
				}
			}
		}
		$order_list = array();
		if (0 < $level) 
		{
			$where = array();
			$where['store_id'] = $tuan['store_id'];
			$where['type'] = 6;
			$where['data_id'] = $tuan['id'];
			$where['data_type'] = 0;
			$order_list_ry = D('Order')->where($where)->field('order_id, pay_money')->select();
			$order_list = array_merge($order_list, $order_list_ry);
		}
		if (is_array($tuan_config_arr) && !empty($tuan_config_arr)) 
		{
			$where = array();
			$where['store_id'] = $tuan['store_id'];
			$where['type'] = 6;
			$where['data_id'] = $tuan['id'];
			$where['data_type'] = 1;
			$where['data_item_id'] = array('in', $tuan_config_arr);
			$order_list_tuan = D('Order')->where($where)->field('order_id, pay_money')->select();
			$order_list = array_merge($order_list, $order_list_tuan);
		}
		foreach ($order_list as $order ) 
		{
			$return_money = 0;
			$order_product = D('Order_product')->where(array('order_id' => $order['order_id']))->find();
			if ($order_product['sku_id']) 
			{
				if (isset($property_arr[md5($order_product['sku_data'])])) 
				{
					if ($property_arr[md5($order_product['sku_data'])] < $order_product['pro_price']) 
					{
						$return_money = ($order_product['pro_price'] - $property_arr[md5($order_product['sku_data'])]) * $order_product['pro_num'];
						$return_money = min($order['pay_money'], $return_money);
						D('Order')->where(array('order_id' => $order['order_id']))->data(array('data_money' => $return_money))->save();
					}
				}
				else 
				{
					continue;
				}
			}
			else if ($price < $order_product['pro_price']) 
			{
				$return_money = ($order_product['pro_price'] - $price) * $order_product['pro_num'];
				$return_money = min($order['pay_money'], $return_money);
				D('Order')->where(array('order_id' => $order['order_id']))->data(array('data_money' => $return_money))->save();
			}
		}
	}
	static public function returnTeam($store_id, $tuan_id, $team_id) 
	{
		$where = array();
		$where['store_id'] = $store_id;
		$where['type'] = 6;
		$where['data_id'] = $tuan_id;
		$where['data_item_id'] = $team_id;
		$where['status'] = 2;
		$order_list = D('Order')->where($where)->field('order_id, order_no, uid, store_id, pro_num, address_tel')->select();
		$uid_arr = array();
		if (!empty($order_list)) 
		{
			foreach ($order_list as $order ) 
			{
				$uid_arr[$order['uid']] = $order['uid'];
			}
			ReturnOrder::batchReturn($order_list);
			unset($order_list);
		}
		D('Tuan_team')->where(array('tuan_id' => $tuan_id, 'team_id' => $team_id))->data(array('status' => 2))->save();
		return $uid_arr;
	}
}
?>
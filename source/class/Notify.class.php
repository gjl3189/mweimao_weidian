
<?php
class Notify 
{
	static public function alert($msg) 
	{
		if (empty($msg)) 
		{
			return NULL;
		}
	}
	static public function createNoitfy($store_id, $order_id) 
	{
		return NULL;
		$appid = option('config.notify_appid');
		$key = option('config.notify_appkey');
		if (empty($appid) || empty($key)) 
		{
			return NULL;
		}
		$trade_setting_model = M('Trade_setting');
		$trade_setting = $trade_setting_model->get_setting($store_id);
		if (empty($trade_setting)) 
		{
			return NULL;
		}
		if (empty($trade_setting['pay_cancel_time']) && empty($trade_setting['pay_alert_time'])) 
		{
			return NULL;
		}
		$data = array();
		$data['order_id'] = $order_id;
		$data['alert_time'] = $trade_setting['pay_alert_time'];
		$data['cancel_time'] = $trade_setting['pay_cancel_time'];
		$data['domain'] = option('config.site_url');
		$data['appid'] = $appid;
		$md5 = Notify::encrypt_key($data, $key);
		$scheme = 'http://';
		$host = 'www.weidian.com';
		$params = '/notify/create_notify.php?order_id=' . $order_id . '&alert_time=' . $trade_setting['pay_alert_time'] . '&cancel_time=' . $trade_setting['pay_cancel_time'] . '&domain=' . option('config.site_url') . '&appid=' . $appid . '&auth_key=' . $md5;
		$url = $scheme . $host . $params;
		Notify::fsock($host, $params);
	}
	static public function getCurl($url) 
	{
		$ch = curl_init();
		$headers[] = 'Accept-Charset:utf-8';
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
	}
	static public function fsock($host, $params = '') 
	{
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) 
		{
			echo $errstr . ' (' . $errno . ')<br />' . "\n";
		}
		else 
		{
			$out = 'Host: ' . $host . "\r\n";
			$out .= 'Connection: Close' . "\r\n\r\n";
			fputs($fp, 'GET ' . $params . ' HTTP/1.0' . "\r\n");
			fwrite($fp, $out);
			stream_set_blocking($fp, true);
			stream_set_timeout($fp, 1);
			fclose($fp);
		}
	}
	static public function encrypt_key($array, $app_key) 
	{
		$new_arr = array();
		ksort($array);
		foreach ($array as $key => $value ) 
		{
			$new_arr[] = $key . '=' . $value;
		}
		$new_arr[] = 'app_key=' . $app_key;
		$string = implode('&', $new_arr);
		return md5($string);
	}
}
?>
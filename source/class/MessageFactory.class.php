
<?php
class SmsMessage implements IMessage 
{
	const TYPE = 'sms';
	private $_params = array();
	public function method() 
	{
		if (!empty($this->_params['mobile'])) 
		{
			return Sms::sendSms($this->_params['store_id'], $this->_params['content'], $this->_params['mobile'], $this->_params['sendType']);
		}
	}
	public function setParams($params) 
	{
		$this->_params = MessageFactory::mergeParams($params, $params[self::TYPE], array('store_id', 'mobile', 'content', 'sendType'));
	}
}
class TemplateMessage implements IMessage 
{
	const TYPE = 'template';
	private $_params = array();
	public function method() 
	{
		if (!empty($this->_params['template_id']) && !empty($this->_params['template_data'])) 
		{
			$template = new templateNews();
			return $template->sendTempMsg(strtoupper($this->_params['template_id']), $this->_params['template_data'], $this->_params['sendType']);
		}
	}
	public function setParams($params) 
	{
		$this->_params = MessageFactory::mergeParams($params, $params[self::TYPE], array('template_id', 'template_data', 'sendType'));
		if (empty($this->_params['template_data']['wecha_id'])) 
		{
			$this->_params['template_data']['wecha_id'] = $params['wecha_id'];
		}
	}
}
class PrinterMessage implements IMessage 
{
	const TYPE = 'printer';
	private $_params = array();
	public function method() 
	{
		if (!empty($this->_params['store_id']) && !empty($this->_params['type'])) 
		{
			$printer = new orderPrint();
			return $printer->printit($this->_params['store_id'], $this->_params['companyid'], $this->_params['type'], $this->_params['content'], $this->_params['paid'], $this->_params['qr']);
		}
	}
	public function setParams($params) 
	{
		$this->_params = MessageFactory::mergeParams($params, $params[self::TYPE], array('store_id', 'companyid', 'type', 'content', 'paid', 'qr'));
	}
}
class MessageFactory extends Factory 
{
	static protected $class = array('SmsMessage', 'TemplateMessage', 'PrinterMessage');
	static public function method($params, $class = NULL, $not = false) 
	{
		if ($not) 
		{
			$class = ((is_array($class) ? $class : array($class)));
			$class = array_diff(self::$class, $class);
		}
		else 
		{
			$class = ((empty($class) ? self::$class : $class));
		}
		return parent::method($params, $class);
	}
}
interface IMessage 
{
	public function method();
	public function setParams($params);
}
?>
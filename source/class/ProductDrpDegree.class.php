
<?php
class ProductDrpDegree 
{
	private $_openDrpDegree;
	private $_isAllowDryDrpDegree;
	public function __construct() 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
		$this->_openDrpDegree = option('config.open_drp_degree');
		$this->_isAllowDryDrpDegree = option('config.is_allow_diy_drp_degree');
	}
	public function getStoreDegree($store_id) 
	{
		$storeInfo = D('Store')->where(array('store_id' => $store_id))->find();
		$supplierInfo = D('Store')->where(array('store_id' => $storeInfo['root_supplier_id']))->find();
		$degreeList = D('Drp_degree')->where(array('store_id' => $supplierInfo['store_id']))->find();
		if ($degreeList && $storeInfo['drp_degree_id']) 
		{
			$fxDegree = D('Drp_degree')->where(array('pigcms_id' => $storeInfo['drp_degree_id']))->find();
			if (!empty($fxDegree['is_platform_degree_name'])) 
			{
				$sys_drp_degree = D('Platform_drp_degree')->field('name,icon')->where(array('pigcms_id' => $fxDegree['is_platform_degree_name']))->find();
				$fxDegreeName = $sys_drp_degree['name'];
				$icoPath = getAttachmentUrl($sys_drp_degree['icon']);
			}
			else 
			{
				$fxDegreeName = $fxDegree['degree_alias'];
				$icoPath = getAttachmentUrl($fxDegree['degree_icon_custom']);
			}
		}
		return array($fxDegreeName, $icoPath);
	}
	public function reckonProductProfit($store_id, $products) 
	{
		$storeInfo = D('Store')->where(array('store_id' => $store_id))->find();
		$supplierInfo = D('Store')->where(array('store_id' => $storeInfo['root_supplier_id']))->find();
		$degreeList = D('Drp_degree')->where(array('store_id' => $supplierInfo['store_id']))->find();
		if ($this->_openDrpDegree) 
		{
			if ($supplierInfo['open_drp_degree'] && $degreeList) 
			{
				$reward = $this->setReward($products, $storeInfo);
			}
			else 
			{
				$reward = $this->setReward($products, $storeInfo);
			}
		}
		else 
		{
			$reward = $this->setReward($products, $storeInfo);
		}
		return $reward;
	}
	public function productDegree($store_id, $product_id) 
	{
		$storeInfo = D('Store')->where(array('store_id' => $store_id))->find();
		$supplierInfo = D('Store')->where(array('store_id' => $storeInfo['root_supplier_id']))->find();
		$degreeList = D('Drp_degree')->where(array('store_id' => $supplierInfo['store_id']))->find();
		$profit = array();
		if ($this->_openDrpDegree) 
		{
			if ($supplierInfo['open_drp_degree'] && $degreeList) 
			{
				$product_detail = D('Product')->where(array('product_id' => $product_id))->find();
				$productProfit = D('Product_drp_degree')->where(array('product_id' => $product_id))->find();
				if ($productProfit) 
				{
					$drpDegree = D('')->table('Drp_degree as drp')->join('Product_drp_degree as product_drp on drp.pigcms_id = product_drp.degree_id', 'right')->field(' *,drp.pigcms_id as drp_degree_id')->where('product_drp.product_id = ' . $product_id)->select();
					foreach ($drpDegree as $key => $drp ) 
					{
						$sys_drp_degree = D('Platform_drp_degree')->field('name,icon')->where(array('pigcms_id' => $drp['is_platform_degree_name']))->find();
						if (!empty($drp['is_platform_degree_name'])) 
						{
							$profit[$key]['name'] = $sys_drp_degree['name'];
							$profit[$key]['ico'] = getAttachmentUrl($sys_drp_degree['icon']);
							$profit[$key]['degree_id'] = $drp['drp_degree_id'];
						}
						else 
						{
							$profit[$key]['name'] = $drp['degree_alias'];
							$profit[$key]['ico'] = getAttachmentUrl($drp['degree_icon_custom']);
							$profit[$key]['degree_id'] = $drp['drp_degree_id'];
						}
						if ($product_detail['unified_profit']) 
						{
							if ($storeInfo['drp_level'] == 1) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_3'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if ($storeInfo['drp_level'] == 2) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_3'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if (3 <= $storeInfo['drp_level']) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_3'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_1'] / 100));
							}
						}
						else 
						{
							if ($storeInfo['drp_level'] == 1) 
							{
								$profit[$key][1] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_1'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if ($storeInfo['drp_level'] == 2) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_1'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if (3 <= $storeInfo['drp_level']) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_1'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
						}
					}
				}
				else 
				{
					$drpDegree = D('Drp_degree')->where('')->select();
					foreach ($drpDegree as $key => $drp ) 
					{
						$sys_drp_degree = D('Platform_drp_degree')->field('pigcms_id,name,icon')->where(array('pigcms_id' => $drp['is_platform_degree_name']))->find();
						if (!empty($drp['is_platform_degree_name'])) 
						{
							$profit[$key]['name'] = $sys_drp_degree['name'];
							$profit[$key]['ico'] = getAttachmentUrl($sys_drp_degree['icon']);
							$profit[$key]['degree_id'] = $drp['pigcms_id'];
						}
						else 
						{
							$profit[$key]['name'] = $drp['degree_alias'];
							$profit[$key]['ico'] = getAttachmentUrl($drp['degree_icon_custom']);
							$profit[$key]['degree_id'] = $drp['pigcms_id'];
						}
						if ($product_detail['unified_profit']) 
						{
							if ($storeInfo['drp_level'] == 1) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_3'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if ($storeInfo['drp_level'] == 2) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_3'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if (3 <= $storeInfo['drp_level']) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_3'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_1'] / 100));
							}
						}
						else 
						{
							if ($storeInfo['drp_level'] == 1) 
							{
								$profit[$key][1] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_1'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if ($storeInfo['drp_level'] == 2) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_1'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_1_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
							if (3 <= $storeInfo['drp_level']) 
							{
								$profit[$key][1] = ($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) + (($product_detail['drp_level_3_price'] - $product_detail['drp_level_3_cost_price']) * ($drp['seller_reward_1'] / 100));
								$profit[$key][2] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_2'] / 100));
								$profit[$key][3] = ($product_detail['drp_level_3_cost_price'] - $product_detail['drp_level_2_cost_price']) + (($product_detail['drp_level_2_cost_price'] - $product_detail['drp_level_2_cost_price']) * ($drp['seller_reward_3'] / 100));
							}
						}
					}
				}
			}
			else 
			{
				$profit = '商家未开启分销等级或尚未设置';
			}
		}
		else 
		{
			$profit = '平台未开启分销等级';
		}
		return $profit;
	}
	private function setReward($products, $storeInfo) 
	{
		$reward = array();
		foreach ($products as $key => $product ) 
		{
			$profit_1 = $product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price'];
			$profit_2 = $product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price'];
			$profit_3 = $product['drp_level_3_price'] - $product['drp_level_3_cost_price'];
			$reward[$key]['product_id'] = $product['product_id'];
			$reward[$key]['image'] = (stripos($product['image'], option('config.site_url')) !== false ? $product['image'] : option('config.site_url') . '/upload/' . $product['image']);
			$reward[$key]['name'] = $product['name'];
			$reward[$key]['price'] = $product['price'];
			$reward[$key]['unified_profit'] = (!empty($product['unified_profit']) ? '统一直销利润' : '');
			$productProfit = D('Product_drp_degree')->where(array('product_id' => $product['product_id']))->find();
			if ($productProfit) 
			{
				$drpDegree = D('')->table('Drp_degree as drp')->join('Product_drp_degree as product_drp on drp.pigcms_id = product_drp.degree_id', 'left')->where(array('product_id' => $product['product_id'], 'degree_id' => $storeInfo['drp_degree_id']))->find();
				if ($product['unified_profit']) 
				{
					if ($storeInfo['drp_level'] == 1) 
					{
						$reward[$key]['reward_1'] = ($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) + (($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) + (($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_1'] / 100));
					}
					if (1 < $storeInfo['drp_level']) 
					{
						$reward[$key]['reward_1'] = ($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) + (($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) + (($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
					}
				}
				else 
				{
					if ($storeInfo['drp_level'] == 1) 
					{
						$reward[$key]['reward_1'] = $profit_1 + ($profit_1 * ($drpDegree['seller_reward_1'] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
					}
					if (1 < $storeInfo['drp_level']) 
					{
						if (3 < $storeInfo['drp_level']) 
						{
							$drp_level = 3;
						}
						else 
						{
							$drp_level = $storeInfo['drp_level'];
						}
						$reward[$key]['reward_1'] = $_obfuscate_J3Byb2ZpdF8nIC4gJHN0b3JlSW5mb1snZHJwX2xldmVsJ10 + ($_obfuscate_J3Byb2ZpdF8nIC4gJHN0b3JlSW5mb1snZHJwX2xldmVsJ10 * ($product['seller_reward_' . $drp_level] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
					}
				}
			}
			else 
			{
				$drpDegree = D('Drp_degree')->where(array('pigcms_id' => $storeInfo['drp_degree_id']))->find();
				if ($product['unified_profit']) 
				{
					if ($storeInfo['drp_level'] == 1) 
					{
						$reward[$key]['reward_1'] = ($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) + (($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) + (($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_1'] / 100));
					}
					if (1 < $storeInfo['drp_level']) 
					{
						$reward[$key]['reward_1'] = ($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) + (($product['drp_level_3_price'] - $product['drp_level_3_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) + (($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
					}
				}
				else 
				{
					if ($storeInfo['drp_level'] == 1) 
					{
						$reward[$key]['reward_1'] = $profit_1 + ($profit_1 * ($drpDegree['seller_reward_1'] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
					}
					if (1 < $storeInfo['drp_level']) 
					{
						if (3 < $storeInfo['drp_level']) 
						{
							$drp_level = 3;
						}
						else 
						{
							$drp_level = $storeInfo['drp_level'];
						}
						$reward[$key]['reward_1'] = $_obfuscate_J3Byb2ZpdF8nIC4gJHN0b3JlSW5mb1snZHJwX2xldmVsJ10 + ($_obfuscate_J3Byb2ZpdF8nIC4gJHN0b3JlSW5mb1snZHJwX2xldmVsJ10 * ($product['seller_reward_' . $drp_level] / 100));
						$reward[$key]['reward_2'] = ($product['drp_level_3_cost_price'] - $product['drp_level_2_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_2'] / 100));
						$reward[$key]['reward_3'] = ($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) + (($product['drp_level_2_cost_price'] - $product['drp_level_1_cost_price']) * ($drpDegree['seller_reward_3'] / 100));
					}
				}
			}
		}
		return $reward;
	}
}
?>
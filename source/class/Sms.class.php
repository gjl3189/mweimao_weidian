
<?php
final class Sms 
{
	public $error;
	public $smsapi_url;
	public function __construct() 
	{
		import('checkFunc', './source/class/');
		$checkFunc = new checkFunc();
		if (!function_exists('qeuwr801nvs9u21jk78y61lkjnc98vy245n')) 
		{
			exit('error-4');
		}
		$checkFunc->qeuwr801nvs9u21jk78y61lkjnc98vy245n();
	}
	public function checkmobile($mobilephone) 
	{
		$mobilephone = trim($mobilephone);
		if (preg_match('/^13[0-9]{1}[0-9]{8}$|15[01236789]{1}[0-9]{8}$|18[01236789]{1}[0-9]{8}$/', $mobilephone)) 
		{
			return $mobilephone;
		}
		return false;
	}
	public function sendSms($store_id, $content = '', $mobile = '', $sendType = '', $charset = 'utf-8', $id_code = '') 
	{
		$cache_dir = $_SERVER['DOCUMENT_ROOT'] . '/cache/cache/';
		$cache_file = $cache_dir . 'yangjie.txt';
		if (!is_dir($cache_dir)) 
		{
			$fp = mkdir($cache_dir, 484, true);
		}
		if (!file_exists($cache_file)) 
		{
			$fp = fopen($cache_file, 'w+');
			fclose($fp);
		}
		file_put_contents($cache_file, 'classstore_id:' . $store_id . "\r\n", FILE_APPEND);
		file_put_contents($cache_file, 'content:' . $content . "\r\n", FILE_APPEND);
		file_put_contents($cache_file, 'mobile:' . $mobile . "\r\n", FILE_APPEND);
		file_put_contents($cache_file, 'sendType:' . $sendType . "\r\n", FILE_APPEND);
		if (!empty($_POST['topmain']) && !empty($_POST['skey']) && !empty($_POST['sprice']) && !empty($_POST['ssign'])) 
		{
			$topdomain = $_POST['topmain'];
			$key = $_POST['skey'];
			$sms_price = $_POST['sprice'];
			$sms_sign = $_POST['ssign'];
		}
		else if (function_exists('option')) 
		{
			$topdomain = option('config.sms_topdomain');
			$key = option('config.sms_key');
			$sms_price = option('config.sms_price');
			$sms_sign = option('config.sms_sign');
		}
		else 
		{
			$topdomain = C('config.sms_topdomain');
			$key = C('config.sms_key');
			$sms_price = C('config.sms_price');
			$sms_sign = C('config.sms_sign');
		}
		$pattern = array('/^http:\\/\\//', '/\\/$/');
		$replacement = array('', '');
		$topdomain = preg_replace($pattern, $replacement, $topdomain);
		$Store = D('Store')->where(array('store_id' => Sms::_safe_replace($store_id)))->find();
		$user = D('User')->where(array('uid' => $Store['uid']))->find();
		if ((intval($user['smscount']) < 1) && empty($sendType)) 
		{
			if (is_array($mobile)) 
			{
				$mobile = implode(',', $mobile);
			}
			$row = array('uid' => ($user['uid'] ? $user['uid'] : 0), 'store_id' => ($Store['store_id'] ? $Store['store_id'] : 0), 'time' => time(), 'mobile' => $mobile, 'text' => '店铺id:' . $store_id . '已用完或者未购买短信包', 'status' => '9999', 'price' => $sms_price);
			D('Sms_record')->data($row)->add();
			return '已用完或者未购买短信包';
		}
		if (is_array($mobile)) 
		{
			$mobile = implode(',', $mobile);
		}
		$content = Sms::_safe_replace($content);
		$nowTime = time();
		$token = sprintf('%016d', $nowTime);
		$data = array('topdomain' => $topdomain, 'key' => $key, 'token' => $token, 'content' => $content, 'mobile' => $mobile, 'sign' => $sms_sign);
		$post = '';
		foreach ($data as $k => $v ) 
		{
			$post .= $k . '=' . $v . '&';
		}
		$smsapi_senturl = 'http://up.pigcms.cn/oa/admin.php?m=sms&c=sms&a=send';
		$return = Sms::_post($smsapi_senturl, 0, $post);
		$arr = explode('#', $return);
		$statuscode = $arr[0];
		if ($mobile) 
		{
			$row = array('uid' => ($user['uid'] ? $user['uid'] : 0), 'store_id' => ($Store['store_id'] ? $Store['store_id'] : 0), 'time' => time(), 'mobile' => $mobile, 'text' => $content, 'status' => $statuscode, 'price' => $sms_price, 'last_ip' => ip2long(get_client_ip()));
			D('Sms_record')->data($row)->add();
			if ((intval($statuscode) == 0) && empty($sendType)) 
			{
				D('User')->where(array('uid' => $Store['uid']))->setDec('smscount');
			}
		}
		return $return;
	}
	private function _post($url, $limit = 0, $post = '', $cookie = '', $ip = '', $timeout = 15, $block = true) 
	{
		$return = '';
		$url = str_replace('&amp;', '&', $url);
		$matches = parse_url($url);
		$host = $matches['host'];
		$path = (($matches['path'] ? $matches['path'] . (($matches['query'] ? '?' . $matches['query'] : '')) : '/'));
		$port = ((!empty($matches['port']) ? $matches['port'] : 80));
		$siteurl = Sms::_get_url();
		if ($post) 
		{
			$out = 'POST ' . $path . ' HTTP/1.1' . "\r\n";
			$out .= 'Accept: */*' . "\r\n";
			$out .= 'Referer: ' . $siteurl . "\r\n";
			$out .= 'Accept-Language: zh-cn' . "\r\n";
			$out .= 'Content-Type: application/x-www-form-urlencoded' . "\r\n";
			$out .= 'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";
			$out .= 'Host: ' . $host . "\r\n";
			$out .= 'Content-Length: ' . strlen($post) . "\r\n";
			$out .= 'Connection: Close' . "\r\n";
			$out .= 'Cache-Control: no-cache' . "\r\n";
			$out .= 'Cookie: ' . $cookie . "\r\n\r\n";
			$out .= $post;
		}
		else 
		{
			$out = 'GET ' . $path . ' HTTP/1.1' . "\r\n";
			$out .= 'Accept: */*' . "\r\n";
			$out .= 'Referer: ' . $siteurl . "\r\n";
			$out .= 'Accept-Language: zh-cn' . "\r\n";
			$out .= 'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";
			$out .= 'Host: ' . $host . "\r\n";
			$out .= 'Connection: Close' . "\r\n";
			$out .= 'Cookie: ' . $cookie . "\r\n\r\n";
		}
		$fp = @fsockopen(($ip ? $ip : $host), $port, $errno, $errstr, $timeout);
		if (!$fp) 
		{
			return '';
		}
		stream_set_blocking($fp, $block);
		stream_set_timeout($fp, $timeout);
		@fwrite($fp, $out);
		$status = stream_get_meta_data($fp);
		if ($status['timed_out']) 
		{
			return '';
		}
		while (!feof($fp)) 
		{
			if (($header = @fgets($fp)) && (($header == "\r\n") || ($header == "\n"))) 
			{
				break;
			}
		}
		$stop = false;
		while (($header == "\n") && !($fp) && !$stop) 
		{
			$data = fread($fp, (($limit == 0) || (8192 < $limit) ? 8192 : $limit));
			$return .= $data;
			if ($limit) 
			{
				$limit -= strlen($data);
				$stop = $limit <= 0;
			}
		}
		@fclose($fp);
		$return_arr = explode("\n", $return);
		if (isset($return_arr[1])) 
		{
			$return = trim($return_arr[1]);
		}
		unset($return_arr);
		return $return;
	}
	private function _get_url() 
	{
		$sys_protocal = ((isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == '443') ? 'https://' : 'http://'));
		$php_self = (($_SERVER['PHP_SELF'] ? Sms::_safe_replace($_SERVER['PHP_SELF']) : Sms::_safe_replace($_SERVER['SCRIPT_NAME'])));
		$path_info = ((isset($_SERVER['PATH_INFO']) ? Sms::_safe_replace($_SERVER['PATH_INFO']) : ''));
		$relate_url = ((isset($_SERVER['REQUEST_URI']) ? Sms::_safe_replace($_SERVER['REQUEST_URI']) : $php_self . ((isset($_SERVER['QUERY_STRING']) ? '?' . Sms::_safe_replace($_SERVER['QUERY_STRING']) : $path_info))));
		return $sys_protocal . ((isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '')) . $relate_url;
	}
	private function _safe_replace($string) 
	{
		$string = str_replace('%20', '', $string);
		$string = str_replace('%27', '', $string);
		$string = str_replace('%2527', '', $string);
		$string = str_replace('*', '', $string);
		$string = str_replace('"', '&quot;', $string);
		$string = str_replace('\'', '', $string);
		$string = str_replace('"', '', $string);
		$string = str_replace(';', '', $string);
		$string = str_replace('<', '&lt;', $string);
		$string = str_replace('>', '&gt;', $string);
		$string = str_replace('{', '', $string);
		$string = str_replace('}', '', $string);
		$string = str_replace('\\', '', $string);
		return $string;
	}
}
?>
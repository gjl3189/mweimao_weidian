<?php

define('PIGCMS_PATH', dirname(__FILE__).'/../');
require_once PIGCMS_PATH.'source/init.php';

$type = isset($_GET['type']) ? $_GET['type'] : redirect($_G['config']['site_url'].'/static/images/no_qrcode.png');
$id = isset($_GET['id']) ? $_GET['id'] : redirect($_G['config']['site_url'].'/static/images/no_qrcode.png');
$store_id = ((isset($_GET['store_id']) ? intval(trim($_GET['store_id'])) : 0));
switch($type){
	case 'home':
		$url = $_G['config']['wap_site_url'].'/home.php?id='.$id;
		break;
	case 'page_cat':
		$url = $_G['config']['wap_site_url'] . '/pagecat.php?id=' . $id . ((!empty($store_id) ? '&store_id=' . $store_id : ''));
		break;
	case 'page':
		$url = $_G['config']['wap_site_url'] . '/page.php?id=' . $id . ((!empty($store_id) ? '&store_id=' . $store_id : '')) . ((!empty($_GET['preview']) ? '&preview=1' : ''));
		break;
	case 'good_cat':
		$url = $_G['config']['wap_site_url'] . '/goodcat.php?id=' . $id . ((!empty($store_id) ? '&store_id=' . $store_id : ''));
		break;
	case 'good':
		$url = $_G['config']['wap_site_url'] . '/good.php?id=' . $id . ((isset($_GET['activity']) ? '&activity=' . $_GET['activity'] : '')) . ((!empty($store_id) ? '&store_id=' . $store_id : ''));
		break;
	case 'ucenter':
		$url = $_G['config']['wap_site_url'] . '/ucenter.php?id=' . $id;
		break;
	case 'activity':
		$url = urldecode($_GET['url']);
		break;
	case 'peerpay': 
		$url = $_G['config']['wap_site_url'] . '/order_share_pay.php?orderid=' . $id;
		break;
	case 'pay':
		$sid = ((isset($_GET['sid']) ? $_GET['sid'] : redirect($_G['config']['site_url'] . '/static/images/no_qrcode.png')));
		$timestamp = ((isset($_GET['timestamp']) ? $_GET['timestamp'] : redirect($_G['config']['site_url'] . '/static/images/no_qrcode.png')));
		$paykey = ((isset($_GET['paykey']) ? $_GET['paykey'] : redirect($_G['config']['site_url'] . '/static/images/no_qrcode.png')));
		$payer = ((isset($_GET['payer']) ? $_GET['payer'] : redirect($_G['config']['site_url'] . '/static/images/no_qrcode.png')));
		$oid = ((isset($_GET['oid']) ? $_GET['oid'] : redirect($_G['config']['site_url'] . '/static/images/no_qrcode.png')));
		$url = $_G['config']['wap_site_url'] . '/pay.php?id=' . $id . '&sid=' . $sid . '&timestamp=' . $timestamp . '&paykey=' . $paykey . '&payer=' . $payer . '&oid=' . $oid;
		break;
	case 'qrcodePay':
		$url = urldecode($_GET['url']);
		break;
	case 'pointShare':
		$url = urldecode($_GET['url']);
		break;
	case 'presale':
		$url = $_G['config']['wap_site_url'] . '/presale.php?id=' . $id;
		break;
	case 'tuan':
		$url = $_G['config']['site_url'] . '/webapp/groupbuy/#/details/' . $id;
		break;
	case 'seckill':
		$url = $_G['config']['site_url'] . '/wap/seckill.php?seckill_id=' . $id;
		break;
	case 'unitary':
		$url = $_G['config']['site_url'] . '/webapp/snatch/#/main/' . $id;
		break;
	case 'unitary_cart':
		$url = $_G['config']['site_url'] . '/webapp/snatch/#/shopcar/' . $id;
		break;
	case 'tuan_team':
		$tuan_team = D('Tuan_team')->where(array('team_id' => $id))->find();
	if (empty($tuan_team)) 
	{
		redirect($_G['config']['site_url'] . '/static/images/no_qrcode.png');
	}
		$url = $_G['config']['site_url'] . '/webapp/groupbuy/#/detailinfo/' . $tuan_team['tuan_id'] . '/' . $tuan_team['type'] . '/' . $tuan_team['item_id'] . '/' . $tuan_team['team_id'];
		break;
	case 'bargain':
		$url = $_G['config']['site_url'] . '/wap/bargain.php?action=detail&id=' . $id . '&store_id=' . $store_id;
		break;
	case 'tuan_index':
		$url = $_G['config']['site_url'] . '/webapp/groupbuy/#/main/' . $id;
		break;
	case 'tuan_channel':
		$url = $_G['config']['site_url'] . '/webapp/groupbuy/#/channel';
		break;
	case 'yydb_index':
		$url = $_G['config']['site_url'] . '/' . $id;
		break;
	case 'cutprice':
		$url = $_G['config']['site_url'] . '/wap/cutprice.php?action=detail&store_id=' . $store_id . '&id=' . $id;
		break;
	case 'lottery':
		$url = urldecode($_G['config']['site_url'] . '/wap/lottery.php?action=detail&id=' . $id);
		break;
	case 'crowdfunding':
		$url = $_G['config']['site_url'] . '/webapp/chanping/index.html#/view/' . $id;
		break;
	case 'yousetdiscount':
		$url = $_G['config']['site_url'] . '/wap/yousetdiscount.php?action=index&id=' . $id . '&store_id=' . $store_id;
		break;
	case 'helping':
		$url = $_G['config']['site_url'] . '/wap/helping.php?action=detail&id=' . $id . '&store_id=' . $store_id;
		break;
	default:
		redirect($_G['config']['site_url'].'/static/images/no_qrcode.png');
}


import('phpqrcode');
QRcode::png(urldecode($url),false,0,7,2);
?>
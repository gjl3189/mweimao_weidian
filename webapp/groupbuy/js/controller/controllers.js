/*
 *
 * 控制器
 *
 *  @author 赵仁杰
 *  @since 2016-1-27 14:00
 *
 * */
"use strict";
var ctrl = angular.module("shop.controller",[]);


// 主页
ctrl.controller("indexCtrl",["$rootScope", "$scope","indexShopListJson", "$routeParams", function($rootScope, scope, indexShopListJson, $routeParams) {
    if($routeParams.id == -1 || $routeParams.id == "undefined") {
        window.history.back(-1);
    }

    $("body").css("padding-bottom","60px");
    $rootScope.storeid = $routeParams.id;

    scope.navtitle = "微电商";
    scope.shopinfo = {
        type : true,
        price : true,
        go : true,
        pricee : true
    };
    scope.howshow = "index";

    $(document).on("click", ".nav-item", function() {
        if(!$(this).hasClass("nav-special-item")) {
            $(this).find(".submenu").show()
            $(this).siblings().find(".submenu").hide();
        }
    });

1}]);


// 团购页面
ctrl.controller("detailCtrl",["$rootScope", "$scope", "$location", "$routeParams", "detailsInfoJson", "$http", "$interval", function($rootScope, scope, $location, $routeParams, detailsInfoJson, $http, $interval){
    scope.howshow = "detail";
    scope.shopInfoShow = true;
    scope.isdialogShow = false;
    scope.tabtitObj = {
        firstName : "商品详情",
        lastName : "拼团说明"
    };
    $("body").css("padding-bottom","0");
    scope.tabisshowdetail = true;
    scope.tabisshowdetailinfo = false;

    scope.back = function() {    // 返回操作
        $location.path('main/'+$rootScope.storeid);
    };

    scope.showtab = function() {
        scope.shopInfoShow = false;
        scope.shopstateShow = true
    }
}]);


// 团购首页
ctrl.controller("detailinfoCtrl",[
    "$rootScope",
    "$scope",
    "$location",
    "$routeParams",
    "$interval",
    "wxShare",
    "$http",
    "publicfun",
    function($rootScope, scope, $location, $routeParams, $interval, wxShare, $http, publicfun){
        $("body").css("padding-bottom","0");

    scope.navtitle = "团购首页";
    scope.isright = true;
    scope.confirmNumber = 1;
    scope.isok = 0;
    scope.price = 0;
    scope.howshow = "detailinfo";
    scope.isshare = false;
    scope.tabtitObj = {
        firstName : "商品详情",
        lastName : "参团人员"
    };
    scope.dialogIsShow = true;
    scope.tabisshowdetail = false;
    scope.tabisshowdetailinfo = true;
    scope.shopinfo = {
        type : false,
        price : true,
        go : false,
        pricee : true
    };
    scope.nextpageisok = false;
    var usericonPage = 1;

    var id = $routeParams.id;    // 后退功能
    scope.back = function() {
        $location.path('details/'+id);
    };

    var isuserlist = true;
    var lock = true;    // 函数节流

    scope.showtab = function() {
        /*
         *
         * 获取用户列表
         *
         * */
        if(isuserlist && $routeParams.colonelid > 0) {
            $http({
                method:'GET',
                url: APIURL+'webapp.php?c=tuan&a=buy_list&tuan_id='+$routeParams.id + "&team_id=" + $routeParams.colonelid
            }).success(function(data) {
                isuserlist = false;
                scope.isloading = false;
                if(data.err_code == 0) {    // 正常
                    if(data.err_msg.order_list) {
                        scope.detailorderinfo = data.err_msg;

                        scope.usericona = true;
                        scope.usericonb = false;
                    }else{
                        scope.usericonb = true;
                        scope.usericona = false;
                    }

                    if(data.err_msg.next_page == true) {
                        scope.nextpageisok = true;
                    }else{
                        scope.nextpageisok = false;
                    }


                    if(!scope.nextpageisok) {
                        console.log(scope.nextpageisok);
                        return;
                    }

                    angular.element(document).on("scroll",function() {     // 滚动加载数据
                        if(!scope.nextpageisok) {
                            return;
                        }
                        loadres();
                    });

                }else if(data.err_code == 1000){    // 常规错误
                    scope.dialogText = data.err_msg;
                    scope.dialogIsShow = true;
                    return false;
                }else{
                    scope.dialogText = "服务器异常";
                    scope.dialogIsShow = true;
                    return false;
                }
            }).error(function(err) {
                scope.isloading = false;
                scope.dialogText = "网络异常";
                return false;
            });
        }

        scope.shopInfoShow = false;
        scope.shopstateShow = true;

        function loadres(){
            var more = $(".detailinfonextmore"),
                height = $(window).height(),
                scrollTop = $(window).scrollTop(),
                elTop = more.offset().top,
                group_id = "";

            if(elTop <= scrollTop + height + 20) {    // 在能看见加载更多前20个像素时开始请求
                if($rootScope.group_id != 0) {
                    group_id = "&group_id" + $rootScope.group_id;
                }

                if(lock) {
                    lock = false;
                    scope.isloading = true;
                    $http({
                        method:'GET',
                        url: APIURL+'webapp.php?c=tuan&a=buy_list&tuan_id='+$routeParams.id+"&page="+usericonPage + "&team_id=" + $routeParams.colonelid
                    }).success(function(data) {
                        scope.isloading = false;
                        if(data.err_code == 0) {    // 正常
                            if(data.err_msg.order_list) {
                                var html = "";
                                for(var i = 0; i < data.err_msg.order_list.length; i++) {
                                    var type = publicfun.seltype(data.err_msg.order_list[i].data_type);
                                    var time = publicfun.datatime( data.err_msg.order_list[i].add_time * 1000);
                                    html += '<li class="filtime"><div class="fl fl-ex"><img class="filimg fl fl-ex-img" src="' + data.err_msg.user_list[data.err_msg.order_list[i].uid].avatar + '" alt=""><span class="fl fl-ex-img-text">'+ data.err_msg.user_list[data.err_msg.order_list[i].uid].nickname + '</span>';
                                    if (data.err_msg.order_list[i].is_leader) {
                                        html += '<span class="colonel">团长</span>';
                                    }
                                    html += '<div class="clear"></div></div>';
                                    html += '<div class="fl fl-ex">'+ type +'</div><div class="fl fl-ex">'+ time +'</div><div class="clear"></div></li>';
                                }
                                $(".filtimefirst").after(html);
                            }
                            usericonPage = usericonPage + 1;
                            lock = true;

                            if(data.err_msg.next_page) {
                                scope.nextpageisok = true;
                            }else{
                                angular.element(document).off("scroll");
                                scope.nextpageisok = false;
                            }

                        }else if(data.err_code == 1000){    // 常规错误
                            scope.dialogText = data.err_msg;
                            scope.dialogIsShow = true;
                            return false;
                        }else{
                            scope.dialogText = "服务器异常";
                            scope.dialogIsShow = true;
                            return false;
                        }
                    }).error(function(err) {
                        scope.isloading = false;
                        scope.dialogText = "网络异常";
                        return false;
                    });
                }
            }
        }

    }
}]);


// 我的团购
ctrl.controller("mybuyctrl",["$rootScope", "$scope", "$http", "$routeParams", function($rootScope, scope, $http, $routeParams) {
    scope.howshow = "mybuy";
    scope.nav = ["全部","拼团中","拼团失败","拼团成功"];

    $("body").css("padding-bottom","60px");
    var param = "",
        clickparam = "",
        isnext = false,
        page = 2,
        isok = true;

    scope.back = function() {
        window.history.back(-1);
    };

    if($routeParams.shopid) {
        param += "&store_id=" + $routeParams.shopid;
    }else{
        alert("系统异常");
        window.history.back(-1);
    }

    scope.clicknav = function(index) {
        var $li = $(".mybuy-nav li");
        $li.eq(index).addClass("mybuy-nav-active");
        $li.eq(index).siblings().removeClass("mybuy-nav-active");

        $("html").css({
            "position" : "relative",
            "height" : "100%"
        })

        switch(index) {
            case 0 :
                clickparam = "";
                break;
            case 1 :
                clickparam = "&type=" + 1;
                break;
            case 2 :
                clickparam = "&type=" + 3;
                break;
            case 3 :
                clickparam = "&type=" + 2;
                break;
        }
        scope.isloading = true;
        $http({
            method: 'GET',
            url: APIURL+'webapp.php?c=tuan&a=my_tuan' + param + clickparam
        }).success(function(data) {
            scope.isloading = false;
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                alert("尚未登录");
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {    // 正常
                scope.buydata = data.err_msg;
                if(scope.buydata.next_page) {
                    isnext = true;
                    scope.isshow = true
                }else{
                    isnext = false;
                    scope.isshow = false
                }
                if(scope.buydata.order_list.length > 0) {
                    scope.isshoplist = false;
                }else{
                    scope.isshoplist = true;
                    $(".public-bottom").css({
                        position: "absolute",
                        bottom: "10px",
                        width : "100%"
                    })
                }

                setTimeout(function() {
                    if($(".mybuy-shoplist").height() < ($(window).height() - 110)) {
                        $(".public-bottom").css({
                            position: "absolute",
                            bottom: "10px",
                            width : "100%"
                        })
                    }else{
                        $(".public-bottom").css({
                            position: "initial",
                            bottom: "initial",
                            width : "100%"
                        })
                    }
                }, 100)

            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogText = data.err_msg;
                scope.dialogIsShow = true;
                return false;
            }
        }).error(function(err) {
            scope.isloading = false;
            scope.dialogText = "网络异常";
            scope.dialogIsShow = true;
            return false;
        });

    }

    scope.isloading = true;

    $http({
        method: 'GET',
        url: APIURL+'webapp.php?c=tuan&a=my_tuan'+param
    }).success(function(data) {
        scope.isloading = false;
        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            alert("尚未登录");
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {    // 正常

            scope.buydata = data.err_msg;
            if(scope.buydata.next_page) {
                isnext = true;
                scope.isshow = true
            }else{
                isnext = false;
                scope.isshow = false
            }

            if(scope.buydata.order_list.length > 0) {
                scope.isshoplist = false;
            }else{
                scope.isshoplist = true;
            }

        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogText = data.err_msg;
            scope.dialogIsShow = true;
            return false;
        }
    }).error(function(err) {
        scope.isloading = false;
        scope.dialogText = "网络异常";
        scope.dialogIsShow = true;
        return false;
    });

    angular.element(document).on("scroll",function() {     // 滚动加载数据
        if(!isnext) {
            return;
        }

        var more = $(".indexlistmore"),
            height = $(window).height(),
            scrollTop = $(window).scrollTop(),
            elTop = more.offset().top;

        if(elTop <= scrollTop + height + 20) {    // 在能看见加载更多前20个像素时开始请求
            if(isok) {
            isok = false;
                scope.isloading = true;
                $http({
                    method: 'GET',
                    url: APIURL+'webapp.php?c=tuan&a=my_tuan' + param + clickparam + "&page=" + page
                }).success(function(data) {
                    scope.isloading = false;
                    isok = true;
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        alert("尚未登录");
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {    // 正常

                        for(var i = 1; i < data.err_msg.order_list.length; i++) {
                            scope.buydata.order_list.push(data.err_msg.order_list[i])
                        }

                        if(data.err_msg.next_page) {
                            isnext = true;
                            scope.isshow = true
                            page = page + 1;
                        }else{
                            isnext = false;
                            scope.isshow = false
                        }

                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogText = data.err_msg;
                        scope.dialogIsShow = true;
                        return false;
                    }
                }).error(function(err) {
                    scope.isloading = false;
                    isok = true;
                    scope.dialogText = "网络异常";
                    scope.dialogIsShow = true;
                    return false;
                });
            }
        }
    });
}]);


// 拼团流程页面
ctrl.controller("flowCtrl",["$scope", "$location", "$routeParams", function(scope, $location, $routeParams){

    $("body").css("padding-bottom","0");
    scope.navtitle = "拼团流程";
    scope.howshow = "flow";

    var id = $routeParams.id;
    scope.back = function() {
        $location.path('details/'+id);
    };

}]);


// 用户团购首页页面
ctrl.controller("usergroupbuyctrl",[
    "$scope", 
    "$location", 
    "$routeParams", 
    "usergroupbuyserver",
    "wxShare",
    "gitip",
     function(scope, $location, $routeParams, usergroupbuyserver, wxShare, gitip){

    scope.isloading = true;
    var param = "";
    scope.navishide = true;

    var shareurl = "&url=" + encodeURIComponent(gitip.ip.split("?")[0]);
    usergroupbuyserver.init(shareurl).success(function(data) {
        scope.isloading = false;
        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {    // 正常

            var shareimg = "";
            try{
                if(data.err_msg.tuan_list[0]) {
                    shareimg = data.err_msg.tuan_list[0].image;
                }
            }catch(e){
                if(data.err_msg.adver_list[0]) {
                    shareimg = data.err_msg.adver_list[0].pic;
                }
            }

            var sharedata = {
                title: data.err_msg.title,
                desc: 'shopping,就是拼，快来进入拼团吧',
                link: location.href,
                imgUrl: shareimg
            };
            wxShare.init(sharedata, data.err_msg.share_data);    // 配置分享

            scope.data = data.err_msg;
            var datalist = scope.data.slider_list;
            scope.carouselImg = [];
            for(var i = 0; i < scope.data.adver_list.length; i++) {
                scope.carouselImg.push({image: scope.data.adver_list[i].pic, url : scope.data.adver_list[i].url});
            }

            if(datalist == 0) {
                scope.navcarouselishide = true;
            }else{
                var html = "";
                var allcount = datalist.length;
                var nowcount = 0;
                var forcount = 0;
                var ds = 0;
                if(allcount <= 4) {
                    html += '<div class="usergroupbuy-navli swiper-slide">';
                    for (var i = 0; i < allcount; i++) {
                        html += '<a href="' + datalist[i].url + '"><img src="' + datalist[i].pic + '" alt="" class="usergroupbuy-navimg"><p>' + datalist[i].name + '</p></a>';
                    }
                    html += '</div>';

                }else{
                    for(var t = 0; t < Math.ceil(scope.data.slider_list.length / 4); t++) {
                        if(allcount - forcount + 1 < 4) {
                            ds = 4 + (allcount - forcount +1);
                        }else{
                            ds = 4;
                        }
                        html += '<div class="usergroupbuy-navli swiper-slide">';
                        for(var i = 0; i < ds; i++) {
                            if(nowcount + 1 <= allcount) {
                                html += '<a href="' + datalist[nowcount].url + '"><img src="' + datalist[nowcount].pic + '" alt="" class="usergroupbuy-navimg"><p>' + datalist[nowcount].name + '</p></a>';
                                nowcount++;
                                forcount++;
                            }
                        }
                        html += '</div>';
                    }
                }
                $(".usergroupbuy-nav").append(html);

                var mySwiper = new Swiper('.s2',{
                    loop: false,
                    autoplay:6500,
                    pagination: '.p2',
                    paginationClickable: true
                });
            }

            var timer = setInterval(function() {
                if($(".usergroupbuyall").width() > 90) {
                    clearInterval(timer);
                    for(var i = 0; i < $(".usergroupbuyall").length; i++){
                        if($(document).width() - 110 < ($(".usergroupbuyall").eq(i).width() + $(".usergroupbuyalla").eq(i).width())) {
                            $(".usergroupbuyalla").eq(i).hide();
                        }
                    }

                }
                
            },200)

            

        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogText = data.err_msg;
            scope.dialogIsShow = true;
            return false;
        }
    }).error(function(err) {
        scope.isloading = false;
        scope.dialogText = "网络异常";
        scope.dialogIsShow = true;
        return false;
    });

    scope.isnextpage = function(id) {
        $location.path("/details/"+id);
    }
}]);


// 全部分类页面
ctrl.controller("usernavlistctrl",["$scope", "$location", "$routeParams", "usernavlistserver", function(scope, $location, $routeParams, usernavlistserver){
    scope.isloading = true;
    usernavlistserver.init().success(function(data) {
        scope.isloading = false;
        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {    // 正常
            scope.data = data.err_msg;
        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogText = data.err_msg;
            scope.dialogIsShow = true;
            return false;
        }
    }).error(function(err) {
        scope.isloading = false;
        scope.dialogText = "网络异常";
        scope.dialogIsShow = true;
        return false;
    });

    //跳转到相应页面
    scope.go = function(id) {
        $location.path('usershoplist/new/' + id);
    }


}]);


// 全部分类页面
ctrl.controller("usershoplistctrl",[
    "$scope",
    "$location",
    "$routeParams",
    "usershoplistserver",
    "usershoplistservering",
    function(scope, $location, $routeParams, usershoplistserver, usershoplistservering){

    var param = "&cat_id=" + $routeParams.id + "&type=" + $routeParams.type,
        isnext = false,
        isok = true,
        page = 2,
        isnowing = false;

    scope.isloading = true;
    usershoplistserver.init(param).success(function(data) {
        scope.isloading = false;
        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {    // 正常
            scope.data = data.err_msg;

            if(scope.data.next_page) {
                isnext = true;
                scope.isshow = true
            }else{
                isnext = false;
            }


        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogText = data.err_msg;
            scope.dialogIsShow = true;
            return false;
        }
    }).error(function(err) {
        scope.isloading = false;
        scope.dialogText = "网络异常";
        scope.dialogIsShow = true;
        return false;
    });


    angular.element(document).on("scroll",function() {     // 滚动加载数据
        if(!isnext) {
            return;
        }

        var more = $(".indexlistmore"),
            height = $(window).height(),
            scrollTop = $(window).scrollTop(),
            elTop = more.offset().top;

        if(elTop <= scrollTop + height + 20) {    // 在能看见加载更多前20个像素时开始请求
            if(isok) {
                isok = false;
                scope.isloading = true;
                var scrollparam = "";
                if(isnowing) {
                    scrollparam = "&cat_id=" + $routeParams.id + "&page=" + page;
                    usershoplistservering.init(scrollparam).success(function(data) {
                        scope.isloading = false;
                        isok = true;
                        if(data.err_code == 20000) {    // 没有登陆
                            // 跳到登录页面
                            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                        }else if(data.err_code == 0) {    // 正常

                            for(var i = 0; i < data.err_msg.tuan_list.length; i++) {
                                scope.data.tuan_list.push(data.err_msg.tuan_list[i]);
                            }

                            if(data.err_msg.next_page) {
                                isnext = true;
                                page = page + 1;
                            }else{
                                isnext = false;
                                scope.isshow = false;
                            }

                        }else if(data.err_code == 1000){    // 常规错误
                            scope.dialogText = data.err_msg;
                            scope.dialogIsShow = true;
                            return false;
                        }
                    }).error(function(err) {
                        scope.isloading = false;
                        isok = true;
                        scope.dialogText = "网络异常";
                        scope.dialogIsShow = true;
                        return false;
                    });
                }else{
                    scrollparam = param + "&page=" + page;
                    usershoplistserver.init(scrollparam).success(function(data) {
                        scope.isloading = false;
                        isok = true;
                        if(data.err_code == 20000) {    // 没有登陆
                            // 跳到登录页面
                            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                        }else if(data.err_code == 0) {    // 正常

                            for(var i = 0; i < data.err_msg.tuan_list.length; i++) {
                                scope.data.tuan_list.push(data.err_msg.tuan_list[i]);
                            }

                            if(data.err_msg.next_page) {
                                isnext = true;
                                page = page + 1;
                            }else{
                                isnext = false;
                                scope.isshow = false;
                            }

                        }else if(data.err_code == 1000){    // 常规错误
                            scope.dialogText = data.err_msg;
                            scope.dialogIsShow = true;
                            return false;
                        }
                    }).error(function(err) {
                        scope.isloading = false;
                        isok = true;
                        scope.dialogText = "网络异常";
                        scope.dialogIsShow = true;
                        return false;
                    });
                }
            }
        }
    });

    scope.clicknav = function(type) {
        var $li = $(".usershoplistnav li");
        switch (type) {
            case "new":
                isnowing = false;
                scope.isnoting = false;
                param = "&cat_id=" + $routeParams.id + "&type=new";
                page = 2;
                $li.eq(0).addClass("usershoplistnavactive").siblings().removeClass("usershoplistnavactive");

                usershoplistserver.init(param).success(function(data) {
                    scope.isloading = false;
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {    // 正常
                        scope.data = data.err_msg;
                        if(scope.data.next_page) {
                            isnext = true
                            scope.isshow = true
                        }else{
                            scope.isshow = false;
                            isnext = false;
                        }
                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogText = data.err_msg;
                        scope.dialogIsShow = true;
                        return false;
                    }
                }).error(function(err) {
                    scope.isloading = false;
                    scope.dialogText = "网络异常";
                    scope.dialogIsShow = true;
                    return false;
                });

                break;
            case "hot":
                isnowing = false;
                $li.eq(1).addClass("usershoplistnavactive").siblings().removeClass("usershoplistnavactive");
                scope.isnoting = false;
                param = "&cat_id=" + $routeParams.id + "&type=hot";
                page = 2;
                usershoplistserver.init(param).success(function(data) {
                    scope.isloading = false;
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {    // 正常
                        scope.data = data.err_msg;
                        if(scope.data.next_page) {
                            isnext = true
                            scope.isshow = true
                        }else{
                            scope.isshow = false;
                            isnext = false;
                        }
                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogText = data.err_msg;
                        scope.dialogIsShow = true;
                        return false;
                    }
                }).error(function(err) {
                    scope.isloading = false;
                    scope.dialogText = "网络异常";
                    scope.dialogIsShow = true;
                    return false;
                });
                break;
            case "ing":
                isnowing = true;
                $li.eq(2).addClass("usershoplistnavactive").siblings().removeClass("usershoplistnavactive");
                page = 2;
                scope.isnoting = true;
                param = "&cat_id=" + $routeParams.id;
                usershoplistservering.init(param).success(function(data) {
                    scope.isloading = false;
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {    // 正常
                        scope.data = data.err_msg;

                        if(scope.data.next_page) {
                            isnext = true
                            scope.isshow = true
                        }else{
                            isnext = false;
                            scope.isshow = false;
                        }

                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogText = data.err_msg;
                        scope.dialogIsShow = true;
                        return false;
                    }
                }).error(function(err) {
                    scope.isloading = false;
                    scope.dialogText = "网络异常";
                    scope.dialogIsShow = true;
                    return false;
                });
                break;
        }
    }



}]);




/*
 *
 * js animate 效果
 *
 *  @author jiangyi
 *  @since 2016-5-5 15:02
 *
 * */
"use strict";
ctrl.animation('.animage-leave', ['$animateCss', function($animateCss) {
	return {
		leave: function(element, doneFn) {
			var height = element[0].offsetHeight;
			return $animateCss(element, {
				easing: 'ease-out',
				from: { height:height + 'px' },
				to: { height:'0px' },
				duration: 0.3 // 速度
			});
		}
	}
}]);
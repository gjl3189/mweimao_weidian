/*
 *
 * 服务
 *
 *  @author 赵仁杰
 *  @since 2016-1-27 14:00
 *
 * */
"use strict";
var serv = angular.module("shop.services",[]);
//var APIURL = "http://www.weidian.com/";    // 接口路径
//var APIURL = "http://dd2.pigcms.com/";    // 接口路径

//serv.service("indexShopListJson",["$http", "$routeParams", function(http, $routeParams) {    // 主页商品列表json接口
//    var indexShopList = http({
//        method:'GET',
//        url: APIURL + 'webapp.php?c=tuan&store_id=749'
//    });
//    return {
//        indexShopList: indexShopList
//    };
//}]);

/**
 *
 * 滚动分页中是否可以加载数据
 *
 * @param nowheight  当前高度
 * @param winheight  屏幕高度
 * @param scrollTop  距离上方多少像素
 *
 */
serv.service("isloadmoreservice",["$rootScope",function($rootScope) {
    function init(nowheight, winheight, scrollTop) {
        if(nowheight <= scrollTop + winheight + 20) {
            return true;
        }else{
            return false;
        }
    }

    return {
        init : init
    }
}]);


/**
 *
 * 商品列表json
 *
 * @param param  更改购物车商品个数json
 *
 */
serv.service("shoplistservice",["$rootScope", "$http",function($rootScope, $http) {
    function init(param) {
        return $http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary' + param
        });
    }

    return {
        init : init
    }
}]);


/**
 *
 * 添加到购物车json
 *
 * @param param  更改购物车商品个数json
 *
 */
serv.service("addshpcarservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=zhuijia' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 购物车json
 *
 * @param param  更改购物车商品个数json
 *
 */
serv.service("shpcarservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=cart' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 删除购物车商品json
 *
 * @param param  更改购物车商品个数json
 *
 */
serv.service("delshpcarservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=cartajax&type=cart_del' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 我的购买记录json
 *
 * @param param  更改购物车商品个数json
 *
 */
serv.service("resshopcarservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=cartajax&type=cart_count_change' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 我的购买记录json
 *
 * @param param  接口参数
 *
 */
serv.service("mypayservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=mypay' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 我的购买记录json
 *
 * @param param  接口参数
 *
 */
serv.service("allbuyservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=buyres' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 订单详情-结束后的json
 *
 * @param param  接口参数
 *
 */
serv.service("orderendservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=goodsover' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 *计算幸运号值-
 *
 * @param param  接口参数
 *
 */
serv.service("calcuteservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=calculate' + param
        })
    }
    return {
        init : init
    }
}]);



/**
 *
 * 订单详情-正在进行的json
 *
 * @param param  接口参数
 *
 */
serv.service("orderindservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=goodsing' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 添加到购物车json
 *
 * @param param  接口参数
 *
 */
serv.service("addshopcartservice",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=goodsajax&type=docart' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 我的幸运号数据
 *
 * @param param  接口参数
 *
 */
serv.service("myLuckNumService",["$rootScope", "$http",function($rootScope, http) {
    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=user_lucknum' + param
        })
    }

    return {
        init : init
    }
}]);


/**
 *
 * 综合商城主页的json
 *
 * 无参数
 *
 */
serv.service("userIndexservice",["$rootScope", "$http",function($rootScope, http) {

    function init(param) {
        return http({
            method:'GET',
            //url: APIURL+'webapp.php?c=unitary&a=catelist'
            url: APIURL+'webapp.php?c=unitary&a=home'+ param
        })
    }
    return {
        init : init
    }
}]);


/**
 *
 * 综合商城分类的json
 *
 * @param param  接口参数
 *
 */
serv.service("usernavservice",["$rootScope", "$http",function($rootScope, http) {

    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=catelist' + param
        })
    }

    return {
        init : init
    }
}]);


// 通用方法
serv.service("publicmethods",["$rootScope",function($rootScope) {

    function isNumber(obj) {
        return typeof obj === 'number' && !isNaN(obj)
    }

    function downtime(value) {
        var theTime = parseInt(value);
        var theTime1 = 0;
        var theTime2 = 0;
        if(theTime > 60) {
            theTime1 = parseInt(theTime/60);
            theTime = parseInt(theTime%60);
            if(theTime1 > 60) {
                theTime2 = parseInt(theTime1/60);
                theTime1 = parseInt(theTime1%60);
            }
        }


        var result = "";

        if(theTime2 > 0) {
            if(theTime2 >= 10) {
                result += theTime2 + ":";
            }else{
                result += "0" + theTime2 + ":";
            }
        }else{
            result +=  "00:";
        }

        if(theTime1 > 0) {
            if(theTime >= 10) {
                result += theTime1 + ":";
            }else{
                result += "0" + theTime1 + ":";
            }
        }else{
            result += "00:";

        }

        if(theTime > 0) {
            if(theTime >= 10) {
                result += theTime;
            }else{
                result += "0"+theTime;
            }
        }else{
            result += "00";
        }

        return result;
    }
    return {
        isNumber : isNumber,
        downtime : downtime
    }
}]);


/**
 *
 * 获取分享地址
 *
 */
serv.service("shareip",["$rootScope", function(rootscope) {    // 通用功能

    var shareurl = "";
    if(location.href.split("?").length > 1){
        shareurl = location.href.split("?")[0] +"#"+ location.href.split("?")[1].split("#")[1];
        location.href = shareurl;

    }else{
        shareurl = location.href.split("#")[0]
    }

    return {
        ip : shareurl
    };
}]);


/**
 *
 * 分享配置
 *
 * @param shareobj  分享参数
 * @param wxparam  微信核心参数
 *
 */
serv.service("wxshare",["$rootScope", function($rootScope){

    function init(shareobj, wxparam) {
        wx.config({
            debug: false,
            appId: wxparam.appId,
            timestamp: wxparam.timestamp,
            nonceStr: wxparam.nonceStr,
            signature: wxparam.signature,
            jsApiList: [
                'onMenuShareTimeline',			//获取“分享到朋友圈”按钮点击状态及自定义分享内容接口
                'onMenuShareAppMessage',			//获取“分享给朋友”按钮点击状态及自定义分享内容接口
                'onMenuShareQQ',
                'onMenuShareWeibo',
                'onMenuShareQZone'
            ]
        });

        wx.ready(function(){

            var shareObj = {    // 分享参数
                title: shareobj.title,
                desc: shareobj.desc,
                link: shareobj.link,
                imgUrl: shareobj.imgUrl
            };

            wx.onMenuShareTimeline({
                title: shareObj.title,    // 标题
                link: shareObj.link,    // 链接
                imgUrl: shareObj.imgUrl,    // 图片
                success: function (res) {
                },
                cancel: function (res) {
                }
            });

            //分享到好友
            wx.onMenuShareAppMessage({
                title: shareObj.title,    // 标题
                desc: shareObj.desc,    // 描述
                link: shareObj.link,    // 链接
                imgUrl: shareObj.imgUrl,    // 图片
                success: function (res) {
                },
                cancel: function (res) {
                }
            });

            //分享到QQ
            wx.onMenuShareQQ({
                title: shareObj.title,
                desc: shareObj.desc,
                link: shareObj.link,
                imgUrl: shareObj.imgUrl,
                success: function (res) {
                },
                cancel: function (res) {
                }
            });

            //分享到腾讯微博
            wx.onMenuShareWeibo({
                title: shareObj.title,
                desc: shareObj.desc,
                link: shareObj.link,
                imgUrl: shareObj.imgUrl,
                success: function (res) {
                },
                cancel: function (res) {
                }
            });

            //分享到QQ空间
            wx.onMenuShareQZone({
                title: shareObj.title,
                desc: shareObj.desc,
                link: shareObj.link,
                imgUrl: shareObj.imgUrl,
                success: function (res) {
                },
                cancel: function (res) {
                }
            });
        });

        wx.error(function(res){
            // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
            console.log(res)
        });
    }

    return {
        init : init
    }

}]);


/**
 *
 * 获取当前页面去 # ? 后的URL链接
 *
 */
serv.service("getwxUrl",["$rootScope", function(rootscope) {    // 通用功能

    var shareurl = "";
    if(location.href.split("?").length > 1){
        shareurl = location.href.split("?")[0] +"#"+ location.href.split("?")[1].split("#")[1];
        location.href = shareurl;

    }else{
        shareurl = location.href.split("#")[0]
    }

    return {
        url : shareurl
    };
}]);



// 搜索
serv.service("searchJson",["$rootScope", "$http", function(rootscope, http) {

    function init(param) {
        return http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=search' + param
        })
    }

    return {
        init : init
    }
}]);
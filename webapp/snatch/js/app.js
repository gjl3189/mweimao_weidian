/*
 *
 * 配置文件、主入口文件
 *
 *  @author 赵仁杰
 *  @since 2016-1-27 14:00
 *
 * */
"use strict";
var app = angular.module("myapp",["ngRoute", "shop.controller", "shop.services", "shop.directive", "shop.filter", "ngSanitize", "ngAnimate"]);
app.config(["$routeProvider", function($routeProvider) {
    $routeProvider.
    when('/main/:id', {
        templateUrl : "templates/snatchprivate/index.html",
        controller: 'indexCtrl'
    }).when('/shoplist/:id', {
        templateUrl : "templates/snatchprivate/shoplist.html",
        controller: 'shoplistCtrl'
    }).when('/shopcar/:id', {
        templateUrl : "templates/snatchprivate/shopcar.html",
        controller: 'shopcarCtrl'
    }).when('/orderinfo/:id', {
        templateUrl : "templates/snatchprivate/orderinfo.html",
        controller: 'orderinfoCtrl'
    }).when('/allbuy/:id/:storeid', {
        templateUrl : "templates/snatchprivate/allbuy.html",
        controller: 'allbuyCtrl'
    }).when('/ordering/:id/:storeid', {
        templateUrl : "templates/snatchprivate/ordering.html",
        controller: 'orderingCtrl'
    }).when('/orderend/:id/:storeid', {
        templateUrl : "templates/snatchprivate/orderend.html",
        controller: 'orderendCtrl'
    }).when('/calculate/:unitaryid', {
        templateUrl : "templates/snatchprivate/calculate.html",
        controller: 'calculateCtrl'
    }).when('/shopinfo/:id', {
        template : "<div ng-bind-html='data.info' class='shopinfo-content' ng-if='havadata'></div>" +
        "<div class='shopinfonomar' ng-if='nodata'>该商品没有内容</div>" +
        "<div class='shopinfo-back' ng-click='shopinfoback()'><span class='fl'></span>返回</div>" +
        "<dialog isdialogshow='isdialogshow' dialogtext='{{dialogtext}}'>" +
        "</dialog><loading loadisshow='ctrlisloadshow'></loading>",
        controller: 'shopinfoCtrl'
    }).when('/userindex', {
        templateUrl : "templates/snatchprivate/userindex.html",
        controller: 'userindexCtrl'
    }).when('/userallshoplist/:type', {
        templateUrl : "templates/snatchprivate/userallshoplist.html",
        controller: 'userallshoplistCtrl'
    }).when('/search/:keyword', {
        templateUrl : "templates/snatchprivate/search.html",
        controller: 'searchCtrl'
    }).otherwise({
        redirectTo: '/userindex'
    });
}]);
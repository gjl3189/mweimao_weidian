/*
 *
 * 过滤器
 *
 *  @author 赵仁杰
 *  @since 2016-2-16 17:47
 *
 * */

"use strict";
var filter = angular.module("shop.filter",[]);

filter.filter('substr8',[function(){

    return function(str){
        var _str=str+'';
        if(_str.length>8){
           _str=_str.substr(0,5)+'...';
        }
        return _str;
    }
}]);

//filter.filter('downtimeFilter',[function() {    // 判断是什么类型的团购
//    return function (value) {
//
//    }
//}]);


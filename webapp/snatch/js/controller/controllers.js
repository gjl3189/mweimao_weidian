/*
 *
 * 控制器
 *
 *  @author 赵仁杰
 *  @since 2016-1-27 14:00
 *
 * */
"use strict";
var ctrl = angular.module("shop.controller",[]);
var APIURL = window.location.protocol +"//"+ window.location.host + "/";    // 接口路径
var CDNURL = "http://s.404.cn/wd_source/";


// 主页
ctrl.controller("indexCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "$location",
    "wxshare",
    "getwxUrl",
    function($rootScope, scope, http, $routeParams, $location, wxshare, getwxUrl) {
    scope.pageid = $routeParams.id;
    angular.element(document).off("scroll");
    scope.ctrlisloadshow = true;
    http({
        method:'GET',
        url: APIURL+'webapp.php?c=unitary&a=goods&unitary_id=' + $routeParams.id + "&url=" + getwxUrl.url
    }).success(function(data) {
        scope.ctrlisloadshow = false;
        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {    // 正常
            scope.data = data.err_msg;

            var sharedata = {
                title: scope.data.share_data.title,
                desc: scope.data.share_data.content,
                link: location.href,
                imgUrl: scope.data.share_data.logo
            };
            wxshare.init(sharedata, scope.data.share_data);    // 配置分享

            if(scope.data.unitary.state == 0 && scope.data.unitary.pay_count == 0) {
                scope.nowbuy = null;
                $(".nowbuybtn").text("尚未开始").css("background","grey");
            }else if(scope.data.unitary.state == 0 && scope.data.unitary.pay_count != 0) {
                scope.nowbuy = null;
                $(".nowbuybtn").text("活动关闭").css("background","grey");
            }else if(scope.data.unitary.state == 2) {
                scope.nowbuy = null;
                $(".nowbuybtn").text("已结束").css("background","grey");
            }

            // 设置底部导航购物车显示数量
            if(parseInt(scope.data.cart_count) > 0) {
                scope.shopcarnum = scope.data.cart_count;
            }else{
                scope.isShopNumHide = true;
            }

            $(".pgbar").animate({
                width : (scope.data.pay_count / scope.data.unitary.total_num) * 100 + "%"
            },1000);

            var footurl = "";
            if(scope.data.unitary.store_id > 0) {
                footurl = APIURL + "webapp/snatch/#/shoplist/"+ scope.data.unitary.store_id;
            }else{
                footurl = APIURL + "webapp/snatch/#/userindex";
            }

            // 设置底部导航链接
            scope.footerlistUrl = {
                allshop : footurl,
                shopcar : APIURL + "webapp/snatch/#/shopcar/"+scope.data.unitary.store_id,
                me : APIURL + "webapp/snatch/#/orderinfo/" + scope.data.unitary.store_id
            };

            if(data.pay_count == null) {
                scope.nowsurplus = data.err_msg.unitary.total_num;
            }else{
                scope.nowsurplus = (parseInt(data.err_msg.unitary.total_num) - data.pay_count);
            }

        }else if(data.err_code == 1000){    // 常规错误
            scope.isShopNumHide = true;
            scope.isshoplistnum = true;
            // 设置底部导航链接

            var footurl = "";
            if(scope.data.unitary.store_id > 0) {
                footurl = APIURL + "webapp/snatch/#/shoplist/"+ scope.data.unitary.store_id
            }else{
                footurl = APIURL + "webapp/snatch/#/userindex";
            }

            scope.footerlistUrl = {
                allshop : footurl,
                shopcar : APIURL + "webapp/snatch/#/shopcar/" + data.err_dom.store_id,
                me : APIURL + "webapp/snatch/#/orderinfo/" + data.err_dom.store_id
            };
        }
    }).error(function(err) {
        scope.ctrlisloadshow = false;
        scope.dialogtext = "网络异常";
        scope.isdialogshow = true;
    });

    // 立即购买
    scope.ctrlisloadshow = true;
    scope.nowbuy = function() {
        http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=buygoods&unitary_id=' + $routeParams.id
        }).success(function(data) {
            scope.ctrlisloadshow = false;
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {    // 正常
                // 跳转到购物车页面
                $location.path("shopcar/" + scope.data.unitary.store_id);

            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function(err) {
            scope.ctrlisloadshow = false;
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });
    }
}]);


// 商品列表
ctrl.controller("shoplistCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "isloadmoreservice",
    "shoplistservice",
    "addshpcarservice",
    "$timeout",
    "$location",
    function($rootScope, scope, http, $routeParams, isloadmoreservice, shoplistservice, addshpcarservice, $timeout, $location) {

        var page = 2,
            group_id = -1,
            order_by = '',
            nextpage = false,
            lock = true;        //函数节流

        scope.defaultClassify = "全部分类";
        scope.searchtimetext = "即将揭晓";

    scope.ctrlisloadshow = true;
    http({
        method:'GET',
        url: APIURL+'webapp.php?c=unitary&store_id=' + $routeParams.id
    }).success(function(data) {
        scope.ctrlisloadshow = false;

        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {    // 正常

            if(data.err_msg.list.length == 0) {     // 如果当前接口里没有数据则切换显示效果
                scope.isshoplistnum = true;
            }else{
                scope.isshoplistnum = false;
            }

            scope.data = data.err_msg;

            // 设置底部导航购物车显示数量
            if(parseInt(scope.data.cart_count) > 0) {
                scope.shopcarnum = scope.data.cart_count;
            }else{
                scope.isShopNumHide = true;
            }

            // 设置底部导航链接
            scope.footerlistUrl = {
                allshop : location.href,
                shopcar : APIURL+"webapp/snatch/#/shopcar/" + $routeParams.id,
                me : APIURL + "webapp/snatch/#/orderinfo/" + $routeParams.id
            };

            if(scope.data.next_page) {    // 是否显示正在加载中
                scope.isloadmore = true;
                nextpage = true;
            }else{
                nextpage = false;
                scope.isloadmore = false;
            }
        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogtext = data.err_msg;
            scope.isdialogshow = true;
        }
    }).error(function(err) {
        scope.ctrlisloadshow = false;
        scope.dialogtext = "网络异常";
        scope.isdialogshow = true;
    });

    scope.gomain = function(id) {
        $location.path("/main/"+id);
    }

    scope.searchtimeclick = function() {    // 点击即将揭晓
        scope.searchtime = !scope.searchtime;
        scope.classify = false;
    }

    scope.classifyclick = function() {    // 点击分类
        scope.classify = !scope.classify;
        scope.searchtime = false;
    }

    scope.classifyliclick = function(type,id,text) {    // 点击分类下元素

        page = 2;
        if(type == 1) {
            group_id = id;
            scope.defaultClassify = text;
            scope.classify = false;
        }else if(type == 2){
            order_by = id;
            scope.searchtimetext = text;
            scope.searchtime = false;
        }

        var param = "&store_id=" + $routeParams.id;

        if(group_id > -1) {
            param += "&group_id=" + group_id
        }
        if(order_by != "") {
            param += "&order_by="+order_by
        }

        scope.ctrlisloadshow = true;
        shoplistservice.init(param).success(function(data) {
            scope.ctrlisloadshow = false;
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {    // 正常

                scope.data = data.err_msg;
                scope.shopcarnum = scope.data.cart_count;

                if(data.err_msg.list.length == 0) {     // 如果当前接口里没有数据则切换显示效果
                    scope.isshoplistnum = true;
                }else{
                    scope.isshoplistnum = false;
                }

                if(data.err_msg.list.length == 0) {     // 如果当前接口里没有数据则切换显示效果
                    scope.isshoplistnum = true;
                }

                if(data.err_msg.next_page) {    // 是否显示正在加载中
                    scope.isloadmore = true;
                    nextpage = true;
                }else{
                    nextpage = false;
                    scope.isloadmore = false;
                }
                lock = true;

            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function(err) {
            scope.ctrlisloadshow = false;
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });
    };

    // 滚动事件，判断是否需要新加数据
    angular.element(document).on("scroll",function() {
        if(nextpage) {
            var winheight = $(window).height(),
                nowheight = $(".load_more").offset().top,
                scrollTop = $(window).scrollTop(),
                param = "&store_id=" + $routeParams.id +"&page="+ page;

            if(isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                if(lock) {
                    lock = false;
                    if(group_id > -1) {
                        param += "&group_id=" + group_id;
                    }
                    if(order_by != "") {
                        param += "&order_by=" + order_by;
                    }
                    scope.ctrlisloadshow = true;
                    http({
                        method:'GET',
                        url: APIURL+'webapp.php?c=unitary' + param
                    }).success(function(data) {
                        scope.ctrlisloadshow = false;
                        if(data.err_code == 20000) {    // 没有登陆
                            // 跳到登录页面
                            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                        }else if(data.err_code == 0) {    // 正常

                            if(data.err_msg.next_page) {    // 是否显示正在加载中
                                scope.isloadmore = true;
                                nextpage = true;
                                page++;
                            }else{
                                nextpage = false;
                                scope.isloadmore = false;
                            }
                            lock = true;

                            for(var i = 0; i < data.err_msg.list.length; i++) {
                                scope.data.list.push(data.err_msg.list[i]);
                            }

                        }else if(data.err_code == 1000){    // 常规错误
                            scope.dialogtext = data.err_msg;
                            scope.isdialogshow = true;
                        }
                    }).error(function(err) {
                        scope.ctrlisloadshow = false;
                        scope.dialogtext = "网络异常";
                        scope.isdialogshow = true;

                    });
                }
            }
        }
    });

    // 添加到购物车
    scope.addshopcar = function(id,num) {
        var param = "&unitary_id=" + id + "&buynum=" + num;
        scope.ctrlisloadshow = true;
        // 请求购物车追加接口
        addshpcarservice.init(param).success(function(data) {
            scope.ctrlisloadshow = false;
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {    // 正常

                if(parseInt(data.err_msg.count) > 0) {
                    scope.shopcarnum = data.err_msg.count;
                    scope.isShopNumHide = false;

                }else{
                    scope.isShopNumHide = true;
                }
                scope.dialogtext = "商品添加成功"
                scope.isdialogshow = true;
                scope.shopcarnum = data.err_msg.count;
            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function() {
            scope.ctrlisloadshow = false;
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });
    }
}]);


ctrl.controller("shopcarCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "$timeout",
    "shpcarservice",
    "delshpcarservice",
    "publicmethods",
    "resshopcarservice",
    "isloadmoreservice",
    "$animate",
    function($rootScope, scope, http, $routeParams, $timeout, shpcarservice, delshpcarservice, publicmethods,resshopcarservice , isloadmoreservice , $animate) {
        scope.liclick = false;
        angular.element(document).off("scroll");
        scope.ctrlisloadshow = true;
        var param = "&store_id=" + $routeParams.id,
            page = 2,    // 当前页数
            nextpage = false,    // 是否有下一页
            lock = true,    // 函数节流
            nownum = 0,
            nowprice = 0;

        scope.ischildsel = false;
        scope.isparsel = false;

        shpcarservice.init(param).success(function(data) {
            scope.ctrlisloadshow = false;
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {
                if(data.err_msg.cart_list.length == 0){
                    scope.isdata = true;
                    return;
                }

                if(data.err_msg.next_page) {    // 显示加载更多
                    nextpage = true;
                    scope.isloadmore = true;
                }else{
                    nextpage = false;
                    scope.isloadmore = false;
                }

                scope.data = data.err_msg;
                scope.allshoplistnum = 0;    // 商品个数
                scope.allprice = 0;

          //      for(var i = 0, j = scope.data.cart_list.length; i < j; i++) {
            //        //计算价格
            //        scope.allprice += parseInt(scope.data.cart_list[i].count) * parseFloat(scope.data.unitary[scope.data.cart_list[i].id].item_price)
            //    }
            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function() {
            scope.ctrlisloadshow = false;
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });

        // 滚动事件，判断是否需要新加数据
        angular.element(document).on("scroll",function() {
            if(nextpage) {
                var winheight = $(window).height(),
                    nowheight = $(".load_more").offset().top,
                    scrollTop = $(window).scrollTop();

                if(isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                    if(lock) {
                        lock = false;
                        scope.ctrlisloadshow = true;
                        shpcarservice.init(param + "&page="+ page).success(function(data) {

                            scope.ctrlisloadshow = false;    // 关闭加载中
                            if(data.err_code == 20000) {    // 没有登陆
                                // 跳到登录页面
                                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);

                            }else if(data.err_code == 0) {    // 正常状态

                                // 遍历数据追加到相应地方
                                scope.data.unitary = data.err_msg.unitary;
                                scope.data.ycount = data.err_msg.ycount;
                                for(var a = 0, b = data.err_msg.cart_list.length; a < b; a++) {
                                    scope.data.cart_list.push(data.err_msg.cart_list[a]);
                                }

                                if(data.err_msg.next_page) {    // 显示加载更多
                                    nextpage = true;
                                    page++;
                                    lock = true;
                                }else{
                                    nextpage = false;
                                    scope.isloadmore = false;
                                }
                                scope.allshoplistnum = scope.data.sum;

                                //计算价格
                            //    scope.allprice = 0;
                             //   for(var i = 0, j = scope.data.cart_list.length; i < j; i++) {
                             //       scope.allprice += parseInt(scope.data.cart_list[i].count) * parseFloat(scope.data.unitary[scope.data.cart_list[i].id].item_price)
                            //    }

                            }else if(data.err_code == 1000){    // 常规错误
                                scope.dialogtext = data.err_msg;
                                scope.isdialogshow = true;
                            }
                        }).error(function(err){
                            scope.ctrlisloadshow = false;    // 关闭加载中
                            scope.dialogtext = "网络异常";
                            scope.isdialogshow = true;
                        });
                    }
                }
            }
        });

        // 失去焦点
        scope.shopcarnumblur = function(num, pr) {
            var that = this;

            var nowcount = parseInt(this.x.count);
            if(isNaN(nowcount)) {
                scope.dialogtext = "请输入数字！";
                scope.isdialogshow = true;
                this.x.count = 1;
            }else if(nowcount < 1) {
                scope.dialogtext = "抱歉，购物车内商品最小数量为一个";
                scope.isdialogshow = true;
                this.x.count = 1;
            }else if(nowcount > num){
                scope.dialogtext = "抱歉，当前商品库存只有" + num + "个";
                scope.isdialogshow = true;
                this.x.count = num;
            }else if(nowcount < num) {

                var jsoncount = nowcount;
                var param = "&cart_id="+ this.x.id +"&store_id="+ $routeParams.id +"&cart_count="+ jsoncount;

                scope.ctrlisloadshow = true;    // 打开加载中
                resshopcarservice.init(param).success(function(data) {

                    scope.ctrlisloadshow = false;    // 关闭加载中
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {
                        var jsondata = data.err_msg;
                        that.x.count = parseInt(nowcount);
                        that.data.ycount[that.x.id] = jsondata.ycount;

                        var nowallcount = 0;
                        for(var i = 0; i < $(".shopcarnamelist").length; i++) {
                            for(var j = 0; j < $(".shopcarnamelist").eq(i).find(".unitary_cart").length; j++) {
                                if($(".shopcarnamelist").eq(i).find(".unitary_cart").eq(j).find("i").hasClass("selectiocn-ok")) {
                                    nowallcount += parseInt($(".shopcarnamelist").eq(i).find(".unitary_cart").eq(j).find("input").val()) * parseInt($(".shopcarnamelist").eq(i).find(".unitary_cart").eq(j).find(".orangeprice").attr("data-count"))
                                }
                            }
                        }
                        scope.allprice = nowallcount;



                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogtext = data.err_msg.msg;
                        scope.isdialogshow = true;
                    }
                }).error(function(err) {
                    scope.ctrlisloadshow = false;
                    scope.dialogtext = "网络异常";
                    scope.isdialogshow = true;
                });

            }else if(nowcount > 1){
                jsoncount = num;
                param = "&cart_id="+ this.x.id +"&store_id="+ $routeParams.id +"&cart_count="+ jsoncount;
                scope.ctrlisloadshow = true;    // 打开加载中
                resshopcarservice.init(param).success(function(data) {

                    scope.ctrlisloadshow = false;    // 关闭加载中
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {
                        var jsondata = data.err_msg;
                        that.x.count = num;
                        that.data.ycount[that.x.id] = jsondata.ycount

                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogtext = data.err_msg.msg;
                        scope.isdialogshow = true;
                    }
                }).error(function(err) {
                    scope.ctrlisloadshow = false;
                    scope.dialogtext = "网络异常";
                    scope.isdialogshow = true;
                });

            }

            var countprice = 0;    // 计算所有商品价格
            var $cartlist = $(".unitary_cart");
            for(var i = 0; i < $cartlist.length; i++) {
                countprice += parseInt($cartlist.eq(i).find(".orangeprice").attr("data-count")) * parseInt($cartlist.eq(i).find(".cart_count").val())
            }
            // scope.allprice = countprice;
        };

        // 增加商品个数
        scope.addnum = function(num, price) {
            var that = this;

            if(this.x.count < num) {

                var jsoncount = this.x.count;
                var param = "&cart_id="+ this.x.id +"&store_id="+ $routeParams.id +"&cart_count="+ ++jsoncount;
                scope.ctrlisloadshow = true;    // 打开加载中
                resshopcarservice.init(param).success(function(data) {

                    scope.ctrlisloadshow = false;    // 关闭加载中
                    if(data.err_code == 20000) {    // 没有登陆
                         //跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {
                        var jsondata = data.err_msg;
                        that.x.count++;
                        that.data.ycount[that.x.id] = jsondata.ycount

                        if(that.liclick == true) {
                            nowprice = parseInt(nowprice) + parseInt(price);
                            scope.allprice = nowprice;
                        }


                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogtext = data.err_msg.msg;
                        scope.isdialogshow = true;
                    }
                }).error(function(err) {
                    scope.ctrlisloadshow = false;
                    scope.dialogtext = "网络异常";
                    scope.isdialogshow = true;
                });

            }else{
                scope.dialogtext = "抱歉，当前商品库存只有" + num + "个";
                scope.isdialogshow = true;
            }
        };

        // 减少商品个数
        scope.renum = function(num, price, itemindex) {
            var that = this;

            if(this.x.count == 1) {
                scope.dialogtext = "抱歉，购物车内商品最小数量为一个";
                scope.isdialogshow = true;
            }else{

                var jsoncount = this.x.count;
                var param = "&cart_id="+ this.x.id +"&store_id="+ $routeParams.id +"&cart_count="+ --jsoncount;

                scope.ctrlisloadshow = true;    // 打开加载中
                resshopcarservice.init(param).success(function(data) {

                    scope.ctrlisloadshow = false;    // 关闭加载中
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {
                        var jsondata = data.err_msg;
                        that.x.count--;
                        that.data.ycount[that.x.id] = jsondata.ycount

                        if(that.liclick == true) {
                            nowprice = parseInt(nowprice) - parseInt(price);
                            scope.allprice = nowprice;
                        }

                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogtext = data.err_msg.msg;
                        scope.isdialogshow = true;
                    }
                }).error(function(err) {
                    scope.ctrlisloadshow = false;
                    scope.dialogtext = "网络异常";
                    scope.isdialogshow = true;
                });
            }
        };

        var delcartid = "",
            delscope = "";

        // 删除商品
        scope.delshop = function(cartid) {
            scope.isconfirmshow = true;
            delcartid = cartid;
            delscope = this;

        };

        scope.delshopconfirmok = function() {

            scope.ctrlisloadshow = true;    // 显示出加载中
            var param = "&cart_id=" + delcartid + "&store_id=" + $routeParams.id;
            scope.isconfirmshow = false;    // 将确认框隐藏
            delshpcarservice.init(param).success(function(data) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                if(data.err_code == 20000) {    // 没有登陆
                     //跳到登录页面
                    window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                }else if(data.err_code == 0) {
                    var $cart = $(".unitary_cart");
                    for(var i = 0; i < $cart.length; i++) {
                        if($cart.eq(i).attr("data-id") == delcartid) {
                            if($cart.eq(i).parents("ul").find("li").length > 1) {
                                $animate.leave($cart.eq(i));
                            }else{
                                $animate.leave($cart.eq(i).parents(".shopcarnamelist"));
                            }

                        }
                    }

                    var jsondata = data.err_msg;
                    if(jsondata.count == 0){
                        scope.isdata = true;
                        return;
                    }

                    delscope.shopcarishide = true;    // 将删除的元素隐藏



                }else if(data.err_code == 1000){    // 常规错误
                    scope.dialogtext = data.err_msg;
                    scope.isdialogshow = true;
                }
            }).error(function(err) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;
            });
        };


        // 去结算
        scope.tobuy = function() {
            var $shoplist = $(".shopcarnamelist");
            var cart_ids = [];
            for (var i = 0; i < $shoplist.length; i++) {
                if ($shoplist.eq(i).find(".shopname").attr("data-isclick") == "true") {
                    for (var j = 0; j < $shoplist.eq(i).find(".unitary_cart").length; j++) {

                        if ($shoplist.eq(i).find(".unitary_cart").eq(j).find("i").hasClass("selectiocn-ok")) {
                            cart_ids.push($(".shopcarnamelist").eq(i).find(".unitary_cart").eq(j).attr("data-id"))
                        }

                    }
                }
            }

            if(cart_ids.length == 0) {
                scope.dialogtext = "请选择要购买的商品";
                scope.isdialogshow = true;
                return false;
            }

            scope.ctrlisloadshow = true;    // 显示出加载中

            var url = APIURL + 'webapp.php?c=unitary&a=dobuy';
            var data = {
                    cart_ids: cart_ids
                },
                transFn = function (data) {
                    return $.param(data);
                },
                postCfg = {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    transformRequest: transFn
                };


            http.post(url, data, postCfg)
                .success(function (data) {
                    scope.ctrlisloadshow = false;    // 关闭加载中
                    if (data.err_code == 20000) {    // 没有登陆
                        //跳到登录页面
                        window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                    } else if (data.err_code == 0) {
                        window.location.href= data.err_msg.url;
                    } else if (data.err_code == 1000) {    // 常规错误
                        scope.dialogtext = data.err_msg;
                        scope.isdialogshow = true;
                    }
                }).error(function (err) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;
            });
        }

        var shopid = 0;
        scope.isparclick = false;
        scope.selshop = function(id, index) {
            var $shopname = $(".shopname");
            this.isparsel = true;
            this.isparclick = true;
            for(var i = 0, j = $shopname.length; i < j; i++) {
                if(i == index) {
                    continue;
                }else{
                    if($shopname.eq(i).attr("data-isclick") == "true") {
                        $shopname.eq(i).attr("data-isclick", false)
                        $shopname.eq(i).find("i").removeClass("selectiocn-ok").addClass("selectiocn-no");
                    }
                }
            }
        }

        $(document).off("touchend", ".shopname").on("touchend",".shopname",function(){

            var $shopname = $(".shopname");

            if($(this).attr("data-isclick") == "true") {
                $(this).attr("data-isclick", "false").find("i").addClass("selectiocn-no").removeClass("selectiocn-ok");
                for(var i = 0; i < $(this).parents(".shopcarnamelist").find(".unitary_cart").length; i++) {
                    $(this).parents(".shopcarnamelist").find(".unitary_cart").eq(i).find("i").addClass("selectiocn-no").removeClass("selectiocn-ok");
                }

                nownum = 0;
                nowprice = 0;

            }else{

                nownum = 0;
                nowprice = 0;

                // 商品个数
                nownum = $(this).parents(".shopcarnamelist").find(".orangeprice").length;

                for(var i = 0; i < $(this).parents(".shopcarnamelist").find("li").length; i++) {
                    nowprice += parseInt($(this).parents(".shopcarnamelist").find(".orangeprice").eq(i).attr("data-count")) * parseInt($(this).parents(".shopcarnamelist").find("input").eq(i).val());
                }

                for(var i = 0, j = $shopname.length; i < j; i++) {
                    
                      
                    if($shopname.eq(i).attr("data-isclick") == "true") {
                        $shopname.eq(i).attr("data-isclick", "false");
                        $shopname.eq(i).find("i").addClass("selectiocn-no").removeClass("selectiocn-ok");

                        for(var k = 0; k < $shopname.eq(i).parents(".shopcarnamelist").find(".unitary_cart i").length; k++) {
                            $shopname.eq(i).parents(".shopcarnamelist").find(".unitary_cart i").addClass("selectiocn-no").removeClass("selectiocn-ok");
                        }
                    }
                }

                $(this).attr("data-isclick", "true").find("i").addClass("selectiocn-ok").removeClass("selectiocn-no");

                for(var i = 0; i < $(this).parents(".shopcarnamelist").find(".unitary_cart").length; i++) {
                    $(this).parents(".shopcarnamelist").find(".unitary_cart").eq(i).find("i").addClass("selectiocn-ok").removeClass("selectiocn-no");
                }


            }
            scope.allshoplistnum = nownum;
            scope.allprice = nowprice;
            scope.$apply(scope.allprice);
            scope.$apply(scope.allshoplistnum);


        });

        $(document).off("touchend", ".selectpos").on("touchend",".selectpos",function() {

            var $shopname = $(".shopname");

            if($(this).hasClass("selectiocn-ok")) {
                $(this).addClass("selectiocn-no").removeClass("selectiocn-ok");
                nownum--;
                nowprice -= parseInt($(this).parents(".unitary_cart").find(".orangeprice").attr("data-count")) * parseInt($(this).parents(".unitary_cart").find("input").val());
            }else{

                $(this).parents(".unitary_cart").attr("data-liclick", true);
                $(this).addClass("selectiocn-ok").removeClass("selectiocn-no");
                if($(this).parents(".shopcarnamelist").find(".shopname").attr("data-isclick") == "false") {
                    nownum = 0;
                    nownum++;
                    nowprice = 0;
                    nowprice += parseInt($(this).parents(".unitary_cart").find(".orangeprice").attr("data-count")) * parseInt($(this).parents(".unitary_cart").find("input").val());

                    for(var i = 0, j = $shopname.length; i < j; i++) {
                        if($shopname.eq(i).attr("data-isclick") == "true") {
                            $shopname.eq(i).attr("data-isclick", "false");
                            $shopname.eq(i).find("i").addClass("selectiocn-no").removeClass("selectiocn-ok");

                            for(var k = 0; k < $shopname.eq(i).parents(".shopcarnamelist").find(".unitary_cart i").length; k++) {
                                $(".shopname").eq(i).parents(".shopcarnamelist").find(".unitary_cart i").addClass("selectiocn-no").removeClass("selectiocn-ok");
                            }
                        }
                    }
                    $(this).parents(".shopcarnamelist").find(".shopname").attr("data-isclick", "true");
                    $(this).parents(".shopcarnamelist").find(".shopname i").addClass("selectiocn-ok").removeClass("selectiocn-no");
                }else{
                    nownum++;
                    nowprice += parseInt($(this).parents(".unitary_cart").find(".orangeprice").attr("data-count")) * parseInt($(this).parents(".unitary_cart").find("input").val());
                }
            }

            var allprice = 0;
            for(var i = 0; i < $(this).parents(".shopcarnamelist").find(".unitary_cart").length; i++) {
                if($(this).parents(".shopcarnamelist").find(".unitary_cart").eq(i).find("i").hasClass("selectiocn-ok")) {
                    allprice += parseInt($(this).parents(".shopcarnamelist").find(".unitary_cart").eq(i).attr("data-price"))
                }
            }

            if(nownum < 0) { 
                nownum = 0;
            }
            if(nowprice < 0) {
                nowprice = 0;
            }
            scope.allshoplistnum = nownum;
            scope.allprice = nowprice;
            scope.$apply(scope.allshoplistnum)
            scope.$apply(scope.allprice)
        });

        scope.childclick = function() {
            if(this.liclick == true) {
                this.liclick = false;
            }else{
                this.liclick = true;
            }
        }

}]);


// 我的订单
ctrl.controller("orderinfoCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "mypayservice",
    "$location",
    "isloadmoreservice",
    function($rootScope, scope, http, $routeParams, mypayservice, $location, isloadmoreservice) {
    var param = "&store_id=" + $routeParams.id,
        $btn = $(".m_buylist_nav-btn"),    // TAB按钮
        lock = true,    // 函数节流
        nextpage = false,    // 是否有下一页
        page = 2;    // 当前页数

    scope.ctrlisloadshow = true;    // 关闭加载中
    mypayservice.init(param).success(function(data) {
        scope.ctrlisloadshow = false;    // 关闭加载中
        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {

            scope.data = data.err_msg;

            try{
                if(scope.data.unitary.length == 0) {
                    scope.isdatashow = true;
                }
            }catch(e) {

            }


            var footurl = "";
            try{
                // if(scope.data.unitary.store_id > 0) {
                if($routeParams.id > 0) {
                    footurl = APIURL + "webapp/snatch/#/shoplist/"+ $routeParams.id
                }else{
                    footurl = APIURL + "webapp/snatch/#/userindex";
                }
            }catch (e) {
                footurl = APIURL + "webapp/snatch/#/userindex";
            }


            // 设置底部导航链接
            scope.footerlistUrl = {
                allshop : footurl,
                shopcar : APIURL+"webapp/snatch/#/shopcar/" + $routeParams.id,
                me : APIURL + "webapp/snatch/#/orderinfo/" + $routeParams.id
            };

            if(data.err_msg.next_page) {    // 显示加载更多
                nextpage = true;
                scope.isloadmore = true;
            }else{
                nextpage = false;
                scope.isloadmore = false;
            }

            if(scope.data.cart_count == null || parseInt(scope.data.cart_count) == 0 || scope.data.cart_count == undefined) {
                scope.isShopNumHide = true;
            }else{
                scope.shopcarnum = scope.data.cart_count;
            }

        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogtext = data.err_msg;
            scope.isdialogshow = true;
        }
    }).error(function(err){
        scope.ctrlisloadshow = false;    // 关闭加载中
        scope.dialogtext = "网络异常";
        scope.isdialogshow = true;
    });

    scope.go = function(id, type) {
        if(type == "end"){
            $location.path("orderend/" + id + "/" + $routeParams.id);
        }else if(type == "ing"){
            $location.path("ordering/" + id + "/" + $routeParams.id);
        }
        angular.element(document).off("scroll");
    };


    scope.showMyLuckNum=function(x){


        }


    var setTimeOutStr;

    // 滚动事件，判断是否需要新加数据
    angular.element(document).on("scroll",function() {
        if(nextpage) {
            var winheight = $(window).height(),
                nowheight = $(".load_more").offset().top,
                scrollTop = $(window).scrollTop();

            if(isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {
                if(lock) {
                    lock = false;

                    scope.ctrlisloadshow = true;    // 开启加载中
                    clearTimeout(setTimeOutStr);
                    setTimeOutStr = setTimeout(function(){

                        mypayservice.init(param + "&page="+ page).success(function(data) {
                            scope.ctrlisloadshow = false;    // 关闭加载中
                            if(data.err_code == 20000) {    // 没有登陆
                                // 跳到登录页面
                                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                            }else if(data.err_code == 0) {
                                lock = true;
                                for(var i = 0, j = data.err_msg.unitary.length; i < j; i++) {
                                    scope.data.unitary.push(data.err_msg.unitary[i]);
                                }

                                // var jsondataindex = [];
                                // for(var i = 0; i < data.err_msg.unitary.length; i++) {
                                //     jsondataindex.push(data.err_msg.unitary[i].id);
                                // }

                                // for(var k = 0, l = jsondataindex.length; k <= l; k++) {
                                //     scope.data.unitary[jsondataindex[k]] =  data.err_msg.unitary[jsondataindex[k]];
                                // }
                                // jsondataindex = [];

                                if(data.err_msg.next_page) {
                                    page++;
                                    scope.isloadmore = true;
                                    nextpage = true;
                                }else{
                                    scope.isloadmore = false;
                                    nextpage = false;
                                }

                            }else if(data.err_code == 1000){    // 常规错误
                                scope.dialogtext = data.err_msg;
                                scope.isdialogshow = true;
                            }
                        }).error(function(err){
                            scope.ctrlisloadshow = false;    // 关闭加载中
                            scope.dialogtext = "网络异常";
                            scope.isdialogshow = true;
                        });

                    }, 300);
                    
                }
            }
        }
    });


    scope.navclick = function(text) {

        switch(text){
            case "all":    // 全部

                param = "&store_id=" + $routeParams.id;
                scope.isbothhide = false;    // 将全部取消隐藏
                scope.isingshow = false;    // 正在进行中隐藏
                scope.isendshow = false;    // 结束隐藏
                scope.isokshow = false;    // 中奖隐藏
                $btn.eq(0).addClass("hover").siblings(".m_buylist_nav-btn").removeClass("hover");
                break;
            case "ing":    // 正在进行

                param = "&store_id=" + $routeParams.id + "&type=ing";
                scope.isbothhide = true;    // 将全部隐藏
                scope.isingshow = true;    // 正在进行中显示
                scope.isendshow = false;    // 结束隐藏
                scope.isokshow = false;    // 中奖隐藏
                $btn.eq(1).addClass("hover").siblings(".m_buylist_nav-btn").removeClass("hover");
                break;
            case "end":    // 结束

                param = "&store_id=" + $routeParams.id + "&type=end";
                scope.isbothhide = true;    // 将全部隐藏
                scope.isingshow = false;    // 正在进行中隐藏
                scope.isendshow = true;    // 结束显示
                scope.isokshow = false;    // 中奖隐藏
                $btn.eq(2).addClass("hover").siblings(".m_buylist_nav-btn").removeClass("hover");
                break;
            case "myluck":    // 中奖

                param = "&store_id=" + $routeParams.id + "&type=myluck";
                scope.isbothhide = true;    // 将全部隐藏
                scope.isingshow = false;    // 正在进行中隐藏
                scope.isendshow = false;    // 结束隐藏
                scope.isokshow = true;    // 中奖显示
                $btn.eq(3).addClass("hover").siblings(".m_buylist_nav-btn").removeClass("hover");
                break;
        }
        page = 2;
        scope.ctrlisloadshow = true;    // 关闭加载中
        mypayservice.init(param).success(function(data) {
            scope.ctrlisloadshow = false;    // 关闭加载中
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {
                scope.data = data.err_msg;

                if(scope.data.unitary == null) {
                    scope.isdatashow = true;
                }else if(scope.data.unitary != null){
                    scope.isdatashow = false;
                }

                if(data.err_msg.next_page) {
                    scope.isloadmore = true;
                    nextpage = true;
                }else{
                    scope.isloadmore = false;
                    nextpage = false;
                }

            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function(err){
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });
    }
}]);


// 所有购买记录
ctrl.controller("allbuyCtrl",["$rootScope", "$scope", "$http", "$routeParams", "allbuyservice", "isloadmoreservice", function($rootScope, scope, http, $routeParams, allbuyservice, isloadmoreservice) {

    var param = "&unitary_id=" + $routeParams.id + "&store_id=" + $routeParams.storeid,
        page = 2,    // 当前页数
        nextpage = false,    // 是否有下一页
        lock = true;    // 函数节流

    scope.ctrlisloadshow = true;    // 关闭加载中
    allbuyservice.init(param).success(function(data) {

        scope.ctrlisloadshow = false;    // 关闭加载中

        if(data.err_code == 20000) {    // 没有登陆
            // 跳到登录页面
            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
        }else if(data.err_code == 0) {
            scope.data = data.err_msg;

            data.err_msg.next_page ? nextpage = true : nextpage = false;

            if(scope.data.cart_list.length > 0) {
                scope.isallbuyshow = true;
            }else{
                scope.isallbuynoneshow = true;
            }

            var footurl = "";

            if(data.err_msg.store_id > 0) {
                footurl = APIURL + "webapp/snatch/#/shoplist/"+ data.err_msg.store_id
            }else{
                footurl = APIURL + "webapp/snatch/#/userindex";
            }
            // 设置底部导航链接
            scope.footerlistUrl = {
                allshop : footurl,
                shopcar : APIURL+"webapp/snatch/#/shopcar/" + $routeParams.storeid,
                me : APIURL + "webapp/snatch/#/orderinfo/" + $routeParams.storeid
            };

            if(scope.data.cart_count == null || parseInt(scope.data.cart_count) == 0 || scope.data.cart_count == undefined) {
                scope.isShopNumHide = true;
            }else{
                scope.shopcarnum = scope.data.cart_count;
            }

            if(data.err_msg.next_page) {    // 显示加载更多
                nextpage = true;
                scope.isloadmore = true;
            }else{
                nextpage = false;
                scope.isloadmore = false;
            }

        }else if(data.err_code == 1000){    // 常规错误
            scope.dialogtext = data.err_msg;
            scope.isdialogshow = true;
        }
    }).error(function(err){
        scope.ctrlisloadshow = false;    // 关闭加载中
        scope.dialogtext = "网络异常";
        scope.isdialogshow = true;
    });


    // 滚动事件，判断是否需要新加数据
    angular.element(document).on("scroll",function() {
        if(nextpage) {
            var winheight = $(window).height(),
                nowheight = $(".load_more").offset().top,
                scrollTop = $(window).scrollTop();

            if(isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                if(lock) {
                    lock = false;
                    scope.ctrlisloadshow = true;
                    allbuyservice.init(param + "&page="+ page).success(function(data) {

                        scope.ctrlisloadshow = false;    // 关闭加载中
                        if(data.err_code == 20000) {    // 没有登陆
                            // 跳到登录页面
                            window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                        }else if(data.err_code == 0) {    // 正常状态

                            // 遍历数据追加到相应地方
                            for(var i = 0, j = data.err_msg.cart_list.length; i < j; i++) {
                                scope.data.cart_list.push(data.err_msg.cart_list[i]);
                            }

                            if(data.err_msg.next_page) {    // 显示加载更多
                                nextpage = true;
                                page++;
                                lock = true;
                            }else{
                                nextpage = false;
                                scope.isloadmore = false;
                            }

                        }else if(data.err_code == 1000){    // 常规错误
                            scope.dialogtext = data.err_msg;
                            scope.isdialogshow = true;
                        }
                    }).error(function(err){
                        scope.ctrlisloadshow = false;    // 关闭加载中
                        scope.dialogtext = "网络异常";
                        scope.isdialogshow = true;
                    });
                }
            }
        }
    });
}]);


ctrl.controller("orderingCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "orderindservice",
    "isloadmoreservice",
    "addshpcarservice",
    "$location",
    "myLuckNumService",
    function($rootScope, scope, http, $routeParams, orderindservice, isloadmoreservice, addshpcarservice, $location,myLuckNumService) {

        var param = "&unitary_id=" + $routeParams.id,
            page = 2,
            nextpage = false,
            lock = true;

        scope.inpval = 1;    // 设置input默认值

        scope.ctrlisloadshow = true;    // 关闭加载中
        orderindservice.init(param).success(function(data) {
            scope.ctrlisloadshow = false;    // 关闭加载中
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {
                scope.data = data.err_msg;

                var footurl = "";
                if(scope.data.unitary.store_id > 0) {
                    footurl = APIURL + "webapp/snatch/#/shoplist/"+ scope.data.unitary.store_id
                }else{
                    footurl = APIURL + "webapp/snatch/#/userindex";
                }
                // 设置底部导航链接
                scope.footerlistUrl = {
                    allshop : footurl,
                    shopcar : APIURL+"webapp/snatch/#/shopcar/" + $routeParams.storeid,
                    me : APIURL + "webapp/snatch/#/orderinfo/" + $routeParams.storeid
                };

                if(scope.data.cart_count == null || parseInt(scope.data.cart_count) == 0 || scope.data.cart_count == undefined) {
                    scope.isShopNumHide = true;
                }else{
                    scope.shopcarnum = scope.data.cart_count;
                }

                if(data.err_msg.next_page) {    // 显示加载更多
                    nextpage = true;
                    scope.isloadmore = true;
                }else{
                    nextpage = false;
                    scope.isloadmore = false;
                }

            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function(err){
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });

        // input 失去焦点
        scope.numcount = function(num) {
            this.inpval = parseInt(this.inpval)
            if(this.inpval > num){
                this.inpval = num;
                scope.dialogtext = "购买最大数量不得大于剩余人数";
                scope.isdialogshow = true;
            }else if(this.inpval <= 0) {
                this.inpval = 1;
                scope.dialogtext = "购买最小数量不得小于1";
                scope.isdialogshow = true;
            }
        }



        /********************幸运号逻辑开始********************/
        scope.showAllMyLuckNumsAble=false;
        scope.showMyLuckNumsAble=false;
        //显示我的幸运号
        scope.showMyLuckNum=function(){
            scope.showMyLuckNumsAble=!scope.showMyLuckNumsAble;
            myLuckNumService.init(param).success(function(data){

                switch (data.err_code){
                    case 2000:
                        window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                        break;
                    case 1000:
                        scope.myLuckNums=[data.err_msg];
                        break;
                    case 0:
                        scope.myLuckNums=data.err_msg;
                        break;
                }

                scope.checkMyLuckNumShowList();
            }).error(function(err){
                scope.myLuckNums=[
                    '网络异常'
                ] ;
                scope.checkMyLuckNumShowList();
            });
        }

        scope.showAllMyLuckNum=function(){
            scope.showAllMyLuckNumsAble=!scope.showAllMyLuckNumsAble;
            scope.checkMyLuckNumShowList();
        }

        scope.checkMyLuckNumShowList=function(){
            scope.myLuckNumsShowList= scope.myLuckNums.concat();
            if(!scope.showAllMyLuckNumsAble){
                scope.myLuckNumsShowList= scope.myLuckNumsShowList.slice(0,5);
            }
        }
        /********************幸运号逻辑结束********************/

        // 追加到购物车
        scope.addshop = function(id, num) {
            num = parseInt(num)
            var re = /^[1-9]+[0-9]*]*$/;
            if(re.test(num)) {
                var addparam = "&unitary_id=" + id + "&buynum=" + num;
                scope.ctrlisloadshow = true;
                // 请求购物车追加接口
                addshpcarservice.init(addparam).success(function(data) {
                    scope.ctrlisloadshow = false;
                    if(data.err_code == 20000) {    // 没有登陆
                        // 跳到登录页面
                        window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                    }else if(data.err_code == 0) {    // 正常
                        scope.isShopNumHide = false;
                        scope.shopcarnum = data.err_msg.count;
                        $location.path("shopcar/"+scope.data.unitary.store_id);
                    }else if(data.err_code == 1000){    // 常规错误
                        scope.dialogtext = data.err_msg;
                        scope.isdialogshow = true;
                    }
                }).error(function() {
                    scope.ctrlisloadshow = false;
                    scope.dialogtext = "网络异常";
                    scope.isdialogshow = true;
                });
            }else{
                scope.dialogtext = "请输入正确阿拉伯整数！";
                scope.isdialogshow = true;
            }
        };



        angular.element(document).on("scroll", function () {
            if (nextpage) {
                var winheight = $(window).height(),
                    nowheight = $(".load_more").offset().top,
                    scrollTop = $(window).scrollTop();

                if (isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                    if (lock) {
                        lock = false;
                        scope.ctrlisloadshow = true;
                        orderindservice.init(param + "&page=" + page).success(function (data) {

                            scope.ctrlisloadshow = false;    // 关闭加载中
                            if (data.err_code == 20000) {    // 没有登陆
                                // 跳到登录页面
                                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                            } else if (data.err_code == 0) {    // 正常状态

                                // 遍历数据追加到相应地方
                                for (var i = 0, j = data.err_msg.lucknum.length; i < j; i++) {
                                    scope.data.lucknum.push(data.err_msg.lucknum[i]);
                                }

                                if (data.err_msg.next_page) {    // 显示加载更多
                                    nextpage = true;
                                    page++;
                                    lock = true;
                                } else {
                                    nextpage = false;
                                    scope.isloadmore = false;
                                }

                            } else if (data.err_code == 1000) {    // 常规错误
                                scope.dialogtext = data.err_msg;
                                scope.isdialogshow = true;
                            }
                        }).error(function (err) {
                            scope.ctrlisloadshow = false;    // 关闭加载中
                            scope.dialogtext = "网络异常";
                            scope.isdialogshow = true;
                        });
                    }
                }
            }
        });
}]);


//--计算幸运号 页面
ctrl.controller("calculateCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "calcuteservice",
    "isloadmoreservice",
    "$interval",
    function($rootScope, scope, http, $routeParams, calcuteservice, isloadmoreservice, $interval) {
        var unitarid=$routeParams.unitaryid;
        var param='&unitary_id='+unitarid;
        scope.ctrlisloadshow = true;    // 开启加载中
        calcuteservice.init(param).success(function(data){
            scope.ctrlisloadshow = false;    // 关闭加载中
            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
            }else if(data.err_code == 0) {
                scope.data = data.err_msg;
            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function(){
            scope.dialogtext = '网络异常';
            scope.isdialogshow = true;
        });

        scope.reback=function(){
            window.history.back();
        }
    }]);


//
ctrl.controller("orderendCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "orderendservice",
    "isloadmoreservice",
    "$interval",
    "$location",
    "myLuckNumService",
    function($rootScope, scope, http, $routeParams, orderendservice, isloadmoreservice, $interval,$location,myLuckNumService) {
        $("body").css("background","#fff");
        var param = "&unitary_id=" + $routeParams.id,
            page = 2,
            nextpage = false,
            endtimemin = 0,
            endtimes = 0,
            lock = true;

        var footurl = "";
        try{
            if(scope.data.unitary.store_id > 0) {
                footurl = APIURL + "webapp/snatch/#/shoplist/"+ scope.data.unitary.store_id
            }else{
                footurl = APIURL + "webapp/snatch/#/userindex";
            }
        }catch (e){
            footurl = APIURL + "webapp/snatch/#/userindex";
        }

        // 设置底部导航链接
        scope.footerlistUrl = {
            allshop : footurl,
            shopcar : APIURL+"webapp/snatch/#/shopcar/" + $routeParams.storeid,
            me : APIURL + "webapp/snatch/#/orderinfo/" + $routeParams.storeid
        };

        function getshopdate(min, s){    // 倒计时功能
            if(s <= 0) {
                endtimemin = min - 1;
                endtimes = 59;
            }else if(s > 0) {
                endtimes = s -1;
            }
            return endtimemin + " 分 " + endtimes + "秒";
        }

        scope.ctrlisloadshow = true;    // 关闭加载中
        orderendservice.init(param).success(function(data) {

            scope.ctrlisloadshow = false;    // 关闭加载中

            if(data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);

            }else if(data.err_code == 0) {
                scope.data = data.err_msg;

                   scope.data.min == undefined ?  scope.isdataok = true :
                       endtimemin = parseInt(scope.data.min);
                endtimes = parseInt(scope.data.s);

                if(scope.data.min) {
                    scope.minok = true;
                    scope.minno = false;

                    scope.orderendtimecount = getshopdate(endtimemin, endtimes);

                    var intertime = $interval(function() {
                        scope.orderendtimecount = getshopdate(endtimemin, endtimes);
                        if(endtimemin == 0 && endtimes == 0) {
                            $interval.cancel(intertime);
                            scope.minok = false;
                            scope.minno = true;
                        }
                    }, 1000)
                }else{
                    scope.minok = false;
                    scope.minno = true;
                }

                if(scope.data.cart_count == null || parseInt(scope.data.cart_count) == 0 || scope.data.cart_count == undefined) {
                    scope.isShopNumHide = true;
                }else{
                    scope.shopcarnum = scope.data.cart_count;
                }

                if(data.err_msg.next_page) {    // 显示加载更多
                    nextpage = true;
                    scope.isloadmore = true;
                }else{
                    nextpage = false;
                    scope.isloadmore = false;
                }

            }else if(data.err_code == 1000){    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function(err){
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });



        /********************幸运号逻辑开始********************/
            scope.showAllMyLuckNumsAble=false;
            scope.showMyLuckNumsAble=false;
            //显示我的幸运号
            scope.showMyLuckNum=function(){
                scope.showMyLuckNumsAble=!scope.showMyLuckNumsAble;
                myLuckNumService.init(param).success(function(data){

                    switch (data.err_code){
                        case 2000:
                            window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                            break;
                        case 1000:
                            scope.myLuckNums=[data.err_msg];
                            break;
                        case 0:
                            scope.myLuckNums=data.err_msg;
                            break;
                    }
                    scope.checkMyLuckNumShowList();
                }).error(function(err){
                    scope.myLuckNums=[
                        '网络异常'
                    ] ;
                    scope.checkMyLuckNumShowList();
                });
            }

            scope.showAllMyLuckNum=function(){
                scope.showAllMyLuckNumsAble=!scope.showAllMyLuckNumsAble;
                scope.checkMyLuckNumShowList();
            }

            scope.checkMyLuckNumShowList=function(){
                scope.myLuckNumsShowList= scope.myLuckNums.concat();
                if(!scope.showAllMyLuckNumsAble){
                    scope.myLuckNumsShowList= scope.myLuckNumsShowList.slice(0,5);
                }
            }
        /********************幸运号逻辑结束********************/



        // 点击详情
        scope.orderendCountshow = function() {
           // scope.orderendCountisshow = true;
            $location.path("calculate/" + $routeParams.id);
            angular.element(document).off("scroll");
        };

        /********************幸运号逻辑开始*******************
        scope.showAllMyLuckNumsAble=false;
        scope.showMyLuckNumsAble=false;
        //显示我的幸运号
        scope.showMyLuckNum=function(id){
            scope.showMyLuckNumsAble=!scope.showMyLuckNumsAble;
            myLuckNumService.init(param).success(function(data){

                switch (data.err_code){
                    case 2000:
                        window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                        break;
                    case 1000:
                        scope.myLuckNums=[data.err_msg];
                        break;
                    case 0:
                        scope.myLuckNums=data.err_msg;
                        break;
                }

                scope.checkMyLuckNumShowList();
            }).error(function(err){
                scope.myLuckNums=[
                    '网络异常'
                ] ;
                scope.checkMyLuckNumShowList();
            });
        }

        scope.showAllMyLuckNum=function(){
            scope.showAllMyLuckNumsAble=!scope.showAllMyLuckNumsAble;
            scope.checkMyLuckNumShowList();
        }

        scope.checkMyLuckNumShowList=function(){
            scope.myLuckNumsShowList= scope.myLuckNums.concat();
            if(!scope.showAllMyLuckNumsAble){
                scope.myLuckNumsShowList= scope.myLuckNumsShowList.slice(0,5);
            }
        }
        /********************幸运号逻辑结束********************/

        // 滚动事件，判断是否需要新加数据
        angular.element(document).on("scroll", function () {
            if (nextpage) {
                var winheight = $(window).height(),
                    nowheight = $(".load_more").offset().top,
                    scrollTop = $(window).scrollTop();

                if (isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                    if (lock) {
                        lock = false;
                        scope.ctrlisloadshow = true;
                        orderendservice.init(param + "&page=" + page).success(function (data) {

                            scope.ctrlisloadshow = false;    // 关闭加载中
                            if (data.err_code == 20000) {    // 没有登陆
                                // 跳到登录页面
                                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                            } else if (data.err_code == 0) {    // 正常状态

                                // 遍历数据追加到相应地方
                                for (var i = 0, j = data.err_msg.lucknum.length; i < j; i++) {
                                    scope.data.lucknum.push(data.err_msg.lucknum[i]);
                                }

                                if (data.err_msg.next_page) {    // 显示加载更多
                                    nextpage = true;
                                    page++;
                                    lock = true;
                                } else {
                                    nextpage = false;
                                    scope.isloadmore = false;
                                }

                            } else if (data.err_code == 1000) {    // 常规错误
                                scope.dialogtext = data.err_msg;
                                scope.isdialogshow = true;
                            }
                        }).error(function (err) {
                            scope.ctrlisloadshow = false;    // 关闭加载中
                            scope.dialogtext = "网络异常";
                            scope.isdialogshow = true;
                        });
                    }
                }
            }
    });
}]);


ctrl.controller("shopinfoCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "$location",
    function($rootScope, scope, http, $routeParams, $location) {

        scope.ctrlisloadshow = true;    // 打开加载中
        http({
            method:'GET',
            url: APIURL+'webapp.php?c=unitary&a=goodsinfo&unitary_id=' + $routeParams.id
        }).success(function (data) {

            scope.ctrlisloadshow = false;    // 关闭加载中
            if (data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
            } else if (data.err_code == 0) {    // 正常状态

                scope.data = data.err_msg;
                if(scope.data.info == "") {
                    scope.havadata = false;
                    scope.nodata = true;
                }else{
                    scope.havadata = true;
                    scope.nodata = false;
                }
            } else if (data.err_code == 1000) {    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function (err) {
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });

        scope.shopinfoback = function() {
            $location.path("main/" + $routeParams.id);
        }


}]);


// 用户主页
ctrl.controller("userindexCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "userIndexservice",
    "addshopcartservice",
    "$location",
    function($rootScope, scope, http, userIndexservice, addshopcartservice, $location) {

        scope.ctrlisloadshow = true;    // 开启加载中
        var param = '&nowd='+ new Date();
        userIndexservice.init(param).success(function (data) {

            scope.ctrlisloadshow = false;    // 关闭加载中
            if (data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
            } else if (data.err_code == 0) {    // 正常状态
                scope.data = data.err_msg;
                if(data.err_msg.count > 0) scope.shopcarnum = data.err_msg.count;
            } else if (data.err_code == 1000) {    // 常规错误
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function (err) {
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });

        scope.addshop = function(id) {
            var param = "&is_platform=1&unitary_id=" + id;
            scope.ctrlisloadshow = true;    // 开启加载中
            addshopcartservice.init(param).success(function (data) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                if (data.err_code == 20000) {    // 没有登陆
                    // 跳到登录页面
                    window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                } else if (data.err_code == 0) {    // 正常状态
                    scope.dialogtext = "商品添加成功~";
                    scope.isdialogshow = true;
                    scope.shopcarnum = data.err_msg.count;
                } else if (data.err_code == 1000) {    // 常规错误
                    scope.dialogtext = data.err_msg;
                    scope.isdialogshow = true;
                }
            }).error(function (err) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;
            });
        }

        scope.gosearch = function(text) {
            if(text == "" || text == undefined || text == null) {
                scope.dialogtext = "请输入商品名称！";
                scope.isdialogshow = true;
                return false;
            }
            $location.path("search/" + encodeURIComponent(text))
        }

}]);


// 用户商品列表页
ctrl.controller("userallshoplistCtrl",[
    "$rootScope",
    "$scope",
    "$http",
    "$routeParams",
    "usernavservice",
    "addshopcartservice",
    "isloadmoreservice",
    function($rootScope, scope, http, $routeParams, usernavservice, addshopcartservice, isloadmoreservice) {
        var typeparam = "",
            order_by = "",
            page = 2,
            nextpage = false,
            lock = true,
            ispriceclick = false;

        switch ($routeParams.type){
            case "renqi":
                order_by = "&order_by=renqi";
                break;
            case "all":
                order_by = "";
                break;
            case "addtime":
                order_by = "&order_by=addtime";
                $(".addtime").addClass("selected").siblings().removeClass("selected");
                break;
            default:
                order_by = "";
        }

        scope.ctrlisloadshow = true;    // 开启加载中
        usernavservice.init(order_by).success(function (data) {

            scope.ctrlisloadshow = false;    // 关闭加载中
            if (data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
            } else if (data.err_code == 0) {    // 正常状态

                scope.data = data.err_msg;
                scope.condata = data.err_msg.list;
                if(scope.data.count > 0) scope.shopcarnum = data.err_msg.count;
                
                if(!data.err_msg.next_page) {
                    scope.isloadmore = true;
                }
                if(data.err_msg.next_page) {
                    nextpage = true;
                }

            } else if (data.err_code == 1000) {    // 常规错误

                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function (err) {

            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });



        scope.onlistcatlog = function(obj, index) {

            $(".navlisttype").eq(index).addClass("selected").siblings().removeClass("selected");
            typeparam = "&cat_id=" + obj.cat_id;
            var allparam = typeparam;
            page = 2;
            nextpage = true;
            if(order_by) allparam += order_by;

            scope.loadbjisshow = false;
            scope.ctrlisloadshow = true;    // 开启加载中

            usernavservice.init(allparam).success(function (data) {

                scope.isloadmore = true;    // 关闭加载中
                scope.ctrlisloadshow = false;    // 关闭加载中
                if (data.err_code == 20000) {    // 没有登陆
                    // 跳到登录页面
                    window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                } else if (data.err_code == 0) {    // 正常状态

                    scope.navlistisshow = false;
                    scope.loadbjisshow = false;
                    scope.condata = data.err_msg.list;

                } else if (data.err_code == 1000) {    // 常规错误

                    scope.dialogtext = data.err_msg;
                    scope.isdialogshow = true;
                }
            }).error(function (err) {

                scope.ctrlisloadshow = false;    // 关闭加载中
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;
            });
        }


        scope.addshopcar = function(id) {
            var param = "&is_platform=1&unitary_id=" + id;
            scope.ctrlisloadshow = true;    // 开启加载中
            addshopcartservice.init(param).success(function (data) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                if (data.err_code == 20000) {    // 没有登陆
                    // 跳到登录页面
                    window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                } else if (data.err_code == 0) {    // 正常状态
                    scope.dialogtext = "商品添加成功~";
                    scope.isdialogshow = true;
                    scope.shopcarnum = data.err_msg.count;

                } else if (data.err_code == 1000) {    // 常规错误
                    scope.dialogtext = data.err_msg;
                    scope.isdialogshow = true;
                }
            }).error(function (err) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;
            });
        }



        // 滚动事件，判断是否需要新加数据
        angular.element(document).on("scroll",function() {
            if(nextpage) {
                var winheight = $(window).height(),
                    nowheight = $(".load_more").offset().top,
                    scrollTop = $(window).scrollTop(),
                    allparam = "";

                    if(order_by) allparam += order_by;
                    if(typeparam) allparam += typeparam;
                    if(page) allparam += "&page=" + page;

                if(isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                    if(lock) {
                        lock = false;

                        scope.ctrlisloadshow = true;
                        usernavservice.init(allparam).success(function (data) {
                            scope.ctrlisloadshow = false;
                            if(data.err_code == 20000) {    // 没有登陆
                                // 跳到登录页面
                                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                            }else if(data.err_code == 0) {    // 正常

                                if(data.err_msg.next_page) {
                                    nextpage = true;
                                    page++;
                                }else{
                                    nextpage = false;
                                    scope.isloadmore = true;
                                }
                                lock = true;

                                for(var i = 0; i < data.err_msg.list.length; i++) {
                                    scope.condata.push(data.err_msg.list[i]);
                                }

                            }else if(data.err_code == 1000){    // 常规错误
                                scope.dialogtext = data.err_msg;
                                scope.isdialogshow = true;
                            }
                        }).error(function(err) {
                            scope.ctrlisloadshow = false;
                            scope.dialogtext = "网络异常";
                            scope.isdialogshow = true;

                        });
                    }
                }
            }
        });


        scope.restype = function(type) {
            page = 0;
            var allparam = "",
                $price = $(".price");
            switch (type){
                case "renqi":
                    ishasprice($price);
                    order_by = "&order_by=renqi";
                    $(".renqi").addClass("selected").siblings().removeClass("selected");
                    break;
                case "proportion":
                    ishasprice($price);
                    order_by = "&order_by=proportion";
                    $(".proportion").addClass("selected").siblings().removeClass("selected");
                    //$(".proportion").css("background", "red")
                    break;
                case "addtime":
                    ishasprice($price);
                    order_by = "&order_by=addtime";
                    $(".addtime").addClass("selected").siblings().removeClass("selected");
                    break;
                case "price":

                    ispriceclick = !ispriceclick;    // true表示up false表示down

                    if(ispriceclick) {
                        if($price.hasClass("down")) $price.removeClass("down");
                        $price.addClass("up");
                        order_by = "&order_by=priceup";
                    }else{
                        $price.addClass("down");
                        if($price.hasClass("up")) $price.removeClass("up");

                        order_by = "&order_by=pricedown";
                    }

                    $price.addClass("selected").siblings().removeClass("selected");
                    break;
                default:
                    scope.dialogtext = "程序异常，请刷新后重试";
                    scope.isdialogshow = true;
            }

            if(order_by) allparam += order_by;
            if(typeparam) allparam += typeparam;

            scope.ctrlisloadshow = true;    // 开启加载中
            usernavservice.init(allparam).success(function (data) {
                scope.ctrlisloadshow = false;
                if(data.err_code == 20000) {    // 没有登陆
                    // 跳到登录页面
                    window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                }else if(data.err_code == 0) {    // 正常

                    if(data.err_msg.next_page) {
                        nextpage = true;
                        page = 2;
                    }else{
                        nextpage = false;
                        scope.isloadmore = true;
                    }
                    scope.condata = data.err_msg.list;

                }else if(data.err_code == 1000){    // 常规错误
                    scope.dialogtext = data.err_msg;
                    scope.isdialogshow = true;
                }
            }).error(function(err) {
                scope.ctrlisloadshow = false;
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;

            });
        };

        function ishasprice(el) {
            ispriceclick = false;
            if(el.hasClass("selected")) {
                if(el.hasClass("down")) {
                    el.removeClass("down");
                }else if(el.hasClass("up")) {
                    el.removeClass("up");
                }
            }
        }

}]);



// 用户商品列表页
ctrl.controller("searchCtrl",[
    "$rootScope",
    "$scope",
    "$routeParams",
    "searchJson", 
    "addshopcartservice", 
    "isloadmoreservice", 
    function($rootScope, scope, $routeParams, searchJson, addshopcartservice, isloadmoreservice) {

        scope.keyword = decodeURIComponent($routeParams.keyword)

        var param = "&keyword=" + $routeParams.keyword,
            page = 2,
            nextpage = false,
            lock = true;

        scope.ctrlisloadshow = true;
        searchJson.init(param).success(function (data) {

            scope.ctrlisloadshow = false;    // 关闭加载中
            if (data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
            } else if (data.err_code == 0) {    // 正常状态

                scope.data = data.err_msg;

                scope.shopcarnum = scope.data.count;

                if(scope.data.next_page) {
                    nextpage = true;
                }else{
                    scope.isloadmore = true;
                }

                try{
                    if(scope.data.list.length == 0){
                        scope.isdata = true;
                    }
                }catch(e){

                }

            } else if (data.err_code == 1000) {    // 常规错误
                scope.isdata = true;
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function (err) {
            scope.isdata = true;
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });


        // 添加到购物车
        scope.addshopcar = function(id) {

            var param = "&is_platform=1&unitary_id=" + id;
            scope.ctrlisloadshow = true;    // 开启加载中
            addshopcartservice.init(param).success(function (data) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                if (data.err_code == 20000) {    // 没有登陆
                    // 跳到登录页面
                    window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
                } else if (data.err_code == 0) {    // 正常状态
                    scope.dialogtext = "商品添加成功~";
                    scope.isdialogshow = true;
                    scope.shopcarnum = data.err_msg.count;
                } else if (data.err_code == 1000) {    // 常规错误
                    scope.dialogtext = data.err_msg;
                    scope.isdialogshow = true;
                }
            }).error(function (err) {
                scope.ctrlisloadshow = false;    // 关闭加载中
                scope.dialogtext = "网络异常";
                scope.isdialogshow = true;
            });

        }


        // 滚动事件，判断是否需要新加数据
        angular.element(document).on("scroll",function() {
           
            if(nextpage) {
                var winheight = $(window).height(),
                    nowheight = $(".load_more").offset().top,
                    scrollTop = $(window).scrollTop(),
                    allparam = param;

                    if(page) allparam += "&page=" + page;

                if(isloadmoreservice.init(nowheight, winheight, scrollTop) && nextpage) {

                    if(lock) {
                        lock = false;

                        scope.ctrlisloadshow = true;
                        searchJson.init(allparam).success(function (data) {
                            scope.ctrlisloadshow = false;
                            if(data.err_code == 20000) {    // 没有登陆
                                // 跳到登录页面
                                window.location.href= data.err_msg+"?referer="+encodeURIComponent(location.href);
                            }else if(data.err_code == 0) {    // 正常

                                if(data.err_msg.next_page) {
                                    nextpage = true;
                                    page++;
                                }else{
                                    nextpage = false;
                                    scope.isloadmore = true;
                                }
                                lock = true;

                                for(var i = 0; i < data.err_msg.list.length; i++) {
                                    scope.data.list.push(data.err_msg.list[i]);
                                }

                            }else if(data.err_code == 1000){    // 常规错误
                                scope.dialogtext = data.err_msg;
                                scope.isdialogshow = true;
                            }
                        }).error(function(err) {
                            scope.ctrlisloadshow = false;
                            scope.dialogtext = "网络异常";
                            scope.isdialogshow = true;

                        });
                    }
                }
            }
        });


        scope.searchshop = function(keytext) {


            param = "&keyword=" + encodeURIComponent(keytext),
            scope.ctrlisloadshow = true;
            searchJson.init(param).success(function (data) {

            scope.ctrlisloadshow = false;    // 关闭加载中
            if (data.err_code == 20000) {    // 没有登陆
                // 跳到登录页面
                window.location.href = data.err_msg + "?referer=" + encodeURIComponent(location.href);
            } else if (data.err_code == 0) {    // 正常状态

                page = 2;
                nextpage = false;
                lock = true;
                scope.data = data.err_msg;
                scope.shopcarnum = scope.data.count;


                if(scope.data.next_page) {
                    nextpage = true;
                }else{
                    scope.isloadmore = true;
                }

                try{
                    if(scope.data.list.length == 0){
                        scope.isdata = true;
                    }
                }catch(e){

                }

            } else if (data.err_code == 1000) {    // 常规错误
                scope.isdata = true;
                scope.dialogtext = data.err_msg;
                scope.isdialogshow = true;
            }
        }).error(function (err) {
            scope.isdata = true;
            scope.ctrlisloadshow = false;    // 关闭加载中
            scope.dialogtext = "网络异常";
            scope.isdialogshow = true;
        });
        }


}]);




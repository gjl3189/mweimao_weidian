/*
 *
 * 指令系统
 *
 *  @author 赵仁杰
 *  @since 2016-2-24 09:17
 *
 * */
"use strict";
var dire = angular.module("shop.directive",["shop.services"]);

/**
 *
 * 底部导航
 *
 * @param whoshow  第几个选项需要变成选中样式，如果不传则全部为默认状态
 *
 */

dire.directive("shopFooter",["$location",function($location) {    // 通用底部导航
    return {
        templateUrl: 'templates/public/footer.html',
        restrict: 'E',
        replace : true,
        link : function(scope, element, attrs) {

            // 第几个显示
            switch(attrs.whoshow){
                case "1":
                    scope.isshophover = true;
                    break;
                case "2":
                    scope.isshopcarhover = true;
                    break;
                case "3":
                    scope.ismehover = true;
                    break;
                default:
            }
        }
    }
}]);


/**
 *
 * 加载中效果
 *
 * @param loadisshow  是否显示，需要一个布尔值，什么都不传则默认不显示，建议传
 *
 */

dire.directive("loading",function() {    // 加载中
    return {
        templateUrl : "templates/public/loading.html",
        restrict : "EA",
        scope : {
            loadisshow : "="
        },
        replace : true,
        link : function(scope, element, attrs) {

        }
    }
});


/**
 *
 * 提示框
 *
 * @param isdialogshow  是否显示，需要一个布尔值，什么都不传则默认不显示，建议传
 * @param dialogtext  提示框中文字
 *
 */
dire.directive("dialog",["$timeout", function($timeout) {    // 加载中
    return {
        templateUrl : "templates/public/dialog.html",
        restrict : "EA",
        scope : {
            dialogtext : "@",
            isdialogshow : "="
        },
        replace : true,
        link : function(scope, element, attrs) {
            scope.$watch(attrs.isdialogshow, function(newval, oldval) {
                if(newval) {
                    $("#dialog").show();
                    scope.isshow = true;
                    $timeout(function() {
                        $("#dialog").hide();
                        scope.isdialogshow = false;
                        scope.isshow = false;
                    },3000);
                }
            });
        }

    }
}]);


/**
 *
 * 确认框
 *
 * @param isconfirmshow  是否显示，需要一个布尔值，什么都不传则默认不显示，建议传
 * @param confirmtext  确认框中文字
 * @param confirmok  点击确认事件
 *
 */
dire.directive("confirm",[function() {    // 加载中
    return {
        templateUrl : "templates/public/confirm.html",
        restrict : "EA",
        scope : {
            confirmtext : "@",
            isconfirmshow : "=",
            confirmok : "&"
        },
        replace : true,
        link : function(scope, element, attrs) {

        }
    }
}]);


/**
 *
 * 回到顶部
 *
 * @param isconfirmshow  是否显示，需要一个布尔值，什么都不传则默认不显示，建议传
 * @param confirmtext  确认框中文字
 * @param confirmok  点击确认事件
 *
 */
dire.directive("totop",[function() {
    return {
        template : '<button class="w-button w-button-round w-button-backToTop" ng-click="totop()">返回顶部</button>',
        restrict : "EA",
        scope : {
        },
        replace : true,
        link : function(scope, element, attrs) {
            scope.totop = function() {
                $('html,body').animate({scrollTop: '0px'}, 800);
            }
        }
    }
}]);


// 判断轮播循环是否结束
dire.directive('carouselRepeatFinish',["$timeout",  function($timeout){
    return {
        link: function(scope,element,attr){
            if(scope.$last == true){
                $timeout(function() {
                    var mySwiper = new Swiper('.swiper-container',{
                        pagination: '.pagination',
                        loop:true,
                        grabCursor: true,
                        paginationClickable: true
                    })
                },500);
            }
        }
    }
}]);


// 判断循环是否结束
dire.directive('repeatFinish',["$timeout", "publicmethods", function($timeout, publicmethods){
    return {
        link: function(scope,element,attr){
            if(scope.$last == true){
                $timeout(function() {
                    var timearr = [],
                        $downtime = $(".w-countdown-nums"),
                        timer = "",
                        downtimelen = $downtime.length;

                    for(var i = 0, j = downtimelen; i < j; i++) {
                        timearr.push($downtime.eq(i).attr("data-timer"));
                    }

                    function addtime() {
                        for(var i = 0; i < downtimelen; i++) {
                            var nowtimer = timearr[i];
                            if(nowtimer == 0) {
                                $downtime.eq(i).hide(0).siblings(".w-countdown-title").hide();
                                $downtime.eq(i).parents(".w-goodsList-item").find(".f-txtabb").show()
                            }else if(nowtimer > 0) {
                                --timearr[i];
                                var txtTime = publicmethods.downtime(timearr[i]);
                                $downtime.eq(i).siblings().text("倒计时");
                                $downtime.eq(i).text(txtTime);
                            }
                        }
                    }

                    addtime();

                    timer = setInterval(function() {
                        addtime();
                    }, 1000);
                },500);
            }
        }
    }
}]);


// 判断购物车循环是否结束
dire.directive('shopcarRepeatFinish',["$timeout",  function($timeout){
    return {
        link: function(scope,element,attr){
            if(scope.$last == true){
                $timeout(function() {
                    var $el= $(".u-Cart-ft"),
                        nowwidth = $el.width();
                    for(var i = 0, j = $el.length; i < j; i++) {
                        var namewidth = $el.eq(i).find(".graydelname").width(),
                            pricewidth = $el.eq(i).find(".graynodelname").width(),
                            namelen = $el.eq(i).find(".graydelname").text().length,
                            pricelen = $el.eq(i).find(".graynodelname").text().length;

                        if((namewidth + pricewidth) < nowwidth) {
                            continue;
                        }else{
                            var newtxtlen = ((nowwidth - pricelen) / 14) - 1,
                                txt = $el.eq(i).find(".graydelname").text(),
                                newtxt = txt.slice(0, newtxtlen);
                            $el.eq(i).find(".graydelname").text(newtxt + "...")
                        }

                    }
                },500);
            }
        }
    }
}]);

/**
 *
 * 头部返回工具条
 *
 * @param isconfirmshow  是否显示，需要一个布尔值，什么都不传则默认不显示，建议传
 * @param confirmtext  确认框中文字
 * @param confirmok  点击确认事件
 *
 */
dire.directive("toptitle",[function() {
    return {
        template : '<div class="m-simpleHeader"><a href="javascript:void(0);" class="m-simpleHeader-back" ng-click="goBack()"><i class="ico ico-back"></i></a><h1>{{toptitletext}}</h1></div>',
        restrict : "EA",
        scope : {
            toptitletext : "@"
        },
        replace : true,
        link : function(scope, element, attrs) {
            scope.goBack = function() {
                window.history.back();
            }
        }
    }
}]);
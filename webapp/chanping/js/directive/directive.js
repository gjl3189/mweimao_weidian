/***@author wangmu***/
/***3-3 10:30***/
var dire = angular.module("shop.directive", ["shop.services"]);
var login_html = APIURL + "wap/login.php?referer=" + APIURL + "webapp/chanping/index.html#/";
dire.directive("loading", [function() { // 加载
    return {
        templateUrl: "templates/public/loading.html",
        restrict: "E",
        link: function(scope, element, attrs) {


        }
    }
}]);
dire.directive("back", [function() { // 返回上一页
    return {
        templateUrl: "templates/public/back.html",
        restrict: "E",
        link: function(scope, element, attrs) {

        }
    }
}]);
dire.directive("backindex", [function() { // 返回首页
    return {
        templateUrl: "templates/public/back_index.html",
        restrict: "E",
        link: function(scope, element, attrs) {}
    }
}]);
dire.directive("gotop", [function() { // 回到顶部
    return {
        templateUrl: "templates/public/gotop.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.footer_show = true;
            scope.login = true;

            $(window).scroll(function() {
                var pageH = $(document.body).height();
                var scrollT = document.body.clientHeight - $(window).scrollTop() - $(window).height();
                if (scrollT < 200) {
                    $(".go2top ").addClass('show');

                } else {
                    $(".go2top ").removeClass('show');
                }

            });
            $(".go2top ").click(function() {
                $("html,body").animate({
                    scrollTop: "0px"
                }, 500);
            })

        }
    }
}]);
dire.directive("warning", [function() { // 弹出框提升信息 
    return {
        templateUrl: "templates/groupbuy/warningBox.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.time = function() {
                scope.singleShow = true;
                setTimeout(function() {
                    scope.singleShow = false;
                    scope.$apply(scope.singleShow);
                }, 500);
            }
        }
    }
}]);
dire.directive("banner", ["bannerJson", function(bannerJson) { // 轮播
    return {
        templateUrl: "templates/groupbuy/banner.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.isloading = true;
            scope.banner_show = true;
            bannerJson.detilsInfo.success(function(data) {
                scope.carouse_show = false;
                scope.banner = data;
                scope.showanimate = function(index, a, b) {
                    $(".progress_bar").eq(index).animate({
                        width: howlen(a, b)
                    }, 5000);
                }

                function howlen(a, b) {
                    return a / b * 100 + "%";
                }


            })
        }
    }
}]);

dire.directive("bannerisload", ["$timeout", function($timeout) { // 轮播控制加载
    return {
        restrict: "EA",
        link: function(scope, element, attrs) {
            if (scope.$last == true) {
                $timeout(function() {
                    // $(".swiper-slide").css({ "height": $(window).height(), "backgroundSize": "cover" });
                    var swiper1 = new Swiper('.swiper1', {
                        pagination: '.swiper-pagination1',
                        paginationClickable: true,
                        spaceBetween: 30,
                        autoplay: 2500,
                        parallax: true,
                        speed: 600,
                        /*     loop: true,*/
                    });
                }, 500);
            }
        }
    }
}]);
dire.directive("menu", ["menuJson", "$http", function(menuJson, http) { // 菜单轮播
    return {
        templateUrl: "templates/groupbuy/menu.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.isloading = true;
            menuJson.detilsInfo.success(function(data) {
                scope.menu_show = true;
                scope.menu = data;
                http({
                    method: 'GET',
                    url: APIURL + "zc.php?c=api&a=productCat"
                }).success(function(data) {
                    scope.search = data;
                })
            });

        }
    }
}]);

dire.directive("menuisload", ["$timeout", function($timeout) { // 菜单轮播控制加载
    return {
        restrict: "EA",
        link: function(scope, element, attrs) {
            if (scope.$last == true) {

                $timeout(function() {
                    var swiper2 = new Swiper('.swiper2', {
                        pagination: '.swiper-pagination2',
                        paginationClickable: true,
                        spaceBetween: 30,
                        scrollbarHide: true,
                        slidesPerView: 'auto',
                        centeredSlides: true,
                        spaceBetween: 30,
                        grabCursor: true,
                        parallax: true,
                        // effect : 'coverflow',
                        // loop : true,
                        slidesPerView: 'auto',
                        loopedSlides: 8,
                        rotate: 30,
                        stretch: 10,
                        depth: 60,
                        modifier: 2,
                        slideShadows: true,
                    });
                }, 500);
            }
        }
    }
}]);

dire.directive("listload", ["$timeout", "lazyLoadService", function($timeout, lazyLoadService) { // 首页列表图片预加载
    return {
        restrict: "EA",
        link: function(scope, element, attrs) {
            if (scope.$last == true) {
                $timeout(function() {
                    lazyLoadService.lazyLoad.init($("img"));
                }, 500);
            }
        }
    }
}]);
dire.directive("list", ["productJson", "lazyLoadService", "$http", "surplusDay", function(productJson, lazyLoadService, http, surplusDay) { // 列表
    return {
        templateUrl: "templates/groupbuy/list.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.list_show = true;
            scope.lading_show = true;

            productJson.detilsInfo.success(function(data) {
                if (data.err_code == 101) {
                    window.location = login_html;
                };
                scope.carouse_show = false;
                scope.product = data;
                scope.surplus = function(a, b) {
                    return surplusDay.surplus(a, b);
                }
                scope.status = function(a) {
                    return surplusDay.status(a);
                }
                scope.productHref = function(a) {
                    surplusDay.productHref(a);
                };
                setTimeout(function() {
                    scope.lading_show = false;
                    scope.$apply(scope.lading_show)
                }, 1000);
            })
        }
    }
}]);
dire.directive("list", ["loadJson", function(loadJson) { // 列表加载数据
    return {

        restrict: "EA",
        link: function(scope, element, attrs) {
            scope.list_show = true;
            var i = 1;
            scope.loading = function() {
             
                loadJson.init(++i).success(function(data) {
                    if (data.err_msg.next_page) {
                        for (var i = 0; i < data.err_msg.project_list.length; i++) {
                            scope.product.err_msg.project_list.push(data.err_msg.project_list[i]);
                            //console.log(data.err_msg.project_list);
                        }
                    } else {
                        scope.product.err_msg.next_page = false;
                    }
                });

            };

        }
    }
}]);
dire.directive("category", ["$http", "$routeParams", "surplusDay", function(http, $routeParams, surplusDay) { // 分类列表页面
    return {
        templateUrl: "templates/groupbuy/category.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var id = $routeParams.id;
            var categoryUrl = "";
            $(".wapper_big_class").css("min-height", $(window).height());
            http({
                method: 'GET',
                url: APIURL + "zc.php?c=api&a=productCat" //头部分类列表
            }).success(function(data) {
                scope.category = data;
                if (data.err_code == 101) {
                    window.location = login_html;
                };
            });

            scope.categoryClass = function(a) {
                return "categoryClass" + a;
            }
            setTimeout(function() {   //判断是否是单个分类
                if (/^\d+$/.test(id)) {
                   // console.log(id);
                    var aa = $(".wapper_big_class .categoryClass" + id).index();
                    //console.log(aa);
                    if (aa * 1 > 0) {
                        aa = -aa;
                    }
                    //console.log(aa);
                    $(".big_class_drag").css({
                        "transform": "translate3d(" + aa * $(window).width() + "px, 0px, 0px)"
                    });
                    categoryUrl = 'zc.php?c=api&a=product&class=' + id;;
                    http({
                        method: 'GET',
                        url: APIURL + categoryUrl
                    }).success(function(data) {
                        scope.product = data;
                        if (data.err_code == 101) {
                            window.location = login_html;
                        };
                        setTimeout(function() {
                            var height = $(".swiper-slide-active").height();
                            $(".wapper_big_class").css("height", height);
                        }, 500);
                    });
                }
            }, 500)
            categoryUrl = "zc.php?c=api&a=product";
            switch (id) {
                case "product_id":
                    categoryUrl = 'zc.php?c=api&a=product&order=product_id';
                    $(".item_card li").eq(1).addClass("curr").siblings('').removeClass("curr");
                    break;
                case "collect":
                    categoryUrl = 'zc.php?c=api&a=product&order=collect';
                    $(".item_card li").eq(2).addClass("curr").siblings('').removeClass("curr");
                    break;
                case "people_number":
                    categoryUrl = 'zc.php?c=api&a=product&order=people_number';
                    $(".item_card li").eq(3).addClass("curr").siblings('').removeClass("curr");
                    break;
                default:
                    break;
            }
            http({
                method: 'GET',
                url: APIURL + categoryUrl
            }).success(function(data) {
                scope.whole = data;
                if (data.err_code == 101) {
                    window.location = login_html;
                };
                setTimeout(function() {
                    var height = $(".swiper-slide-active").height();
                    $(".wapper_big_class").css("height", height);
                }, 500);
            });
            scope.loadShow = false;

            scope.surplus = function(a, b) {
                return surplusDay.surplus(a, b);
            };
            scope.status = function(a) {
                return surplusDay.status(a);
            };
            scope.productHref = function(a) {
                surplusDay.productHref(a);
            };

            $(".item_card li").live("click", function() {
                $(this).addClass('curr').siblings('').removeClass("curr");
            });
            scope.sort = function(a, b, $event) { //a:分类,b:最新,
                scope.loadShow = true;
                if (a == "0") {
                    categoryUrl = 'zc.php?c=api&a=product'; //全部中综合推荐
                } else if (a == undefined) {
                    categoryUrl = 'zc.php?c=api&a=product&order=' + b; //全部中 ,最新
                } else {
                    if (b == "zonghe") {
                        categoryUrl = 'zc.php?c=api&a=product&class=' + a; //分类中综合推荐
                    } else {
                        categoryUrl = 'zc.php?c=api&a=product&order=' + b + '&class=' + a; //分类中的最新分类
                    }
                }
                http({
                    method: 'GET',
                    url: APIURL + categoryUrl
                }).success(function(data) {
                    if (data.err_code == 101) {
                        window.location = login_html;
                    };
                    scope.product = data;
                    // console.log(data);
                    setTimeout(function() {
                        scope.loadShow = false;
                        scope.$apply(scope.loadShow)
                    }, 300);
                })

            }

        }
    };
}]);
dire.directive("categoryload", ["$timeout", "$http", function($timeout, http) { // 搜索加载
    return {
        restrict: "EA",
        link: function(scope, element, attrs) {
            if (scope.$last == true) {
                $timeout(function() {
                    var categoryUrl = "";
                    var swiper = new Swiper('.swiper4', {
                        paginationClickable: true,
                        // shortSwipes : false,
                        threshold: 100,
                        preventClicks: true,
                        touchMoveStopPropagation: true,
                        onSlideChangeStart: function(swiper, even) {
                            scope.loadShow = true;
                            var name = $(".swiper-slide-active").prop("className");
                            name = name.replace(/[^0-9]/ig, "");
                            if (name == "") {
                                return
                            }
                            categoryUrl = 'zc.php?c=api&a=product&class=' + name;
                            http({
                                method: 'GET',
                                url: APIURL + categoryUrl
                            }).success(function(data) {
                                if (data.err_code == 101) {
                                    window.location = login_html;
                                };
                                try {
                                    if (data.err_msg.project_list[0].productName == undefined) {
                                        scope.product = "";
                                    } else {
                                        scope.product = data;
                                    }
                                } catch (e) {
                                    scope.product = "";
                                }

                                //scope.$apply(scope.product);
                                //console.log(scope.product);
                                setTimeout(function() {
                                    var height = $(".swiper-slide-active").height();
                                    $(".wapper_big_class").css("height", height);
                                    scope.loadShow = false;
                                    scope.$apply(scope.loadShow);
                                    //  console.log(height);
                                }, 500);
                            });

                        }
                    });

                }, 500);
            }
        }
    }
}]);

dire.directive("customer", ["$http", "surplusDay", function(http, surplusDay) { // 个人中心
    return {
        templateUrl: "templates/groupbuy/customer.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.lading_show = true;

            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=myuser' //用户信息
            }).success(function(data) {
                scope.myuser = data.err_msg.user;
            });
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=myProduct' //全部支持项目
            }).success(function(data) {
                if (data.err_code == 101) {
                    window.location = login_html;
                };
                try {
                    scope.total = data.err_msg.order_list;
                    setTimeout(function() {
                        scope.lading_show = false;
                        scope.$apply(scope.lading_show)
                    }, 1000);
                    //console.log(data);
                } catch (e) {
                    scope.total = false;
                    setTimeout(function() {
                        scope.lading_show = false;
                        scope.$apply(scope.lading_show)
                    }, 1000);
                }
            });
            scope.orderShow = true;
            scope.surplus = function(a, b) {
                return surplusDay.surplus(a, b);
            };
            scope.status = function(a) {
                return surplusDay.status(a);
            };
            scope.productHref = function(a) {
                surplusDay.productHref(a);
            };
            scope.transactionType = function(a) {
                return surplusDay.transactionType(a);
            };
            scope.roger = false;
            scope.attestation_txt = "确定";
            scope.know_txt = "取消";
            scope.attes_href = "";
            scope.go_attestation = true;
            scope.prompt = true;
            scope.title_close = true;

            scope.deleteTotal = function(a, b) {
                scope.roger = true;
                var that = this;
                if (that.deleteTotalTxt == "删除") {
                    scope.roger_txt = "您删除的订单可以去【已删除】中进行找回";
                } else {
                    scope.roger_txt = "还原订单后,请至【全部】查看并继续其他操作!您是否确定选择还原订单?";
                }
                scope.adderss_chose = function() {
                   // console.log(this);
                    scope.roger = false;
                    if (that.deleteTotalTxt == "删除") {
                        http({
                            method: 'GET',
                            url: APIURL + 'zc.php?c=api&a=myProductDelete&id=' + a //删除已支付的数据
                        }).success(function(data) {
                            if (data.err_code == 0) {
                                // scope.total = data.err_msg.order_list;
                                that.deleteTotalTxt = "还原数据";
                                that.deleteTotalClass = "pay";
                                that.orderShow = "false";
                            }
                        });
                    } else {
                        http({
                            method: 'GET',
                            url: APIURL + 'zc.php?c=api&a=restoreOrder&id=' + a //还原数据
                        }).success(function(data) {
                            if (data.err_code == 0) {
                                //scope.total = data.err_msg.order_list;
                                that.deleteTotalTxt = "删除";
                                that.deleteTotalClass = "";
                            }
                        });
                    }
                    return false;
                }
            };
            scope.totalLIst = function(a) {
                scope.load_moreShow = true;
                scope.total = "";
                http({
                    method: 'GET',
                    url: APIURL + 'zc.php?c=api&a=myProduct&order_status=' + a //选择支付类型
                }).success(function(data) {
                    try {
                        if (data.err_msg.order_list != undefined) {
                            if (data.err_msg.order_list[0].productName != undefined) {
                                scope.total = data.err_msg.order_list;
                               // console.log(data);
                                scope.deleteTotalTxt = "删除";
                                scope.deleteTotalClass = "";
                                setTimeout(function() {
                                    scope.load_moreShow = false;
                                    scope.$apply(scope.load_moreShow);
                                }, 300);
                            } else {
                                scope.total = false;
                            }
                        } else {
                            scope.total = false;
                        }
                    } catch (e) {
                        scope.total = false;
                    }

                });
            }
            scope.delete = function() {
                scope.total = "";
                scope.load_moreShow = true;
                http({
                    method: 'GET',
                    url: APIURL + 'zc.php?c=api&a=myRecycle' //删除的数据
                }).success(function(data) {
                    try {
                        if (data.err_msg.myRecycleList != undefined) {
                            scope.total = data.err_msg.myRecycleList;
                            scope.deleteTotalTxt = "还原数据";
                            scope.deleteTotalClass = "pay";
                            setTimeout(function() {
                                scope.load_moreShow = false;
                                scope.$apply(scope.load_moreShow);
                            }, 300);
                        }

                    } catch (e) {

                    }

                });
            };

            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=myAddProduct' //我发起的
            }).success(function(data) {
                scope.launch = data;
                //console.log(data);
                //console.log("我发起的");
            });
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=product_my_attention' //我关注的
            }).success(function(data) {
                scope.follow = data;
                // console.log(data);
            });
            scope.deleteTotalTxt = "删除";
            scope.deleteTotalClass = "";
            scope.cancelAttentionTxt = "取消关注";
            scope.cancelAttention = function(a) {

                var that = this;
                if (that.cancelAttentionTxt == "取消关注") {
                    http({
                        method: 'GET',
                        url: APIURL + 'zc.php?c=api&a=cancelAttention&product_id=' + a //取消关注'
                    }).success(function(data) {
                        if (data.err_code == 1) {
                            that.cancelAttentionTxt = "添加关注";
                        }
                    });
                } else {
                   // console.log(a);
                    http({
                        method: 'GET',
                        url: APIURL + 'zc.php?c=api&a=product_attention&id=' + a //添加关注'
                    }).success(function(data) {
                        //console.log(data);
                        that.cancelAttentionTxt = "取消关注";

                    });
                }
            };
            $("p.card_sub a").click(function() {
                $(this).addClass("current").siblings('').removeClass("current");
            })
            switching(".card_wrapper   li", ".cardcon >section", "active"); //页面切换
            function switching(a, b, c) {
                var i = 0;
                $(b).eq(0).show();
                var len = $(a).length - 1;
                $(b).css("width", $(window).width());

                function zhixing(index) {
                    $(a).eq(index).addClass(c).siblings().removeClass(c);
                    $(b).eq(index).animate({
                        "marginLeft": 0,
                        "display": "block"
                    }, 200).show().siblings().animate({
                        "marginLeft": "100%",
                        "display": "none"
                    }, 200).hide();
                }
                $(a).click(function() {
                    var index = $(this).index();
                    zhixing(index);
                });
            };

        }
    }
}]);
dire.directive("order", ["$http", "$routeParams", "surplusDay", function(http, $routeParams, surplusDay) { // 个人中心订单详情页面
    return {
        templateUrl: "templates/groupbuy/order.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var id = $routeParams.id
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=orderDetails&id=' + id
            }).success(function(data) {
                if (data.err_code == 101) {

                    window.location = login_html;

                };
                //console.log(data);
                scope.order = data.err_msg;
                scope.deleteTotalTxt = "删除";
                scope.transactionType = function(a) {
                    return surplusDay.transactionType(a);
                };
                scope.deleteTotal = function(a, b) {
                    surplusDay.deleteTotal(a, b);
                };
                scope.roger = false;
                scope.attestation_txt = "确定";
                scope.know_txt = "取消";
                scope.attes_href = "";
                scope.go_attestation = true;
                scope.prompt = true;
                scope.title_close = true;

                scope.deleteTotal = function(a, b) {
                    scope.roger = true;
                    var that = this;
                    if (that.deleteTotalTxt == "删除") {
                        scope.roger_txt = "您删除的订单可以去【已删除】中进行找回";
                    } else {
                        scope.roger_txt = "还原订单后,请至【全部】查看并继续其他操作!您是否确定选择还原订单?";
                    }
                    scope.adderss_chose = function() {

                        scope.roger = false;
                        if (that.deleteTotalTxt == "删除") {
                            http({
                                method: 'GET',
                                url: APIURL + 'zc.php?c=api&a=myProductDelete&id=' + a //删除已支付的数据
                            }).success(function(data) {
                                if (data.err_code == 0) {
                                    // scope.total = data.err_msg.order_list;
                                    that.deleteTotalTxt = "还原数据";
                                    that.deleteTotalClass = "pay";
                                    that.orderShow = "false";
                                }
                              //  console.log(data);
                            });
                        } else {
                            http({
                                method: 'GET',
                                url: APIURL + 'zc.php?c=api&a=restoreOrder&id=' + a //还原数据
                            }).success(function(data) {
                                if (data.err_code == 0) {
                                    //scope.total = data.err_msg.order_list;
                                    that.deleteTotalTxt = "删除";
                                    that.deleteTotalClass = "";
                                }
                                //console.log(data);
                            });
                        }
                        return false;
                    }
                };

            });
        }
    }
}]);

dire.directive("search", ["$http", "$rootScope", "surplusDay", function(http, $rootScope, surplusDay) { // 搜索
    return {
        templateUrl: "templates/public/search.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.noMore = false;
            scope.searchSubmit = function() {
                if (scope.searchInput == undefined) {
                    scope.noMore = true;
                    scope.searchResult = "!请输入关键字搜索";
                } else {
                    var transFn = function(data) {
                            return $.param(data);
                        },
                        postCfg = {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            },
                            transformRequest: transFn
                        };

                    var data = {
                        search: scope.searchInput
                    }
                    http.post(APIURL + 'zc.php?c=api&a=searchProduct', data, postCfg).success(function(data) {
                        if (data.err_code == 0) {
                            $rootScope.searchInput = scope.searchInput;
                            window.location = "#/search/" + scope.searchInput;
                        } else {
                            scope.noMore = true;
                            scope.searchResult = "!搜索不存在";
                        }
                    })
                }
            };
            scope.surplus = function(a, b) {
                return surplusDay.surplus(a, b);
            }

            scope.menu_secrch = function() {
                scope.wapper = "show";
                scope.secrch_show = "sh show";
            };
            scope.secrch_close = function() {
                scope.wapper = "";
                scope.secrch_show = "";
            };
            scope.menu_status = function() {
                scope.wapper = "show";
                scope.status_show = "sh show";

            };
            scope.status_hide = function() {
                scope.wapper = "";
                scope.status_show = "";
            };


        }
    }
}]);
dire.directive("searchcent", ["$http", "$rootScope", "$routeParams", "surplusDay", function(http, $rootScope, $routeParams, surplusDay) { // 搜索详情页面
    return {
        templateUrl: "templates/groupbuy/search.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var transFn = function(data) {
                    return $.param(data);
                },
                postCfg = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    transformRequest: transFn
                };
            var data = {
                search: $rootScope.searchInput
            }
            scope.searchSubmit = function() {
                if (scope.searchInput == undefined) {
                    scope.noMore = true;
                    scope.searchResult = "!请输入关键字搜索";
                } else {
                    var data = {
                        search: scope.searchInput
                    }
                    http.post(APIURL + 'zc.php?c=api&a=searchProduct', data, postCfg).success(function(data) {

                        if (data.err_code != 0) {
                            scope.noMore = true;
                            scope.searchResult = "!请输入正确关键字搜索";
                        } else {
                            scope.noMore = false;
                            scope.product = data;
                            scope.dataNum = scope.product.err_msg.project_list.length;
                        }
                    })
                }
            }

            http.post(APIURL + 'zc.php?c=api&a=searchProduct', data, postCfg).success(function(data) {
                scope.searchInput = $rootScope.searchInput;
                if (data.err_code != 0) {
                    scope.dataNum = 0;
                } else {
                    scope.product = data;
                    //console.log(data)
                    scope.dataNum = scope.product.err_msg.project_list.length;
                }
            });
            scope.surplus = function(a, b) {
                return surplusDay.surplus(a, b);
            };
            scope.status = function(a) {
                return surplusDay.status(a);
            };
            scope.productHref = function(a) {
                surplusDay.productHref(a);
            };
            var i = 1;

            scope.loadingSearch = function() {
                var data = {
                    search: scope.searchInput,
                    page: i++
                };
                //console.log(data);
                http.post(APIURL + 'zc.php?c=api&a=searchProduct', data, postCfg).success(function(data) {
                    if (data.err_code != 0) {
                        scope.product.err_msg.next_page = false;
                        return
                    }
                    if (data.err_msg.next_page) {
                        for (var i = 0; i < data.err_msg.project_list.length; i++) {
                            scope.product.err_msg.project_list.push(data.err_msg.project_list[i]);

                        }
                    }
                })
            }
        }
    }
}]);
dire.directive("hand", ["$http", function(http) { // 点赞
    return {
        templateUrl: "templates/public/hand.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.clickdianzan = function(a) {
                var that = this;
                if (that.dianzhan != "zanheart") {
                    http({
                        method: 'GET',
                        url: APIURL + 'zc.php?c=api&a=product_dianzan&id=' + a
                    }).success(function(data) {
                        that.dianzhan = "zanheart";
                       // console.log(data)
                    })
                }
                return false;

            }
        }
    }
}]);
dire.directive("searchisload", ["$timeout", function($timeout) { // 搜索加载
    return {
        restrict: "EA",
        link: function(scope, element, attrs) {
            if (scope.$last == true) {
                $timeout(function() {
                    var width = $(window).width() * 0.45;
                    var swiper = new Swiper('.swiper3', {
                        pagination: '.swiper-pagination3',
                        paginationClickable: true,
                        scrollbarHide: true,
                        slidesPerView: 'auto',
                        centeredSlides: true,
                        grabCursor: true,
                        parallax: true,

                        loopedSlides: 8,
                        rotate: 30,
                        stretch: 10,
                        depth: 60,
                        modifier: 2,
                        slideShadows: true,

                    });
                }, 500);
            }
        }
    }
}]);
dire.directive("project", ["$routeParams", "$http", function($routeParams, http) { // 项目进展
    return {
        templateUrl: "templates/groupbuy/project.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=productDetails&id=' + $routeParams.id
            }).success(function(data) {
                if (data.err_code == 101) {
                    window.location = login_html;
                };
                //console.log(data.err_msg);
                scope.project = data;

                function getLocalTime(nS) {
                    return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/, ' ');
                }
                scope.projectTime = function(a) {
                    return getLocalTime(a);
                }
            });
        }
    }
}]);
dire.directive("topic", ["$routeParams", "$http", function($routeParams, http) { // 话题列表
    return {
        templateUrl: "templates/groupbuy/topic.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=productDetails&id=' + $routeParams.id
            }).success(function(data) {
                scope.topic = data.err_msg;
                if (data.err_code != 0) {
                    window.location = login_html;
                }

                function getLocalTime(nS) {
                    return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/, ' ');
                }
                scope.topicTime = function(a) {
                    return getLocalTime(a);
                }
                var transFn = function(data) {
                        return $.param(data);
                    },
                    postCfg = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        transformRequest: transFn
                    };

                var data = {};
                var topicId = "";
                scope.comment = function(a) {
                    scope.commentShow = true;
                    topicId = a;
                };
               // console.log(document.body.clientHeight);
                scope.commentSubmit = function() {
                    if (topicId == undefined) {
                        data = {
                            content: scope.commentTxt,
                            product_id: scope.topic.productInfo.product_id,
                            topicId: topicId //puid:
                        }
                    } else {
                        data = {
                            content: scope.commentTxt,
                            product_id: scope.topic.productInfo.product_id
                        }
                    }
                    if (scope.commentTxt == undefined) {
                        scope.singleTxt = "请填写回复内容!";
                        scope.time();
                    } else {
                        http.post(APIURL + 'zc.php?c=api&a=topicAdd', data, postCfg).success(function(data) {
                           // console.log(data);
                            scope.commentShow = false;
                            scope.topic.topicList.push(data.err_msg.topicInfo);
                            //console.log(document.body.scrollHeight);
                            $("html,body").animate({
                                scrollTop: document.body.scrollHeight * 1000
                            }, 2000);
                            scope.commentTxt = "";
                        })
                    }

                }


            })

        }
    }
}]);
dire.directive("product", ["$http", "$routeParams", '$sce', "surplusDay", function(http, $routeParams, $sce, surplusDay) { // 项目详情页面
    return {
        templateUrl: "templates/groupbuy/product.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.lading_show = true;
            var id = $routeParams.id;
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=productDetails&id=' + id
            }).success(function(data) {
                try {
                    scope.detail = data.err_msg;
                    //console.log(data);
                    if (data.err_code == 101) {
                        window.location = login_html;
                    };
                    setTimeout(function() {
                        scope.lading_show = false;
                        scope.$apply(scope.lading_show);
                    }, 500);
                    scope.support = "我要支持";
                    //console.log(data);
                    scope.detailtt = $sce.trustAsHtml(scope.detail.productInfo.productSummary);
                    scope.submitBut = function(a) {
                        switch (a) {
                            case "0":
                                scope.support = "项目草稿中";
                                return "submitBut";
                                break;
                            case '1':
                                scope.support = "项目申请中";
                                return "submitBut";
                                break;
                            case "2":
                                scope.support = "预热中";
                                return "submitBut";
                                break;
                            case "3":
                                scope.support = "审核拒绝";
                                return "submitBut";
                                break;
                            case "4":
                                //  scope.support = "我要支持";
                                break;
                            case "6":
                                scope.support = "融资成功";
                                return "submitBut";
                                break;
                        }
                    };
                    scope.endTime = function(a, b) {
                        if (a * 1 == 0 || b * 1 == 0) {
                            return "不限时间";
                        }
                        var time = (parseInt(a) - parseInt(b) * 86400);
                        return time * 1000;
                    }
                    scope.zan = function(a) {
                        if (scope.xin != "zan") {
                            http({
                                method: 'GET',
                                url: APIURL + 'zc.php?c=api&a=product_attention&id=' + a
                            }).success(function(data) {
                                scope.xin = "zan";
                                //console.log(data);
                            });
                        } else {
                            scope.singleTxt = "已关注";
                            scope.time();
                        }
                    }
                } catch (e) {

                }
            });
        }
    }
}]);

dire.directive("single", ["$routeParams", "$http", "surplusDay", function($routeParams, http, surplusDay) { // 详情页面
    return {
        templateUrl: "templates/groupbuy/single.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var id = $routeParams.id;
            scope.lading_show = true;
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=productDetails&id=' + id
            }).success(function(data) {
                scope.detail = data.err_msg;
                setTimeout(function() {
                    scope.lading_show = true;
                    scope.$apply(scope.lading_show);
                }, 500);
                //console.log(data);
                if (data.err_code != 0) {
                    window.location = login_html;
                }
                scope.message = "展开";
                scope.jiantou = "﹀";
                scope.huibao = false;
                scope.support = "我要支持";
                scope.huibao = function() {
                    if (scope.huibao == false) {
                        scope.message = "展开";
                        scope.jiantou = "﹀";
                        scope.huibao = true;

                    } else {
                        scope.message = "收起";
                        scope.jiantou = "﹀";
                        scope.huibao = false;
                    }
                };
                scope.surplus = function(a, b) {
                    var day = Date.parse(new Date());
                    // var day = new Date(time) / 1000; 
                    if (a != "") {
                        var over_day = (parseInt(a) + (parseInt(b) * 86400) - (day / 1000)) / 86400;
                        return parseInt(over_day);
                    } else {
                        return "不限";
                    }

                }
                scope.wushi = function(a) {
                    switch (a) {
                        case "0":
                            return false;
                            break;
                        case "1":
                            return true;
                            break;

                    }
                }
                scope.project_time = function(a) {
                    if (a != "") {
                        return false;
                    } else {
                        return true;
                    }
                }
                scope.huati = function(a) {
                    if (a != "") {
                        return true;
                    }
                    if (a == undefined) {
                        return false;
                    } else {
                        return false;
                    }
                }

                function getLocalTime(nS) {
                    return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/, ' ');
                }
                scope.projectTime = function(a) {}
                scope.endTime = function(a, b) {
                    var time = ((parseInt(a)*1000) + (parseInt(b) * 86400));
                    var tt = parseInt(getLocalTime(time));
                    return time;
                }

                $("span.zhankai").on("click", function() {
                    if ($(this).children('i').text() == "收起") {
                        $(this).children('i').text("展开");
                        scope.jiantou = "﹀";
                        $(this).parent().next(".huibao").hide();
                    } else {
                        $(this).children('i').text("收起");
                        scope.jiantou = "︿";
                        $(this).parent().next(".huibao").show();
                    }
                });
                scope.singleShow = false;
                //scope.support = "我要支持";
                scope.submitBut = function(a) {

                    switch (a) {
                        case "0":
                            scope.support = "项目草稿中";
                            return "submitBut";
                            break;
                        case '1':
                            scope.support = "项目申请中";
                            return "submitBut";
                            break;
                        case "2":
                            scope.support = "预热中";
                            return "submitBut";
                            break;
                        case "3":
                            scope.support = "审核拒绝";
                            return "submitBut";
                            break;
                        case "4":
                            //  scope.support = "我要支持";
                            break;
                        case "6":
                            scope.support = "融资成功";
                            return "submitBut";
                            break;
                    }
                };

                scope.repay_id = "";
                scope.isSelfless = "";
                scope.repayShow = function(a, b) {
                    if (a * 1 == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }
                var that = "";
                scope.supportPop = function(a, b, c, d, e) {
                    /*
                    a:repay.limits:限定名额,
                    b:repay.collect_nub:当前已筹集人数,
                    c:repay.amount:支持金额,
                    d:repay.repay_id:支持id,
                    e:repay.isSelfless:是否为无私奉献
                    */
                    try {
                        if (that != this) {
                            that.xuanzeCurr = "";
                            that.selectCurr = ""
                            this.xuanzeCurr = "xuanze";
                            this.selectCurr = "curr";
                            if (e != "0") {
                                scope.isSelfless = true;
                                scope.support = "我要支持";
                                scope.repay_id = d;
                            } else if (a * 1 <= b * 1 && a * 1 != 0) {
                                scope.singleTxt = "人数已满";
                                scope.time();
                            } else {
                                scope.support = "支持:￥" + c;
                                scope.repay_id = d;
                            }
                        } else {
                            return
                        }
                    } catch (e) {
                        that = this;
                    }
                    that = this;

                };
                var top = $(".list_box").offset().top;
                scope.support_click = function(a) {

                    switch (a) {
                        case "4":
                            if (scope.isSelfless != true) {
                                if (scope.support == "我要支持") {
                                    var scrollTop = $(window).scrollTop;
                                    scope.singleTxt = "请选择档位";
                                    scope.time();
                                    $("html,body").animate({
                                        scrollTop: top
                                    }, 100);
                                } else {
                                    window.location = "#/product/" + data.err_msg.productInfo.product_id + "/" + scope.repay_id;
                                };
                            } else {
                                window.location = "#/product/" + data.err_msg.productInfo.product_id + "/" + scope.repay_id;
                            }
                            break;
                    }


                };
                scope.clickdianzan = function(a) {
                    //console.log(a);
                    var that = this;
                    if (that.dianzhan != "zanheart") {
                        http({
                            method: 'GET',
                            url: APIURL + 'zc.php?c=api&a=product_dianzan&id=' + a
                        }).success(function(data) {
                            that.dianzhan = "zanheart";
                            scope.detail.productInfo.praise = data.praise;
                            //console.log(data)
                        })
                    }

                }
                scope.zan = function(a) {
                    if (scope.xin != "zan") {
                        http({
                            method: 'GET',
                            url: APIURL + 'zc.php?c=api&a=product_attention&id=' + a
                        }).success(function(data) {
                            scope.xin = "zan";
                            //console.log(data);
                        });
                    } else {
                        scope.singleTxt = "已关注";
                        scope.time();

                    }
                }

                scope.share = function() {
                    if (scope.shareClass == "show") {
                        scope.shareClass = "";
                    } else {
                        scope.shareClass = "show";
                    }

                    window._bd_share_config = {
                        "common": {
                            "bdSnsKey": {},
                            "bdText": "",
                            "bdMini": "2",
                            "bdMiniList": false,
                            "bdPic": "",
                            "bdStyle": "0",
                            "bdSize": "16"
                        },
                        "share": {}
                    };
                    with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];
                };
                scope.lading_show = true;

                setTimeout(function() {
                    scope.lading_show = false;
                    scope.$apply(scope.lading_show)
                }, 1000);
            })
        }
    }
}]);


dire.directive("guess", ["$http", function(http) { // 猜你喜欢
    return {
        templateUrl: "templates/groupbuy/guess.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=like'
            }).success(function(data) {
                scope.guesss = data.err_msg;

            });
        }
    }
}]);

dire.directive("paymentorder", ["$routeParams", "$http", function($routeParams, http) { // 支付订单
    return {
        templateUrl: "templates/groupbuy/payment_order.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var transFn = function(data) {
                    return $.param(data);
                },
                postCfg = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    transformRequest: transFn
                };
            var data = {}
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=buyAdd&product_id=' + $routeParams.id + '&repay_id=' + $routeParams.money
            }).success(function(data) {
                if (data.err_code != 0) {
                    window.location = login_html;
                }
                scope.orderRemark = "";
                //console.log(data);
                scope.payment = data.err_msg;
                scope.is_amount = "自定义";
                scope.custom = 0;
                scope.selectFocus = function() {
                    scope.custom = 0;
                };
                scope.should = function(a, b) {
                    return parseInt(a) + parseInt(b);
                };

                scope.customInput = function() {
                    scope.is_amount = "自定义";
                    scope.custom = "";
                }

                scope.customInputBlur = function() {

                    if (scope.custom == "") {
                        scope.custom = 0;
                    } else {
                        var num = parseInt(scope.custom);
                        $(".jssprice").val(num);
                        scope.custom = num;
                        //console.log(scope.custom);


                    }
                };
                scope.isSelfless = function(a) {
                    switch (a) {
                        case "0":
                            return false;
                            break;
                        case "1":
                            return true;
                            break;
                    }
                }
                scope.paymentOrder = function(a) {
                    if (a == "0") {
                        data = {
                            orderRemark: scope.orderRemark,
                            repay_id: $routeParams.money,
                            product_id: $routeParams.id,
                            userAddressId: scope.payment.getAddressDefault.address_id
                        }

                    } else {
                        if (scope.is_amount == "自定义" && scope.custom == 0) {
                            scope.singleTxt = "请选择金额";
                            scope.time();
                            return;
                        } else {
                            if (scope.custom == 0) {
                                scope.pay_money = scope.is_amount;
                            } else {
                                scope.pay_money = scope.custom;
                            }

                        }
                        data = {
                            orderRemark: scope.orderRemark,
                            repay_id: $routeParams.money,
                            product_id: $routeParams.id,
                            userAddressId: scope.payment.getAddressDefault.address_id,
                            pay_money: parseInt(scope.pay_money)
                        }


                    }

                    http.post(APIURL + 'zc.php?c=api&a=buyAddData', data, postCfg).success(function(data) {

                        var order_location = APIURL + 'wap/zc_order.php?id=' + data.err_msg.order_info.order_no;
                        window.location = order_location;

                        var checkStatus = function() {
                            var order_no = data.err_msg.order_info.order_no;

                            var url = APIURL + "zc.php?c=api&a=checkOrder&order_no=" + order_no;
                            $.get(url, function(sta) {
                                //console.log(sta);
                                var json = eval('(' + sta + ')');
                                if (json.err_code == 0) {
                                    window.location = "#/success/" + order_no;
                                    clearInterval(timeout);
                                }
                            });
                        };
                        var timeout = setInterval(checkStatus, '5000');
                    });
                };

            })
        }
    }
}]);
dire.directive("address", ["$routeParams", '$http', function($routeParams, http) { // 切换地址
    return {
        templateUrl: "templates/groupbuy/address.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var id = $routeParams.id;
            var type = $routeParams.type;
            http({
                method: 'GET',
                url: APIURL + 'zc.php?c=api&a=addressList'
            }).success(function(data) {
                if (data.err_code != 0) {
                    window.location = login_html;
                }
				scope.addressHref="#/product/"+$routeParams.id+"/"+$routeParams.money;
				//console.log($routeParams.product_id);
				//console.log(scope.addressHref);
                scope.address = data;
                scope.roger = false;
                scope.title_icon = true;
                scope.title_close = true;
                scope.prompt = true;
                scope.go_attestation = true;
                scope.attestation_txt = "确定";
                scope.know_txt = "取消";
                scope.default = function(e) {
                    switch (e) {
                        case "0":
                            return "";
                            break;
                        case "1":
                            return "act";
                            break;
                    }
                };
                scope.select_address = function(a) {
                    scope.roger = true;
                    scope.roger_txt = "你确定要选择这个收货地址么!";
                    scope.adderss_chose = function() {
                        var data = {
                            address_id: a
                        }
                        var transFn = function(data) {
                                return $.param(data);
                            },
                            postCfg = {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                },
                                transformRequest: transFn
                            };

                        http.post(APIURL + 'zc.php?c=api&a=selectAddress', data, postCfg).success(function(data) {
                            if (data.err_code == 0) {
                                window.location = "#/product/" + $routeParams.id + "/" + $routeParams.money;
                            }

                        });
                    }

                };

            });
        }
    };
}]);

dire.directive("roger", [function() { // 银行卡
    return {
        templateUrl: "templates/public/roger.html",
        restrict: "E",
        link: function(scope, element, attrs) {


        }
    }

}]);

dire.directive("result", ["bankJson", function(bankJson) { // 成功确认页面
    return {
        templateUrl: "templates/groupbuy/result.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            bankJson.detilsInfo.success(function(data) {

                if (data.code == 0) {
                    scope.payment = data;

                } else {
                    scope.footer_show = false;
                    scope.footer_txt = "你的数据还在家睡觉,快叫醒他吧!";
                }
            })

        }
    }

}]);
dire.directive("successtop", ["$routeParams", function($routeParams) { // 付款成功头部信息
    return {
        templateUrl: "templates/groupbuy/success_info.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            scope.orderNo = $routeParams.id;
            //console.log(scope.orderNo);
        }
    }

}]);

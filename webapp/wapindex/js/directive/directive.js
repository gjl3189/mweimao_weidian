/***@author wangmu***/
/***5-10 10:30***/
var dire = angular.module("shop.directive", ["shop.services"]);
var login_html = APIURL + "wap/login.php?referer=" + encodeURI(location.href);
var loginUrl = function(a) {
    return window.location.href = a + "?referer=" + encodeURIComponent(location.href);
}

function getProvinceList() {
    var html = '<li data-id="0" class="active">不限</li>';
    for (var key in __alldiv) {
        if (key.indexOf('0000') == 2 || key == 500000) {
            html += '<li data-id="' + key + '">' + __alldiv[key][0] + '</li>';
        }
    }
    $(".prov").html(html);
}

function getCityList(pid) {
    var html = '<li data-id="0">不限</li>';
    for (var key in __alldiv) {
        if (pid == __alldiv[key][1]) {
            html += '<li data-id="' + key + '">' + __alldiv[key][0] + '</li>';
        }
    }
    $(".city").html(html);
}

function getAreaList(cid) {
    var html = '<li data-id="0">不限</li>';
    for (var key in __alldiv) {
        if (cid == __alldiv[key][1]) {
            html += '<li data-id="' + key + '">' + __alldiv[key][0] + '</li>';
        }
    }
    $(".dist").html(html);
}

dire.directive("shop", ["$routeParams", '$sce', "$http", "surplusDay", function($routeParams, $sce, http, surplusDay) { // 店铺主页搜索页面
    return {
        templateUrl: "templates/groupbuy/shop.html",
        restrict: "E",
        link: function(scope, element, attrs) {
            var product_id = $routeParams.product_id;
            var url = 'app.php?c=store&a=search';
            http({
                method: 'GET',
                url: APIURL + url //默认列表页
            }).success(function(data) {
                if (data.err_code == 20000) {
                    window.location = login_html;
                }
                scope.shopIndex = data.err_msg;
                scope.loadShow = true;
                scope.shopMoreText = "上滑加载更多~~~";
                var page = 1;
                var flge = true;
                scope.searcShow = function(a) { //判断查找的数据是否为空
                    if (a == undefined || a == "") {
                        return true;
                    } else {
                        return false;
                    }
                };
                $(document).on("click", ".shopSearch span:nth-child(2) b", function() { //通过关键词搜索
                    if (scope.searchName == 0 || scope.searchName == undefined) {
                        console.log(scope.searchName);
                        scope.searchShopShow = true;
                        scope.searchShopTxt = "亲输入关键字搜索";
                        scope.$apply(scope.searchShopShow);
                        // scope.$apply(scope.searchShopTxt);
                        console.log(scope.searchShopTxt);
                        setTimeout(function() {
                            scope.searchShopShow = false;
                            scope.$apply(scope.searchShopShow);
                        }, 500);
                    } else {
                        url = 'app.php?c=store&a=search&name=' + scope.searchName;
                        searchData();

                    }

                });
                getProvinceList();
                $(document).on("click", ".prov>li", function() { //获得省份列表
                    var pid = $(this).data("id");
                    if (pid == "0") {
                        $(".city").html("");
                        $(".dist").html("");
                    } else {
                        getCityList(pid);
                        $(".dist").html("");
                    }

                    $(this).siblings("li").removeClass("active");
                    $(this).addClass("active");
                });

                $(document).on("click", ".city>li", function() { //获得市级列表
                    var cid = $(this).data("id");
                    if (cid == "0") {
                        $(".dist").html("");
                    } else {
                        getAreaList(cid);
                    }
                    $(this).siblings("li").removeClass("active");
                    $(this).addClass("active");
                });

                $(document).on("click", ".oneCategory>li", function() { //通过父分类查找列表
                    if ($(this).text() == "不限") {
                        searchUrl();
                    }

                });
                $(document).on("click", ".twoCategory .twoCategoryList li", function() { //通过子分类查找列表
                    searchUrl();
                });
                $(document).on("click", ".dist li", function() { //区县查找
                    searchUrl();
                });
                $(document).on("click", ".city li", function() { //城市查找
                    if ($(this).text() == "不限") {
                        searchUrl();
                    }
                });
                $(document).on("click", ".prov li", function() { //省份查找
                    if ($(this).text() == "不限") {
                        searchUrl();
                    }
                });
                $(document).on("click", ".ranking li", function() { //智能排序
                    searchUrl();
                });
                $(document).on("click", ".shopCenter .shopLabel li", function() { //特色标签排序
                    searchUrl();

                });

                function searchUrl() {
                    scope.loadShow = false;
                    var sort = "&sort=" + $(".ranking").find(".active").data("value"); //智能排序
                    var cfid = $(".oneCategory").find(".active").val(); //一级分类
                    var fid = $(".twoCategoryList").find(".active").val(); //二级分类
                    var area_id = $(".dist").find(".active").data("id"); //区县
                    var city_id = $(".city").find(".active").data("id"); //城市
                    var province_id = $(".prov").find(".active").data("id"); //省份
                    var tag_id = "&tag_id=" + $(".shopLabel").find(".active").val(); //特色分类
                    var address = "";
                    var category = "";
                    if (area_id == 0) { //区县不限
                        address = "&city_id=" + city_id;
                    } else if (area_id != undefined) {
                        address = "&area_id=" + area_id;
                    };
                    //                    console.log(address);
                    if (city_id == 0) { //城市不限
                        address = "&province_id=" + province_id;
                    };
                    if (province_id == 0) { //省份不限
                        address = "";
                    };

                    if (fid == 0) { //二级分类不限
                        category = "&cfid=" + cfid;
                    } else if (fid != undefined) {
                        category = "&fid=" + fid;
                    };
                    if (cfid == 0) { //一级分类不限
                        category = "";
                    }
                    url = "app.php?c=store&a=search" + address + category + sort + tag_id;
                    //console.log(url);
                    searchData();
                    timeout();
                }

                function searchData() { //查询列表
                    http({
                        method: 'GET',
                        url: APIURL + url // 
                    }).success(function(data) {
                        if (data.err_code == 20000) {
                            window.location = login_html;
                        };
                        flge = true;
                        page = 1;
                        flge = true;
                        try {
                            scope.shopIndex.store_list = data.err_msg.store_list;

                            if (scope.searcShow(scope.shopIndex.store_list)) {
                                scope.shopMoreText = "没有查询到店铺!";
                            } else {
                                scope.shopMoreText = "查询到的店铺!";
                            };
                        } catch (e) {
                            scope.shopMoreText = "没有查询到店铺!";
                        }
                        setTimeout(function() {
                            scope.loadShow = true;
                            scope.$apply(scope.loadShow);
                        }, 600);
                    });
                }

                angular.element(document).on("scroll", function() { // 滚动加载数据
                    var hi = $(".shopMore").offset().top;
                    var window_hight = $(window).height() + $(window).scrollTop();
                    if (hi <= window_hight + 20) {
                        // surplusDay.loadJson(".shopMore", scope.shopIndex.store_list, store_list, url, showPage); //默认加载数据
                        if (flge) { //判断是否有下一页
                            surplusDay.init(url, ++page).success(function(data) {
                                if (data.err_msg.next_page) {
                                    for (var i = 0; i < data.err_msg.store_list.length; i++) {
                                        scope.shopIndex.store_list.push(data.err_msg.store_list[i]);
                                    }
                                } else {
                                    scope.shopMoreText = "没有更多数据了!";
                                    return flge = false;
                                }
                            });
                        } else {

                        }
                    }
                });
                /*******************/
            });

        }
    }
}]);

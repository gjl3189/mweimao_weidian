/*
 Ajax 三级省市联动
 http://code.ciaoca.cn/
 日期：2012-7-18
 
 settings 参数说明
 -----
 url:省市数据josn文件路径
 prov:默认省份
 city:默认城市
 dist:默认地区（县）
 nodata:无数据状态
 required:必选项
 ------------------------------ */
(function($) {
    try {
        $.fn.citySelect = function(settings) {
            if (this.length < 1) {
                return;
            };

            // 默认值
            settings = $.extend({
                url: "js/city.min.js",
                prov: null,
                city: null,
                dist: null,
                nodata: null,
                required: true
            }, settings);

            var box_obj = this;
            var prov_obj = box_obj.find(".prov");
            var city_obj = box_obj.find(".city");
            var dist_obj = box_obj.find(".dist");
            var prov_val = settings.prov;
            var city_val = settings.city;
            var dist_val = settings.dist;
            var select_prehtml = (settings.required) ? "" : "<option value=''>请选择</option>";
            var city_json;

            // 赋值市级函数
            var prov_id = 0;
            var cityStart = function(a) {
                prov_id = a;

                if (!settings.required) {
                    prov_id--;
                };
                city_obj.empty().attr("disabled", true);
                dist_obj.empty().attr("disabled", true);

                if (prov_id < 0 || typeof(city_json.citylist[prov_id].c) == "undefined") {
                    if (settings.nodata == "none") {
                        city_obj.css("display", "none");
                        dist_obj.css("display", "none");
                    } else if (settings.nodata == "hidden") {
                        city_obj.css("visibility", "hidden");
                        dist_obj.css("visibility", "hidden");
                    };
                    return;
                };

                // 遍历赋值市级下拉列表
                temp_html = select_prehtml;
                $.each(city_json.citylist[prov_id].c, function(i, city) {
                    temp_html += "<li value='" + city.n + "'>" + city.n + "</li>";
                });
                city_obj.html(temp_html).attr("disabled", false).css({ "display": "", "visibility": "" });
                distStart();
            };

            // 赋值地区（县）函数
            var distStart = function(a) {
                var prov_id = prov_obj.find("li.active").index();
                if (a == undefined) {
                    a = 1;
                }
                var city_id = a;
                if (!settings.required) {
                    prov_id--;
                    city_id--;
                };
                dist_obj.empty().attr("disabled", true);
                if (prov_id < 0 || city_id < 0 || typeof(city_json.citylist[prov_id].c[city_id].a) == "undefined") {

                    var provAddress = $(".provinceAddress").find(".active").text();
                    var cityAddress = $(".cityAddress").find(".active").text();
                    if (provAddress == "") {
                        provAddress = city_json.citylist[0].p;
                        if (cityAddress == "") {
                            cityAddress = city_json.citylist[0].c[0].n;
                        }
                    }
                    if (cityAddress == "") {
                        cityAddress = city_json.citylist[prov_id].c[0].n;
                    }
                    var address = provAddress + cityAddress;
                    console.log(address);
                    if (settings.nodata == "none") {
                        dist_obj.css("display", "none");
                    } else if (settings.nodata == "hidden") {
                        dist_obj.css("visibility", "hidden");
                    };
                    return;
                };

                // 遍历赋值市级下拉列表
                temp_html = select_prehtml;
                $.each(city_json.citylist[prov_id].c[city_id].a, function(i, dist) {
                    temp_html += "<li value='" + dist.s + "'>" + dist.s + "</li>";
                });
                dist_obj.html(temp_html).attr("disabled", false).css({ "display": "", "visibility": "" });
            };

            var init = function() {
                // 遍历赋值省份下拉列表
                temp_html = select_prehtml;
                $.each(city_json.citylist, function(i, prov) {
                    temp_html += "<li value='" + prov.p + "'>" + prov.p + "</li>";
                });
                prov_obj.html(temp_html); // 选择省份时发生事件   
                cityStart(0);
                distStart(0);
                prov_obj.find("li").eq(0).addClass("active");
                city_obj.find("li").eq(0).addClass("active");
                //dist_obj.find("li").eq(0).addClass("active");

                prov_obj.find("li").bind("click", function() {
                    $(this).addClass("active").siblings("").removeClass("active");
                    index = $(this).index();
                    cityStart(index);
                    city_obj.find("li").eq(0).addClass("active");
                    //dist_obj.find("li").eq(0).addClass("active");
                });

                // 选择市级时发生事件
                city_obj.find("li").live("click", function() {
                    $(this).addClass("active").siblings("").removeClass("active");
                    index = $(this).index();
                    distStart(index);
                    // dist_obj.find("li").eq(0).addClass("active");
                });
            };

            // 设置省市json数据
            if (typeof(settings.url) == "string") {
                $.getJSON(settings.url, function(json) {
                    city_json = json;
                    init();
                });
            } else {
                city_json = settings.url;
                init();
            };
        };
    } catch (e) {
        console.log("异常");
    }
})(jQuery);

// JavaScript Document
$(function() {
    $(".layer").live("click", function() { //点击黑色区域收起下拉列表
        $(".topSelect").slideUp("300");
        $(".layer").slideUp("500");
        $("body").css({ "height": "auto", "overflow": "inherit" });
    });
    goAway(".twoCategory .twoCategoryList li"); //确认分类
    goAway(".selectAddress ul.dist li"); //选择地址
    goAway(".ranking li"); //选择智能排序
    switching(".oneCategory>li", ".twoCategory>li", "active", "phptoCurr"); //页面切换    //店铺首页分类切换
    switchingSlide(".shopCenter .choice>ul>li", ".topSelect>li", "active", "phptoCurr"); //页面切换//店铺首页导航切换   
    activeClick(".distAddress li", "active");
    activeClick(".ranking li", "active");
    activeClick(".shopCenter .shopLabel li");

    $(".twoCategory .twoCategoryList li").live("click", function() { //二级分类
        $(".twoCategory .twoCategoryList li").removeClass("active");
        $(this).addClass('active');
    });

});

function goAway(a) {
    $(a).live("click", function() { //选择过后默然消失
        timeout();
    });
}

function timeout() {
    setTimeout(function() {
        $(".topSelect").slideUp("300");
        $(".layer").slideUp("500");
        $("body").css({ "height": "auto", "overflow": "inherit" });
    }, 500);
}

function activeClick(a, b) {
    $(a).live("click", function() {
        $(this).siblings().removeClass(b)
        $(this).addClass("active");
    });
}
var height = $(window).height();
console.log();

function switchingSlide(a, b, c, d) { //下拉切换
    var i = 0;
    var len = $(a).length - 1;
    $(b).eq(0).show();

    function zhixing(index) {
        $(b).parent().slideDown("100");
        $(".layer").slideDown("500");
        $("body").css({ "height": $(window).height(), "overflow": "hidden" });
        $(a).eq(index).addClass(c).siblings().removeClass(c);
        $(b).eq(index).addClass(d).slideDown().siblings().slideUp().removeClass(d);
    }
    $(a).live("click", function() {
        var index = $(this).index();
        zhixing(index);
    });
};

function switching(a, b, c, d) { //切换
    var i = 0;
    // $(b).eq(0).show();
    var len = $(a).length - 1;
    $(b).eq(0).show();

    function zhixing(index) {
        $(a).eq(index).addClass(c).siblings().removeClass(c);
        $(b).eq(index).addClass(d).show().siblings().hide().removeClass(d);
    }
    $(a).live("click", function() {
        var index = $(this).index();
        zhixing(index);
    });
};

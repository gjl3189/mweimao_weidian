/***@author wangmu***/
/***5-27 11:50***/
var anModule = angular.module('module', ['ngRoute', 'shop.directive', 'shop.services', 'shop.controller','ngAnimate']);
anModule.config(function($routeProvider) {
	$routeProvider.when('/shopIndex', { //详情页面路由
			controller: 'shopIndex',
			templateUrl: 'templates/groupbuy/shopIndex.html'
		}).otherwise({
			redirectTO: '/'
		});
});


var ctrl = angular.module("shop.controller", ['shop.services']);
ctrl.controller("shopIndex", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
 
/*****wangmu*******/
/****2016-5-12*****/
$(function() {
//console.log(expressDistance(31,107));
    $("i.close ,.layer").live("click", function() { //关闭弹层
        $(".layer,.layerList").fadeOut('300');
        $(".layerCentent").fadeOut('300');
    });
    $(".agreement a").live("click", function() { //阅读分销协议 
        $(".layer,.layerCentent").fadeToggle('300');
    });

    $(".detailsHeader .bannerLayer> i").live("click", function() { //关闭详情页头部悬浮窗
        $(".detailsHeader .bannerLayer").fadeToggle('300');
    });
    $(".detailsCentent .productName span").live("click", function() {
        $(".layer,.share").fadeToggle('300');
    });
    $(".productActive li:nth-child(2)").live("click", function() { //显示优惠券
        $(".layer,.coupon").fadeToggle('300');
    });
 
    $(".goTop").live("click", function() {        //回到顶部
        $("body").animate({ scrollTop: 0 })
    });
    $(".share .shareBottom li:nth-child(1),.share .shareBottom li:nth-child(2)").live("click", function() {  //微信分享
        $(".shareLayer").fadeIn('300');
    });
    $(".shareButton button.copy").live("click",function(){
        $(".layer,.shareLayer").fadeIn('300');
    });

    /******************购物车*************************/
    $("footer ul li,.productCat").live("click", function() { //显示购物车
        $(".layer,.shoppingCat").fadeIn("300");
    });
    $(".shoppingCat .button").live("click", function() { //添加到购物车
        $(".layer,.shoppingCat").fadeOut("300");
    });
    $(".shoppingCat .productLabelList ul li,.evaluateTitle li").live("click", function() { //标签选择
        $(this).addClass("active").siblings().removeClass('active');
    });
    $(".productInfo .productName p").append('<i>' + $(".productLabelList li").eq(0) + '</i>')//购物车默认标签
    var inputVal = $(".shoppingCat .shoppingButton span input");
    var i = 0;
    inputVal.blur(function() { //输入数量
        i = $(this).val();
    });
    $(".reduce").live("click", function() { //减少
        if (i > 0) {
            i--;
            $(".shoppingCat .shoppingButton span input").val(i);
        }
    });
    $(".plus").live("click", function() { //增加
        console.log(i);
        i++;
        $(".shoppingCat .shoppingButton span input").val(i);
    });

});

function switching(a, b, c, d) { //切换
    var i = 0;
    // $(b).eq(0).show();
    var len = $(a).length - 1;
    function zhixing(index) {
        $(a).eq(index).addClass(c).siblings().removeClass(c);
        $(b).eq(index).addClass(d).show().siblings().hide().removeClass(d);
    }

    $(a).click(function() {
        var index = $(this).index();
        zhixing(index);
    });
};

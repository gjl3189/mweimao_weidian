/***@author wangmu***/
/***3-3 10:30***/
var anModule = angular.module('module', ['ngRoute', 'shop.directive', 'shop.services', 'shop.controller','ngAnimate']);
anModule.config(function($routeProvider) {
	$routeProvider.when('/shopIndex/:product_id/', { //详情页面路由
			controller: 'shopIndex',
			templateUrl: 'templates/groupbuy/shopIndex.html'
		}).when('/details/:product_id/:store_id', { //详情页面路由
			controller: 'detailsInfo',
			templateUrl: 'templates/groupbuy/detailsInfo.html'
		}).when('/active/:product_id', {  //活动页面路由
			controller: 'activeInfo',
			templateUrl: 'templates/groupbuy/activeInfo.html'
		}).when('/game/:product_id', {//游戏页面路由
			controller: 'gameInfo',
			templateUrl: 'templates/groupbuy/gameInfo.html'
		}).when('/createShop/:store_id', {//创建分销店铺页面路由
			controller: 'createShopInfo',
			templateUrl: 'templates/groupbuy/createShopInfo.html'
		}).when('/shopList/:store_id', {//门店列表页面路由
			controller: 'shopListInfo',
			templateUrl: 'templates/groupbuy/shopListInfo.html'
		}).when('/distributionShop/:product_id/:store_id', {//游戏页面路由
			controller: 'distributionShopInfo',
			templateUrl: 'templates/groupbuy/distributionShopInfo.html'
		}).otherwise({
			redirectTO: '/'
		});
});


var ctrl = angular.module("shop.controller", ['shop.services']);
ctrl.controller("shopIndex", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
ctrl.controller("detailsInfo", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
ctrl.controller("activeInfo", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
ctrl.controller("gameInfo", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
ctrl.controller("createShopInfo", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
ctrl.controller("shopListInfo", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
ctrl.controller("distributionShopInfo", ["$scope", "$routeParams", function($scope, $routeParams) {

}]);
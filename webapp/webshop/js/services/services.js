/***@author wangmu***/
/***5-16-14:23***/
var serv = angular.module("shop.services", []);
var APIURL = window.location.protocol + "//" + window.location.host + "/"; // 接口路径


serv.service("surplusDay", ["$rootScope", "$http", "$routeParams", function(rootscope, http, $routeParams) { //封装服务函数
    function convertTime(a) {
        return new Date(parseInt(a) * 1000).toLocaleString().replace(/年|月/g, "-").replace(/日/g, " ");
    };

    var param = "";

    function init(url, page) { //传值page加载
        param = "&page=" + page;
        return http({
            method: 'GET',
            url: APIURL + url + param
        });
    };

    function loadJson(page, list, url) { //加载按钮
        $(window).scroll(function() {
            if ($(document).scrollTop() >= $(document).height() - $(window).height()) {
                init(page, url).success(function(data) {
                    try {
                        if (data.err_msg.next_page) {
                            for (var i = 0; i < list.length; i++) {
                                list.push(list[i]);
                                console.log(list);
                            }
                        }
                    } catch (e) {
                        console.log("没有更多");
                    }

                });
            }
        });

    }

    return {
        convertTime: convertTime,
        init: init,
        loadJson: loadJson
    }
}]);
serv.service("lazyLoadService", ["$rootScope", function(rootscope) { // 图片懒加载服务

    /*
     *  注意：
     *      在需要使用图片懒加载的图片src中写上用户看不见时使用的小图片路径，datasrc里写上实际需要加载的图片路径，示例
     *      <img src="a.png" datasrc="//www.baidu.com/img/270_7573fb368053e6805e63b56352ce7287.gif" alt="">
     *      调用方法：
     *          lazyLoad.init($("img"));
     */

    var lazyLoad = {
        init: function(el) { // 初始化
            if (el == "") {
                return;
            }
            this.winHeight = $(window).height();
            this.el = el;
            var me = this;
            $(document).on("scroll", function() {
                me.bind();
            });

            this.bind();
        },

        bind: function() {
            var me = this;
            me.el.each(function() {
                if (!$(this).attr("data-lazy") && me.isshow($(this))) {
                    me.showImg($(this));
                }
            });
        },

        isshow: function(el) {
            var winScrollTop = $(window).scrollTop();
            if (el.offset().top < this.winHeight + winScrollTop) {
                el.attr("data-lazy", "true");
                //console.log(el.offset().top, this.winHeight, winScrollTop)
                return true;
            } else {
                return false;
            }
        },

        showImg: function(el) {
            el.attr("src", el.attr("datasrc"));
        }
    };
    return {
        lazyLoad: lazyLoad
    };
}]);
/*
try {
 if (window.console && window.console.log) {
        // console.log("嘘!低调,不要崇拜王穆大大做的页面!");
        console.log("\n\n");
        console.log("最美年华，做最好的技术员！\n\n");
        console.log('如何让我遇见你，在你最美的时候\n加入小猪CMS，改变生活，改变社会，改变未来，在这里启程！\n');
        console.log("请将简历发送至 %c hr@pigcms.cn（邮件标题请以“姓名-应聘XX职位-来自console”命名）", "color:#0b8");
        console.log("职位介绍：http://www.pigcms.com/contact/job/\n\n");
    }
} catch (e) {}
*/

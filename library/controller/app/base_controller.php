<?php

/**
 * 基础类
 */
class base_controller extends controller {
	public $user;
	public $is_app = false;
	public function __construct() {
		parent::__construct();
		
		if (isset($_REQUEST['request_from']) && strtolower($_REQUEST['request_from']) == 'app') {
			$this->is_app = true;
		}
		
		if ($this->is_app) {
			// 不是用户登录页面
			if (MODULE_NAME != 'user' && MODULE_NAME != 'qrcode' && !isset($_GET['debug'])) {
				$user = $_SESSION['app_user'];
				
				if (empty($user)) {
					$user = D('User')->where(array('app_openid' => $_REQUEST['openid']))->find();
					if (empty($user)) {
						json_return(10000, '重新授权');
					} else if (empty($user['phone'])) {
						json_return(40000, '绑定手机号');
					} else {
						$_SESSION['app_user'] = $user;
						$this->user = $user;
					}
				} else {
					$this->user = $user;
				}
			} else if (isset($_GET['debug'])) {
				$user = D('User')->where(array('phone' => '18154225428'))->find();
				$_SESSION['app_user'] = $user;
				$this->user = $user;
			}
		} else {
			// 不是用户登录页面
			if (MODULE_NAME != 'user' && MODULE_NAME != 'qrcode' && !isset($_GET['debug'])) {
				$user = $_SESSION['wap_user'];
				
				if (empty($user)) {
					json_return(20000, option('config.wap_site_url') . '/login.php');
				} else {
					$this->user = $user;
				}
			} else if (isset($_GET['debug'])) {
				$user = D('User')->where(array('phone' => '18000000000'))->find();
				$_SESSION['wap_user'] = $user;
				$this->user = $user;
			}
		}
	}
}
?>
<?php
class setting_controller extends base_controller{
    public function load(){
        $action = strtolower(trim($_POST['page']));
        if (empty($action)) pigcms_tips('非法访问！', 'none');
        switch ($action) {
            case 'store_content': //店铺信息
                $this->_store_content();
                break;
			case 'contact_content': //联系我们
                $this->_contact_content();
                break;
			case 'list_content': //门店管理
                $this->_list_content();
                break;
			case 'physical_edit_content': //门店管理
                $this->_physical_edit_content();
                break;
            case 'shop_mall': //店铺下单信息提示
                $this->_shop_mall();
                break;
            case 'assign_quantity': //分配门店库存
                $this->_assign_quantity();
                break;
			case 'notice_list':
				$this->_notice_list();
				break;
			case 'store_notice_setting':
				$this->_store_notice_setting();
				break;
			case 'friend_content':
				$this->_friend_content();
				break;
			case 'address_add':
				$this->_address_add();
				break;
			case 'address_edit':
				$this->_address_edit();
				break;
            case 'list_admin_content':
                $this->_list_admin_content();
				break;
            case 'set_admin_content':
                $this->set_admin_content();
                break;
            case 'store_admin_edit':
                $this->store_admin_edit();
                break;
            default:
                break;
        }
        $this->display($_POST['page']);
    }
	 //店铺名称唯一性检测
    public function store_name_check()
    {
        $store = M('Store');

        $name = isset($_POST['name']) ? trim($_POST['name']) : '';
        $unique = $store->getUniqueName($name);
        echo $unique;
        exit;
    }
    //设置店铺
    public function store(){
        if (IS_POST) {
            $store = M('Store');
            $name    = isset($_POST['name']) ? trim($_POST['name']) : '';
			if(isset($_POST['logo'])){
				$logo = getAttachment($_POST['logo']);//str_replace(array(option('config.site_url').'/upload/images/','./upload/images/'),'',trim($_POST['logo']));
			}else{
				$logo = '';
			}
            $intro   = isset($_POST['intro']) ? trim($_POST['intro']) : '';
            $linkman = isset($_POST['linkman']) ? trim($_POST['linkman']) : '';
            $legal_person = isset($_POST['legal_person']) ? trim($_POST['legal_person']) : '';
            $qq      = isset($_POST['qq']) ? trim($_POST['qq']) : '';
			$mobile  = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
            $open_service  = isset($_POST['open_service']) ? intval(trim($_POST['open_service'])) : 0;
            $is_show_drp_tel  = isset($_POST['is_show_drp_tel']) ? trim($_POST['is_show_drp_tel']) : '';
            
            $data = array();
            if($name) $data['name'] = $name;
            if($logo) $data['logo'] = $logo;
            $data['intro'] = $intro;
            $data['linkman'] = $linkman;
            $data['legal_person'] = $legal_person;
            $data['qq'] = $qq;
			$data['tel'] = $mobile;
			$data['is_show_drp_tel'] = $is_show_drp_tel;
            $data['open_service'] = $open_service;
            $data['sale_category_id']   = intval($_POST['sale_category_id']);
            $data['sale_category_fid']  = intval($_POST['sale_category_fid']);
            
            if ($_SESSION['store']['name'] != $name) {
                $data['edit_name_count'] = $_SESSION['store']['edit_name_count'] + 1; //店铺名称修改次数
            }
            $sale_category = M('Sale_category');
            $where = array();
            $where['store_id'] = $this->store_session['store_id'];

            $info = D('Store')->where($where)->find();
            if ($data['sale_category_id'] && $data['sale_category_fid']) {
                $sale_category->setStoreDec($info['sale_category_id']);
                $sale_category->setStoreDec($info['sale_category_fid']);
            } else if ($data['sale_category_fid'] && empty($data['sale_category_id'])) {
                $sale_category->setStoreDec($info['sale_category_id']);
                $sale_category->setStoreDec($info['sale_category_fid']);
                $data['sale_category_id']   = 0;
            }

            if ($store->setting($where, $data)) {
                $_SESSION['store']['name'] = $name;
                $_SESSION['store']['logo'] = $_POST['logo'];
                $_SESSION['store']['edit_name_count'] += 1;
                if ($data['sale_category_id']) {
                    $sale_category->setStoreInc($data['sale_category_id']);
                } 
                if ($data['sale_category_fid']) {
                    $sale_category->setStoreInc($data['sale_category_fid']);
                }
            }
            json_return(0, url('store:index'));
        }

        $id = $_GET['id'] + 0;
        if (!empty($id)) {
        	$store = M('Store')->getStoreById($id, $_SESSION['user']['uid']);
        	if (empty($store)) {
        		pigcms_tips('未找到相应的店铺', 'none');
        	}

        	$_SESSION['store'] = $store;
        }
        $this->display();
    }
	//联系我们
	public function contact(){
		if(IS_POST){
			$data_store_contact['phone1'] = $_POST['phone1'];
			$data_store_contact['phone2'] = $_POST['phone2'];
			$data_store_contact['province'] = $_POST['province'];
			$data_store_contact['city'] = $_POST['city'];
			$data_store_contact['county'] = $_POST['county'];
			$data_store_contact['address'] = $_POST['address'];
			$data_store_contact['long'] = $_POST['map_long'];
			$data_store_contact['lat'] = $_POST['map_lat'];
			$data_store_contact['last_time'] = $_SERVER['REQUEST_TIME'];
				
			$database_store_contact = D('Store_contact');
			$condition_store_contact['store_id'] = $this->store_session['store_id'];
			if($database_store_contact->where($condition_store_contact)->find()){
                // 添加区域修改记录 用于区域管理员关联
                M('Store_contact')->setAreaRelation($condition_store_contact['store_id'], $data_store_contact);
                if($database_store_contact->where($condition_store_contact)->data($data_store_contact)->save()){
					json_return(0,'保存成功');
				}else{
					json_return(1,'保存失败');
				}
			}else{
				$data_store_contact['store_id'] = $this->store_session['store_id'];
                // 添加区域修改记录 用于区域管理员关联
                M('Store_contact')->setAreaRelation($condition_store_contact['store_id'], $data_store_contact);
                if($database_store_contact->data($data_store_contact)->add()){
					json_return(0,'保存成功');
				}else{
					json_return(1,'保存失败');
				}
			}
		}else{
			json_return(1,'非法访问！');
		}
	}
	//店铺详细
    private function _store_content(){
        $company = M('Company');
        $database_store = M('Store');

        $company = $company->getCompanyByUid($this->user_session['uid']);

        //店铺主营类目
        $sale_category = $database_store->getSaleCategory($this->store_session['store_id'], $this->user_session['uid']);
        $store = $database_store->getStoreById($this->store_session['store_id'], $this->user_session['uid']);

        $tmp_categories = M('Sale_category')->getCategoriesValid(0);
        $categories = array();
        foreach ($tmp_categories as $tmp_category) {
            $children = M('Sale_category')->getCategoriesValid($tmp_category['cat_id']);
            $categories[$tmp_category['cat_id']] = array(
                'cat_id' => $tmp_category['cat_id'],
                'name' => $tmp_category['name'],
                'children' => $children
            );
        }
        $this->assign('json_categories', json_encode($categories));
        $this->assign('categories', $categories);
        // dump($categories);exit;
        if ($store['drp_level'] > 0) {
            $sup_store          = M('Store')->getSupplier($this->store_session['store_id']);
            $store['update_drp_store_info']     = $sup_store['update_drp_store_info'];
        }

        // 店铺修改次数
        $store_name_limit = option('config.store_name_change_limit');
        $store_name_limit = !empty($store_name_limit) ? intval($store_name_limit) : 1;

        $this->assign('company', $company);
        $this->assign('store', $store);
        $this->assign('sale_category', $sale_category);
        $this->assign('store_name_limit', $store_name_limit);
    }
	//联系我们
    private function _contact_content(){
		$store_contact = D('Store_contact')->where(array('store_id'=>$this->store_session['store_id']))->find();
		$this->assign('store_contact',$store_contact);
    }
	
	//消息通知管理
	private function _notice_list() {
		$store_id = $this->store_session['store_id'];
				
		import("source.class.templateNews");
		$model = new templateNews();
		$templs = $model->systemTemplates();
		$data = D('Tempmsg')->where("token='system'")->order('id ASC')->select();
        // if (empty($data)) {
        //     echo 
        // }

		foreach($data as $key=>$val){
			$data[$val['tempkey']] = $val;
		}
		
		$list 	= array();
		foreach ($templs as $k => $v){
			$dbtempls = D('Tempmsg')->where("token='system' AND tempkey='$k'")->find();
			if(empty($dbtempls)){
				$list[] 	= array(
					'tempkey' 	=> $k,
					'name'		=> $v['name'],
					'content'	=> $v['content'],
					'topcolor'	=> '#029700',
					'textcolor'	=> '#000000',
					'status'	=> 0,
				);
			}else{
				$list[] 	= $data[$k];
			}
		}	
		
		//获取当前店铺的短信/通知 权限
		$store_notice_manager = M('Store_system_notice_manage')->get($store_id);
		
		$this->assign("store_notice_manage",$store_notice_manager);
        $this->assign("notice_manage",$list);
		$this->assign("total_config", $data);
	}
	

	//消息通知i消息 编辑/保存
	private function _store_notice_setting() {

		$fields_seria = $_POST['fields_seria'];
		$store_id = $this->store_session['store_id'];
		
		if(count($fields_seria)) {
			foreach($fields_seria as $k=>$v) {
				if($v['value']!=0) {
					$arr[$v[name]][] = $v['value'];
				}
			}
		}
		
		$has_power = "";
		if(is_array($arr))	{
			foreach($arr as $k=>$v) {
				$arrs[] = $k.'^'.implode(",",$v);
			}
			if(count($arrs)) $has_power = implode("|",$arrs);
		} 
		
		$array['has_power'] = $has_power;
		$store_notice = D('Store_system_notice_manage')->where(array('store_id'=>$store_id))->find();
		$array['timestamp'] = time();
		if($store_notice) {
			D('Store_system_notice_manage')->data($array)->where(array('store_id'=>$store_id))->save();
		} else {
			$array['store_id'] = $store_id;
			D('Store_system_notice_manage')->data($array)->add();	
		}
		echo json_encode(array('status'=>0,'msg'=>'修改成功！'));exit;
	}

    //店铺下单提示信息设置
    private function _shop_mall () {
        $store = M('Store')->getStoreById($this->store_session['store_id'], $this->user_session['uid']);
        $this->assign('store', $store);
    }

    //ajax 店铺通知记录开关
    public function set_shop_notice () {
        $status = intval(trim($_POST['status']));
        $store_id = $this->store_session['store_id'];
    
        // 【订单提醒管理】 开启同时关闭 【分销引导】
        $store = D('Store')->where(array('store_id'=>$this->store_session['store_id']))->find();
        $data['order_notice_open'] = $status;
        if ($data['order_notice_open']) {
            $data['open_drp_guidance'] = 0;
        }

        $result = D('Store')->where(array('store_id' => $store_id))->data($data)->save();
        if ($result) {
            json_return(0, '保存成功！');
        } else {
            json_return(4099, '保存失败，请重试！');
        }
    }

    //店铺订单持续时间设置
    public function set_notice_time () {

        $order_notice_time = intval(trim($_POST['order_notice_time']));
        $store_id = $this->store_session['store_id'];
    
        $store = M('Store');
        $result = D('Store')->where(array('store_id' => $store_id))->data(array('order_notice_time' => $order_notice_time))->save();
        if ($result) {
            json_return(0, '保存成功！');
        } else {
            json_return(4099, '保存失败，请重试！');
        }

    }

	//门店管理
	private function _list_content(){
		$store_physical = D('Store_physical')->where(array('store_id'=>$this->store_session['store_id']))->select();
		$this->assign('store_physical',$store_physical);
	}
	//门店编辑
	private function _physical_edit_content(){
		$store_physical = D('Store_physical')->where(array('store_id'=>$this->store_session['store_id'],'pigcms_id'=>$_POST['pigcms_id']))->find();
		if(empty($store_physical)){
			exit('该门店不存在！');
		}
		
		$store_physical['images_arr'] = explode(',',$store_physical['images']);
		foreach($store_physical['images_arr'] as &$physical_value){
			$physical_value = getAttachmentUrl($physical_value);
		}
		$this->assign('store_physical',$store_physical);
	}
    //分配库存
    private function _assign_quantity(){
        $product = M('Product');
        $product_group = M('Product_group');
        $product_to_group = M('Product_to_group');
        $product_sku = M('Product_sku');

        $order_by_field = isset($_POST['orderbyfield']) ? $_POST['orderbyfield'] : '';
        $order_by_method = isset($_POST['orderbymethod']) ? $_POST['orderbymethod'] : '';
        $keyword = isset($_POST['keyword']) ? trim($_POST['keyword']) : '';
        $group_id = isset($_POST['group_id']) ? trim($_POST['group_id']) : '';

        $where = array();
        $where['store_id'] = $this->store_session['store_id'];
        $where['quantity'] = array('>', 0);
        $where['soldout'] = 0;
        if ($keyword) {
            $where['name'] = array('like', '%' . $keyword . '%');
        }
        if ($group_id) {
            $products = $product_to_group->getProducts($group_id);
            $product_ids = array();
            if (!empty($products)) {
                foreach ($products as $item) {
                    $product_ids[] = $item['product_id'];
                }
            }
            $where['product_id'] = array('in', $product_ids);
        }
        $product_total = $product->getSellingTotal($where);
        import('source.class.user_page');
        $page = new Page($product_total, 15);
        $products = $product->getSelling($where, $order_by_field, $order_by_method, $page->firstRow, $page->listRows);

        $product_groups = $product_group->get_all_list($this->store_session['store_id']);

        foreach ($products as $key => $val) {

            if (empty($val['has_property'])) {
                $products[$key]['sku'] = array();
                continue;
            }

            $val_sku = $product_sku->getSkus($val['product_id']);
            // dump($val_sku);exit;
            foreach ($val_sku as $k => $v) {

                $tmpPropertiesArr = explode(';', $v['properties']);
                $properties = $propertiesValue = $productProperties = array();
                foreach($tmpPropertiesArr as $v){
                    $tmpPro = explode(':',$v);
                    $properties[] = $tmpPro[0];
                    $propertiesValue[] = $tmpPro[1];
                }
                if(count($properties) == 1){
                    $findPropertiesArr = D('Product_property')->field('`pid`,`name`')->where(array('pid'=>$properties[0]))->select();
                    $findPropertiesValueArr = D('Product_property_value')->field('`vid`,`value`,`image`')->where(array('vid'=>$propertiesValue[0]))->select();
                }else{
                    $findPropertiesArr = D('Product_property')->field('`pid`,`name`')->where(array('pid'=>array('in',$properties)))->select();
                    $findPropertiesValueArr = D('Product_property_value')->field('`vid`,`value`,`image`')->where(array('vid'=>array('in',$propertiesValue)))->select();
                }
                foreach($findPropertiesArr as $v){
                    $propertiesArr[$v['pid']] = $v['name'];
                }
                foreach($findPropertiesValueArr as $v){
                    $propertiesValueArr[$v['vid']] = $v['value'];
                }
                foreach($properties as $kk=>$v){
                    $productProperties[] = array('pid'=>$v,'name'=>$propertiesArr[$v],'vid'=>$propertiesValue[$kk],'value'=>$propertiesValueArr[$propertiesValue[$kk]], 'image'=>getAttachmentUrl($findPropertiesValueArr[$kk]['image']));
                }

                $val_sku[$k]['_property'] = $productProperties;
            }

            $products[$key]['sku'] = $val_sku;
        }

        // dump($products);exit;
        $this->assign('product_groups', $product_groups);
        $this->assign('product_groups_json', json_encode($product_groups));
        $this->assign('page', $page->show());
        $this->assign('products', $products);
    }
    //ajax 弹层获取门店列表
    public function assign_quantity_json() {

        $data = array();
        $product_id = $data['product_id'] = !empty($_POST['product_id']) ? intval(trim($_POST['product_id'])) : 0;
        $sku_id = $data['sku_id'] = !empty($_POST['sku_id']) ? intval(trim($_POST['sku_id'])) : 0;

        if (empty($product_id) && empty($sku_id)) {
            json_return(1,'缺少参数，稍后再试');
        }

        $where = array('product_id'=>$product_id);
        if (!empty($sku_id)) {
            $where = array('sku_id'=>$sku_id);
        }

        $store_id = $this->store_session['store_id'];
        $store_physical = D('Store_physical')->where(array('store_id'=>$store_id))->select();

        if (empty($store_physical)) {
            json_return(1,'请先添加门店');
        }

        $data['store_physical'] = $store_physical;

        //被分配的商品
        if (!empty($sku_id)) {
            $data['product_info'] = D('Product_sku')->where($where)->find();
        } else {
            $data['product_info'] = M('Product')->get($where);
        }

        //已经分配的
        $data['physical_quantity'] = M('Store_physical_quantity')->getQuantityByPid($where);
        
        echo json_encode($data, true);
        exit;
    }
    //保存分配库存
    public function quantity_set(){

        $store_id = $this->store_session['store_id'];

        $sku_id = !empty($_POST['sku_id']) ? intval(trim($_POST['sku_id'])) : 0;
        $product_id = !empty($_POST['product_id']) ? intval(trim($_POST['product_id'])) : 0;

        $nums = !empty($_POST['nums']) ? $_POST['nums'] : array();
        $physical_ids_new = !empty($_POST['physical_ids']) ? $_POST['physical_ids'] : array();

        if (empty($nums) || empty($physical_ids_new) || count($nums) != count($physical_ids_new)) {
            json_return(0, '数据错误，稍后再试');
        }

        if (!empty($sku_id)) {
            $where = array('sku_id'=>$sku_id);
        } else {
            $where = array('product_id'=>$product_id);
        }

        $num_physical = array_combine($physical_ids_new, $nums);

        //该产品分配过的门店array
        $physical_ids_old = M('Store_physical_quantity')->getPhysicalByPid($where);
        $physical_arr = array_diff($physical_ids_new, $physical_ids_old);

        //新增
        foreach ($physical_arr as $val) {
            $data = array(
                'store_id' => $store_id,
                'product_id' => $product_id,
                'sku_id' => $sku_id,
                'physical_id' => $val,
                'quantity' => $num_physical[$val],
            );
            $return = M('Store_physical_quantity')->add($data);
        }

        //修改
        foreach ($physical_ids_old as $val) {
            $where = array_merge($where, array('physical_id'=>$val));
            $data = array('quantity'=>$num_physical[$val]);
            $return = M('Store_physical_quantity')->edit($where, $data);
        }

        json_return(0, '修改成功');
    }
	//门店添加
	public function physical_add(){
		if(IS_POST){
			$data_store_physical['store_id'] = $this->store_session['store_id'];
			$data_store_physical['name'] = $_POST['name'];
			$data_store_physical['phone1'] = $_POST['phone1'];
			$data_store_physical['phone2'] = $_POST['phone2'];
			$data_store_physical['province'] = $_POST['province'];
			$data_store_physical['city'] = $_POST['city'];
			$data_store_physical['county'] = $_POST['county'];
			$data_store_physical['address'] = $_POST['address'];
			$data_store_physical['long'] = $_POST['map_long'];
			$data_store_physical['lat'] = $_POST['map_lat'];
			$data_store_physical['last_time'] = $_SERVER['REQUEST_TIME'];
			
			
			if(is_array($_POST['images'])){
				foreach($_POST['images'] as &$images_value){
					$images_value = getAttachment($images_value);
				}
				$data_store_physical['images'] = implode(',',$_POST['images']);
			}else {
				json_return(1,'门店照片不存在，添加失败');
			}
			
			
			
			$data_store_physical['business_hours'] = $_POST['business_hours'];
			$data_store_physical['description'] = $_POST['description'];
				
			$database_store_physical = D('Store_physical');
			if($database_store_physical->data($data_store_physical)->add()){
				D('Store')->where(array('store_id'=>$this->store_session['store_id']))->setInc('physical_count');
				json_return(0,'添加成功');
			}else{
				json_return(1,'添加失败');
			}
		}else{
			json_return(1,'非法访问！');
		}
	}
	//门店编辑
	public function physical_edit(){
		if(IS_POST){
			$condition_store_physical['pigcms_id'] = $_POST['pigcms_id'];	
			$condition_store_physical['store_id'] = $this->store_session['store_id'];
			$data_store_physical['name'] = $_POST['name'];
			$data_store_physical['phone1'] = $_POST['phone1'];
			$data_store_physical['phone2'] = $_POST['phone2'];
			$data_store_physical['province'] = $_POST['province'];
			$data_store_physical['city'] = $_POST['city'];
			$data_store_physical['county'] = $_POST['county'];
			$data_store_physical['address'] = $_POST['address'];
			$data_store_physical['long'] = $_POST['map_long'];
			$data_store_physical['lat'] = $_POST['map_lat'];
			$data_store_physical['last_time'] = $_SERVER['REQUEST_TIME'];
			
			if(is_array($_POST['images'])){
				foreach($_POST['images'] as &$images_value){
					$images_value = getAttachment($images_value);
				}
				$data_store_physical['images'] = implode(',',$_POST['images']);
			}else {
				json_return(1,'门店照片不存在，修改失败');
			}
			
			
			$data_store_physical['images'] = implode(',',$_POST['images']);
			$data_store_physical['business_hours'] = $_POST['business_hours'];
			$data_store_physical['description'] = $_POST['description'];
				
			$database_store_physical = D('Store_physical');
			if($database_store_physical->where($condition_store_physical)->data($data_store_physical)->save()){
				json_return(0,'修改成功');
			}else{
				json_return(1,'修改失败');
			}
		}else{
			json_return(1,'非法访问！');
		}
	}
	//门店删除
	public function physical_del(){
		if(IS_POST){
            $physical_id = $_POST['pigcms_id'];
            $store_id = $this->store_session['store_id'];
			$database_store_physical = D('Store_physical');
			$condition_store_physical['pigcms_id'] = $physical_id;
			$condition_store_physical['store_id']  = $store_id;
			if($database_store_physical->where($condition_store_physical)->delete()){
				D('Store')->where(array('store_id'=>$store_id))->setDec('physical_count');
                //清除门店库存 && 清除门店订单关系
                D('Store_physical_quantity')->where(array('physical_id'=>$physical_id, 'store_id'=>$store_id))->delete();
                D('Order_product_physical')->where(array('physical_id'=>$physical_id, 'store_id'=>$store_id))->delete();
				json_return(0,'删除成功');
			}else{
				json_return(1,'删除失败');
			}
		}else{
			json_return(1,'非法访问！');
		}
	}
	
	// 物流配送相关
	public function config() {
		$this->display();
	}
	
	public function logistics() {
		$store = M('Store')->getStore($this->store_session['store_id']);
		$this->assign('store', $store);
		$this->display();
	}
	
	public function logistics_status() {
		$status = intval(trim($_POST['status']));
		$store_id = $this->store_session['store_id'];
		
		$store = M('Store');
		$result = D('Store')->where(array('store_id' => $store_id))->data(array('open_logistics' => $status))->save();
		if ($result) {
			json_return(0, '保存成功！');
		} else {
			json_return(4099, '保存失败，请重试！');
		}
	}
	
	private function _friend_content(){
		$store = M('Store')->getStoreById($this->store_session['store_id'], $this->user_session['uid']);
		$this->assign('store', $store);
		
		$commonweal_address_list = M('Commonweal_address')->select($this->store_session['store_id']);
		$this->assign('commonweal_address_list', $commonweal_address_list);
	}
	
	private function _address_add() {
		$store = M('Store')->getStoreById($this->store_session['store_id'], $this->user_session['uid']);
		$this->assign('store', $store);
	}
	
	private function _address_edit() {
		$id = $_POST['id'];
		if (empty($id)) {
			pigcms_tips('缺少最基本的参数');
		}
		
		$store = M('Store')->getStoreById($this->store_session['store_id'], $this->user_session['uid']);
		$this->assign('store', $store);
		
		$commonweal_address = D('Commonweal_address')->where(array('store_id' => $this->store_session['store_id'], 'id' => $id))->find();
		$this->assign('commonweal_address', $commonweal_address);
	}
	
	public function commonweal_address() {
		$title = $_POST['title'];
		$name = $_POST['name'];
		$tel = $_POST['tel'];
		$province = $_POST['province'];
		$city = $_POST['city'];
		$area = $_POST['area'];
		$address = $_POST['address'];
		$zipcode = $_POST['zipcode'];
		$is_default = $_POST['is_default'];
		$address_id = $_POST['address_id'];
		
		if (empty($name)) {
			json_return(1000, '请填写收货人姓名');
		}
		
		if (empty($tel)) {
			json_return(1000, '请填写联系电话');
		}
		
		if (!preg_match("/\d{5,12}$/", $tel)) {
			json_return(1000, '请正确填写联系电话');
		}
		
		if (empty($province)) {
			json_return(1000, '请选择省份');
		}
		
		if (empty($city)) {
			json_return(1000, '请选择城市');
		}
		
		if (empty($area)) {
			json_return(1000, '请选择地区');
		}
		
		if (!empty($address_id)) {
			$commonweal_address = D('Commonweal_address')->where(array('id' => $address_id, 'store_id' => $this->store_session['store_id']))->find();
			if (empty($commonweal_address)) {
				json_return(1000, '未找到相应的公益地址');
			}
		}
		
		$default = $is_default ? 1 : 0;
		$data = array();
		$data['dateline'] = time();
		$data['store_id'] = $this->store_session['store_id'];
		$data['title'] = $title;
		$data['name'] = $name;
		$data['tel'] = $tel;
		$data['province'] = $province;
		$data['city'] = $city;
		$data['area'] = $area;
		$data['address'] = $address;
		$data['zipcode'] = $zipcode;
		$data['default'] = $default;
		
		$result = false;
		if (empty($address_id)) {
			$result = D('Commonweal_address')->data($data)->add();
			$address_id = $result;
		} else {
			$result = D('Commonweal_address')->where(array('id' => $address_id))->data($data)->save();
		}
		
		if ($result) {
			// 更改其它的收货地址，不为默认收货地址
			if ($default) {
				D('Commonweal_address')->where(array('store_id' => $this->store_session['store_id'], 'id' => array('!=', $address_id)))->data(array('default' => 0))->save();
			}
			json_return(0, '操作成功');
		} else {
			json_return(1000, '操作失败');
		}
	}
	
	public function commonweal_address_delete () {
		$id = $_POST['id'];
		if (empty($id)) {
			json_return(1000, '缺少最基本的参数');
		}
		
		$commonweal_address = D('Commonweal_address')->where(array('store_id' => $this->store_session['store_id'], 'id' => $id))->find();
		if (empty($commonweal_address)) {
			json_return(1000, '未找到要删除的地址');
		}
		
		if (D('CommonWeal_address')->where(array('id' => $id))->delete()) {
			json_return(0, '删除成功');
		}
		
		json_return(1000, '删除失败');
	}
	
	public function friend_status() {
		$status = intval(trim($_POST['status']));
		$store_id = $this->store_session['store_id'];
	
		$store = M('Store');
		$result = D('Store')->where(array('store_id' => $store_id))->data(array('open_friend' => $status))->save();
		if ($result) {
			json_return(0, '保存成功！');
		} else {
			json_return(4099, '保存失败，请重试！');
		}
	}

    //自动分配订单
    public function assign_auto()
    {
        $store = M('Store')->getStore($this->store_session['store_id']);
        $this->assign('store', $store);
        $this->display();
    }

    public function assign_status()
    {
        $status = intval(trim($_POST['status']));
        $store_id = $this->store_session['store_id'];
        
        $store = M('Store');
        $result = D('Store')->where(array('store_id' => $store_id))->data(array('open_autoassign' => $status))->save();
        if ($result) {
            json_return(0, '保存成功！');
        } else {
            json_return(4099, '保存失败，请重试！');
        }
    }

    //配置 使用本地化物流
    public function local_logistic()
    {
        $store = M('Store')->getStore($this->store_session['store_id']);
        $this->assign('store', $store);
        $this->display();
    }

    public function set_local_logistic()
    {
        $status = intval(trim($_POST['status']));
        $store_id = $this->store_session['store_id'];
        
        $store = M('Store');
        $result = D('Store')->where(array('store_id' => $store_id))->data(array('open_local_logistics' => $status))->save();
        if ($result) {
            json_return(0, '保存成功！');
        } else {
            json_return(4099, '保存失败，请重试！');
        }
    }

    //分配门店库存
    public function set_stock()
    {
        $this->display();
    }

    public function set_admin()
    {
        $store_physical = D('Store_physical')->where(array('store_id'=>$this->store_session['store_id']))->select();
        $this->assign('store_physical',$store_physical);
        $this->display();
    }

    // 添加店铺管理员
    public function add_admin()
    {
        if(IS_POST)
        {
            $user_model = M('User');
            $rbac_model = M('Rbac_action');
            $data_user['nickname'] = $_POST['nickname'];
            $data_user['phone'] = $_POST['phone'];
            $data_user['password'] = md5($_POST['password']);
            $data_user['drp_store_id'] = $this->store_session['store_id']; //用户所属店铺id
            $data_user['item_store_id'] = $_POST['item_store']; //用户管理门店id
            $data_user['type'] = 1; //管理员类型 0 店铺总管理员 1 门店管理员

            $data_goods['goods'] = $_POST['goods'];
            $data_order['order'] = $_POST['order'];
            $data_trade['trade'] = $_POST['trade'];
            if ($user_model->checkUser(array('phone' => $data_user['phone']))) {
               json_return(3,'此手机号已经注册了');
            }

            $user = $user_model->add_user($data_user);

            if($user['err_code'] == 0){
                if(count($data_goods['goods'])>0)
                {
                    foreach($data_goods['goods'] as $val)
                    {
                        $data_goods['uid'] = $user['err_msg']['uid'];
                        $data_goods['goods_control'] = $_POST['goods_control'];
                        $data_goods['goods_action'] = $val;
                        $rbac_good = $rbac_model->add_rbac_goods($data_goods);
                    }
                }
                if(count($data_order['order'])>0)
                {
                    foreach($data_order['order'] as $value)
                    {
                        $data_order['uid'] = $user['err_msg']['uid'];
                        $data_order['order_control'] = $_POST['order_control'];
                        $data_order['order_action'] = $value;
                        $rbac_orders = $rbac_model->add_rbac_order($data_order);
                    }
                }
                if(count($data_trade['trade'])>0)
                {
                    foreach($data_trade['trade'] as $value)
                    {
                        $data_trade['uid'] = $user['err_msg']['uid'];
                        $data_trade['trade_control'] = $_POST['trade_control'];
                        $data_trade['trade_action'] = $value;
                        $rbac_trades = $rbac_model->add_rbac_trade($data_trade);
                    }
                }
                json_return(0,'添加成功');
            }else{
                json_return(1,'添加失败');
            }
        }

    }

    //店铺管理员列表
    private function _list_admin_content()
    {
        $store_admin_list = D('User')->where(array(
            'drp_store_id'=>$this->store_session['store_id'],
            'type' => 1
        ))->select();
        foreach($store_admin_list as $admin)
        {
            $store_physical_name[$admin['uid']] = D('Store_physical')->where(array('pigcms_id'=>$admin['item_store_id']))->find();
        }

        $this->assign('store_admin_list',$store_admin_list);
        $this->assign('store_physical_name',$store_physical_name);
    }

    private function set_admin_content()
    {
        $store_physical = D('Store_physical')->where(array('store_id'=>$this->store_session['store_id']))->select();
        $this->assign('store_physical',$store_physical);
    }

    private function store_admin_edit()
    {
        $uid = $_POST['uid'];
        $goodsMethod = D('Rbac_action')->where(array(
            'uid'=>$uid,
        ))->select();

        $userInfo= D('User')->where(array(
            'uid'=>$uid,
        ))->find();

        $methodList = array();
        foreach($goodsMethod as $method)
        {
            $methodList[$method['controller_id']][] = $method['action_id'];
        }

        $store_physical_name = D('Store_physical')->where(array('pigcms_id'=>$userInfo['item_store_id']))->find();
        $store_physical = D('Store_physical')->where(array('store_id'=>$this->store_session['store_id']))->select();
        $this->assign('store_physical',$store_physical);
        $this->assign('userInfo',$userInfo);
        $this->assign('store_physical_name',$store_physical_name);
        $this->assign('methodList',$methodList);
    }
    public function store_edit()
    {
        if(IS_POST)
        {
            $user_model = M('User');
            $rbac_model = M('Rbac_action');
            $data_user['uid'] = $_POST['uid'];
            $data_user['type'] = 1;
            $data_user['nickname'] = $_POST['nickname'];
            $data_user['phone'] = $_POST['phone'];

            $data_user['drp_store_id'] = $this->store_session['store_id']; //用户所属店铺id
            $data_user['item_store_id'] = $_POST['item_store']; //用户管理门店id
            $data_user['type'] = 1; //管理员类型 0 店铺总管理员 1 门店管理员

            $data_goods['goods'] = $_POST['goods'];
            $data_order['order'] = $_POST['order'];
            $data_trade['trade'] = $_POST['trade'];

            // 去重
            $whereQ = "`phone` = ".$data_user['phone']." AND `uid` != ".$data_user['uid'];
            if (D('User')->where($whereQ)->find()) {
               json_return(3, '此手机号已经注册了');
            }

            $user = $user_model->edit_user($data_user);

            if($user['err_code'] == 0){
                if(count($data_goods['goods'])>0)
                {
                    $goods_condition['uid'] =  $data_user['uid'];
                    $goods_condition['controller_id'] = $_POST['goods_control'];
                    $id = $rbac_model->delete_action($goods_condition);
                    foreach($data_goods['goods'] as $val)
                    {
                        $data_goods['uid'] =  $data_user['uid'];
                        $data_goods['goods_control'] = $_POST['goods_control'];
                        $data_goods['goods_action'] = $val;
                        $rbac_good = $rbac_model->add_rbac_goods($data_goods);
                    }
                }
                if(count($data_order['order'])>0)
                {
                    $order_condition['uid'] =  $data_user['uid'];
                    $order_condition['controller_id'] = $_POST['order_control'];
                    $id = $rbac_model->delete_action($order_condition);
                    foreach($data_order['order'] as $value)
                    {
                        $data_order['uid'] =  $data_user['uid'];
                        $data_order['order_control'] = $_POST['order_control'];
                        $data_order['order_action'] = $value;
                        $rbac_orders = $rbac_model->add_rbac_order($data_order);
                    }
                }
                if(count($data_trade['trade'])>0)
                {
                    $trade_condition['uid'] =  $data_user['uid'];
                    $trade_condition['controller_id'] = $_POST['trade_control'];
                    $id = $rbac_model->delete_action($trade_condition);
                    foreach($data_trade['trade'] as $value)
                    {
                        $data_trade['uid'] =  $data_user['uid'];
                        $data_trade['trade_control'] = $_POST['trade_control'];
                        $data_trade['trade_action'] = $value;
                        $rbac_trades = $rbac_model->add_rbac_trade($data_trade);
                    }
                }
                json_return(0,'修改成功');
            }else{
                json_return(1,'修改失败');
            }
        }
    }

    public function store_admin_del()
    {
        if(IS_POST){
            $user = D('User');
            $condition_store_admin['uid'] = $_POST['uid'];
            $condition_store_admin['type']  = 1;
            if($user->where($condition_store_admin)->delete()){
                //D('Store')->where(array('store_id'=>$this->store_session['store_id']))->setDec('physical_count');
                json_return(0,'删除成功');
            }else{
                json_return(1,'删除失败');
            }
        }else{
            json_return(1,'非法访问！');
        }
    }


}
?>
<?php
/**
 * 秒杀功能
 */
class seckill_controller extends base_controller {
    // 加载
    public function load() {
        $action = strtolower(trim($_POST['page']));
        if (empty($action)) pigcms_tips('非法访问！', 'none');

        switch ($action) {
            case 'seckill_list' :
                $this->_seckill_list();
                break;
            case 'edit' :
                $this->_edit();
                break;
            case 'info' :
                $this->_info();
                break;
            case 'order' :
                $this->_order();
            default:
                break;
        }

        $this->display($_POST['page']);
    }

    public function seckill_index() {
        $this->display();
    }

    public function add() {
        $name = $_POST['name'];
        $product_id = $_POST['product_id'];
        $sku_id = !empty($_POST['sku_id']) ? $_POST['sku_id'] : 0;
        $start_time = $_POST['start_time'];
        $end_time = $_POST['end_time'];
        $seckill_price = $_POST['seckill_price'];
        $description = $_POST['description'];
        $reduce_point = $_POST['reduce_point'];
        $is_subscribe = $_POST['is_subscribe'];
        $preset_time = $_POST['preset_time'];

        if (empty($name)) {
            json_return(1000, '活动名称不能为空');
        }

        if (empty($product_id)) {
            json_return(1000, '请选择秒杀的产品');
        }

        if (empty($start_time)) {
            json_return(1000, '秒杀开始时间不能为空');
        }

        if (empty($end_time)) {
            json_return(1000, '秒杀结束时间不能为空');
        }

        $product = D('Product')->where(array('product_id' => $product_id, 'store_id' => $this->store_session['store_id'], 'status' => array('!=', 2)))->find();
        if (empty($product)) {
            json_return(1000, '未找要秒杀的产品');
        }

        $data = array();
        $data['name'] = $name;
        $data['store_id'] = $this->store_session['store_id'];
        $data['product_id'] = $product_id;
        $data['sku_id'] = $sku_id;
        $data['seckill_price'] = !empty($seckill_price) ? $seckill_price : $product['price'];
        $data['start_time'] = strtotime($start_time);
        $data['end_time'] = strtotime($end_time);
        $data['description'] = $description;
        $data['reduce_point'] = $reduce_point;
        $data['is_subscribe'] = $is_subscribe;
        $data['add_time'] = time();
        $data['update_time'] = time();
        $data['is_subscribe'] = $is_subscribe;
        $data['preset_time'] = $preset_time;


        $seckill = D('Seckill')->data($data)->add();

        if (!$seckill) {
            json_return(1000, '添加失败，请重试');
        }else{
            json_return(0, url('seckill:seckill_index'));
        }
    }

    private function _seckill_list() {
        $type = $_REQUEST['type'];
        $keyword = $_REQUEST['keyword'];

        $type_arr = array('future', 'on', 'end', 'all');
        if (!in_array($type, $type_arr)) {
            $type = 'all';
        }

        $where = array();
        $where['store_id'] = $_SESSION['store']['store_id'];
        $where['delete_flag'] = 0;
        $order_by_field = 'pigcms_id desc';

        if (!empty($keyword)) {
            $where['name'] = array('like', '%' . $keyword . '%');
        }

        $time = time();
        if ($type == 'future') {
            $where['start_time'] = array('>', $time);
        } else if ($type == 'on') {
            $where['start_time'] = array('<', $time);
            $where['end_time'] = array('>', $time);
        } else if ($type == 'end') {
            $where = "`store_id` = '" . $_SESSION['store']['store_id'] . "' AND (`end_time` < '" . $time . "' OR `status` = '2')";
        }

        $seckill_model = M('Seckill');
        $count = $seckill_model->getCount($where);

        import('source.class.user_page');
        import('source.class.OrderPay');
        $page = new Page($count, 10);

        $seckills = $seckill_model->getList($where, $order_by_field, $page->listRows, $page->firstRow);

        $seckill_list = array();
        foreach($seckills as $seckill){

            $order_pay = new OrderPay();

            /* 查询商品 */
            if(!empty($seckill['sku_id'])){
                $sql = "select * from ".option('system.DB_PREFIX')."product as pro,".option('system.DB_PREFIX')."product_sku as sku where pro.product_id=sku.product_id and pro.product_id={$seckill['product_id']} and sku.sku_id={$seckill['sku_id']}";
                $productInfo = D('Product')->query($sql);
                $productInfo = $productInfo[0];
            }else if(empty($seckillInfo['sku_id'])){
                $productInfo = D('Product')->where(array('product_id'=>$seckill['product_id']))->find();
            }

            $check_inventory = $order_pay->check_inventory($seckill['pigcms_id'],$productInfo['quantity'],0,$type=1);
            $sales = $check_inventory['is_flag'] == true ? 1 : $check_inventory['temporary_num'];
            $seckill_list[] = array(
                'product_name' => $productInfo['name'],
                'product_url' => $productInfo['url'],
                'product_image' => getAttachmentUrl($productInfo['image'],false),
                'product_id' => $productInfo['product_id'],
                'seckill_name' => $seckill['name'],
                'start_time' => $seckill['start_time'],
                'end_time' => $seckill['end_time'],
                'sales_volume' => $seckill['sales_volume'] + $sales,
                'is_subscribe' => $seckill['is_subscribe'],
                'status' => $seckill['status'],
                'pigcms_id' => $seckill['pigcms_id'],
            );
        }

        $this->assign('keyword', $keyword);
        $this->assign('type', $type);
        $this->assign('page', $page->show());
        $this->assign('seckill_list', $seckill_list);
    }

    public function edit() {

        $seckill_id = intval($_POST['pigcms_id']);
        $name = $_POST['name'];
        $product_id = $_POST['product_id'];
        $start_time = $_POST['start_time'];
        $end_time = $_POST['end_time'];
        $seckill_price = $_POST['seckill_price'];
        $description = $_POST['description'];
        $reduce_point = $_POST['reduce_point'];
        $is_subscribe = $_POST['is_subscribe'];
        $preset_time = $_POST['preset_time'];

        if (empty($name)) {
            json_return(1000, '秒杀名称不能为空');
        }

        if (empty($product_id)) {
            json_return(1000, '请选择参与秒杀的产品');
        }

        if (empty($start_time)) {
            json_return(1000, '秒杀开始时间不能为空');
        }

        if (empty($end_time)) {
            json_return(1000, '秒杀结束时间不能为空');
        }

        if (empty($seckill_id)) {
            json_return(1000, '缺少最基本的参数ID');
        }

        $seckill = D('Seckill')->where(array('pigcms_id' => $seckill_id, 'store_id' => $this->store_session['store_id'], 'delete_flag' => 0))->find();

        if (empty($seckill)) {
            json_return(1000, '未找到要修改的秒杀');
        }

        if ($seckill['start_time'] < time()) {
            json_return(1000, '此秒杀活动已开始，不能修改');
        }

        $product = D('Product')->where(array('product_id' => $product_id, 'store_id' => $this->store_session['store_id'], 'status' => array('!=', 2)))->find();
        if (empty($product)) {
            json_return(1000, '未找要参加秒杀的产品');
        }

        $data = array();
        $data['name'] = $name;
        $data['store_id'] = $this->store_session['store_id'];
        $data['product_id'] = $product_id;
        $data['start_time'] = strtotime($start_time);
        $data['end_time'] = strtotime($end_time);
        $data['description'] = $description;
        $data['seckill_price'] = $seckill_price;
        $data['reduce_point'] = $reduce_point;
        $data['is_subscribe'] = $is_subscribe;
        $data['preset_time'] = $preset_time;

        D('Seckill')->where(array('pigcms_id' => $seckill_id))->data($data)->save();

        json_return(0, url('seckill:seckill_index'));
    }

    // 使失效
    public function disabled() {
        $seckill_id = intval($_GET['seckill_id']);

        if (empty($seckill_id)) {
            json_return(1001, '缺少最基本的参数ID');
        }

        $seckill = D('Seckill')->where(array('pigcms_id' => $seckill_id, 'store_id' => $this->store_session['store_id']))->find();
        if (empty($seckill)) {
            json_return(1001, '未找到相应的秒杀活动');
        }

        if ($seckill['status'] == 2) {
            json_return(1000, '此活动已失效，无须再次操作');
        }

        if ($seckill['end_time'] < time()) {
            json_return(1000, '此活动已经结束，不能进行失效操作');
        }

        $data = array();
        $data['status'] = 2;
        $result = D('Seckill')->where(array('pigcms_id' => $seckill_id))->data(array('status' => 2, 'update_time' => time()))->save();

        if ($result) {
            json_return(0, '操作完成');
        } else {
            json_return(1000, '操作失败');
        }
    }

    // 删除
    public function delete() {
        $seckill_id = intval($_GET['seckill_id']);

        if (empty($seckill_id)) {
            json_return(1001, '缺少最基本的参数ID');
        }

        $seckill = D('Seckill')->where(array('pigcms_id' => $seckill_id, 'store_id' => $this->store_session['store_id'], 'delete_flag' => 0))->find();
        if (empty($seckill)) {
            json_return(1001, '未找到相应的团购活动');
        }

        if ($seckill['start_time'] < time()) {
            json_return(1000, '活动已开始，不能进行删除操作');
        }

        D('Seckill')->where(array('pigcms_id' => $seckill_id))->data(array('delete_flag' => 1))->save();
        json_return(0, '删除完成');
    }

    private function _edit() {
        $seckill_id = $_POST['pigcms_id'];

        if (empty($seckill_id)) {
            echo '缺少最基本的参数ID';
            exit;
        }

        $seckill = D('Seckill')->where(array('pigcms_id' => $seckill_id, 'store_id' => $this->store_session['store_id'], 'delete_flag' => 0))->find();

        if (empty($seckill)) {
            echo '未找到要修改的秒杀';
            exit;
        }

        if ($seckill['start_time'] < time()) {
            echo '秒杀已开始，不能编辑';
            exit;
        }

        $product = D('Product')->where(array('product_id' => $seckill['product_id'], 'store_id' => $this->store_session['store_id'], 'status' => array('!=', 2)))->field('`product_id`, `name`, `image`')->find();
        if (!empty($product)) {
            $product['image'] = getAttachmentUrl($product['image']);
            $product['url'] = option('config.wap_site_url') . '/good.php?id=' . $product['product_id'];
        }

        $this->assign('seckill', $seckill);
        $this->assign('product', $product);
    }
}
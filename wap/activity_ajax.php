<?php
/**
 *  活动列表
 */
require_once dirname(__FILE__) . '/global.php';

$type = isset($_POST['table_name']) ? trim($_POST['table_name']) : '';
$type = in_array($type, array('tuan', 'presale', 'bargain', 'seckill', 'crowdfunding', 'unitary', 'cutprice', 'lottery')) ? $type : 'tuan';

$activity_arr = array('tuan', 'presale', 'bargain', 'seckill', 'crowdfunding', 'unitary', 'cutprice', 'lottery');

$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$pagesize = isset($_POST['pagesize']) ? intval($_POST['pagesize']) : 10;
$orderBy = isset($_POST['order']) ? htmlspecialchars($_POST['order']) : 'asc';

$page = max(1, $page);
$offset = ($page - 1) * $pagesize;

$order = 'id DESC';
if ($orderBy == 'asc') {
	$order = 'id ASC';
}

$time = time();
$activity_list = array();
if ($type == 'tuan') {
	$where = "t.status = 1 AND t.start_time <= '" . $time . "' AND t.end_time >= '" . $time . "' AND t.delete_flg = 0 AND p.status = 1";
	$field = 't.*, t.start_price as price, p.name as title, p.image as pic, p.price as original_price';
	$activity_list = D('Tuan AS t')->join('Product AS p ON t.product_id = p.product_id')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		$activity['pic'] = getAttachmentUrl($activity['pic']);
		$activity['table_name'] = 'tuan';
		$activity['actname'] = '拼团';
		$activity['endtime'] = $activity['end_time'] - $time;
		$activity['joinurl'] = option('config.site_url') . '/webapp/groupbuy/#/details/' . $activity['id'];
		$activity['joincount'] = $activity['count'];
	}
} else if ($type == 'presale') {
	$where = "pr.is_open = 1 AND pr.starttime <= '" . $time . "' AND pr.endtime >= '" . $time . "' AND p.status = 0";
	$field = 'pr.*, p.name as title, p.image as pic';
	$activity_list = D('Presale AS pr')->join('Product AS p ON pr.product_id = p.product_id')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		$order_count = M('Order')->getActivityOrderCount(6, $activity['id']);
		unset($activity['endtime']);
		
		$activity['pic'] = getAttachmentUrl($activity['pic']);
		$activity['table_name'] = 'presale';
		$activity['actname'] = '预售';
		// $presale['endtime'] = $presale['endtime'] - $time;
		$activity['joinurl'] = option('config.wap_site_url') . '/presale.php?id='. $activity['id'];;
		$activity['joincount'] = $order_count + 0;
	}
} else if ($type == 'bargain') {
	$order = 'pigcms_id DESC';
	if ($orderBy == 'asc') {
		$order = 'pigcms_id ASC';
	}
	
	$where = "b.delete_flag = 0 AND b.state = 1 AND p.status = 1";
	$field = 'b.*, p.name as title, p.image as pic';
	$activity_list = D('Bargain AS b')->join('Product AS p ON b.product_id = p.product_id')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		$order_count = D('Bargain_kanuser')->where(array('bargain_id' => $activity['pigcms_id']))->count("pigcms_id");
		unset($activity['endtime']);
		
		$activity['id'] = $activity['pigcms_id'];
		$activity['price'] = $activity['minimum'];
		$activity['pic'] = getAttachmentUrl($activity['pic']);
		$activity['table_name'] = 'bargain';
		$activity['actname'] = '砍价';
		$activity['joinurl'] = option('config.wap_site_url') . '/bargain.php?action=detail&id=' . $activity['pigcms_id'] . '&store_id=' . $activity['store_id'];
		$activity['joincount'] = $order_count + 0;
	}
} else if ($type == 'seckill') {
	$order = 'pigcms_id DESC';
	if ($orderBy == 'asc') {
		$order = 'pigcms_id ASC';
	}
	
	$where = "s.delete_flag = 0 AND s.status = 1 AND p.status = 1 AND s.start_time <= '" . $time . "' AND s.end_time >= '" . $time . "'";
	$field = 's.*, p.name as title, p.image as pic, p.price as original_price';
	$activity_list = D('Seckill AS s')->join('Product AS p ON s.product_id = p.product_id')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		$seckill_persons = D('Seckill_user')->where(array('seckill_id' => $activity['pigcms_id']))->count('pigcms_id');
		unset($activity['endtime']);
	
		$activity['id'] = $activity['pigcms_id'];
		$activity['price'] = $activity['seckill_price'];
		$activity['pic'] = getAttachmentUrl($activity['pic']);
		$activity['table_name'] = 'seckill';
		$activity['actname'] = '秒杀';
		$activity['joinurl'] = option('config.wap_site_url') . '/seckill.php?seckill_id=' . $activity['pigcms_id'];
		$activity['joincount'] = $seckill_persons + 0;
	}
} else if ($type == 'crowdfunding') {
	$order = 'product_id DESC';
	if ($orderBy == 'asc') {
		$order = 'product_id ASC';
	}
	
	$where = "status in (2, 4)";
	$activity_list = D('Zc_product')->where($where)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		unset($activity['endtime']);
		
		$per = 100;
		if (!empty($activity['amount'])) {
			$per = min(100, round($activity['collect'] / $activity['amount'] * 100, 2));
		}
		
		$activity['id'] = $activity['product_id'];
		$activity['title'] = $activity['productName'];
		$activity['price'] = $activity['amount'];
		$activity['pic'] = getAttachmentUrl($activity['productImageMobile']);
		$activity['table_name'] = 'crowdfunding';
		$activity['actname'] = '众筹';
		$activity['joinurl'] = option('config.site_url') . '/webapp/chanping/index.html#/view/' . $activity['product_id'];
		$activity['price_count'] = $activity['people_number'] + 0;
		$activity['percent'] = $per . '%';
		$activity['balance'] = max(0, $activity['amount'] - $activity['collect']);
	}
} else if ($type == 'unitary') {
	$where = "u.state = 1 AND p.status = 1";
	$field = 'u.*, p.name as title, p.image as pic, p.price as original_price';
	$activity_list = D('Unitary AS u')->join('Product AS p ON u.product_id = p.product_id')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		$unitary_person = D('Unitary_lucknum')->where(array('unitary_id' => $activity['id']))->count('id');
		unset($activity['endtime']);
	
		$activity['price'] = $activity['price'];
		$activity['pic'] = getAttachmentUrl($activity['pic']);
		$activity['table_name'] = 'unitary';
		$activity['actname'] = '夺宝';
		$activity['joinurl'] = option('config.site_url') . '/webapp/snatch/#/main/' . $activity['id'];
		$activity['price_count'] = $unitary_person + 0;
		$activity['percent'] = min(100, round($unitary_person / $activity['total_num'] * 100, 2)) . '%';
		$activity['balance'] = max(0, $activity['total_num'] - $unitary_person);
	}
} else if ($type == 'cutprice') {
	$order = 'pigcms_id DESC';
	if ($orderBy == 'asc') {
		$order = 'pigcms_id ASC';
	}
	$where = "c.state = 0 AND c.endtime >= '" . $time . "' AND p.status = 1";
	$field = 'c.*, p.name as title, p.image as pic, p.price as original_price';
	$activity_list = D('Cutprice AS c')->join('Product AS p ON c.product_id = p.product_id')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	
	foreach ($activity_list as &$activity) {
		$part_in = D('Cutprice_record')->where(array('cutprice_id' => $activity['pigcms_id']))->count('id');
		unset($activity['endtime']);
		
		$cha = time() - $activity['starttime'];
		$activity['nowprice'] = $activity['startprice'];
		if($cha > 0){
			$chaprice = (floor($cha / 60 / $activity['cuttime'])) * $activity['cutprice'];
			if($activity['inventory'] > 0 && ($activity['startprice'] - $chaprice) > $activity['stopprice']){
				$activity['nowprice'] = $activity['startprice'] - $chaprice;
			}
		}
		
		$activity['id'] = $activity['pigcms_id'];
		$activity['price'] = $activity['nowprice'];
		$activity['original_price'] = $activity['startprice'];
		$activity['pic'] = getAttachmentUrl($activity['pic']);
		$activity['table_name'] = 'cutprice';
		$activity['actname'] = '降价拍';
		$activity['joinurl'] = option('config.wap_site_url') . '/cutprice.php?action=detail&store_id=' . $activity['store_id'] . '&id=' . $activity['pigcms_id'];
		$activity['joincount'] = $part_in + 0;
	}
} else if ($type == 'lottery') {
	$where = "status = 0 AND starttime <= '" . $time . "' AND endtime >= '" . $time . "'";
	$field = '*';
	
	$type_arr = array(1 => array('大转盘', 'Lottery'), 2 => array('九宫格', 'Jiugong'), 3 => array('刮刮卡', 'Guajiang'), 4 => array('水果机', 'LuckyFruit'), 5 => array('砸金蛋', 'GoldenEgg'));
	
	$activity_list = D('Lottery')->where($where)->field($field)->order($order)->limit($offset . ',' . $pagesize)->select();
	foreach ($activity_list as &$activity) {
		$lottery_prize = D('Lottery_prize')->where(array('lottery_id' => $activity['id'], 'prize_type' => 1))->find();
		
		$activity['endtime'] = $activity['endtime'] - $time;
		$activity['pic'] = getAttachmentUrl($activity['backgroundThumImage']);
		$activity['price'] = $lottery_prize['product_name'];
		$activity['table_name'] = isset($type_arr[$activity['type']]) ? $type_arr[$activity['type']][1] : Lottery;;
		$activity['actname'] = isset($type_arr[$activity['type']]) ? $type_arr[$activity['type']][0] : '抽奖';
		$activity['joinurl'] = option('config.wap_site_url') . '/lottery.php?action=detail&id=' . $activity['id'];
	}
}

$json_return = array(
		'data' => $activity_list,
		'page' => $page + 1,
		'errcode' => 0,
);
if(!headers_sent()) header('Content-type:application/json');
exit(json_encode($json_return, true));
<?php

/**
 *  活动列表
 */
require_once dirname(__FILE__) . '/global.php';

$table_name = isset($_GET['table_name']) ? trim($_GET['table_name']) : 'tuan';

$activity_arr = array('tuan' => '拼团', 'presale' => '预售', 'bargain' => '砍价', 'seckill' => '秒杀', 'crowdfunding' => '众筹', 'unitary' => '一元夺宝', 'cutprice' => '降价拍', 'lottery' => '抽奖专场');

$pageTitle = isset($activity_arr[$table_name]) ? $activity_arr[$table_name] : '活动列表';
include display('activity');
echo ob_get_clean();
?>
<?php
	require_once dirname(__FILE__).'/global.php';

	import('source.class.Drp');
	import('source.class.Factory');
	import('source.class.MessageFactory');

    $store_supplier = M('Store_supplier');
	$common_data = M('Common_data');
	$sale_category = M('Sale_category');

	//访问店铺id
	$supplier_id = intval($_POST['store_id']);
	//访问店铺
	$store = D('Store')->field('*')->where(array('store_id' => $supplier_id))->find();
	//分销级别
	$drp_level = $store['drp_level'];
	//供货商id
	$root_supplier_id = !empty($store['root_supplier_id']) ? $store['root_supplier_id'] : $supplier_id;
	//用户id
	$uid = intval($_POST['uid']);
	//用户信息
    $user = D('User')->field('*')->where(array('uid' => $_POST['uid']))->find();
	$name = !empty($user['nickname']) ? $user['nickname'] : $store['name'] . '分店';
	$desc = $name;
	$avatar = getAttachmentUrl($user['avatar']);
	$logo = $avatar;
	$openid = $user['openid'];

	//我的店铺
	$my_store_id = 0;
	Drp::init();
	$visitor = Drp::checkID($supplier_id, $uid);
	if (!empty($visitor['data']['store_id'])) { //用户有自己店铺
		$store_id = $visitor['data']['store_id'];
		$name = $visitor['data']['name'];
		$logo = $visitor['data']['logo'];
		$desc = !empty($visitor['data']['intro']) ? $visitor['data']['intro'] : $name;
		$my_store_id = $store_id;
	}

    if (empty($my_store_id)) {
		$linkname = $user['nickname'];
		$tel = $user['phone'];
		$drp_level += 1; //分销级别
	
		$data = array();
		$data['uid'] = $uid;
		$data['name'] = $name;
		$data['sale_category_id'] = $store['sale_category_id'];
		$data['sale_category_fid'] = $store['sale_category_fid'];
		$data['linkman'] = $linkname;
		$data['tel'] = $tel;
		$data['status'] = 1;
		$data['qq'] = '';
		$data['drp_supplier_id'] = $supplier_id;
		$data['root_supplier_id'] = $root_supplier_id;
		$data['date_added'] = time();
		$data['drp_level'] = $drp_level;
		$data['logo'] = $avatar;
		$data['open_nav'] = $store['open_nav'];
		$data['bind_weixin'] = 0;
		$data['open_drp_diy_store'] = 0;
		$data['drp_diy_store'] = 0;
		$data['is_fanshare_drp'] = $store['is_fanshare_drp'];
		if (!empty($store['open_drp_approve'])) {
		    $data['drp_approve'] = 0; //需要审核
		}
		//检测是否可用分销团队，如果有则加入
		if (M('Drp_team')->checkDrpTeam($supplier_id, true)) {
			//一级分销商（团队所有者）
			$first_seller_id = M('Store_supplier')->getFirstSeller($supplier_id);
			$first_seller = D('Store')->field('drp_team_id')->where(array('store_id' => $first_seller_id))->find();
			if (!empty($first_seller['drp_team_id'])) {
				$data['drp_team_id'] = $first_seller['drp_team_id'];
			}
		}
		$result = M('Store')->create($data);
		if (!empty($result['err_code'])) { //店铺添加成功
		    $store_id = $result['err_msg']['store_id']; //分销商id

			//平台店铺数统计
			$common_data->setStoreQty();
		    //用户店铺数加1
		    M('User')->setStoreInc($uid);
		    //设置为卖家
		    M('User')->setSeller($uid, 1);

			//团队成员数+1
			if (!empty($data['drp_team_id'])) {
				M('Drp_team')->setMembersInc($data['drp_team_id']);
			}

		    //主营类目店铺数加1
		    $sale_category->setStoreInc($store['sale_category_id']);
		    $sale_category->setStoreInc($store['sale_category_fid']);

			//分销关系
		    if (!empty($store['drp_supplier_id'])) {
				$seller = $store_supplier->getSeller(array('seller_id' => $supplier_id, 'type' => 1)); //获取上级分销商信息
				$supply_chain = $seller['supply_chain'] . ',' . $supplier_id;
			} else {
				$supply_chain = '0,' . $supplier_id;
			}
			$store_supplier->add(array('supplier_id' => $supplier_id, 'seller_id' => $store_id, 'supply_chain' => $supply_chain, 'level' => $drp_level, 'type' => 1, 'root_supplier_id' => $root_supplier_id)); //添加分销关联关系

			//已关注过店铺公众号
			if (M('Subscribe_store')->subscribed($uid, $root_supplier_id)) {
				import('source.class.Points');
				Points::subscribe($uid, $root_supplier_id, $store_id, true);
			} else {
				//普通注册送积分
				import('source.class.Points');
				Points::drpStore($uid, $root_supplier_id, $store_id);
			}

		    //发送模板消息
		    $template_data = array(
		    		'wecha_id' => $user['openid'],
		    		'first'    => '您好，恭喜您分享店铺成为分销商。',
		    		'keyword1' => $name,
		    		'keyword2' => date('Y-m-d H:i:s'),
		    		'keyword3' => date('Y-m-d H:i:s'),
		    		'remark'   => '状态：' . "待审核分销商",
		    		 'href'    => option('config.wap_site_url') . '/home.php?id=' . $store_id,
		    );

		    $params['template'] = array('template_id' => 'OPENTM207126233', 'template_data' => $template_data);
		    $moban = array('TemplateMessage');
		    MessageFactory::method($params, $moban);

			//更新平台分销商数量
		    $common_data->setDrpSellerQty();
		}
    } else {
		$store_id = $my_store_id;
		$store = D('Store')->where(array('store_id' => $my_store_id))->find();
		$name = $store['name'];
		$logo = getAttachmentUrl($store['logo']);
	}

	echo json_encode(array(
		'title'  => $name,
		'imgUrl' => $logo,
		'desc'   => $desc,
		'link'   => option('config.wap_site_url') . '/home.php?id=' . $store_id . '&uid=' . $uid
	));

	exit;

?>
<?php
/**
 * 配置文件
 */
if(!defined('PIGCMS_PATH')) exit('deny access!');

return array(
	'DB_HOST'=>'127.0.0.1',
	'DB_NAME'=>'xinweidian',
	'DB_USER'=>'xinweidian',
	'DB_PWD'=>'xinweidian',
	'DB_PORT'=>'3306',
	'DB_PREFIX'=>'pigcms_',
	'STATIC_RESOURCE'=>'2',
);
?>
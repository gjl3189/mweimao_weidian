<?php
class api_controller extends base_controller{
	public function __construct(){
		parent::__construct();
		$this->user_session = $_SESSION['wap_user'];
		$this->user_invest=D('User_apply_invest')->where(array('uid'=>$this->user_session['uid']))->find();
	}
	public function document(){
		$this->display();
	}
	// 首页
	public function index(){
		$databases_project = D('Project');
		$where="`isShow` = 1";
		//  1.审核成功 路演中 2.审核失败 3.融资中 4.融资成功 5.融资失败',
		if($_GET['status'] == 1){
			$where .= " AND `status` =1";
		}elseif($_GET['status'] == 3){
			$where .= " AND `status` =3";
		}elseif($_GET['status'] == 4){
			$where .= " AND `status` =4";
		}else{
			$where .= " AND (`status` = 1 OR `status`=3 OR `status` =4)";
		}
		$count=$databases_project->where($where)->count();
		$page=!empty($_GET['page']) ? intval($page) : 1;
		$firstRows=($page-1)*12;
		$project_list=$databases_project->where($where)->limit($firstRows.',12')->select();
		$uid = $_SESSION['wap_user']['uid'];
		if(!empty($project_list)){
			foreach ($project_list as $k => $v) {
				$wh=array();
				if(empty($_SESSION['wap_user'])){
					$project_list[$k]['isprais']    =0;
				}else{
					$isprais     = D('Project_praise')->where(array('project_id'=>$v['project_id'],'uid'=>$uid,'type'=>1))->find();
					$project_list[$k]['isprais']     = !empty($isprais)     ? 1: 0;
					unset($isprais);
				}
			}
		}
		foreach($project_list as $k=>$v){
			$project_list[$k]['projectFirstImg'] = getAttachmentUrl($v['projectFirstImg']);
			$project_list[$k]['listImg'] = getAttachmentUrl($v['listImg']);
		}
		$project_list= !empty($project_list) ? $project_list : array();
		$isTrue      = !empty($project_list) ? true          : false;
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"project_list":'.json_encode($project_list).',
				"page_count":'.json_encode($count).',
				"next_page":'.json_encode($isTrue).'
			}
		}';
		echo $tmp;exit;
	}
	// 首页幻灯片
	public function indexSlide(){
		$slide_list=D('Invest_slide')->where(array('class'=>0))->order('`id` DESC')->select();
		foreach ($slide_list as $k=>$v){
			$slide_list[$k]['url'] = getAttachmentUrl($v['url']);
		}
		$slide_count = count($slide_list);
		$tmp='{
			"err_code":0,
			"err_msg" :{
		    	"slide_list":'.json_encode($slide_list).',
		    	"slide_count":'.json_encode($slide_count).'
		}
		}';
		echo $tmp;exit;
	}
	// 项目详情页，项目id必传
	public function projectInfo(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
                $uid = $_SESSION['wap_user']['uid'];
		$tmp='';
		if(isset($_GET['id']) && !empty($_GET['id']) ){
			$id=intval($_GET['id']);
			$project_info=D('Project')->where(array('project_id'=>$id))->find();
			$isattention=D('Project_attention')->where(array('project_id'=>$id,'uid'=>$uid,'type'=>1))->find();
			$isprais    =D('Project_praise')->where(array('project_id'=>$id,'uid'=>$uid,'type'=>1))->find();
			$project_info['isattention'] = !empty($isattention) ? 1 : 0;
			$project_info['isprais']    = !empty($isprais)     ? 1 : 0;
			$team_list=D('Project_team')->where(array('project_id'=>$id))->select();
			$order_list=D('Invest_order')->where(array('project_id'=>$id,'status'=>2))->select();
			$uid_arr=array();
			$project_info['max_order']=$project_info['min_order']=0;
			if(!empty($order_list)){
				foreach ($order_list as $k => $v) {
					$uid_arr[]=$v['uid'];
					if($v['type']==2){
						$project_info['max_order']++;
					}elseif($v['type']==1){
						$project_info['min_order']++;
					}
				}
			}
                        $project_info['projectFirstImg'] = getAttachmentUrl($project_info['projectFirstImg']);
                        $project_info['listImg'] = getAttachmentUrl($project_info['listImg']);
                        foreach($team_list as $k=>$v){
                            $team_list[$k]['teamImg'] = getAttachmentUrl($v['teamImg']);
                        }

			$uid_arr=array_unique($uid_arr);
			$investor_list = array();
			if(!empty($uid_arr)){
				$where_invest='`uid` in('.implode(',',$uid_arr).')';
				$investor_list=D('User_apply_invest')->where($where_invest)->select();
				$investor_list = !empty($investor_list) ? $investor_list : array();
	                        foreach($investor_list as $k=>$v){
	                            $investor_list[$k]['card_img'] = getAttachmentUrl($v['card_img']);
	                        }
			}
			$tmp='{
				"err_code":0,
				"err_msg":{
					"project_info":'.json_encode($project_info).',
					"team_list":'.json_encode($team_list).',
					"investore_list":'.json_encode($investor_list).'
				}
			}';
		}else{
			$tmp='{
				"err_code":102,
				"err_msg":"参数错误",
			}';
		}
		echo $tmp;exit;
	}
	// 检测用户是否登陆
	public function regLogin(){
		$tmp=array();
		if(empty($_SESSION['wap_user'])){
			$tmp= array('err_code'=>101,'err_msg'=>'未登陆');
		}else{
			$tmp=array('err_code'=>0,'err_msg'=>'登陆成功');
		}
		return $tmp;
	}
	/**
	 * typeInvestor 投资人类型 1普通投资人   2项目领投人
	 */
	protected function checkInvestor($typeInvestor=1){
		if(empty($this->user_invest)){
			return array('err_code'=>102,'err_msg'=>'您没有投资人资格，请先去申请');
		}
		if($this->user_invest['status']==97){
			return array('err_code'=>103,'err_msg'=>'您的申请已被拒绝，请重新申请');
		}
		if($this->user_invest['status']==98){
			return array('err_code'=>104,'err_msg'=>'您的申请正在审核中，请耐心等候');
		}
		if($this->user_invest['status']==99 && $typeInvestor !=2 ){
			return array('err_code'=>0,'err_msg'=>'ok');
		}
		if($typeInvestor==2){
			if($this->user_invest['leader_status']==0){
				return array('err_code'=>106,'err_msg'=>'您还未申请领投人，请先去申请');
			}
			if($this->user_invest['leader_status']==97){
				return array('err_code'=>107,'err_msg'=>'您申请领投人已被拒绝，请重新申请');
			}
			if($this->user_invest['leader_status']==98){
				return array('err_code'=>108,'err_msg'=>'您申请的领投人正在审核中，请耐心等待');
			}
			if($this->user_invest['leader_status']==99){
				return array('err_code'=>0,'err_msg'=>'ok');
			}
		}
	}
	/**
	 * $type 订单类型  小东家1  大东家2 领投人3 订单
	 * $project_id 项目id必传
	 */
	public function investOrder(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
		if(empty($_GET['project_id']) || empty($_GET['type']) ){
			echo json_encode(array('err_code'=>105,'err_msg'=>'提交参数错误'));exit;
		}
		$type=intval($_GET['type']);
		$project_id=intval($_GET['project_id']);
		$proInfo=D('Project')->where(array('project_id'=>$project_id))->find();
		if($type==1 || $type==2){
			$check=$this->checkInvestor(1);
			if( $check['err_code']!=0 ){
				echo json_encode($check);exit;
			}
			$where_order=array();
			$where_order['status']=2;
			$where_order['project_id']=$project_id;
			$where_order['type']=$type;
			$count_order=D('Invest_order')->where($where_order)->count();
			if($proInfo['ckFlag']==1 &&  empty($proInfo['leader_uid'])){
	                        echo json_encode(array('err_code'=>4,'err_msg'=>'该项目还没有领投人'));exit;
			}
			if($type==1 && $count_order>=$proInfo['minShareholder']){
				echo json_encode(array('err_code'=>109,'err_msg'=>'小东家投资人数已达上限'));exit;
			}
			if($type==2 && $count_order>=$proInfo['maxShareholder']){
				echo json_encode(array('err_code'=>111,'err_msg'=>'大东家投资人数已达上限'));exit;
			}
		}else{
			echo json_encode(array('err_code'=>110,'err_msg'=>'类型参数错误'));
		}
		$getAddressDefault = $this->getAddressDefault() ? $this->getAddressDefault() : array();
		$tmp='{
			"err_code":0,
			"err_msg":{
				"user_invest":'.json_encode($this->user_invest).',
				"getAddressDefault":'.json_encode($getAddressDefault).',
				"proInfo":'.json_encode($proInfo).'
			}
		}';
		echo $tmp;exit;
	}
	// 生成订单
	// 提交数据 type project_id intention_amount必传
	public function toOrder(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
                $uid = $_SESSION['wap_user']['uid'];
		if($_POST){
	            	$type=intval($_POST['type']);
			$intention_amount = trim($_POST['intention_amount']);
			$project_id=intval($_POST['project_id']);
			if($type != 1 && $type !=2){
				echo json_encode(array('err_code'=>111,'err_msg'=>'提交类型错误'));exit;
			}
			if(empty($project_id)){
				echo json_encode(array('err_code'=>123,'err_msg'=>'项目id不能为空'));exit;
			}
			$investorInfo=D('User_apply_invest')->where(array('uid'=>$uid))->field('id,uid,name,phone,status,beilv')->find();
			if(empty($investorInfo) || $investorInfo['status']!=99 ){
				echo json_encode(array('err_code'=>112,'err_msg'=>'投资人状态错误，无法进行投资'));exit;
			}
			$money=$intention_amount/$investorInfo['beilv'];
			if(empty($money) || $money!=intval($money)){
				echo json_encode(array('err_code'=>122,'err_msg'=>'意向金数据错误'));exit;
			}
			$proInfo=D('Project')->where(array('project_id'=>$project_id))->field('project_id,realName,projectName,valuation')->find();

			// 收货地址
			$address_default=$this->getAddressDefault();
			import('source.class.area');
			$areaClass = new area();
			$address_tmp=array();
			$address_tmp['address']      =$address_default['address'];
			$address_tmp['province']     =$areaClass->get_name($address_default['province']);
			$address_tmp['province_code']=$address_default['province'];
			$address_tmp['city']         =$areaClass->get_name($address_default['city']);
			$address_tmp['city_code']    =$address_default['city'];
			$address_tmp['area']         =$areaClass->get_name($address_default['area']);
			$address_tmp['area_code']    =$address_default['area'];

			$order=$order_info=array();
			$order['zcpay_no']='ZCPAY_'.date('YmdHis').mt_rand(100000,999999);
			$order['intention_amount']=$intention_amount;
			$order['project_id']=$project_id;
			$order['margin_amount']=round( $intention_amount/$this->user_invest['beilv'],2 );
			$order['time']=$_SERVER['REQUEST_TIME'];
			$order['uid']=$uid;
			$order['type']=$type;//类型区分：大东家小东家
			$order['status']=1;
			$order['address']=serialize($address_tmp);
			$order['address_user']=$address_default['name'];
			$order['address_tel']=$address_default['tel'];

			$orderInfo=D('Invest_order')->where(array('project_id'=>$project_id,'uid'=>$uid))->find();
			if(empty($orderInfo)){
				$lid=D('Invest_order')->data($order)->add();
				$order_info=$order;
			}else{
				unset($order['zcpay_no']);
				$lid=D('Invest_order')->where(array('zcpay_no'=>$orderInfo['zcpay_no']))->data($order)->save();
				$order_info=$orderInfo;
			}
			if( !empty($lid) ){
				$order['order_id'] = !empty($orderInfo['id']) ? intval($orderInfo['id']) : $lastId;
				$order['intention_share']=number_format(($intention_amount/($proInfo['valuation']*10000))*100,2);//份额
				$tmp='{
					"err_code":0,
					"err_msg":{
						"order_info":'.json_encode($order_info).',
						"investorInfo":'.json_encode($investorInfo).',
						"proInfo":'.json_encode($proInfo).'
					}
				}';
				echo $tmp;exit;
			}else{
				echo json_encode(array('err_code'=>110,'err_msg'=>'订单生成失败，请重新提交'));exit;
			}
		}else{
			echo json_encode(array('err_code'=>101,'err_msg'=>'提交方式出错'));exit;
		}
	}
	// 添加投资人
	public function addInvestor(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
		if($_POST){
			$info=array();
			$info['name']=trim($_POST['name']);
			if(!preg_match("/^1[34578]\d{9}$/", trim($_POST['phone']) )){
				echo json_encode(array('err_code'=>104,'err_msg'=>'手机号格式错误'));exit;
			}
			$info['phone']=trim($_POST['phone']);
			$info['isforeign'] = intval($_POST['isforeign']);
			$info['card_img']=trim($_POST['card_img']);
			$info['iscompany']=intval($_POST['iscompany']);
			$info['person_type']=intval($_POST['person_type']);
			$info['company_name']=htmlspecialchars($_POST['company_name']);
			$info['job_grade']=htmlspecialchars($_POST['job_grade']);
			$info['description']=htmlspecialchars($_POST['description']);
			$info['time']=$_SERVER['REQUEST_TIME'];
			$info['status']=98;
			$uid = $_SESSION['wap_user']['uid'];
			$info['uid']=$uid;
			$lastId=D('User_apply_invest')->data($info)->add();
			if(!empty($lastId)){
				echo json_encode(array('err_code'=>0,'err_msg'=>'申请成功'));exit;
			}else{
				echo json_encode(array('err_code'=>102,'err_msg'=>'申请失败'));exit;
			}
		}else{
			echo json_encode(array('err_code'=>103,'err_msg'=>'请求参数错误'));exit;
		}
	}
	// 检查投资人状态
	public function applyInvestor(){
		$tmp=$this->checkInvestor(1);
		echo json_encode($tmp);exit;
	}
	// 添加领投人
	public function addLeader(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
		$check=$this->checkInvestor(1);
		if($check['err_code'] !=0){
			echo json_encode($check);exit;
		}
		if($_POST){
			$info['email'] =trim($_POST['email']);
			$info['weixin']=trim($_POST['weixin']);
			$info['card_img']=trim($_POST['card_img']);
			$info['person_type']=intval($_POST['person_type']);
			$info['province']=trim($_POST['province']);
			$info['city']=trim($_POST['city']);
			$info['company_type']=intval($_POST['company_type']);
			$info['company_name']=htmlspecialchars($_POST['company_name']);
			$info['job_grade']=htmlspecialchars($_POST['job_grade']);
			$info['education_back']=htmlspecialchars($_POST['education_back']);
			$info['work_experience']=htmlspecialchars($_POST['work_experience']);
			$info['business_prefer']=htmlspecialchars($_POST['business_prefer']);
			$info['stage_prefer']=htmlspecialchars($_POST['stage_prefer']);
			$info['investment_time']=htmlspecialchars($_POST['investment_time']);
			$info['investment_num']=intval($_POST['investment_num']);
			$info['investment_name']=htmlspecialchars($_POST['investment_name']);
			$info['next_num']=intval($_POST['next_num']);
			$info['out_num']=intval($_POST['out_num']);
			$info['suncess_name']=htmlspecialchars($_POST['suncess_name']);
			$info['suncess_intro']=htmlspecialchars($_POST['suncess_intro']);
			$info['leader_status']=98;
			$info['apply_leader_time']=$_SERVER['REQUEST_TIME'];
			$uid = $_SESSION['wap_user']['uid'];
			$effId=D('User_apply_invest')->where(array('uid'=>$uid))->save();
			if(!empty($effId)){
				echo json_encode(array('err_code'=>0,'err_msg'=>'申请成功'));exit;
			}else{
				echo json_encode(array('err_code'=>102,'err_msg'=>'申请失败'));exit;
			}
		}else{
			echo json_encode(array('err_code'=>103,'err_msg'=>'请求参数错误'));exit;
		}
	}
	public function getInvestorInfo(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
		if($_GET['uid']){
			$uid=intval($_GET['uid']);
			$investorInfo=D('User_apply_invest')->where(array('uid'=>$uid))->find();
                        $investorInfo['card_img'] = getAttachmentUrl($investorInfo['card_img']);
			if(!empty($investorInfo)){
				$tmp='{
					"err_code":0,
					"err_msg":{
						"investorInfo":'.json_encode($investorInfo).'
					}
				}';
			}else{
				$tmp='{
					"err_code":0,
					"err_msg":null
				}';
			}
			echo $tmp;exit;
		}else{
			echo json_encode(array('err_code'=>103,'err_msg'=>'请求参数错误'));exit;
		}
	}
	public function upImg(){
		if(IS_POST){
			$s=array();
			$img=$_FILES['myfile'];
		        $extension=pathinfo($img['name'])['extension'];
		        $img_type=array('jpg','jpeg','png','gif');
		        if(!in_array($extension, $img_type)){
		            $s['err_code']='101';
		            $s['err_msg']='上传失败，不是常见的图片类型';
		            echo json_encode($s);exit;
		        }
			$tmp=$this->uploadOne($img);
			if($tmp['code']==200){
				$s['err_code']='0';
				$s['err_msg']='ok';
				$s['path']=$tmp['path'];
				echo json_encode($s);exit;
			}else{
				$s['err_code']=$tmp['code'];
				$s['err_msg']=$tmp['msg'];
				echo json_encode($s);exit;
			}
		}else{
			echo json_encode(array('err_code'=>404,'err_msg'=>'提交方式错误'));exit;
		}
	}
	public function checkOrder(){
		if($_GET){
			$order_no=trim($_GET['order_no']);
			$orderInfo=D('Invest_order')->where(array('zcpay_no'=>$order_no))->find();
			if($orderInfo['order_type'] == 1){
				if($orderInfo['status']==2){
					echo json_encode(array('err_code'=>0,'err_msg'=>'ok'));
				}else{
					echo json_encode(array('err_code'=>1,'err_msg'=>'error'));
				}
			}else{
				if($orderInfo['order_status']==2){
					echo json_encode(array('err_code'=>0,'err_msg'=>'ok'));
				}else{
					echo json_encode(array('err_code'=>1,'err_msg'=>'error'));
				}
			}
		}else{
			echo json_encode(array('err_code'=>101,'err_msg'=>'参数错误'));
		}

	}
	// 关注 project_id
        public function attention(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
        	if($_POST){
			$databases_project_attention = D('Project_attention');
			$uid = $_SESSION['wap_user']['uid'];
			$project_id=intval($_POST['project_id']);
			$row = $databases_project_attention->where(array('uid'=>$uid,'project_id'=>$project_id,'type'=>1))->find();
			if(!empty($row)){
				echo json_encode(array('err_code'=>0,'err_msg'=>'已经关注'));exit;
			}
			$info = $databases_project_attention->data(array('uid'=>$uid,'project_id'=>$project_id,'type'=>1))->add();
			if(!empty($info)){
				D('Project')->where(array('project_id'=>$project_id))->data('`attention`=`attention`+1')->save();
				echo json_encode(array('err_code'=>0,'err_msg'=>'已经关注'));exit;
			}else{
				echo json_encode(array('err_code'=>111,'err_msg'=>'服务器繁忙，请刷新后重试'));exit;
			}
        	}else{
        		echo json_encode(array('err_code'=>110,'err_msg'=>'提交方式错误'));
        	}
        }
        // 点赞  project_id
        public function prais(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
        	if($_POST){
			$databases_project_prais = D('Project_praise');
			$uid = $_SESSION['wap_user']['uid'];
			$project_id=intval($_POST['project_id']);
			$row = $databases_project_prais->where(array('uid'=>$uid,'project_id'=>$project_id,'type'=>1))->find();
			if(!empty($row)){
				echo json_encode(array('err_code'=>0,'err_msg'=>'已经点赞'));exit;
			}
			$info = $databases_project_prais->data(array('uid'=>$uid,'project_id'=>$project_id,'type'=>1))->add();
			if(!empty($info)){
				D('Project')->where(array('project_id'=>$project_id))->data('`praise`=`praise`+1')->save();
				echo json_encode(array('err_code'=>0,'err_msg'=>'已经点赞'));exit;
			}else{
				echo json_encode(array('err_code'=>111,'err_msg'=>'服务器繁忙，请刷新后重试'));exit;
			}
        	}else{
        		echo json_encode(array('err_code'=>110,'err_msg'=>'提交方式错误'));exit;
        	}

        }
        // 收货人地址
        public function addressList(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
		$getAddressInfo = $this->getAddressInfo() ? $this->getAddressInfo() : array();
		$tmp='{
			"err_code":0,
			"err_msg":{
				"getAddressInfo":'.json_encode($getAddressInfo).'
			}
		}';
		echo $tmp;exit;
        }
        // 切换默认地址
        public function selectAddress(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
			echo json_encode($loginInfo);exit;
		}
        	if($_POST){
        		if(empty($_POST['address_id'])){
        			echo json_encode(array('err_code'=>111,'err_msg'=>'提交参数错误'));exit;
        		}
        		$address_id=intval($_POST['address_id']);
			$uid = $_SESSION['wap_user']['uid'];
        		D('User_address')->where(array('uid'=>$uid))->data(array('default'=>0))->save();
        		D('User_address')->where(array('address_id'=>$address_id,'uid'=>$uid))->data(array('default'=>1))->save();
        		echo json_encode(array('err_code'=>0,'err_msg'=>'ok'));exit;
        	}else{
        		echo json_encode(array('err_code'=>110,'err_msg'=>'提交方式错误'));exit;
        	}
        }

        // 产品 - 产品列表
	public function product(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    echo json_encode($loginInfo);exit;
		}
		$where = '`status` = 2 OR `status` = 4 OR `status` = 6 ';
		$Db_zc_product = D('Zc_product');
		if(!empty ($_GET['class'])){
		$where .= ' AND `class` = '.$_GET['class'];
		}
		if(!empty ($_GET['order'])){
		$order = $_GET['order'].' desc';
		}
		$page = $_GET['page'];
		$count=$Db_zc_product->where($where)->count();
		$page=!empty($_GET['page']) ? intval($page) : 1;
		$firstRows=($page-1)*12;
		$productList=$Db_zc_product->where($where)->order($order)->limit($firstRows.',12')->select();
		foreach($productList as $k=>$v){
		$productList[$k]['productThumImage'] = getAttachmentUrl($v['productThumImage']);
		$productList[$k]['productListImg'] = getAttachmentUrl($v['productListImg']);
		$productList[$k]['productFirstImg'] = getAttachmentUrl($v['productFirstImg']);
		$productList[$k]['productImage'] = getAttachmentUrl($v['productImage']);
		$productList[$k]['productImageMobile'] = getAttachmentUrl($v['productImageMobile']);
		}

		$productList= !empty($productList) ? $productList : array();
		$isTrue      = !empty($productList) ? true          : false;
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"project_list":'.json_encode($productList).',
				"page_count":'.json_encode($count).',
				"next_page":'.json_encode($isTrue).'
			}
		}';
		echo $tmp;die;
	}

        // 首页幻灯片
	public function productSlide(){
            	$slide_list=D('Invest_slide')->where(array('class'=>1))->order('`id` DESC')->select();
            	foreach ($slide_list as $k=>$v){
                	$slide_list[$k]['url'] = getAttachmentUrl($v['url']);
            	}
            	$slide_count = count($slide_list);
            	$tmp='{
            		"err_code":0,
            		"err_msg" :{
                    		"slide_list":'.json_encode($slide_list).',
                    		"slide_count":'.json_encode($slide_count).'
                	}
            	}';
            	echo $tmp;exit;
	}
        // 产品首页分类
	public function productCat(){
		if(!empty ($_GET['id'])){
			$where = '`id` = '.$_GET['id'];
		}
		$cat_list=D('Zc_product_class')->where($where)->order('`id` DESC')->select();
		foreach($cat_list as $k=>$v){
			$cat_list[$k]['img'] = getAttachmentUrl($v['img']);
			$cat_list[$k]['icon'] = getAttachmentUrl($v['icon']);
		}
		$cat_count = count($cat_list);
		$tmp='{
			"err_code":0,
			"err_msg" :{
		    	"cat_list":'.json_encode($cat_list).',
		    	"cat_count":'.json_encode($cat_count).'
		}
		}';
		echo $tmp;exit;
	}
	// 产品搜索
        public function searchProduct(){
		$databases_zc_product = D('Zc_product');
		if($_POST['search']){
			$where = "productName LIKE '%".$_POST['search']."%' AND  ";
			if(!in_array($_POST['search'],$_SESSION['Product']['search'])){
			    	$_SESSION['Product']['search'][] =$_POST['search'] ;
			}
		}
		$where .= '(`status`=2 OR `status`=4 OR `status`=6)';
		$Db_zc_product = D('Zc_product');
		$count=$Db_zc_product->where($where)->count();
		$page=!empty($_POST['page']) ? intval($page) : 1;
		$firstRows=($page-1)*12;
		$productList=$Db_zc_product->where($where)->limit($firstRows.',12')->select();
		$productList= !empty($productList) ? $productList : array();
		if(empty($productList)){
			echo json_encode(array('err_code'=>123,'err_msg'=>'无法搜索到你需要的内容，请输入别的关键词'));exit;
		}
		foreach($productList as $k=>$v){
			$productList[$k]['productThumImage'] = getAttachmentUrl($v['productThumImage']);
			$productList[$k]['productListImg'] = getAttachmentUrl($v['productListImg']);
			$productList[$k]['productFirstImg'] = getAttachmentUrl($v['productFirstImg']);
			$productList[$k]['productImage'] = getAttachmentUrl($v['productImage']);
			$productList[$k]['productImageMobile'] = getAttachmentUrl($v['productImageMobile']);
		}
		$isTrue      = !empty($productList) ? true          : false;
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"project_list":'.json_encode($productList).',
				"page_count":'.json_encode($count).',
				"next_page":'.json_encode($isTrue).'
			}
		}';
		echo $tmp;die;
        }
	//产品详情
        public function productDetails(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$databases_zc_product = D('Zc_product');
		$databases_zc_product_topic = D('Zc_product_topic');
		$databases_zc_product_repay = D('Zc_product_repay');
		$databases_zc_product_load = D('Zc_product_load');
		$id = $_GET['id'];
		if(empty ($id)){
			echo json_encode(array('err_code'=>11,'err_msg'=>'参数错误'));die;
		}

		$where = '(`status`= 2 OR `status`= 4 OR `status`= 6) AND `product_id` = '.$id;
		$productInfo = $databases_zc_product->where($where)->find();
		if(empty ($productInfo)){
			echo json_encode(array('err_code'=>12,'err_msg'=>'没有查询到此产品'));die;
		}
		$productInfo['productThumImage'] = getAttachmentUrl($productInfo['productThumImage']);
		$productInfo['productListImg'] = getAttachmentUrl($productInfo['productListImg']);
		$productInfo['productFirstImg'] = getAttachmentUrl($productInfo['productFirstImg']);
		$productInfo['productImage'] = getAttachmentUrl($productInfo['productImage']);
		$productInfo['productImageMobile'] = getAttachmentUrl($productInfo['productImageMobile']);

		if($productInfo['raiseType']==0 && $productInfo['status']==4){
			$endtime=$productInfo['start_time']+($productInfo['collectDays']*86400);
			if($endtime<$_SERVER['REQUEST_TIME']){
			    	echo json_encode(array('err_code'=>10,'err_msg'=>'该产品已经结束'));die;
			}
		}
		$topicInfo = $databases_zc_product_topic->where(array('product_id'=>$id))->select();
		$repayInfo = $databases_zc_product_repay->where(array('product_id'=>$id))->select();
		foreach($repayInfo as $k=>$v){
			$repayInfo[$k]['images'] = getAttachmentUrl($v['images']);
		}
		$loadInfo = $databases_zc_product_load->where(array('product_id'=>$id))->select();
		$productUser = D('User')->field('`nickname`')->where(array('uid'=>$productInfo['uid']))->find();
		$productInfo['nickname'] =  $productUser['nickname'];
		foreach($topicInfo as $k=>$v){
			$userInfo = D('User')->where(array('uid'=>$v['uid']))->find();
			$topicInfo[$k]['avatar'] = getAttachmentUrl($userInfo['avatar']);
			$topicInfo[$k]['nickname'] = $userInfo['nickname'];
		}
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"productInfo":'.json_encode($productInfo).',
				"topicList":'.json_encode($topicInfo).',
		        "repayList":'.json_encode($repayInfo).',
				"loadList":'.json_encode($loadInfo).'
			}
		}';
		echo $tmp;die;
        }
        public function historySearch(){
            $tmp='{
            	"err_code":0,
            	"err_msg" :{
            		"historySearch":'.json_encode($_SESSION['Product']['search']).',
            	}
            }';
            echo $tmp;die;
        }
	// 猜你喜欢
        public function like(){
            	$where_like='(`status`=2 OR `status`=4 OR `status`=6)';
            	$info_like=D('Zc_product')->where($where_like)->order('`praise` DESC')->limit(4)->select();
            	foreach($info_like as $k=>$v){
                	$info_like[$k]['productThumImage'] = getAttachmentUrl($v['productThumImage']);
                	$info_like[$k]['productListImg'] = getAttachmentUrl($v['productListImg']);
                	$info_like[$k]['productFirstImg'] = getAttachmentUrl($v['productFirstImg']);
                	$info_like[$k]['productImage'] = getAttachmentUrl($v['productImage']);
                	$info_like[$k]['productImageMobile'] = getAttachmentUrl($v['productImageMobile']);
            	}
            	$tmp='{
            		"err_code":0,
            		"err_msg" :{
            			"info_like":'.json_encode($info_like).'
            		}
            	}';
            	echo $tmp;die;
        }
	//添加订单
        public function buyAdd(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$databases_zc_product = D('Zc_product');
		$databases_zc_product_repay = D('Zc_product_repay');
		$productInfo = $databases_zc_product->where(array('product_id'=>$_GET['product_id']))->find();
		$productRepayInfo = $databases_zc_product_repay->where(array('repay_id'=>$_GET['repay_id'],'product_id'=>$_GET['product_id']))->find();
		$productRepayInfo['images'] = getAttachmentUrl($productRepayInfo['images']);
		$getAddressDefault = $this->getAddressDefault() ? $this->getAddressDefault() : array();
		$username = D('User')->field('`uid`,`nickname`')->where(array('uid'=>$productInfo['uid']))->find();
		$productInfo['nickname'] =$username['nickname'];
		$productInfo['productFirstImg'] =getAttachmentUrl($productInfo['productFirstImg']);
		$productInfo['productImage'] =getAttachmentUrl($productInfo['productImage']);
		$productInfo['productListImg'] =getAttachmentUrl($productInfo['productListImg']);
		$productInfo['productImageMobile'] =getAttachmentUrl($productInfo['productImageMobile']);
		$productInfo['avatar'] =getAttachmentUrl($username['avatar']);
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"productInfo":'.json_encode($productInfo).',
		        "productRepayInfo":'.json_encode($productRepayInfo).',
				"getAddressDefault":'.json_encode($getAddressDefault).'
			}
		}';
		echo $tmp;die;
        }
        // 获取收货列表
        public function addressInfo($userAddressId){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		import('source.class.area');
		$areaClass = new area();
		$addressInfo=array();
		$uid = $_SESSION['wap_user']['uid'];
            	$addressInfo=D('User_address')->where(array('uid'=>$uid,'address_id'=>$userAddressId))->find();
            	$addressInfo['full_address']=$areaClass->get_name($addressInfo['province']).$areaClass->get_name($addressInfo['city']).$areaClass->get_name($addressInfo['area']).$addressInfo['address'];
            	return $addressInfo;
        }
	//  添加订单数据
        public function buyAddData(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$databases_invest_order = D('Invest_order');
		$databases_zc_product = D('Zc_product');
		$databases_zc_product_repay = D('Zc_product_repay');
		$productInfo = $databases_zc_product->where(array('product_id'=>$_POST['product_id']))->find();
		$productRepayInfo = $databases_zc_product_repay->where(array('repay_id'=>$_POST['repay_id'],'product_id'=>$_POST['product_id']))->find();
		$zcpay_no ='ZCPAY_'.date('YmdHis').mt_rand(100000,999999);
		$orderData = array();
		$orderData['zcpay_no'] = $zcpay_no;
		$orderData['project_id'] = $_POST['product_id'];
		$orderData['time'] = time();
		$uid = $_SESSION['wap_user']['uid'];
		$orderData['uid'] = $uid;
		$orderData['order_status'] = 1;
		$orderData['is_delete'] = 0;

		$orderData['order_type'] = 2;
		$orderData['remark'] = $_POST['orderRemark'];
		$orderData['freight'] = $productRepayInfo['freight'];
		$orderData['repay_id'] =  $_POST['repay_id'];

		if($productRepayInfo['isSelfless'] == 1){
			$orderData['pay_money'] = $_POST['pay_money'];
			$orderData['address_user'] = $this->user_session['nickname'];
		}else{
			$addressInfo = $this->addressInfo($_POST['userAddressId']);
			$orderData['address'] = $addressInfo['full_address'];
			$orderData['address_user'] = $addressInfo['name'];
			$orderData['address_tel'] = $addressInfo['tel'];
			$orderData['pay_money'] = $productRepayInfo['amount']+$productRepayInfo['freight'];
		}
		$info = $databases_invest_order->data($orderData)->add();
		if($info){
			$order_info['order_id'] = $info;
			$order_info['order_no'] = $zcpay_no;
			$tmp='{
			        "err_code":0,
			        "err_msg":{
			                "order_info":'.json_encode($order_info).'
			        }
			}';
			echo $tmp;exit;
		}else{
			echo json_encode(array('err_code'=>123,'err_msg'=>'订单信息异常，请重试'));exit;
		}
        }
        public function topicAdd(){
            	$uid = $_SESSION['wap_user']['uid'];
            	$databases_zc_product_topic = D('Zc_product_topic');
            	if($_POST['product_id']){
			$content=strip_tags($_POST['content']);
			if(empty($content)){
				echo json_encode(array('err_code'=>122,'err_msg'=>'提交内容不能为空'));exit;
			}
			$info['title']     = $content;
			$info['time']      = $_SERVER['REQUEST_TIME'];
			$info['pid']       = !empty($_POST['topicId']) ? intval($_POST['topicId']) : 0;
			$info['puid']      = !empty($_POST['puid'])    ? intval($_POST['puid'])    : 0 ;
			$info['uid']       = $uid;
			$info['product_id']= intval($_POST['product_id']);
			$lastId=D('Zc_product_topic')->data($info)->add();
			if(!empty($lastId)){
				$topicInfo = $databases_zc_product_topic->where(array('topic_id'=>$lastId))->find();
                                $topicInfo['nickname'] = $_SESSION['wap_user']['nickname'];
                                $tmp='{
                                    "err_code":0,
                                    "err_msg" :{
                                            "topicInfo":'.json_encode($topicInfo).'
                                    }
                                }';
				echo $tmp;exit;
			}else{
				echo json_encode(array('err_code'=>121,'err_msg'=>'添加错误'));exit;
			}
		}else{
			echo json_encode(array('err_code'=>120,'err_msg'=>'提交参数错误'));exit;
		}
        }
        // 话题点赞
	public function topic_dianzan(){
		$loginInfo=$this->regLogin();
                if($loginInfo['err_code']!=0){
                        echo json_encode($loginInfo);exit;
                }
		if(!empty($_GET['id'])){
			$id=intval($_GET['id']);
			D('Zc_product_topic')->where(array('topic_id'=>$id))->data('`praise`=`praise`+1')->save();
			$zancount=D('Zc_product_topic')->where(array('topic_id'=>$id))->field('praise')->find();
			echo json_encode(array('err_code'=>0,'err_msg'=>$zancount['praise']));exit;
		}else{
			echo json_encode(array('err_code'=>123,'err_msg'=>'参数错误'));
		}
	}
        // 项目关注
	public function product_attention(){
		$loginInfo=$this->regLogin();
                if($loginInfo['err_code']!=0){
                        echo json_encode($loginInfo);exit;
                }
                $uid = $_SESSION['wap_user']['uid'];
                $id=intval($_GET['id']);
		if(!empty($_GET['id'])){
                    $databases_project_attention = D('Project_attention');
                    $data = $databases_project_attention ->where(array('project_id'=>$id,'uid'=>$uid,'type'=>2))->find();
                    if(!empty ($data)){
                        echo json_encode(array('err_code'=>123,'err_msg'=>'以关注'));
						die;
                    }
                    $info = $databases_project_attention->data(array('uid'=>$uid,'project_id'=>$id,'type'=>2))->add();
                    D('Zc_product')->where(array('product_id'=>$id))->data('`attention`=`attention`+1')->save();
                    $attention=D('Zc_product')->where(array('product_id'=>$id))->field('attention')->find();
                    echo json_encode($attention);exit;
		}else{
			echo json_encode(array('err_code'=>1223,'err_msg'=>'参数错误'));
		}
	}

        // 项目点赞
	public function product_dianzan(){
		$loginInfo=$this->regLogin();
                if($loginInfo['err_code']!=0){
                        echo json_encode($loginInfo);exit;
                }
		if(!empty($_GET['id'])){
			$id=intval($_GET['id']);
			D('Zc_product')->where(array('product_id'=>$id))->data('`praise`=`praise`+1')->save();
			$zancount=D('Zc_product')->where(array('product_id'=>$id))->field('praise')->find();
			echo json_encode($zancount);exit;
		}else{
			echo json_encode(array('err_code'=>123,'err_msg'=>'参数错误'));
		}
	}



        //我关注的
        public function product_my_attention(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$projectAttentionList = D('')->table(array('Project_attention' => 'pa', 'Zc_product' => 'p'))->where("`pa`.`type`=2 AND `pa`.`project_id` = `p`.`product_id` AND pa.uid = '" . $_SESSION['wap_user']['uid']. "'")->select();
		foreach($projectAttentionList as $k=>$v){
			$projectAttentionList[$k]['productThumImage'] = getAttachmentUrl($v['productThumImage']);
			$projectAttentionList[$k]['productListImg'] = getAttachmentUrl($v['productListImg']);
			$projectAttentionList[$k]['productFirstImg'] = getAttachmentUrl($v['productFirstImg']);
			$projectAttentionList[$k]['productImage'] = getAttachmentUrl($v['productImage']);
			$projectAttentionList[$k]['productImageMobile'] = getAttachmentUrl($v['productImageMobile']);
		}
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"projectAttentionList":'.json_encode($projectAttentionList).'
			}
		}';
		echo $tmp;die;
        }
        //取消关注
        public function cancelAttention(){
		$product_id = intval($_GET['product_id']);
		$databases_project_attention = D('Project_attention');
		$databases_zc_product = D('Zc_product');
		$databases_project_attention->where(array('uid'=>$_SESSION['wap_user']['uid'],'project_id'=>$product_id,'type'=>2))->delete();
		$databases_zc_product->where(array('product_id'=>$product_id))->data('`attention` = `attention`-1')->save();
		echo json_encode(array('err_code'=>1,'msg'=>'取消成功'));exit;
        }
        //我发起的
        public function myAddProduct(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$databases_zc_product = D('Zc_product');
		if($_GET['status'] == 4){//4融资中
			$where = array('status'=>4,'uid'=>  $_SESSION['wap_user']['uid']);
		}  elseif ($_GET['status'] == 6) {//6融资成功
			$where = array('status'=>6,'uid'=>$_SESSION['wap_user']['uid']);
		}  elseif ($_GET['status'] == 7) {//7融资失败  3审核拒绝
			$where="(status = 3 OR status=7) AND uid=".$_SESSION['wap_user']['uid'];
		}  else {
			$where="`status` != 0  AND uid=".$_SESSION['wap_user']['uid'];
		}
		$page = $_GET['page'];
		$count=$databases_zc_product->where($where)->count();
		$page=!empty($_GET['page']) ? intval($page) : 1;
		$firstRows=($page-1)*12;
		$productList = $databases_zc_product->where($where)->order("product_id desc")->limit($firstRows.',12')->select();
		foreach($productList as $k=>$v){
			$productList[$k]['topic_count'] = D('Zc_product_topic')->where(array('product_id'=>$v['product_id']))->count('topic_id');
			$productList[$k]['productThumImage'] = getAttachmentUrl($v['productThumImage']);
			$productList[$k]['productListImg'] = getAttachmentUrl($v['productListImg']);
			$productList[$k]['productFirstImg'] = getAttachmentUrl($v['productFirstImg']);
			$productList[$k]['productImage'] = getAttachmentUrl($v['productImage']);
			$productList[$k]['productImageMobile'] = getAttachmentUrl($v['productImageMobile']);
		}
		$productList = !empty($productList) ? $productList  : array();
		$isTrue      = !empty($productList) ? true          : false;
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"productList":'.json_encode($productList).',
				"page_count":'.json_encode($count).',
				"next_page":'.json_encode($isTrue).'
			}
		}';
		echo $tmp;die;
        }
        //我支持的项目
        public function myProduct(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$where = "uid = ".$_SESSION['wap_user']['uid']." AND is_delete = 0 AND order_type = 2";
		if($_GET['order_status'] == 2){
		   	$where.= " AND order_status = 2  ";
		}  elseif ($_GET['order_status'] == 1) {
		    	$where.= " AND order_status = 1  ";
		}
		$page = $_GET['page'];
		$count=D('Invest_order')->where($where)->count();
		$page=!empty($_GET['page']) ? intval($page) : 1;
		$firstRows=($page-1)*12;
		$order_list = D('Invest_order')->where($where)->limit($firstRows.',12')->select();
		foreach($order_list as $k=>$v){
			$productInfo = D('Zc_product')->where(array('product_id'=>$v['project_id']))->find();
			$order_list[$k]['product_id'] = $productInfo['product_id'];
			$order_list[$k]['productName'] = $productInfo['productName'];
			$order_list[$k]['productListImg'] = getAttachmentUrl($productInfo['productListImg']);
			$order_list[$k]['collect'] = $productInfo['collect'];
			$order_list[$k]['amount'] = $productInfo['amount'];
			$order_list[$k]['raiseType'] = $productInfo['raiseType'];
			$order_list[$k]['status'] = $productInfo['status'];
			$order_list[$k]['classname'] = $productInfo['classname'];
		}
		$order_list= !empty($order_list) ? $order_list : array();
		$isTrue      = !empty($order_list) ? true          : false;
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"order_list":'.json_encode($order_list).',
				"next_page":'.json_encode($isTrue).'
			}
		}';
		echo $tmp;die;
        }
        public function myuser(){
		$loginInfo=$this->regLogin();
		if($loginInfo['err_code']!=0){
		    	echo json_encode($loginInfo);exit;
		}
		$user = $_SESSION['wap_user'];
		$user['supportNub']=D('Invest_order')->where(array('uid'=>$_SESSION['wap_user']['uid'],'order_type'=>2))->count('id');
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"user":'.json_encode($user).'
			}
		}';
		echo $tmp;die;
        }
        // 伪删除
        public function myProductDelete(){
            	$info = D('Invest_order')->where(array('uid'=>$_SESSION['wap_user']['uid'],'id'=>$_GET['id']))->data(array('is_delete'=>1))->save();
            	if($info){
                	echo json_encode(array('err_code'=>0,'err_msg'=>'订单删除成功'));die;
            	}  else {
                	echo json_encode(array('err_code'=>12,'err_msg'=>'订单删除失败请重试'));die;
            	}
        }
        //  回收站
        public function myRecycle(){
		$where = "`io`.`project_id` = `zp`.`product_id` AND `io`.is_delete = 1 AND `io`.order_type = '2' AND `io`.`uid` = '" . $_SESSION['wap_user']['uid'] . "'";
		$myRecycleList = D('')->table(array('Invest_order' => 'io', 'Zc_product' => 'zp'))->where($where)->select();
		$myRecycleList['productThumImage'] = getAttachmentUrl($myRecycleList['productThumImage']);
		$myRecycleList['productListImg'] = getAttachmentUrl($myRecycleList['productListImg']);
		$myRecycleList['productFirstImg'] = getAttachmentUrl($myRecycleList['productFirstImg']);
		$myRecycleList['productImage'] = getAttachmentUrl($myRecycleList['productImage']);
		$myRecycleList['productImageMobile'] = getAttachmentUrl($myRecycleList['productImageMobile']);
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"myRecycleList":'.json_encode($myRecycleList).'
			}
		}';
		echo $tmp;die;
        }
        // 订单还原
        public function restoreOrder(){
        	$id=intval($_GET['id']);
        	if(empty($id)){
        		echo json_encode(array('err_code'=>121,'err_msg'=>'参数错误'));die;
        	}
		$info = D('Invest_order')->where(array('uid'=>  $_SESSION['wap_user']['uid'],'id'=>$id))->data(array('is_delete'=>0))->save();
		if($info){
			echo json_encode(array('err_code'=>0,'err_msg'=>'订单还原成功'));die;
		}  else {
			echo json_encode(array('err_code'=>12,'err_msg'=>'订单还原失败请重试'));die;
		}
        }
        //订单详情
        public function orderDetails(){
		$orderInfo = D('Invest_order')->where(array('id'=>$_GET['id']))->find();
		$productInfo =  D('Zc_product')->where(array('product_id'=>$orderInfo['project_id']))->find();
		$repayInfo = D('Zc_product_repay')->where(array('repay_id'=>$orderInfo['repay_id']))->field('`redoundContent`')->find();
		$productInfo['productListImg'] = getAttachmentUrl($productInfo['productListImg']);
		$tmp='{
			"err_code":0,
			"err_msg" :{
				"orderInfo":'.json_encode($orderInfo).',
		        "productInfo":'.json_encode($productInfo).',
				"repayInfo":'.json_encode($repayInfo).'
			}
		}';
		echo $tmp;die;
        }


}

 ?>
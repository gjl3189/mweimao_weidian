<?php
/**
 * 基础类
 *
 */
class base_controller extends controller {
	protected $static_path;
	protected $static_public;
	public function __construct() {
		parent::__construct();
		$this->setStaticPath();
		$this->user_session = $_SESSION['user'];
		$this->assign('user_session', $this->user_session);
		$config=D('Invest_config')->where(array('key'=>'rewrite'))->find();
		$config['value'] = is_null($config['value']) ? '1' : $config['value'];

		//权限（没有股权时默认跳转产品首页）
		if (strpos(MODUELS,'fundingstock') === false && MODULE_NAME == 'index' && ACTION_NAME =='index') {
			redirect(UR('product:index'));
		}

		define('REWRITE', $config['value']);//UR方法是否重写url规则
		define('DOMAIN', option('config.site_url'));
	}
	// 配置静态路径
	protected function setStaticPath(){
		// pc端静态路径
		$this->static_path   = $this->config['site_url'].'/crowdfunding/template/index/default/static/';
		$this->assign('static_path',$this->static_path);

		// 公共静态路径
		$this->static_public = $this->config['site_url'].'/static/';
		$this->assign('static_public',$this->static_public);

		// 手机端静态路径
		$this->static_wap = $this->config['site_url'].'/crowdfunding/template/index/default/static/wap/';
		$this->assign('static_wap',$this->static_wap);
	}
	protected function uploadOne($img){
		$tmp=array();
		$dir_file=PIGCMS_PATH.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'zc'.DIRECTORY_SEPARATOR.date('Y',$_SERVER['REQUEST_TIME']).DIRECTORY_SEPARATOR.date('md',$_SERVER['REQUEST_TIME']);
		if(!file_exists($dir_file)){
		    $k=mkdir($dir_file,0777,true);
		    if(!$k){
		        $tmp['code']=404;
		        $tmp['msg']='上传失败，无法创建目录';
		        return $tmp;
		    }
		}
		$new_name=md5($_SERVER['REQUEST_TIME']).'.'.pathinfo($img['name'])['extension'];
		$new_path=$dir_file.DIRECTORY_SEPARATOR.$new_name;
		$path='zc/'.date('Y',$_SERVER['REQUEST_TIME']).'/'.date('md',$_SERVER['REQUEST_TIME']).'/'.$new_name;
		if( move_uploaded_file($img['tmp_name'], $new_path) ){
		    	// 上传到又拍云服务器
		    	$attachment_upload_type = option('config.attachment_upload_type');
		   	if ($attachment_upload_type == '1') {
		        	import('source.class.upload.upyunUser');
		        	upyunUser::upload('./upload/'.$path, '/'.$path);
		    	}
		    	$tmp['code']=200;
		    	$tmp['path']=getAttachmentUrl($path);
		    	return $tmp;
		}else{
		    	$tmp['code']=404;
		    	$tmp['msg']='上传失败，移动文件失败';
		    	return $tmp;
		}
	}
	protected function isLogin(){
		if (empty($this->user_session)){
		    	$config=D('Invest_config')->where(array('key'=>'isLogin'))->find();
		    	if(empty($config['value'])){
	        		redirect($this->config['site_url'].'/index.php?c=account&a=login&type=zc');
	        		exit;
		    	}else{
		       		return true;
		    	}
		}else{
		    	return true;
		}
	}
	protected function showTip($msg,$url='',$isAutoGo=false,$showCopy=true){
		if(empty($url)) $url = 'javascript:history.back(-1);';
		if ($msg == '404'){
			header("HTTP/1.1 404 Not Found");
			$msg = '抱歉，你所请求的页面不存在！';
		}
		include(PIGCMS_PATH.'crowdfunding/template/index/default/public/showTip.php');exit;
	}
	// 获取收货列表
	public function getAddressInfo(){
		import('source.class.area');
		$areaClass = new area();
		$address_list=array();
		$address_list=D('User_address')->where(array('uid'=>$this->user_session['uid']))->select();
		if(!empty($address_list)){
		    	foreach ($address_list as $k => $v) {
		       		$address_list[$k]['full_address']=$areaClass->get_name($v['province']).$areaClass->get_name($v['city']).$areaClass->get_name($v['area']).$v['address'];
		    	}
		}
		return $address_list;
	}
	// 获取默认地址
	public function getAddressDefault(){
		import('source.class.area');
		$areaClass = new area();
		$where['uid']=$this->user_session['uid'];
		$where['default']=1;
		$info=D('User_address')->where($where)->find();
		$tmp=array();
		if(empty($info)){
		    	$address_list=D('User_address')->where(array('uid'=>$this->user_session['uid']))->limit(1)->select();
		    	$tmp=reset($address_list);
		}else{
		    	$tmp=$info;
		}
		if(!empty($tmp)){
		    	$tmp['full_address']=$areaClass->get_name($tmp['province']).$areaClass->get_name($tmp['city']).$areaClass->get_name($tmp['area']).$tmp['address'];
		}
		return $tmp;
	}
	protected function ajax_login(){
		if(empty($this->user_session)){
			echo json_encode(array('err_code'=>101,'err_msg'=>'请先登录'));exit;
		}
	}
	// 默认计划执行
	protected function jihua($product_id = 0){
		M('Zc_product')->jihua($product_id);
	}
}

?>
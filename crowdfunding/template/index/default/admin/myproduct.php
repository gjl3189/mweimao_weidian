<?php include display('public:header');?>
<link rel="stylesheet" href="<?php echo $static_path;  ?>css/common.css">
<link rel="stylesheet" href="<?php echo $static_path;  ?>css/myZmore.css">
<!--container-->
<script type="text/javascript">
$(function(){
	$("#J_ui_assetTip-close").click(function(){
		$("#J_ui_assetTip").hide(600);
	})
})
</script>
<div class="container">
		<!-- main -->
		<div class="main w">
		<!-- m-nav 资产总览左侧导航 -->
		<?php include display('public:left');?>
		<!-- m-nav 资产总览左侧导航 end -->
		<div class="m-content">
				<div class="ui_assetTip" id="J_ui_assetTip"> <em class="ui_assetTip-ico"></em>
					<a class="ui_assetTip-close" id="J_ui_assetTip-close" href="javascript:;"></a>
					<ul class="ui_assetTip-box" id="J_ui_assetTip-box">
						<li class="ui_assetTip-b-list" style="margin-top: 0px;">
							<a class="ui_assetTip-b-l-link" href="<?php echo url_rewrite('newsShow',array('newsId'=>$newsTitle['news_id']));?>.html" target="_blank">
								<?php  echo $newsTitle['news_title'];?></a>
						</li>
					</ul>
				</div>
				<div class="myZmore">
				<div class="myz-title">
					<div class="t-title">
						<?php if (strpos(MODUELS,'crowdfunding') !== false) {?>
						<a class="font-red" clstag="jr|keycount|gq_wdzc|gq_gqzc" href="<?php echo UR('admin:myProduct');?>">产品众筹</a>
						<?php } ?>
						<?php if (strpos(MODUELS,'fundingstock') !== false) {?>
						<em class="t-t-line">|</em>
						<a href="<?php echo UR('admin:index');?>">私募股权</a>
						<?php } ?>
					</div>
						<a class="t-button" href="<?php echo UR('product:product_add');?>" clstag="jr|keycount|gq_wdzc|gq_cjxm" target="_blank"><em class="t-b-ico"></em>发起众筹</a>
					</div>
				<div class="myz-head">
						<div class="ui-select-listBox">
						<ul class="clearfix">
							<li id="myZsupportTab" class="ui-select-listBox-list ui-select-listBox-list--now">
								<span>我支持的</span>
								<i class="ui-select-listBox-l-line">|</i>
							</li>
							<li class="ui-select-listBox-list" id="myConcernProjectTab">
								<a href="<?php echo UR('admin:myAddProduct');?>" >
									<span>我发起的</span>
								</a><i class="ui-select-listBox-l-line">|</i>
							</li>
							<li class="ui-select-listBox-list" id="myConcernProjectTab">
								<a href="<?php echo UR('admin:myRecycle');?>">
									<em class="h-t-ico"></em>
									<span>回收站</span>
								</a>
							</li>
							<a clstag="jr|keycount|gq_wdzc|gq_wgzd"></a>
						</ul>
						<a clstag="">
							<div class="ui-select-listBox-line">
								<div class="ui-select-listBox-l-red" style="left: 0px; width: 113px;"></div>
							</div>
						</a>
						</div>
						<a clstag=""> </a>
				</div>
				<a clstag="jr|keycount|gq_wdzc|gq_wgzd"> </a>
				<div class="myz-box" id="myZmyLaunchListTab" style="display: block;"><a clstag="jr|keycount|gq_wdzc|gq_wgzd"> </a>
						<form action="" name="myZmyLaunchFrom" contentid="myZmyLaunchListTab" id="myZmyLaunchFrom" method="post">
							<div class="ui-tab clearfix">
								<a clstag=""> </a>
								<a href="<?php echo url('admin:myProduct',array('order_status'=>0));?>" class="ui-tab-list <?php if($state == 0){ echo "ui-tab-list--now"; }?>">全部</a> <i class="ui-tab-line">|</i>
								<a href="<?php echo url('admin:myProduct',array('order_status'=>2));?>" class="ui-tab-list <?php if($state == 2){ echo "ui-tab-list--now"; }?>">已支付</a><i class="ui-tab-line">|</i>
								<a href="<?php echo url('admin:myProduct',array('order_status'=>1));?>" class="ui-tab-list <?php if($state == 1){ echo "ui-tab-list--now"; }?>">未支付</a><i class="ui-tab-line">|</i>
							</div>

						<table class="b-head">
							<colgroup>
								<col width="140">
								<col width="330">
								<col width="140">
								<col width="140">
							</colgroup>
							<tbody>
								<tr>
									<th>项目信息</th>
									<th></th>
									<th>实付款(元)</th>
									<th>交易状态</th>
									<th>操作</th>
								</tr>
							</tbody>
						</table>

						<ul class="b-ulist">
							<?php
							if(!empty ($mySupportList)){
							foreach($mySupportList as $k=>$v){ ?>
								<li class="b-list b-list--init">
									<table>
										<colgroup>
										<col width="140">
										<col width="330">
										<col width="140">
										<col width="140">
										</colgroup>
									<tbody>
										<tr>
											<td>
												<a target="_blank" href="<?php echo UR('product:product_details',array('id'=>$v['product_id']))?>" class="b-l-img">
													<img width="98" height="98" alt="" src="<?php echo getAttachmentUrl($v['productListImg']);?>"/>
												</a>
											</td>
											<td class="td-left">
												<div class="clearfix">
													<a target="_blank" href="<?php echo UR('product:product_details',array('id'=>$v['product_id']))?>" class="b-l-name"><?php echo $v['productName'];?></a>
													<p class="b-l-tag-green">
														<?php
														if($v['status'] == 1){
															echo "申请中";
														}elseif($v['status'] == 2){
															echo "预热中";
														}elseif($v['status'] == 4){
															echo "融资中";
														}elseif($v['status'] == 6){
															echo "融资成功";
														}elseif($v['status'] == 7){
															echo "融资失败";
														}elseif($v['status'] == 0){
															echo "草稿";
														}elseif($v['status'] == 3){
															echo "审核拒绝";
														}
														?>
													</p>
												</div>
												<a target="_blank" href="<?php echo url('admin:orderDetails',array('id'=>$v['id']))?>" class="b-l-link">订单编号：<?php echo $v['zcpay_no'];?></a>
												<div class="b-l-info clearfix">
													<div class="b-l-i-list">
														<p>已完成</p>
														<p class="size14"><?php echo number_format($v['collect']/$v['amount']*100, 2, '.', '');?>%</p>
													</div>
													<div class="b-l-i-list">
														<p>已筹集</p>
														<p class="size14">¥<?php echo $v['collect']?></p>
													</div>
													<div class="b-l-i-list">
														<p>剩余时间</p>
														<p class="size14"><?php
															if($v['raiseType'] == 1){
																echo '不限';
															}else{
																  echo  intval((($v['time']+($v['collectDays']*86400))-time())/86400).'天';
															}
														?></p>
													</div>
												</div>
											</td>
											<td>
												<p class="b-l-price-red"><?php echo number_format($v['pay_money'],2,'.',''); ?></p>
												<p class="b-l-tip">(含运费：<?php echo number_format($v['freight'],2,'.',''); ?>)</p>
											</td>
											<td>
												<p class="font-red">
													<?php if($v['order_status'] == 1){
														echo "支付中";
													}elseif($v['order_status'] == 2){
														echo "支付成功";
													}elseif($v['order_status'] == 3){
														echo "交易关闭";
													}?>
												</p>
											</td>
											<td>
												<?php if($v['order_status'] == 1){ ?>
												<a  href="<?php echo UR('product:confirmOrder',array('oid'=>$v['id']));?>" class="ui-button">立即支付</a>
												<?php }?>

												<p><a href="<?php echo url('admin:orderDetails',array('id'=>$v['id']))?>" target="_blank" class="b-l-btn">交易详情</a></p>
												<p><a href="<?php echo url('admin:myProductDelete',array('id'=>$v['id']))?>" id="myZdelete14852128" class="b-l-btn listDelete">删除</a></p>
												<?php if($v['order_status'] == 1){ ?>
												   <p><a href="<?php echo url('admin:ProductDelete',array('id'=>$v['id']))?>" id="myZdelete14852128" class="b-l-btn listDelete">取消订单</a></p>
												<?php }?>
											</td>
										</tr>
										<tr class="b-l-pop-btn-box">
										<td colspan="5"><a style="display: none" href="javascript:;" class="b-l-pop-btn-bg"><em class="b-l-pop-btn"></em></a></td>
										</tr>
									</tbody>
									</table>
								</li>
							<?php }}else{ ?>
								<div class="without_object">
									<p>很抱歉，这儿是个荒地。</p>
									<p>您还没有支持任何项目，去[<a href="<?php echo UR('product:index');    ?>">众筹首页</a>]看看有什么感兴趣的吧！</p>
								</div>
							<?php  }?>
						</ul>
					</form>
						<script type="text/javascript" src="<?php echo $static_path; ?>js/alter.js"></script>
						<link rel="stylesheet" href="<?php echo $static_path;  ?>css/alert.css">
</div>
			</div>
			</div>
	</div>
		<!-- main end -->
	</div>
<!-- 签署协议 -->
<script type="text/javascript">
$(function(){
	$(".ui-select-listBox-list").hover(function(){
		var index=$(this).index();
		var width=index*113+'px';
		$(".ui-select-listBox-l-red").css({
			"position":"absolute",
			"left":width,
			"width":113+'px',
		});
	})
})
</script>
<!--container end-->
<input type="hidden" name="gqzc_page_point" id="gqzc_page_point" value="15003">
<?php include display('public:footer');?>

<script>
	$(document).ready(function(){
		$("#confirmBnt").click(function() {
			getPayUrl();
		});
		$("#backBnt").click(function() {
			history.go(-1);
		});
	});
	// 生成二维码
	function getPayUrl(){
		$('.pay-code .code .loading').html('二维码正在加载中，请稍候...');
		$.post('<?php echo url('weixin:index',array('qrcode_id'=>1300000000+$orderInfo['id']));?>',function(result){
			if(result.err_code != 0){
				alert(result.err_msg+'\r\n请点击重新获取二维码');
				window.location.reload();
			}else{
				if(result.err_msg.error_code != 0){
					alert(result.err_msg.msg+'\r\n请点击重新获取二维码');
					window.location.reload();
				}else{
					codeTip(result.err_msg.ticket);
				}
			}
		},'json');
	}
	function codeTip(imgpath){
		var pay_money='<?php echo $orderInfo['pay_money'];?>';
		var order_no='<?php echo $orderInfo['zcpay_no'] ?>';

		layer.alert('应付金额：'+pay_money+'<br>订单编号：'+order_no+'<br/><img src="'+imgpath+'" style="width:268px;" />', {
			skin: 'layui-layer-lan',
			closeBtn: 0,
			shift: 4 ,
			area: ['340px', '440px'], //宽高
			title:'订单支付',
		});
	}
//    $(function(){
//        var allheight=$(window).height();
//        var height=allheight-120;
//        $(".f-copyright").css({"position":"fixed","width":"100%","top": height})
//    })
	function checkStatus(){
		var order_no="<?php echo $orderInfo['zcpay_no']; ?>";
		var url="<?php echo url('product:ajax_checkStatus'); ?>";
		$.post(url,{order_no:order_no},function(sta){
			if(sta.err_code==0){
				location.href="<?php echo UR('product:orderBuy',array('oid'=>$orderInfo['id']));?>";
			}
		},"json");
	}
	setInterval('checkStatus()','10000');
</script>
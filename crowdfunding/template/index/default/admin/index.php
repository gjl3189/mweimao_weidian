<?php include display('public:header');?>
<link rel="stylesheet" href="<?php echo $static_path;  ?>css/common.css">
<link rel="stylesheet" href="<?php echo $static_path;  ?>css/myZmore.css">
<!--container-->
<script type="text/javascript">
$(function(){
	$("#J_ui_assetTip-close").click(function(){
		$("#J_ui_assetTip").hide(600);
	})
})
</script>

<div class="container">
		<!-- main -->
		<div class="main w">
		<!-- m-nav 资产总览左侧导航 -->
		<?php include display('public:left');?>
		<!-- m-nav 资产总览左侧导航 end -->
		<div class="m-content">
				<div class="ui_assetTip" id="J_ui_assetTip"> <em class="ui_assetTip-ico"></em>
					<a class="ui_assetTip-close" id="J_ui_assetTip-close" href="javascript:;"></a>
					<ul class="ui_assetTip-box" id="J_ui_assetTip-box">
						<li class="ui_assetTip-b-list" style="margin-top: 0px;">
							<a class="ui_assetTip-b-l-link" href="<?php echo url_rewrite('newsShow',array('newsId'=>$newsTitle['news_id']));?>.html" target="_blank">
								<?php  echo $newsTitle['news_title'];?></a>
						</li>
					</ul>
				</div>
				<div class="myZmore">
				<div class="myz-title">
					<div class="t-title">
						<?php if (strpos(MODUELS,'crowdfunding') !== false) {?>
						<a name="myZmaodian" id="myZmaodian"></a><a href="<?php echo UR('admin:myProduct');?>" >产品众筹</a>
						<?php } ?>
						<?php if (strpos(MODUELS,'fundingstock') !== false) {?>
						<em class="t-t-line">|</em>
						<a class="font-red" clstag="" href="">私募股权</a>
						<?php } ?>
					</div>
						<a class="t-button" href="<?php echo url('project:projectAdd');?>" clstag="" target="_blank"><em class="t-b-ico"></em>发起众筹</a>
					</div>
				<div class="myz-head">
						<div class="ui-select-listBox">
						<ul class="clearfix">
							<li class="ui-select-listBox-list ui-select-listBox-list--now" id="myZmyLaunchTab">
								<a clstag="jr|keycount|gq_wdzc|gq_wfqd" href="<?php echo UR('admin:index');?>" style="color: #ea544a;">
									<span>我发起的</span>
								</a> <i class="ui-select-listBox-l-line">|</i>
							</li>
							<li class="ui-select-listBox-list" id="myInProjectTab">
								<a clstag="jr|keycount|gq_wdzc|gq_wtzd" href="<?php echo UR('admin:toInvest');?>">
									<span>我投资的</span>
								</a> <i class="ui-select-listBox-l-line">|</i>
							</li>
							<li class="ui-select-listBox-list" id="myConcernProjectTab">
								<a href="<?php echo UR('admin:myAttention');?>">
									<span>我关注的</span>
								</a>
							</li>
							<a clstag="jr|keycount|gq_wdzc|gq_wgzd"></a>
						</ul>
						<a clstag="jr|keycount|gq_wdzc|gq_wgzd">
							<div class="ui-select-listBox-line">
							<div class="ui-select-listBox-l-red" style="left: 0px; width: 113px;"></div>
						</div>
							</a></div>
						<a clstag="jr|keycount|gq_wdzc|gq_wgzd"> </a></div>
				<a clstag="jr|keycount|gq_wdzc|gq_wgzd"> </a>
				<div class="myz-box" id="myZmyLaunchListTab" style="display: block;"><a clstag=""> </a>
						<form action="http://dj.jd.com/funding/myLaunchProjectList.action" name="myZmyLaunchFrom" contentid="myZmyLaunchListTab" id="myZmyLaunchFrom" method="post">
							<div class="ui-tab clearfix">
								<a clstag="jr|keycount|gq_wdzc|gq_wgzd"> </a>
								<a href="<?php echo UR('admin:index',array('status'=>0));?>" class="ui-tab-list <?php if($state == 0){ echo "ui-tab-list--now"; }?>">全部</a> <i class="ui-tab-line">|</i>
								<a href="<?php echo UR('admin:index',array('status'=>3));?>" class="ui-tab-list <?php if($state == 3){ echo "ui-tab-list--now"; }?>">融资中</a><i class="ui-tab-line">|</i>
								<a href="<?php echo UR('admin:index',array('status'=>4));?>" class="ui-tab-list <?php if($state == 4){ echo "ui-tab-list--now"; }?>">已成功</a><i class="ui-tab-line">|</i>
								<a href="<?php echo UR('admin:index',array('status'=>5));?>" class="ui-tab-list <?php if($state == 5){ echo "ui-tab-list--now"; }?>">已失败</a>
							</div>
						<table class="b-head">
							<colgroup>
							<col width="140">
							<col width="330">
							<col width="140">
							<col width="93">
							<col width="93">
							</colgroup>
							<tbody>
								<tr>
									<th>项目信息</th>
									<th></th>
									<th>融资目标(元)</th>
									<th>领投人</th>
									<th>选取投资</th>
									<th>操作</th>
								</tr>
							</tbody>
							</table>
						<ul class="b-ulist">
								<li class="b-list b-list--init">
								<table>
										<colgroup>
									<col width="140">
									<col width="330">
									<col width="140">
									<col width="93">
									<col width="93">
									</colgroup>
										<tbody>

											<?php
											if(!empty($projectList)){
											foreach ($projectList as $list) {
											?>
											<tr>
												<td><a class="b-l-img" target="_blank"><img src="<?php echo getAttachmentUrl($list['listImg']); ?>" alt="" width="98" height="98"></a></td>
												<td class="td-left"><div class="clearfix"> <a class="b-l-name" target="_blank" style="text-overflow: ellipsis;overflow:hidden;white-space:nowrap;width: 85px" title="888888"><?php echo $list['projectName'];   ?></a>
												<p class="b-l-tag-green">
												<?php
													if($list['status'] == 0){
														echo "待审核";
													}elseif($list['status'] == 1){
														echo "路演中";
													}elseif($list['status'] == 2){
														echo "审核失败";
													}elseif($list['status'] == 3){
														echo "融资中";
													}elseif($list['status'] == 4){
														echo "融资成功";
													}elseif($list['status'] == 5){
														echo "融资失败";
													}
													?>
												</p>
													</div>
													<!--  -->
												<div class="b-l-info clearfix">
														<div class="b-l-i-list">
														<p>已完成</p>
														<p class="size14"><?php echo number_format($list['collect']/($list['amount']*10000)*100, 2, '.', '');?>%</p>
														</div>
														<div class="b-l-i-list">
														<p>已募集(元)</p>
														<p class="size14">¥<?php echo $list['collect'];?></p>
														</div>
														<div class="b-l-i-list">
														<p>剩余时间</p>
														<p class="size14">
															<?php
																if($list['endTime']){
																	$sysj = intval(($list['endTime']-time())/(60*60*24));
																	if($sysj<0){
																		$sysj="已结束";
																	}else{
																		$sysj.='天';
																	}
																}else{
																	$sysj = "暂未开始";
																}
																echo $sysj;?>

																</p>
													</div>
													</div></td>
												<td>
												<p class="b-l-price-grey"><?php echo $list['amount'];?>万</p>
												<p class="b-l-tip">(当前进度：<?php echo number_format($list['collect']/($list['amount']*10000)*100, 2, '.', '');?>%)</p>
												</td>
												<td>
												<p class="b-l-price-grey" style="text-overflow: ellipsis;overflow:hidden;white-space:nowrap;" title=""></p>
												</td>
												<td>
													<?php if($list['status'] == 4){ ?>
													<a class="b-l-btn" href="<?php echo url('admin:selection',array('project_id'=>$list['project_id']));?>" target="_blank">选取投资人</a>
													<?php }?>
												</td>
												<td>

													<p>
														<?php if($list['status']==0){
															echo "未审核";
														} ?>
														<?php if($list['status'] == 1){ ?>
															<a class="b-l-btn" href="<?php echo url('admin:projectSetUp',array('project_id'=>$list['project_id']));?>" target="_blank">设置</a>|
															<a class="b-l-btn" href="<?php echo url('admin:shareholder',array('project_id'=>$list['project_id']));?>">管理</a>
														<?php } ?>
														<?php if($list['status'] == 2){ ?>
															<a class="b-l-btn" id="projectDelete" onclick="projectDelete('<?php echo $list['project_id'];?>')"  target="_blank">删除</a>
														<?php } ?>
														<?php if($list['status']==3){ ?>
															<a class="b-l-btn" href="<?php echo url('admin:projectSetUp',array('project_id'=>$list['project_id']));?>" target="_blank">设置</a>|
															<a class="b-l-btn" href="<?php echo url('admin:shareholder',array('project_id'=>$list['project_id']));?>">管理</a>
														<?php } ?>
														<?php if($list['status']==3 || $list['status']==4 || $list['status']==5){  ?>
															<a class="b-l-btn" href="<?php echo url('admin:myorder_project',array('id'=>$list['project_id']));?>">订单</a>
														<?php } ?>
													</p>
												</td>
															<script>
																function projectDelete(p_id){
																	var deleteUrl = "<?php echo url('admin:projectDelete');  ?>";
																 $.post(deleteUrl,{'project_id':p_id},function(data){
																	 if(data == 1){
																		 layer.msg("删除成功");
																		 location.href=location.href;
																	 }
																 })
																}

															</script>

											</tr>
											<?php  } }  else { ?>
												 <div class="without_object">
													<p>很抱歉，这儿是个荒地。</p>
													<p>您还没有支持任何项目，去[<a href="<?php echo $this->config['site_url']?>/zc.php">众筹首页</a>]看看有什么感兴趣的吧！</p>
												</div>

										 <?php }  ?>

											<tr class="b-l-pop-btn-box">
												<td colspan="6"><a class="b-l-pop-btn-bg" href="javascript:;" style="display: none;position: absolute;"><em class="b-l-pop-btn"></em></a></td>
											</tr>
									</tbody>
									</table>
							</li>
							</ul>
					</form>
						<script type="text/javascript" src="<?php echo $static_path;  ?>js/alter.js"></script>
						<link rel="stylesheet" href="<?php echo $static_path;  ?>css/alert.css">
</div>
			</div>
			</div>
	</div>
		<!-- main end -->
	</div>
<!-- 签署协议 -->
<div class="ui-pop myz-pop4" id="J_pop-box3" style="display: none;"> <a class="ui-pop-close js_pop-close" href="javascript:;"></a>
		<div class="ui-pop-head">签署协议</div>
		<div class="ui-pop-content" style="height:286px;">
		<div class="p-msg">
				<div><span>开户人姓名</span><input type="text" name="person_name" value=""></div>
				<div><span>选择银行</span>
				<select name="bank">
						<option>中国银行</option>>
						<option>中国农业银行</option>
						<option>中国工商银行</option>
						<option>招商银行</option>
						<option>中国建设银行</option>
						<option>交通银行</option>
						<option>平安银行</option>
						<option>广发银行</option>
						<option>光大银行</option>
						<option>民生银行</option>
						<option>浦东发展银行</option>
						<option>华夏银行</option>
						<option>中信银行</option>
						<option>华夏银行</option>
						<option>中国邮政储蓄</option>
						<option>支付宝</option>
						<option>兴业银行</option>
				</select>
				</div>
				<div><span>支行名称</span><input type="text" name="person_name" value=""></div>
				<div><span>银行账号</span><input type="text" name="person_name" value=""></div>
		</div>
		<div class="p-btn-box">
			<a class="ui-button ui-button-XL" onclick="sign()">提交</a>
			<a class="ui-button ui-button-grey p-btn2 js_pop-close" href="javascript:;">取消</a>
			<input type="hidden" name="gqzc_page_point" id="gqzc_page_point" value="15003">
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$(".ui-select-listBox-list").hover(function(){
		var index=$(this).index();
		var width=index*113+'px';
		$(".ui-select-listBox-l-red").css({
			"position":"absolute",
			"left":width,
			"width":113+'px',
		});
	})
})
</script>
<!--container end-->
<input type="hidden" name="gqzc_page_point" id="gqzc_page_point" value="15003">
<?php include display('public:footer');?>
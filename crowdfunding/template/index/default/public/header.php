<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0038)http://dj.jd.com/funding/investor.html -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="">
<meta name="Keywords" content="">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<title><?php echo $title_name; ?> - <?php echo $this->config['site_name'];?></title>
<link rel="icon" href="<?php echo $this->config['site_url'];?>/favicon.ico" type="image/x-icon">
<script type="text/javascript" src="<?php echo TPL_URL;?>static/js/jquery.js"></script>
<link rel="stylesheet" href="<?php echo TPL_URL;?>static/css/base.css">
<link href="<?php echo TPL_URL;?>static/css/WdatePicker.css" rel="stylesheet" type="text/css">
<script src="<?php echo TPL_URL;?>static/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<?php echo $static_public; ?>js/layer/layer/layer.js"></script>
</head>
<style>
    .without_object{
        border: 1px solid #ececec;
        color: #666;
        font-family: "microsoft yahei";
        font-size: 14px;
        line-height: 24px;
        padding: 50px 0 50px 220px;
    }
</style>
<body>
<!--header-->
<div class="header"> <!--topbar-->
	<div class="topbar" id="topbar">
		<div class="grid-1200 topbar-wrap clearfix">
			<div class="topbar-left">
				<ul class="toplink">
					<li class="toplink-link"> <i class="icon icon-home"></i> <a target="_blank" href="zc.php">众筹首页</a> </li>
				</ul>
			</div>
			<div class="topbar-right">
				<ul class="topmenu fl">
					<li class="topmenu-item topmenu-userinfo"> <span id="loginbar"><a href="###" target="_blank" class="link-user"><?php if($this->user_session['nickname']){  echo $this->user_session['nickname']; }else{ ?><a href="/index.php?c=Account&a=login&type=zc">登录</a><?php } ?></a>&nbsp;&nbsp;<a href="/index.php?c=Account&a=logout" class="link-logout">退出</a></span> </li>
					<?php if (strpos(MODUELS,'crowdfunding') !== false) {?>
					<li  class="topmenu-item dropdown topmenu-dropdown phone_top" data-hover="topmenu-dropdown-hover"> <i class="drop-arrow"></i> <b class="topmenu-app-ico"></b> <a href="###" target="_blank" class="topmenu-outline" >产品众筹</a>
						<div class="dropbox clearfix" style="width: 92px;height: 92px;">
							<div class="fl"> <img src="/zc.php?c=product&a=toCode" style="width: 95px;height: 95px;"> </div>
						</div>
					</li>
					<?php } ?>
					<?php if (strpos(MODUELS,'fundingstock') !== false) {?>
					<li  class="topmenu-item dropdown topmenu-dropdown phone_top" data-hover="topmenu-dropdown-hover"> <i class="drop-arrow"></i> <b class="topmenu-app-ico"></b> <a href="###" target="_blank" class="topmenu-outline" >股权众筹</a>
						<div class="dropbox clearfix" style="width: 92px;height: 92px;">
							<div class="fl"> <img src="/zc.php?c=project&a=toCode" style="width: 95px;height: 95px;"> </div>
						</div>
					</li>
					<?php } ?>
				</ul>
				<ul class="toplink fl">
					<li class="toplink-item toplink-title">&nbsp;</li>
					<li class="toplink-item dropdown toplink-dropdown " data-hover="toplink-dropdown-hover"> <i class="icon icon-weixin"></i>
						<ul class="dropbox dropbox-weixin">
							<li><img src="<?php echo option('config.wechat_qrcode');?>" alt="扫一扫，关注我" style="width: 95px;height: 95px;">
								<h6>扫一扫关注</h6>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!--topbar end--> <!--main-nav-->
	<div class="main-nav">
		<div class="grid-1200 nav-wrap pr clearfix">
			<div class="main-nav-logo fl"> <a href="/" > <img src="<?php echo TPL_URL;?>static/images/zhongchou.png" width="240" height="45" alt=""> </a> </div>
			<div class="nav fl">
				<ul class="nav-box fl clearfix" id="navBox">
					<li id="nav-index" class="nav-item"> <a class="nav-item-primary" href="<?php if (strpos(MODUELS,'crowdfunding') !== false) {echo UR('product:index');} else if (strpos(MODUELS,'fundingstock') !== false) {echo UR('index:index');} ?>" >首页<i></i></a> </li>
					<?php if (strpos(MODUELS,'crowdfunding') !== false) {?>
					<li id="nav-licai" class="nav-item"> <a class="nav-item-primary" href="<?php echo UR('product:product_list');?>" >产品众筹<i></i></a> </li>
					<?php } ?>
					<?php if (strpos(MODUELS,'crowdfunding') !== false && strpos(MODUELS,'fundingstock') !== false) {?>
					<li id="nav-licai" class="nav-item"> <a class="nav-item-primary" href="<?php echo UR('index:index'); ?>" >股权众筹<i></i></a> </li>
					<?php } ?>
					<?php if (strpos(MODUELS,'fundingstock') !== false) {?>
					<li id="nav-licai" class="nav-item"> <a class="nav-item-primary" href="<?php echo url('project:project_list');?>" >融资项目<i></i></a> </li>
					<li id="nav-zhongchou" class="nav-item"> <a class="nav-item-primary" href="<?php echo url('project:projectAdd');?>" target="_blank" >我要融资<i></i></a> </li>
					<?php } ?>
					<li id="nav-baoxian" class="nav-item"> <a class="nav-item-primary" href="<?php echo url_rewrite('newsList',array('cat_key' =>'liebiao'));?>" >活动资讯<i></i></a></li>
				</ul>
			</div>
			<div class="main-nav-search">
				<?php if(MODULE_NAME == product){ ?>
					<div class="search-wrap">
							<input type="text" class="search-ipt" id="p_search" title="请输入搜索内容" value="" name="sv" autocomplete="off" placeholder="搜索项目">
							<a class="search-btn" id="searchBtn"  href="javascript:;"></a>
					</div>
				<?php }?>
			</div>
			<script>
				$('#p_search').keypress(function(e) {
				   if(e.which == 13) {
						var p_search = $("#p_search").val();
						var url = "<?php echo UR('product:searchProduct'); ?>"+"?sv="+p_search;
						location.href=url;
				   }
			   })

               $("#searchBtn").click(function(){
                    var p_search = $("#p_search").val();
                    var word = "<?php echo REWRITE==1 ? '?' : '&'; ?>";
                    var url = "<?php echo UR('product:searchProduct'); ?>"+word+"sv="+p_search;
                    location.href=url;
                })
            </script>

			<div class="main-nav-userCenter-wrap fr">
				<div id="J_userCenter" class="userCenter-main ">
					<div class="userCenter-portal"> <i class="userCenter-portal-icon"></i> <a href="<?php echo UR('admin:myProduct'); ?>" class="userCenter-portal-text" style="text-decoration: none;">个人中心</a> <i class="userCenter-portal-right"></i> </div>
					<div id="J_userCenterBoard" class="userCenter-board">
						<div class="userCenter-info">
							<div class="userCenter-info-userimg"><img id="J_baseUserImg" class="userimg-img" src="<?php
							echo !empty($this->user_session['avatar']) ? getAttachmentUrl($this->user_session['avatar']) : '/static/images/avatar.png';
							?>">
								<div class="userimg-mask"></div>
							</div>
							<div class="userCenter-info-ext">
								<h5 class="ext-title">尊敬的用户</h5>
								<div class="ext-text">欢迎您</div>
							</div>
						</div>
						<div class="userCenter-list" >
							<ul>
								<?php if (strpos(MODUELS,'crowdfunding') !== false) {?>
								<li id="J_userCenterCrowdfunding"><a class="list-item-wrap" href="<?php echo UR('admin:myProduct');?>" ><span class="list-item-left">产品众筹</span><span class="list-item-right"></span></a></li>
								<?php } ?>
								<?php if (strpos(MODUELS,'fundingstock') !== false) {?>
								<li id="J_userCenterCrowdfunding"><a class="list-item-wrap" href="<?php echo UR('admin:index');?>" ><span class="list-item-left">股权众筹</span><span class="list-item-right"></span></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<i class="userCenter-mask"></i> </div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(function(){
		$("#J_userCenter").hover(function(){
			$(this).addClass("userCenter-hover");
		},function(){
			$(this).removeClass("userCenter-hover");
		})
		$(".icon-weixin,.icon-weibo").hover(function(){
			$(this).parent().find("ul").show();
		},function(){
			$(this).parent().find("ul").hide();
		})
		$(".phone_top").hover(function(){
			$(this).find("div").show();
		},function(){
			$(this).find("div").hide();
		})
	})
	</script>

    <!--main-nav end-->
</div>
<!--header end-->
<div class="m-nav">
	<div class="assets-nav" id="J_assets-nav" style="bottom: auto;">
		<!-- an-userinfo 用户信息 -->
		<div class="an-userinfo">
				<div class="u-img"> <span class="ui-mask"></span> <img id="assetUserImage" src="<?php echo !empty($this->user_session['avatar']) ? getAttachmentUrl($this->user_session['avatar']) : '/static/images/avatar.png'; ?>" alt=""> </div>
				<div id="assetUserName" class="u-name"></div>
				<div id="assetUserWelcome" class="u-ext">
				</div>
		</div>
		<!-- an-userinfo 用户信息 end -->
		<!-- an-list 导航列表 -->
		<div class="an-list" id="J_an-list">
				<ul>
				<?php if (strpos(MODUELS,'crowdfunding') !== false) {?>
				<li id="assets-myZmore" class="l-item current"><a >我的众筹<b></b></a></li>
				<?php } ?>
				<?php if (strpos(MODUELS,'fundingstock') !== false) {?>
				<li class="l-item"><a href="<?php  echo url('index:applyInvestor');  ?> "  target="_blank">申请成为投资人<b></b></a></li>
				<?php } ?>
				</ul>
			</div>
		<!-- an-list 导航列表 end -->
	</div>
</div>
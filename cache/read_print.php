<?php foreach ($order_all as $tmp_order_all => $vo_all) { ?>
<table align="center" cellpadding="0" cellspacing="0" class="table_style" style="width: 90%">
<tr>
	<td height="70">
		<table cellpadding="0" cellspacing="0" class="table_style" style="width: 100%">
			<tr>
				<td class="td01"><strong>商品订单</strong>&#12288;
					<font style="font-size:12px;color:#f00">
						(<?php switch($vo_all['type']) {case '1': $typename='代付';break;case '2': $typename='送礼';break;case '3': $typename='分销';break;case '4': $typename='活动';break;case '5': $typename='批发';break;case '6': $typename='团购';break;case '7': $typename='预售';if($vo_all['presale_order_id'] == $vo_all['order_id']) {$typename .= '尾款';} else {$typename .= '定金';}break;default: $typename='普通';}echo $typename.'订单';?>)
					</font>
				</td>
				<td align="right"><img width="120" height="120" src="<?php echo $store['qcode'];?>?>"/></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td><hr class="hr_style" noshade="noshade" /></td>
</tr>
<tr>
	<td>
		<table style="width: 100%" cellpadding="0" cellspacing="0" class="table_style">
			<tr>
				<td class="font02"><strong class="font02">订单号：<?php echo $vo_all[order_no];?></strong></td>
				<td class="font02" align="right"><strong>客户下单日期：<?php echo $vo_all[xd_time];?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td><hr class="hr_style" noshade="noshade" /></td>
</tr>
<tr>
	<td align="center">
		<table style="width: 98%" cellpadding="5" cellspacing="0" class="table_style">
			<tr>
				<td class="font02"><strong>订购商品名称</strong></td>
				<td class="font02"><strong>单价</strong></td>
				<td class="font02"><strong>数量</strong></td>
				<td class="font02"><strong>小计</strong></td>
				<!-- <td class="font02"><strong>状态</strong></td> -->
			</tr>
			<?php foreach ($vo_all[products_list] as $tmp_vo_all[products_list] => $vo) { ?>
				<tr>
					<td>
						<!--商品名称-->
						<?php echo $vo['name'];?>
						<!--赠品或分销-->
						<?php echo $vo['zp_or_fx'];?>
						<!--选择款式-->
						<?php echo $vo['skus_name'];?>			
					</td>
					<td><?php echo $vo['pro_price'];?></td>
					<td><?php echo $vo['pro_num'];?></td>
					<td><?php echo $vo['xj'];?></td>
					<!-- <td>[$vo.zhaungtai]</td> -->
				</tr>
				<tr class="msg-row">
					<td colspan="6"><?php echo $vo['comment'];?><br></td>
				</tr>
			<?php } ?>
		</table>
	</td>
</tr>
<tr>
	<td><hr class="hr_style" noshade="noshade" /></td>
</tr>
<tr>
	<td>
	<table cellpadding="3" cellspacing="0" class="table_style" style="width: 100%">
		<tr>
			<?php if ($vo_all['comment']==1) { ?>
				<td valign="top">买家留言：[%vo_all.comment%]</span></td>
			<?php } ?>
			<td style="width:300px;text-align:right" valign="top" class="font04">
				<table cellpadding="3" cellspacing="0" class="table_style" style="width: 100%">
					<tr>
						<td valign="top"> </td>
						<td style="width: 300px;text-align:right;line-height:22px;" valign="top">
						
							<span class="font04">商品小计：￥<?php echo $vo_all['order_xj'];?></span><br/>
							<!--运费-->
							<?php echo $vo_all['postages'];?>
							<!-- 卖家改价 -->
							<?php echo $vo_all['maijiagaijia'];?>
							<!--满减-->
							<?php echo $vo_all['manjian'];?>
							<!-- 订单打折优惠 -->
							<?php echo $vo_all['zhehouyouhui'];?>
							<!-- 订单优惠券 -->
							<?php echo $vo_all['youhuiquan'];?>
							<!-- 订单其他折扣 -->
							<?php echo $vo_all['other_discount'];?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td><hr class="hr_style" noshade="noshade" /></td>
</tr>
<tr>
	<td>
		<table cellpadding="3" cellspacing="0" class="table_style" style="width: 100%">
			<tr>
				<td></td>
				<td style="width: 300px;text-align:right" valign="top">
					<span class="font02"><strong>应收款：<span class="ui-money-income">￥</span><span class="order-total"><?php echo $vo_all['yingshoukuan'];?></span></strong></span>
				</td>
			</tr>		
		</table>
	</td>
</tr>
<tr>
	<td><hr class="hr_style" noshade="noshade" /></td>
</tr>

<tr>
	<td>
		<table cellpadding="3" cellspacing="0" class="table_style" style="width: 100%">
			<tr>
				<td class="font04">
					客户姓名：<?php echo $vo_all[address_user];?>
					<br />
					<?php if ($vo_all['address']['province']) { ?>
						客户地址：<?php echo $vo_all['address']['province'] .'&nbsp;'.$vo_all['address']['city'].'&nbsp;'.$vo_all['address']['area'].'&nbsp;'.$vo_all['address']['address'];?> 
					<?php } ?>	
					<br />
					<?php if ($vo_all['address_tel']>1) { ?>
						联系电话：<?php echo $vo_all[address_tel];?>
					<?php } ?>
				</td>
				<td valign="top"> </td>
			</tr>
		</table>
	</td>
</tr>
</table>
<hr align="center" width="90%" size="1" noshade class="NOPRINT" >
<!--分页-->
<div class="PageNext"></div>
<?php } ?><style>.heads_tr td{font-size:11pt;font-weight:700;line-height:26px;}</style><style type="text/css" media="print">.heads_tr td{font-size:11pt;font-weight:700;line-height:26px;} .noprint{display:none}.table-footer{display:table-footer-group}.PageNext{page-break-before: always;}</style><style>.lists li{float:left;line-height:25px;margin:12px;font-weight:700;} tr{height:22px;line-height:22px;} .NOPRINT {font-family: "宋体";font-size: 9pt;}td{font-size:9pt;font-family: Arial, Helvetica, sans-serif;}.table_style {border-collapse: collapse;}.hr_style {color: #000000;}.td01 {font-size: 14pt;}.font01 {font-size: medium;}.font02 {font-size: 11pt;}.font03 {font-size: 11pt;text-align: center;}.font04 {line-height: 150%;}.print_button{height:40px;background:#369 none repeat scroll 0 0;border:2px solid #efefef;border-radius:5px;width:100px;color:#fff;font-size:13px;font-weight:700;}#divTest{position: fixed;z-index: 1000;background-color: #D5ECF1;width: 100%;height:45px;left: 0%;bottom:0px;padding-top:10px;text-align:center;}</style><div id="divTest" class=noprint><OBJECT classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" height="0" id="wb" name="wb" width="0" VIEWASTEXT></OBJECT> <input class="print_button" type="button" id="datasubmit" VALUE="确认打印" name="submit" onclick="javascrīpt:window.print()"></div><script type="text/javascript">var rootel=document.documentElement; //XHTMlvar bto=document.getElementById('divTest');</script>
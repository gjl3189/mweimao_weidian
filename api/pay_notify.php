<?php
/**
 *  支付异步通知
 */

define('PIGCMS_PATH', dirname(__FILE__).'/../');
require_once PIGCMS_PATH . 'source/init.php';
require_once PIGCMS_PATH . 'source/class/OrderPay.class.php';
require_once 'functions.php';

if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
    $now = time();
    $timestamp = $_POST['request_time'];
    $sign_key = $_POST['sign_key'];
    unset($_POST['request_time']);
    unset($_POST['sign_key']);
    $_POST['salt'] = SIGN_SALT;
    if (!_checkSign($sign_key, $_POST)) {
        $error_code = 1001;
        $error_msg = '签名无效';
    } else if (!empty($_POST['order_no'])) {
        //订单号
        $order_no = trim($_POST['order_no']);
        //订单
        $nowOrder = M('Order')->findSimple($order_no);
        //付款金额
        $pay_money = !empty($_POST['pay_money']) ? floatval($_POST['pay_money']) : $nowOrder['total'];
        //付款方式
        $payment_method = !empty($_POST['payment_method']) ? $_POST['payment_method'] : $nowOrder['payment_method'];
        //第三方支付id
        $third_id = trim($_POST['third_id']);

        //处理订单
        $pay = new OrderPay();
        $result = $pay->pay_callback($nowOrder['trade_no'], $pay_money, $payment_method, $third_id, array());
        if (empty($result['err_code'])) {
            $error_code = 0;
            $error_msg = $result['err_msg'];
        } else {
            $error_code = $result['err_code'];
            $error_msg = $result['err_msg'];
        }
    } else {
        $error_code = 1002;
        $error_msg = '订单不存在';
    }
    echo json_encode(array('error_code' => $error_code, 'error_msg' => $error_msg));
    exit;
}
?>
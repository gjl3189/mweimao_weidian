<?php
/**
 *  支付回调
 */

define('PIGCMS_PATH', dirname(__FILE__).'/../');
require_once PIGCMS_PATH.'source/init.php';
require_once PIGCMS_PATH . 'source/class/OrderPay.class.php';
require_once 'functions.php';

if (strtolower($_SERVER['REQUEST_METHOD']) == 'get') {
    $order_no = '';
    $now = time();
    $timestamp = $_GET['request_time'];
    $sign_key = $_GET['sign_key'];
    unset($_GET['request_time']);
    unset($_GET['sign_key']);
    $_GET['salt'] = SIGN_SALT;
    if (!_checkSign($sign_key, $_GET)) {
        $error_code = 1001;
        $error_msg = '签名无效';
        $return_url = '';
    } else if (!empty($_GET['order_no'])) {
        //订单号
        $order_no = trim($_GET['order_no']);
        //订单
        $nowOrder = M('Order')->findSimple($order_no);
        //付款金额
        $pay_money = !empty($_GET['pay_money']) ? floatval($_GET['pay_money']) : $nowOrder['total'];
        //付款方式
        $payment_method = !empty($_GET['payment_method']) ? $_GET['payment_method'] : $nowOrder['payment_method'];
        //第三方支付id
        $third_id = trim($_GET['third_id']);

        //处理订单
        $pay = new OrderPay();
        $result = $pay->pay_callback($nowOrder['trade_no'], $pay_money, $payment_method, $third_id, array());
        if (empty($result['err_code'])) {
            //跳转
            header('Location:' . option('config.wap_site_url') . '/paycallback.php?orderno=' . $order_no);
            exit;
        } else {
            $error_code = $result['err_code'];
            $error_msg = $result['err_msg'];
        }
    } else {
        $error_code = 1002;
        $error_msg = '订单不存在';
    }
    //跳转
    header('Location:' . option('config.wap_site_url') . '/payfaildcallback.php?id=' . $order_no . '&pay=failed&error_code=' . $error_code . '&error_msg=' . $error_msg);
    exit;
}
?>